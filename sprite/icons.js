'use strict'

var SVGSpriter = require('svg-sprite')
var path = require('path')
var fs = require('fs')
var glob = require('glob')
var cwd = path.join(process.cwd(), 'client', 'media', 'svg')
var files	= glob.glob.sync('**/*.svg', {cwd: cwd});

var config = {
  "mode": {
    "symbol": {
        "dest": "server/svg",
        "sprite": "symbol-defs.svg",
        "inline": true,
    },
  },
}

var spriter = new SVGSpriter(config)

// Register some SVG files with the spriter
files.forEach(function(file) {
  spriter.add(
    path.resolve(path.join(cwd, file)),
    file,
    fs.readFileSync(path.join(cwd, file), {encoding: 'utf-8'})
  )
})

// Compile the sprite
spriter.compile(function(error, result, cssData) {

  // Run through all configured output modes
  for (var mode in result) {

    // Run through all created resources and write them to disk
    for (var type in result[mode]) {
      fs.writeFileSync(result[mode][type].path, result[mode][type].contents)
    }
  }
})
