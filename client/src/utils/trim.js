const trim = (value) => {
  const payload = value.replace(/(^\s+|\s+$)/g, '').replace(/ {2,}/g, ' ')
  return payload
}

export default trim
