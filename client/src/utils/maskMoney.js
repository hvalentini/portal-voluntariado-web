import currencyFormatter from 'currency-formatter'

const moneyMask = (e) => {

  let value = e.toString()

  if(value.length == 1) value = value.replace(/(\d)/,"0.0$1");

  //ate tres digitos sem pontuacao
  if (value.indexOf('.') === -1 && value.length < 4) {
    value = `${value}00`
    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{1,2})$/, ',$1')
    value = value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  } else {
    if (value.indexOf('0.') != -1 && value.length === 3 ) value = `${value}0`
    if (value.indexOf('.') == -1 ) value = `${value}00`
    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{1,2})$/, ',$1')
    value = value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

  const partValue = value.substring(0, 2)

  if((partValue == '00' && value.length == 5) || (value[0] == '0' && value.length >= 5))
    value = value.substring(1)

  if((partValue == '00' && value.length > 5) || (partValue == '0.'))
    value = value.substring(2)

  value = value !== '' ? `R$ ${value}` : ''
  return value
}

export default moneyMask
