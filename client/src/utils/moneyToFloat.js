const moneyToFloat = (valor) => {

  if(valor.length == 1) {
    valor = valor.replace(/(\d)/,"0.0$1");
    return valor
  }

  if ((valor.length > 2) && (valor.indexOf('R$') != -1)) {
    valor = valor.substring(3, (valor.length))
    valor = valor.replace(/\D/g, '')
    valor = valor.replace(/(\d{1,2})$/, '.$1')
  }

  return valor
}

export default moneyToFloat
