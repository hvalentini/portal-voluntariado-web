import Store from 'store2'

const KEY = 'STORE-LOGGED-USER'

function save(objs) {
  if (!objs) return
  Store.session.set(KEY, objs)
}

function get() {
  return Store.session.get(KEY)
}

module.exports = {
  get,
  save,
}
