const trimObject = (obj) => {
  if (!obj) { return {} }

  const payload = { ...obj }

  Object.keys(obj).forEach((key) => {
    if (typeof obj[key] === 'string') {
      payload[key] = obj[key].trim()
    }
  })

  return payload
}

export default trimObject
