import Store from 'store2'

const KEY = 'STORE-AVATARS-LEFT'

function save(avatars) {
  if (!avatars) return
  Store.session.set(KEY, avatars)
}

function get() {
  return Store.session.get(KEY)
}

module.exports = {
  get,
  save,
}
