const phoneMask = (phone) => {
  let formatPhone = phone || ''
  formatPhone = formatPhone.toString().replace(/[^0-9]/g, '')
  if (formatPhone.length >= 11) {
    formatPhone = formatPhone.replace(/(\d{2})(\d{5})(\d{4})/, '($1) $2-$3')
  }
  if (formatPhone.length <= 10) {
    formatPhone = formatPhone.replace(/(\d{2})(\d{4})(\d{4})/, '($1) $2-$3')
  }
  return formatPhone
}

export default phoneMask
