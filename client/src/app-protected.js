import React from 'react'
import {
  BrowserRouter as Router,
  Route,
}
from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import muiTheme from './muiTheme'
import store from './state/store'
import LandingLeader from './components/LandingLeader/LandingLeader'
import LandingVolunteer from './components/LandingVolunteer/LandingVolunteer'
import PageNewPassword from './pages/PageNewPassword/PageNewPassword.react'
import LandingAdmin from './pages/LandingAdmin/LandingAdmin'
import ApiWarning from './components/ApiWarning/ApiWarning'
// import LandingLeaderNewAction from './components/LandingLeaderNewAction/LandingLeaderNewAction'
// import LandingLeaderPartnerInstitutions from
// './components/LandingLeaderPartnerInstitutions/LandingLeaderPartnerInstitutions'

// Dependência do Material-UI
injectTapEventPlugin()
  // Forma que o React Router controla as URLs pelo History do Navegador
const history = createBrowserHistory()
render((
  <Provider store={store}>
    <Router history={history}>
      <MuiThemeProvider muiTheme={muiTheme.muiTheme}>
        <div className="wrapper">
          <Route path="/nova-senha" component={PageNewPassword} />
          <Route path="/lider" component={LandingLeader} />
          <Route path="/voluntario" component={LandingVolunteer} />
          <Route path="/admin" component={LandingAdmin} />
          {/* <Route
            path="/instituicoes-parceiras"
            component={LandingLeaderPartnerInstitutions}
          /> */}
          <ApiWarning />
        </div>
      </MuiThemeProvider>
    </Router>
  </Provider>
), document.getElementById('root'))
