import * as types from './types'
import ComiteService from '../services/ComiteService'

export const handleGetSaldoAtual = payload => (
  (dispatch) => {
    ComiteService
      .getSaldoAtual(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.COMMITTEE_UPDATE_SALDO_ATUAL, payload: res.data })
        }
        return console.log('Erro ao buscar comitê para esse usuário logado ')
      })
  }
)

export const getCommittees = payload => (
  (dispatch) => {
    if (!payload || payload.ordenacao) {
      dispatch({ type: types.LIST_ACTIONS_LOADING, payload: true })
    }
    if (payload && payload.filtro && !payload.ordenacao) {
      dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    }
    ComiteService
      .getListCommmittee(payload)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
          return dispatch({ type: types.LIST_COMMITTEES_FETCH, payload: res.data })
        }
        dispatch({ type: types.LIST_ACTIONS_LOADING, payload: false })
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return console.log('Erro ao buscar comites')
      })
  }
)

export const handleChangeCommittee = payload => (
  { type: types.COMMITTEE_UPDATE, payload }
)

export const handleAddCommittee = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    ComiteService
      .addCommittee(payload)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'comites',
            msg: 'O cadastro do novo comitê foi realizado com sucesso.',
            rotaPagina: '/admin/comites',
            typeAction: 'add',
          }
          dispatch({ type: types.COMMITTEE_SELECTS_CLEAR })
          dispatch({ type: types.COMMITTEE_CLEAR })
        } else {
          newModal = { tipo: 'warning', entidade: 'cidades', msg: 'Oops... Algo deu errado no cadastro do comite' }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const clearCommittee = () => (
  { type: types.COMMITTEE_CLEAR }
)

export const handleSaveEditCommittee = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    ComiteService
      .saveEditCommittee(payload)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'comites',
            msg: 'A alteração do comitê foi realizada com sucesso.',
            rotaPagina: '/admin/comites',
            typeAction: 'edit',
          }
        } else {
          newModal = { tipo: 'warning', entidade: 'cidades', msg: 'Oops... Algo deu errado ao alterar o comite' }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const paginateCommmite = payload => (
  (dispatch) => {
    dispatch({ type: types.LIST_ACTIONS_LOADING, payload: true })
    ComiteService
      .getListCommmittee(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.LIST_COMMITTEES_PAGINATE, payload: res.data })
        }
        dispatch({ type: types.LIST_ACTIONS_LOADING, payload: false })
        return console.log('Erro ao buscar comites')
      })
  }
)

export const deleteCommittee = _id => (
  (dispatch) => {
    ComiteService
      .deleteCommittee(_id)
      .then((res) => {
        if (res.ok) {
          const newRes = {
            deleteSuccess: true,
          }
          return dispatch({ type: types.MODAL_CLOSE, payload: newRes })
        }
        //  ABRIR MODAL DE RETORNO DE ERRO AO DELETAR UMA MODAL.
        const newModal = { tipo: 'warningDelete', entidade: 'comites', msg: res.data.message }
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const searchCommittee = id => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    ComiteService
      .getCommittee(id)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.UI_LOADING_LIST, payload: false })
          return dispatch({ type: types.COMMITTEE_SEARCH, payload: res.data })
        }
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        return console.log('Erro ao buscar comite')
      })
  }
)

export const handlePrintCommittees = (payload, callback) => (
  (dispatch) => {
    ComiteService
      .getListCommmittee(payload)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.COMMITTEE_SET_PRINT, payload: res.data })
        }
        return callback()
      })
  }
)

export const selectCityCommitee = payload => (
  { type: types.COMMITTEE_CITY, payload }
)
