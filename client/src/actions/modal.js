import * as types from './types'

export const setModal = (tipo, entidade, msg, action, rotaPagina, id) => (
  (dispatch) => {
    const newModal = { tipo, entidade, msg, action, rotaPagina, id }
    dispatch({ type: types.MODAL_SET, payload: newModal })
  }
)

export const closeModal = () => (
  (dispatch) => {
    dispatch({ type: types.MODAL_CLOSE })
  }
)
