import * as types from './types'
import ActionsService from '../services/AcoesService'

const updateActions = (payload, loading = true) => (
  (dispatch) => {
    dispatch({ type: types.ACTIONS_SET_LOADING_CAROUSEL, payload: loading })
    dispatch({ type: types.FILTERS_UPDATE, payload: { query: payload } })
    ActionsService
      .handlGetAcoesFiltros(payload)
      .then((res) => {
        if (res.ok) {
          const resultado = [...res.data]
          dispatch({ type: types.ACTIONS_SET_LOADING_CAROUSEL, payload: false })
          return dispatch({ type: types.COMMITEVOLUNTEERACTION_UPDATE, payload: resultado })
        }
        return console.log('Erro ao buscar faixa salarial ')
      })
  }
)

export default updateActions
