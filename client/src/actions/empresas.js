import * as types from './types'
import EmpresaService from '../services/EmpresaService'

export const handleCompanies = () => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    EmpresaService
      .handleCompanies()
      .then((res) => {
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        if (res.ok) {
          const resultado = res.data
          return dispatch({ type: types.LIST_COMPANY_FETCH, payload: resultado })
        }
        return console.log('Erro ao buscar cidades')
      })
  }
)

export const handleChangeCompany = payload => (
  { type: types.COMPANY_EDIT, payload }
)

export const handleAddCompany = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    EmpresaService
      .addCompany({ nome: payload })
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'empresas',
            msg: 'O cadastro da nova empresa foi realizado com sucesso.',
            rotaPagina: '/admin/empresas',
            typeAction: 'add',
          }
          dispatch({ type: types.COMPANY_CLEAR, payload: newModal })
        } else {
          const msg = res.status === 409
                      ? `Oops... Já existe uma empresa cadastrada com o nome ${payload}.`
                      : 'Oops... Algo deu errado no cadastro da empresa'
          newModal = { tipo: 'warning', entidade: 'empresas', msg }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const searchCompany = id => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    EmpresaService
      .getCompany(id)
      .then((res) => {
        if (res.ok) {
          const resultado = res.data
          dispatch({ type: types.UI_LOADING_LIST, payload: false })
          return dispatch({ type: types.COMPANY_SEARCH, payload: resultado })
        }
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        return console.log('Erro ao buscar cidades')
      })
  }
)

export const deleteCompany = _id => (
  (dispatch) => {
    EmpresaService
      .deleteCompany(_id)
      .then((res) => {
        if (res.ok) {
          const newRes = {
            deleteSuccess: true,
          }
          return dispatch({ type: types.MODAL_CLOSE, payload: newRes })
        }
        //  ABRIR MODAL DE RETORNO DE ERRO AO DELETAR UMA MODAL.
        const newModal = { tipo: 'warningDelete', entidade: 'empresas', msg: res.data.message }
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const clearCompany = () => (
  { type: types.COMPANY_CLEAR }
)

export const handleSaveEditCompany = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    EmpresaService
      .saveEditCompany(payload)
      .then((res) => {
        let newModal = []
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'empresas',
            msg: `A alteração foi efetuada com sucesso para ${payload.nome}`,
            rotaPagina: '/admin/empresas',
            typeAction: 'edit',
          }
        } else {
          const msg = res.status === 409
                      ? `Oops... Já existe uma empresa cadastrada com o nome ${payload.nome}.`
                      : `Oops... Não foi possível alterar nome da empresa ${payload.nome}.`
          newModal = { tipo: 'warning', entidade: 'empresas', msg }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)
