import * as types from './types'
import CidadeService from '../services/CidadeService'

export const getCities = () => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    CidadeService
      .handleCities()
      .then((res) => {
        if (res.ok) {
          const resultado = res.data
          dispatch({ type: types.UI_LOADING_LIST, payload: false })
          return dispatch({ type: types.LIST_CITY_FETCH, payload: resultado })
        }
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        return console.log('Erro ao buscar cidades')
      })
  }
)

export const handleChangeCity = payload => (
  { type: types.CITY_EDIT, payload }
)

export const handleAddCity = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    CidadeService
      .addCity(payload)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'cidades',
            msg: 'O cadastro da nova cidade foi realizado com sucesso.',
            rotaPagina: '/admin/cidades',
            typeAction: 'add',
          }
          dispatch({ type: types.CITY_CLEAR, payload: newModal })
        } else {
          newModal = { tipo: 'warning', entidade: 'cidades', msg: 'Oops... Algo deu errado no cadastro da cidade' }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const handleSaveEditCity = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    CidadeService
      .saveEditCity(payload)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'cidades',
            msg: `A alteração foi efetuada com sucesso para ${payload.nome}/${payload.uf}`,
            rotaPagina: '/admin/cidades',
            typeAction: 'edit',
          }
        } else {
          newModal = {
            tipo: 'warning',
            entidade: 'cidades',
            msg: `Oops... Não foi possível alterar nome/uf da cidade de ${payload.nome}/${payload.uf}.`,
          }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const deleteCity = _id => (
  (dispatch) => {
    CidadeService
      .deleteCity(_id)
      .then((res) => {
        if (res.ok) {
          const newRes = {
            deleteSuccess: true,
          }
          return dispatch({ type: types.MODAL_CLOSE, payload: newRes })
        }
        //  ABRIR MODAL DE RETORNO DE ERRO AO DELETAR UMA MODAL.
        const newModal = { tipo: 'warningDelete', entidade: 'cidades', msg: res.data.message }
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const clearCity = () => (
  { type: types.CITY_CLEAR }
)

export const searchCity = id => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    CidadeService
      .getCity(id)
      .then((res) => {
        if (res.ok) {
          const resultado = res.data
          dispatch({ type: types.UI_LOADING_LIST, payload: false })
          return dispatch({ type: types.CITY_SEARCH, payload: resultado })
        }
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        return console.log('Erro ao buscar cidades')
      })
  }
)

export const getStates = () => (
  (dispatch) => {
    CidadeService
      .getState()
      .then((res) => {
        if (res.ok) {
          const resultado = res.data
          return dispatch({ type: types.LIST_STATE_FETCH, payload: resultado })
        }
        return console.log('Erro ao buscar cidades')
      })
  }
)
