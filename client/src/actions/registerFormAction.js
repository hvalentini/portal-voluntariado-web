import find from 'lodash.find'
import * as types from './types'
import TipoDeAcaoService from '../services/TipoDeAcaoService'
import AcoesService from '../services/AcoesService'

export const uploadFormGallery = files => (
  (dispatch, state) => {
    const form = state().acoes
    const filesUp = files.files
    const data = new FormData()
    // const idVoluntario = '58a1981d587de272c85cb115' // definir de onde pegar este id
    const idVoluntario = files.idVoluntario
    // const gallery = { _id: id, galeria: filesTemp }

    filesUp.forEach((file) => {
      const files2 = file
      const fileID = Date.now() + files2.name.replace(/(\[|\])/gi, '') + '@' + idVoluntario
      files2.id = fileID
      files2.idTemp = idVoluntario
      files2.key = file.preview
      data.append(fileID, files2, idVoluntario)
    })

    if (form.acaoEdit.galeria) {
      dispatch({ type: types.ACTIONS_EDIT_ACTION_PREVIEW_GALLERY, payload: filesUp })
    } else {
      dispatch({ type: types.REGISTER_FORM_ACTION_PREVIEW_GALLERY, payload: filesUp })
    }

    AcoesService
      .handleUpload(data)
      .then((res) => {
        if (res.ok) {
          if (form.acaoEdit.galeria) {
            dispatch({ type: types.ACTIONS_EDIT_ACTION_UPDATE_GALLERY_IMAGE, payload: res.data })
          } else {
            dispatch({ type: types.REGISTER_FORM_ACTION_UPDATE_GALLERY_IMAGE, payload: res.data })
          }
        }
      })
  }
)

export const handleChangeCheckbox = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit.galeria) {
      return dispatch({ type: types.ACTIONS_EDIT_ACTION_CHANGE_CHECKBOX, payload })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_CHECKBOX, payload })
  }
)

export const handleRemoveGalleryItem = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit.galeria) {
      const pictures = [
        ...payload.images.slice(0, payload.index),
        ...payload.images.slice(payload.index + 1),
      ]
      return dispatch({
        type: types.ACTIONS_EDIT_REMOVE_GALLERY_ITEM, payload: { galeria: pictures } })
    }
    const pictures = [
      ...payload.images.galeria.slice(0, payload.index),
      ...payload.images.galeria.slice(payload.index + 1),
    ]
    return dispatch({ type: types.REGISTER_FORM_ACTION_REMOVE_GALLERY_ITEM, payload: pictures })
  }
)

export const handleActionOccurred = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit.cadastroRetroativo === true
      || form.acaoEdit.cadastroRetroativo === false) {
      return dispatch({ type: types.ACTIONS_EDIT_ACTION_OCCURRED, payload })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_OCCURRED, payload })
  }
)

export const handleChangeProperty = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      return dispatch({ type: types.ACTIONS_EDIT_PROPERTY, payload })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_PROPERTY, payload })
  }
)

export const handleChangeStarteDate = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      const dataInicial = payload
      return dispatch({ type: types.ACTIONS_EDIT_START_DATE, payload: { dataInicial } })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_START_DATE, payload })
  }
)

export const handleChangeEndDate = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      const dataFinal = payload
      return dispatch({ type: types.ACTIONS_EDIT_END_DATE, payload: { dataFinal } })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_END_DATE, payload })
  }
)

export const handleChangeRadioButton = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      return dispatch({ type: types.ACTIONS_CHANGE_RADIO_BUTTON, payload })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_RADIO_BUTTON, payload })
  }
)

export const handleChangeStartHours = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      const horaInicial = payload
      return dispatch({ type: types.ACTIONS_CHANGE_START_HOURS, payload: { horaInicial } })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_START_HOURS, payload })
  }
)

export const handleChangeEndHours = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      const horaFinal = payload
      return dispatch({ type: types.ACTIONS_CHANGE_END_HOURS, payload: { horaFinal } })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_END_HOURS, payload })
  }
)

export const handleChangeTypeAction = payload => (
  (dispatch, state) => {
    const newObj = {}
    newObj.tipoAcao = find(payload.types, { _id: `${payload.value}` })
    const form = state().acoes
    if (form.acaoEdit.tipoAcao) {
      return dispatch({ type: types.ACTIONS_EDIT_TYPE_ACTION, payload: newObj })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_TYPE_ACTION, payload: newObj })
  }
)

export const handleFetchActionsTypes = payload => (
  (dispatch) => {
    TipoDeAcaoService
      .handleGetFetchActionsTypes(payload)
      .then((res) => {
        if (res.ok) {
          const resultado = [...res.data]
          return dispatch({ type: types.REGISTER_FORM_ACTION_FETCH_ACTION_TYPES, payload: resultado })
        }
        return console.log('Erro ao buscar imagens')
      })
  }
)

export const handleChangeDurationHours = payload => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit._id) {
      return dispatch({ type: types.ACTIONS_CHANGE_DURATION_HOURS, payload })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CHANGE_DURATION_HOURS, payload })
  }
)

export const addAction = payload => (
  (dispatch) => {
    const participantes = payload.participantes.selected.map((item) => (
      {
        voluntarioId: item.voluntarioId ? item.voluntarioId : item._id,
        nome: item.nome,
        cargo: item.cargoNome,
        email: item.email,
        urlAvatar: item.urlAvatar,
      }
    ))

    const voluntario = {
      voluntarioId: payload.loggedUser._id,
      nome: payload.loggedUser.nome,
      urlAvatar: payload.loggedUser.urlAvatar,
      cargo: payload.loggedUser.cargoNome,
      email: payload.loggedUser.email,
    }

    const galeria = payload.action.galeria ? payload.action.galeria.map((item) => (
      {
        urlFoto: item.urlFoto,
        fotoDestaque: item.fotoDestaque,
      }
    )) : []
    const occurred = payload.action.cadastroRetroativo
    const query = {}

    const dataInicial = payload.action.dataInicial
    const dataFinal = payload.action.dataFinal
    const horaInicial = payload.action.horaInicial
                      ? payload.action.horaInicial : payload.action.dataInicial
    const horaFinal = payload.action.horaFinal
                    ? payload.action.horaFinal : payload.action.dataFinal

    const startHour = horaInicial.getHours()
    const startMin = horaInicial.getMinutes()
    const endHour = horaFinal.getHours()
    const endMin = horaFinal.getMinutes()
    dataInicial.setHours(startHour)
    dataInicial.setMinutes(startMin)
    dataFinal.setHours(endHour)
    dataFinal.setMinutes(endMin)

    query.nome = payload.action.nome
    query.cadastroRetroativo = payload.action.cadastroRetroativo
    query.acaoPontual = payload.action.acaoPontual
    query.dataInicial = dataInicial
    query.horaInicial = horaInicial ? horaInicial.getHours() : 0
    query.dataFinal = dataFinal
    query.horaFinal = horaFinal ? horaFinal.getHours() : 0
    query.tempoDuracao = payload.action.tempoDuracao ? payload.action.tempoDuracao : 0
    query.descricao = payload.action.descricao
    query.totalBeneficiados = !occurred || payload.action.totalBeneficiados === '' ? 0 : parseFloat(payload.action.totalBeneficiados)
    query.totalCartas = !occurred ? 0 : parseFloat(payload.action.totalCartas || 0)
    query.valorGasto = !occurred ? 0 : parseFloat(payload.action.valorGasto.toString().replace(/\D/g, '').replace(/(\d{1,2})$/, '.$1'))
    query.gostaram = []
    query.galeria = !occurred ? [] : galeria
    query.criadaPor = voluntario
    query.local = payload.local.selected._id
    query.tipoAcao = payload.action.tipoAcao._id
    query.comite = payload.loggedUser.comite._id
    query.participantes = !occurred ? [] : participantes

    dispatch({ type: types.REGISTER_FORM_ACTION_LOADER })
    const rota = payload.type === 'admin' ? '/admin/acoes' : '/lider/acoes-voluntarias'
    AcoesService
    .handleAddAction(query)
    .then((res) => {
      let newModal = {}
      if (res.ok) {
        newModal = {
          tipo: 'successOne',
          entidade: 'acoes',
          msg: 'O cadastro da nova ação foi realizado com sucesso.',
          rotaPagina: rota,
          typeAction: 'add',
        }
        dispatch({ type: types.REGISTER_FORM_ACTION_SUCCESS, payload: true })
      } else {
        newModal = { tipo: 'warning', entidade: 'acoes', msg: 'Não foi possível cadastrar a ação.' }
        dispatch({ type: types.REGISTER_FORM_ACTION_ERROR, payload: false })
      }
      return dispatch({ type: types.MODAL_SET, payload: newModal })
    })
  }
)


export const editAction = payload => (
  (dispatch) => {
    function convertDate(date) {
      const now = new Date(date)
      now.setTime((now.getTime() + now.getTimezoneOffset() * 60000))
      return now
    }

    const participantes = payload.participantes.selected.map((item) => (
      {
        voluntarioId: item.voluntarioId ? item.voluntarioId : item._id,
        nome: item.nome,
        cargo: item.cargoNome,
        email: item.email,
        urlAvatar: item.urlAvatar,
      }
    ))

    const voluntario = {
      voluntarioId: payload.loggedUser._id,
      nome: payload.loggedUser.nome,
      urlAvatar: payload.loggedUser.urlAvatar,
      cargo: payload.loggedUser.cargoNome,
      email: payload.loggedUser.email,
    }

    const galeria = payload.action.galeria ? payload.action.galeria.map((item) => (
      {
        urlFoto: item.urlFoto,
        fotoDestaque: item.fotoDestaque,
      }
    )) : []
    const occurred = payload.action.cadastroRetroativo
    const query = {}
    let dataInicial = payload.action.dataInicial
    let dataFinal = payload.action.dataFinal
    let horaInicial = payload.action.horaInicial
                      ? payload.action.horaInicial : payload.action.dataInicial
    let horaFinal = payload.action.horaFinal
                    ? payload.action.horaFinal : payload.action.dataFinal
    dataInicial = typeof dataInicial === 'string' ? convertDate(dataInicial) : dataInicial
    dataFinal = typeof dataFinal === 'string' ? convertDate(dataFinal) : dataFinal
    horaInicial = typeof horaInicial === 'string' ? convertDate(horaInicial) : horaInicial
    horaFinal = typeof horaFinal === 'string' ? convertDate(horaFinal) : horaFinal

    const startHour = typeof horaInicial === 'number' ? horaInicial : horaInicial.getHours()
    const startMin = typeof horaInicial === 'number' ? dataInicial.getMinutes() : horaInicial.getMinutes()
    const endHour = typeof horaFinal === 'number' ? horaFinal : horaFinal.getHours()
    const endMin = typeof horaFinal === 'number' ? dataFinal.getMinutes() : horaFinal.getMinutes()

    if (typeof startHour === 'number') {
      dataInicial.setHours(startHour)
      dataInicial.setMinutes(startMin)
    }

    if (typeof endHour === 'number') {
      dataFinal.setHours(endHour)
      dataFinal.setMinutes(endMin)
    }

    query.nome = payload.action.nome
    query.cadastroRetroativo = payload.action.cadastroRetroativo
    query.acaoPontual = payload.action.acaoPontual
    query.dataInicial = dataInicial
    query.horaInicial = typeof horaInicial === 'object' ? horaInicial.getHours() : horaInicial
    query.dataFinal = dataFinal
    query.horaFinal = typeof horaFinal === 'object' ? horaFinal.getHours() : horaFinal
    query.tempoDuracao = payload.action.tempoDuracao
    query.descricao = payload.action.descricao
    query.totalBeneficiados = !occurred || payload.action.totalBeneficiados === '' ? 0 : parseFloat(payload.action.totalBeneficiados)
    query.totalCartas = !occurred ? 0 : parseFloat(payload.action.totalCartas ? payload.action.totalCartas : 0)
    query.valorGasto = !occurred ? 0 : parseFloat(payload.action.valorGasto)
    query.galeria = !occurred ? [] : galeria
    query.editadoPor = voluntario
    query.local = payload.local.selected && payload.local.selected._id ? payload.local.selected._id : ''
    query.tipoAcao = payload.action.tipoAcao._id
    query.comite = payload.loggedUser.comite ? payload.loggedUser.comite._id : payload.action.comite._id
    query.participantes = !occurred ? [] : participantes
    query._id = payload.action._id

    dispatch({ type: types.REGISTER_FORM_ACTION_LOADER })

    AcoesService
    .handleEditAction(query)
    .then((res) => {
      let newModal = {}
      if (res.ok) {
        const rota = payload.type === 'admin' ? '/admin/acoes' : '/lider/acoes-voluntarias'
        newModal = {
          tipo: 'successOne',
          entidade: 'acoes',
          msg: 'Ação alterada com sucesso.',
          rotaPagina: rota,
          typeAction: 'edit',
        }
        dispatch({ type: types.REGISTER_FORM_ACTION_SUCCESS, payload: true })
      } else {
        let msg = 'Não foi possível editar a ação'
        if(res.data.message === 'child "valorGasto" fails because ["valorGasto" must be a number]') {
          msg = 'Favor informar o valor gasto.'
        }
        dispatch({ type: types.REGISTER_FORM_ACTION_ERROR, payload: false })
        newModal = { tipo: 'warning', entidade: 'acoes', msg: msg  }
      }
      return dispatch({ type: types.MODAL_SET, payload: newModal })
    })
  }
)

export const clearError = () => (
  { type: types.REGISTER_FORM_ACTION_ERROR, payload: { loader: false, error: '' } }
)

export const cleanNewAction = () => (
  { type: types.REGISTER_FORM_CLEAN_NEW_ACTION, payload: { loader: false, error: '' } }
)

export const handleDialogSuccess = () => (
  { type: types.REGISTER_FORM_ACTION_SUCCESS, payload: false }
)

export const handleCleanTypeAction = () => (
  (dispatch, state) => {
    const form = state().acoes
    if (form.acaoEdit.tipoAcao) {
      return dispatch({ type: types.ACTIONS_CLEAN_TYPE_ACTION })
    }
    return dispatch({ type: types.REGISTER_FORM_ACTION_CLEAN_TYPE_ACTION })
  }
)
