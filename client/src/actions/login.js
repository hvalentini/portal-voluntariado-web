import * as types from './types'
import LoginService from '../services/LoginService'

export const updateLogin = payload => (
  { type: types.LOGIN_UPDATE, payload }
)

export const updateLoginUser = payload => (
  { type: types.LOGIN_UPDATE_USER, payload }
)

export const closeDialog = () => (
  dispatch => dispatch({ type: types.LOGIN_UPDATE, payload: { isDialogOpen: false } })
)

export const sendRecoverRequest = emailCpf => (
  (dispatch) => {
    dispatch({ type: types.LOGIN_UPDATE, payload: { loader: true, message: '' } })
    LoginService
    .handlePassRecover(emailCpf)
    .then((res) => {
      if (res.ok) {
        return dispatch({ type: types.LOGIN_UPDATE, payload: { isDialogOpen: true, loader: false, message: 'Requisição feita com sucesso. As orientações de recuperação foram enviadas no seu e-mail.' } })
      }
      const error = res.data ? res.data.message : 'Erro ao enviar recuperação de senha.'
      return dispatch({ type: types.LOGIN_UPDATE, payload: { isDialogOpen: false, error, loader: false, message: 'Erro ao enviar requisição de recuperação de senha.' } })
    })
  }
)

export const loginAuth = () => (
  (dispatch, state) => {
    const user = state().login.user
    dispatch({ type: types.LOGIN_UPDATE, payload: { loader: true } })
    LoginService
      .postLoginAuth(user)
      .then((res) => {
        if (res.ok) {
          const volunteer = res.data.user
          return dispatch({ type: types.LOGIN_UPDATE,
            payload: { success: true, loader: true, error: null, volunteer } })
        }
        const error = res.data ? res.data.message : 'Sistema indisponível no momento, tente novamente ou entre em contato com o administrador do sistema.'
        return dispatch({ type: types.LOGIN_UPDATE, payload: { error, loader: false } })
      })
  }
)

export const loginAuthAdmin = () => (
  (dispatch, state) => {
    const user = state().login.user
    dispatch({ type: types.LOGIN_UPDATE, payload: { loader: true } })
    LoginService
      .postLoginAuthAdmin(user)
      .then((res) => {
        if (res.ok) {
          const volunteer = res.data.user
          return dispatch({ type: types.LOGIN_UPDATE,
            payload: { success: true, loader: true, error: null, volunteer } })
        }
        const error = res.data ? res.data.message : 'Sistema indisponível no momento, tente novamente ou entre em contato com o administrador do sistem.'
        return dispatch({ type: types.LOGIN_UPDATE, payload: { error, loader: false } })
      })
  }
)
