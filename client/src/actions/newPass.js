import * as types from './types'
import LoginService from '../services/LoginService'
import ParametroSistemaService from '../services/ParametroSistemaService'

export const updateNewPass = payload => (
  { type: types.NEW_PASS_UPDATE, payload }
)
export const updateNewPassData = payload => (
  { type: types.NEW_PASS_UPDATE_DATA, payload }
)

export const closeDialog = () => (
  dispatch => dispatch({ type: types.NEW_PASS_UPDATE, payload: { isDialogOpen: false } })
)

export const sendChangePass = (newPass, user) => (
  (dispatch) => {
    dispatch({ type: types.NEW_PASS_UPDATE, payload: { loader: true, message: '' } })
    LoginService
    .postNewPass(newPass)
    .then((res) => {
      let newModal = {}
      if (res.ok) {
        newModal = {
          tipo: 'successOne',
          entidade: 'locais',
          msg: 'Senha alterada com sucesso!',
          rotaPagina: `/redirect?nome=${user.nome}&email=${user.email}`,
        }
      } else {
        newModal = {
          tipo: 'warning',
          entidade: 'locais',
          msg: res.data ? res.data.message : res.problem,
        }
      }
      dispatch({ type: types.NEW_PASS_UPDATE, payload: { loader: false } })
      return dispatch({ type: types.MODAL_SET, payload: newModal })
    })
  }
)

export const sendChangePassAdmin = newPass => (
  (dispatch) => {
    dispatch({ type: types.NEW_PASS_UPDATE, payload: { loader: true, message: '' } })
    ParametroSistemaService
    .changePassword(newPass)
    .then((res) => {
      if (res.data.ok) {
        const newModal = {
          tipo: 'successOne',
          entidade: 'parametroSistema',
          msg: 'Sua senha foi alterada com sucesso.',
          rotaPagina: '/admin',
        }
        dispatch({ type: types.MODAL_SET, payload: newModal })
        return dispatch({ type: types.NEW_PASS_UPDATE,
          payload: { alterouSenha: true, loader: false } })
      }
      const msg = res.data ? res.data.message : 'Sistema indisponível no momento, tente novamente ou entre em contato com o administrador do sistema.'
      const newModal = { tipo: 'warning', entidade: 'parametroSistema', msg }
      dispatch({ type: types.MODAL_SET, payload: newModal })
      return dispatch({ type: types.NEW_PASS_UPDATE,
        payload: { alterouSenha: false, loader: false } })
    })
  }
)
