// TODO: Criar os tipos de action utilizados no projeto

export const LIST_CITY_FETCH = 'LIST_CITY_FETCH'
export const LIST_COMPANY_FETCH = 'LIST_COMPANY_FETCH'
export const LIST_STATE_FETCH = 'LIST_STATES_FETCH'
export const LIST_COMMITTEES_FETCH = 'LIST_COMMITTEES_FETCH'
export const LIST_COMMITTEES_PAGINATE = 'LIST_COMMITTEES_PAGINATE'
export const LIST_ACTIONS_FETCH = 'LIST_ACTIONS_FETCH'
export const LIST_ACTIONS_PAGINATE = 'LIST_ACTIONS_PAGINATE'
export const LIST_ACTIONS_LOADING = 'LIST_ACTIONS_LOADING'


// UI
export const UI_GALLERY_MODAL_OPEN = 'UI_GALLERY_MODAL_OPEN'
export const UI_GALLERY_MODAL_CLOSE = 'UI_GALLERY_MODAL_CLOSE'
export const UI_SET_GALLERY_MODAL_CONTENT = 'UI_SET_GALLERY_MODAL_CONTENT'
export const UI_MODAL_SUCCESS = 'UI_MODAL_SUCCESS'
export const UI_MODAL_WARNING = 'UI_MODAL_WARNING'
export const UI_DELETE_SUCCESS = 'UI_DELETE_SUCCESS'
export const UI_DELETE_WARNING = 'UI_DELETE_WARNING'
export const UI_SET_GALLERY_MODAL_CURRENTSLIDE = 'UI_SET_GALLERY_MODAL_CURRENTSLIDE'
export const UI_CLEAR_MSG = 'UI_CLEAR_MSG'
export const UI_LOADING_LIST = 'UI_LOADING_LIST'
export const UI_SET_LOADER_BTN = 'UI_SET_LOADER_BTN'
export const UI_LOADING_FORM = 'UI_LOADING_FORM'

// MODAL
export const MODAL_SET = 'MODAL_SET'
export const MODAL_OPEN = 'MODAL_OPEN'
export const MODAL_CLOSE = 'MODAL_CLOSE'

// API WARNING
export const API_WARNING = 'API_WARNING'

// WEB APP TEST
export const WEB_TEST = 'WEB_TEST'

// USERSAUTOCOMPLETE
export const USERSAUTOCOMPLETE_UPDATE_DATASOURCE =
  'USERSAUTOCOMPLETE_UPDATE_DATASOURCE'
export const USERSAUTOCOMPLETE_UPDATE_SELECTED =
  'USERSAUTOCOMPLETE_UPDATE_SELECTED'
export const USERSAUTOCOMPLETE_REMOVE_SELECTED =
  'USERSAUTOCOMPLETE_REMOVE_SELECTED'
export const USERSAUTOCOMPLETE_ERROR_TEXT = 'USERSAUTOCOMPLETE_ERROR_TEXT'
export const USERSAUTOCOMPLETE_EDIT_SELECTED =
  'USERSAUTOCOMPLETE_EDIT_SELECTED'
export const USERSAUTOCOMPLETE_CLEAN_SELECTED =
  'USERSAUTOCOMPLETE_CLEAN_SELECTED'
export const USERSAUTOCOMPLETE_EDIT_DATASOURCE =
  'USERSAUTOCOMPLETE_EDIT_DATASOURCE'
export const USERSAUTOCOMPLETE_CHANGE_USERS = 'USERSAUTOCOMPLETE_CHANGE_USERS'
export const USERSAUTOCOMPLETE_SET_SELECTED = 'USERSAUTOCOMPLETE_SET_SELECTED'
export const USERSAUTOCOMPLETE_FILTER_DATASOURCE =
  'USERSAUTOCOMPLETE_FILTER_DATASOURCE'
export const USERSAUTOCOMPLETE_FILTER_VOLUNTEERS =
  'USERSAUTOCOMPLETE_FILTER_VOLUNTEERS'

// LOCALSAUTOCOMPLETE
export const LOCALSAUTOCOMPLETE_UPDATE_DATASOURCE =
  'LOCALSAUTOCOMPLETE_UPDATE_DATASOURCE'
export const LOCALSAUTOCOMPLETE_UPDATE_SELECTED =
  'LOCALSAUTOCOMPLETE_UPDATE_SELECTED'
export const LOCALSAUTOCOMPLETE_UPDATE_ERROR_TEXT =
  'LOCALSAUTOCOMPLETE_UPDATE_ERROR_TEXT'
export const LOCALSAUTOCOMPLETE_SET_BASE = 'LOCALSAUTOCOMPLETE_SET_BASE'
export const LOCALSAUTOCOMPLETE_OPEN_MODAL = 'LOCALSAUTOCOMPLETE_OPEN_MODAL'
export const LOCALSAUTOCOMPLETE_CLOSE_MODAL = 'LOCALSAUTOCOMPLETE_CLOSE_MODAL'
export const LOCALSAUTOCOMPLETE_UPDATE = 'LOCALSAUTOCOMPLETE_UPDATE'

// LANDING_AVATARS
export const LANDING_AVATARS_RANDOM_LEFT = 'LANDING_AVATARS_RANDOM_LEFT'
export const LANDING_AVATARS_RANDOM_RIGHT = 'LANDING_AVATARS_RANDOM_RIGHT'

// COMITES
export const COMMITTEE_UPDATE = 'COMMITTEE_UPDATE'
export const COMMITTEE_UPDATE_SALDO_ATUAL = 'COMMITTEE_UPDATE_SALDO_ATUAL'
export const COMMITTEE_CLEAR = 'COMMITTEE_CLEAR'
export const COMMITTEE_SEARCH = 'COMMITTEE_SEARCH'
export const COMMITTEE_SET_PRINT = 'COMMITTEE_SET_PRINT'

// REGISTER_FORM
export const REGISTER_FORM_UPDATE = 'REGISTER_FORM_UPDATE'
export const REGISTER_FORM_CLEAR_NAME = 'REGISTER_FORM_CLEAR_NAME'
export const REGISTER_FORM_UPDATE_STEP = 'REGISTER_FORM_UPDATE_STEP'
export const REGISTER_FORM_UPDATE_STEP1 = 'REGISTER_FORM_UPDATE_STEP1'
export const REGISTER_FORM_UPDATE_STEP2 = 'REGISTER_FORM_UPDATE_STEP2'
export const REGISTER_FORM_CHANGE_STEP3 = 'REGISTER_FORM_CHANGE_STEP3'
export const REGISTER_FORM_SESSION_VOLUNTEER =
  'REGISTER_FORM_SESSION_VOLUNTEER'

// ACOES
export const ACTION_FETCH_AMOUNT_BENEFICIATED =
  'ACTION_FETCH_AMOUNT_BENEFICIATED'
export const ACTION_FETCH_AMOUNT_ACTIONS = 'ACTION_FETCH_AMOUNT_ACTIONS'
export const ACTION_FETCH_GALLERY = 'ACTION_FETCH_GALLERY'
export const ACTIONS_FILTERS = 'ACTIONS_FILTERS'
export const ACTION_PREVIEW_GALLERY = 'ACTION_PREVIEW_GALLERY'
export const ACTIONS_BY_LEADER = 'ACTIONS_BY_LEADER'
export const ACTIONS_EDITION = 'ACTIONS_EDITION'
export const ACTIONS_SET_LOADING = 'ACTIONS_SET_LOADING'
export const ACTIONS_SET_LOADING_CAROUSEL = 'ACTIONS_SET_LOADING_CAROUSEL'
export const ACTIONS_EDIT_PROPERTY = 'ACTIONS_EDIT_PROPERTY'
export const ACTIONS_EDIT_ACTION_OCCURRED = 'ACTIONS_EDIT_ACTION_OCCURRED'
export const ACTIONS_CHANGE_RADIO_BUTTON = 'ACTIONS_CHANGE_RADIO_BUTTON'
export const ACTIONS_EDIT_START_DATE = 'ACTIONS_EDIT_START_DATE'
export const ACTIONS_EDIT_END_DATE = 'ACTIONS_EDIT_END_DATE'
export const ACTIONS_EDIT_TYPE_ACTION = 'ACTIONS_EDIT_TYPE_ACTION'
export const ACTIONS_CHANGE_DURATION_HOURS = 'ACTIONS_CHANGE_DURATION_HOURS'
export const ACTIONS_EDIT_REMOVE_GALLERY_ITEM =
  'ACTIONS_EDIT_REMOVE_GALLERY_ITEM'
export const ACTIONS_EDIT_ACTION_CHANGE_CHECKBOX =
  'ACTIONS_EDIT_ACTION_CHANGE_CHECKBOX'
export const ACTIONS_EDIT_ACTION_PREVIEW_GALLERY =
  'ACTIONS_EDIT_ACTION_PREVIEW_GALLERY'
export const ACTIONS_EDIT_ACTION_UPDATE_GALLERY_IMAGE =
  'ACTIONS_EDIT_ACTION_UPDATE_GALLERY_IMAGE'
export const ACTIONS_CHANGE_START_HOURS = 'ACTIONS_CHANGE_START_HOURS'
export const ACTIONS_CHANGE_END_HOURS = 'ACTIONS_CHANGE_END_HOURS'
export const ACTIONS_EDIT_FORM_ACTION_SUCCESS =
  'ACTIONS_EDIT_FORM_ACTION_SUCCESS'
export const ACTIONS_EDIT_FORM_ACTION_ERROR = 'ACTIONS_EDIT_FORM_ACTION_ERROR'
export const ACTIONS_CLEAN_TYPE_ACTION = 'ACTIONS_CLEAN_TYPE_ACTION'
export const ACTIONS_SET_PRINT = 'ACTIONS_SET_PRINT'
export const ACTIONS_SET_ORDER = 'ACTIONS_SET_ORDER'
export const ACTIONS_UPDATE_AMOUNT_ACTIONS = 'ACTIONS_UPDATE_AMOUNT_ACTIONS'
export const ACTIONS_SET_LOADER = 'ACTIONS_SET_LOADER'

// VOLUNTARIOS
export const VOLUNTEERS_FETCH_AMOUNT = 'VOLUNTEERS_FETCH_AMOUNT'
export const VOLUNTEERS_VALIDATE_CPF = 'VOLUNTEERS_VALIDATE_CPF'
export const VOLUNTEERS_ENT_UPDATE = 'VOLUNTEERS_ENT_UPDATE'
export const VOLUNTEERS_ERROR_CPF = 'VOLUNTEERS_ERROR_CPF'
export const VOLUNTEERS_CERTIFICATE = 'VOLUNTEERS_CERTIFICATE'
export const VOLUNTEERS_FETCH_VOLUNTEERS = 'VOLUNTEERS_FETCH_VOLUNTEERS'
export const VOLUNTEERS_SEARCH_VOLUNTEERS = 'VOLUNTEERS_SEARCH_VOLUNTEERS'
export const VOLUNTEERS_SEARCH_TERM_VOLUNTEERS =
  'VOLUNTEERS_SEARCH_TERM_VOLUNTEERS'
export const VOLUNTEERS_SORTBY = 'VOLUNTEERS_SORTBY'
export const VOLUNTEERS_FETCH_VOLUNTEERS_FAIL =
  'VOLUNTEERS_FETCH_VOLUNTEERS_FAIL'
export const VOLUNTEERS_SELECTED_FILTER = 'VOLUNTEERS_SELECTED_FILTER'
export const VOLUNTEERS_LAST_FILTER = 'VOLUNTEERS_LAST_FILTER'

// FAIXA SALARIAL
export const SALARY_FETCH_SALARY_RANGE = 'SALARY_FETCH_SALARY_RANGE'

// STATISTICS
export const STATISTIC_VOLUNTEERS_COMMITTEES =
  'STATISTIC_VOLUNTEERS_COMMITTEES'
export const STATISTIC_ACTIONS_COMMITTEES = 'STATISTIC_ACTIONS_COMMITTEES'
export const STATISTIC_UPDATE = 'STATISTIC_UPDATE'
export const STATISTIC_ALL_PROGRAM = 'STATISTIC_ALL_PROGRAM'
export const STATISTIC_ACTIONS_MY_COMITE = 'STATISTIC_ACTIONS_MY_COMITE'
export const STATISTIC_SET_LOADER_ALL_PROGRAM = 'STATISTIC_SET_LOADER_ALL_PROGRAM'
export const STATISTIC_SET_LOADER_MY_COMITE = 'STATISTIC_SET_LOADER_MY_COMITE'
export const STATISTIC_SET_LOADER_QTD_VOLUNTEER = 'STATISTIC_SET_LOADER_QTD_VOLUNTEER'
export const STATISTIC_SET_LOADER_QTD_ACTIONS = 'STATISTIC_SET_LOADER_QTD_ACTIONS'

// NOTIFICACOES
export const NOTIFICATIONS_GET_ALL = 'NOTIFICATIONS_GET_ALL'
export const NOTIFICATIONS_READ = 'NOTIFICATIONS_READ'
export const NOTIFICATIONS_UPDATE = 'NOTIFICATIONS_UPDATE'
export const NOTIFICATIONS_UPDATE_ENTIDADE = 'NOTIFICATIONS_UPDATE_ENTIDADE'

// NEW_ACTION
export const NEW_ACTION_CHANGE_NAME = 'NEW_ACTION_CHANGE_NAME'

// NEW_PASS_UPDATE
export const NEW_PASS_UPDATE = 'NEW_PASS_UPDATE'
export const NEW_PASS_UPDATE_DATA = 'NEW_PASS_UPDATE_DATA'

// NEW_LOCAL
export const NEW_LOCAL_CHANGE = 'NEW_LOCAL_CHANGE'
export const NEW_LOCAL_EDIT_PHONE = 'NEW_LOCAL_EDIT_PHONE'
export const NEW_LOCAL_ADD_LOCAL = 'NEW_LOCAL_ADD_LOCAL'
export const NEW_LOCAL_OPEN_DIALOG = 'NEW_LOCAL_OPEN_DIALOG'
export const NEW_LOCAL_CLOSE_DIALOG = 'NEW_LOCAL_CLOSE_DIALOG'
export const NEW_LOCAL_GET = 'NEW_LOCAL_GET'
export const NEW_LOCAL_CLEAR = 'NEW_LOCAL_CLEAR'
export const NEW_LOCAL_LOADER = 'NEW_LOCAL_LOADER'

// REGISTER FORM ACTION
export const REGISTER_FORM_ACTION_PREVIEW_GALLERY =
  'REGISTER_FORM_ACTION_PREVIEW_GALLERY'
export const REGISTER_FORM_ACTION_UPDATE_GALLERY_IMAGE =
  'REGISTER_FORM_ACTION_UPDATE_GALLERY_IMAGE'
export const REGISTER_FORM_ACTION_CHANGE_CHECKBOX =
  'REGISTER_FORM_ACTION_CHANGE_CHECKBOX'
export const REGISTER_FORM_ACTION_REMOVE_GALLERY_ITEM =
  'REGISTER_FORM_ACTION_REMOVE_GALLERY_ITEM'
export const REGISTER_FORM_ACTION_OCCURRED = 'REGISTER_FORM_ACTION_OCCURRED'
export const REGISTER_FORM_ACTION_CHANGE_RADIO_BUTTON =
  'REGISTER_FORM_ACTION_CHANGE_RADIO_BUTTON'
export const REGISTER_FORM_ACTION_CHANGE_PROPERTY =
  'REGISTER_FORM_ACTION_CHANGE_PROPERTY'
export const REGISTER_FORM_ACTION_CHANGE_START_DATE =
  'REGISTER_FORM_ACTION_CHANGE_START_DATE'
export const REGISTER_FORM_ACTION_CHANGE_END_DATE =
  'REGISTER_FORM_ACTION_CHANGE_END_DATE'
export const REGISTER_FORM_ACTION_CHANGE_START_HOURS =
  'REGISTER_FORM_ACTION_CHANGE_START_HOURS'
export const REGISTER_FORM_ACTION_CHANGE_END_HOURS =
  'REGISTER_FORM_ACTION_CHANGE_END_HOURS'
export const REGISTER_FORM_ACTION_FETCH_ACTION_TYPES =
  'REGISTER_FORM_ACTION_FETCH_ACTION_TYPES'
export const REGISTER_FORM_ACTION_CHANGE_TYPE_ACTION =
  'REGISTER_FORM_ACTION_CHANGE_TYPE_ACTION'
export const REGISTER_FORM_ACTION_CHANGE_DURATION_HOURS =
  'REGISTER_FORM_ACTION_CHANGE_DURATION_HOURS'
export const REGISTER_FORM_ACTION_SUCCESS = 'REGISTER_FORM_ACTION_SUCCESS'
export const REGISTER_FORM_ACTION_ERROR = 'REGISTER_FORM_ACTION_ERROR'
export const REGISTER_FORM_CLEAN_NEW_ACTION = 'REGISTER_FORM_CLEAN_NEW_ACTION'
export const REGISTER_FORM_ACTION_LOADER = 'REGISTER_FORM_ACTION_LOADER'
export const REGISTER_FORM_ACTION_CLEAN_TYPE_ACTION =
  'REGISTER_FORM_ACTION_CLEAN_TYPE_ACTION'

// LOGIN_UPDATE
export const LOGIN_UPDATE = 'LOGIN_UPDATE'
export const LOGIN_UPDATE_USER = 'LOGIN_UPDATE_USER'

// LOGGED_USER
export const LOGGED_USER_UPDATE = 'LOGGED_USER_UPDATE'

// FILTERS
export const FILTERS_UPDATE = 'FILTERS_UPDATE'
export const FILTERS_EDIT_FILTER = 'FILTERS_EDIT_FILTER'
export const FILTERS_EDIT_START_DATE = 'FILTERS_EDIT_START_DATE'
export const FILTERS_EDIT_END_DATE = 'FILTERS_EDIT_END_DATE'
export const FILTERS_EDIT_TYPE = 'FILTERS_EDIT_TYPE'
export const FILTERS_EDIT_VOLUNTARY = 'FILTERS_EDIT_VOLUNTARY'
export const FILTERS_UPDATE_PARTNERS = 'FILTERS_UPDATE_PARTNERS'
export const FILTERS_EDIT_VOLUNTARY_NAME = 'FILTERS_EDIT_VOLUNTARY_NAME'
export const FILTER_SET_LOADING = 'FILTER_SET_LOADING'
export const FILTER_SET_LOADER = 'FILTER_SET_LOADER'
export const FILTERS_CLEAN_PARTNERS = 'FILTERS_CLEAN_PARTNERS'

// PARTNERS
export const PARTNERS_UPDATE = 'PARTNERS_UPDATE'
export const PARTNERS_UPDATE_EDIT = 'PARTNERS_UPDATE_EDIT'
export const PARTNERS_OPEN_MODAL = 'PARTNERS_OPEN_MODAL'
export const PARTNERS_CLOSE_MODAL = 'PARTNERS_CLOSE_MODAL'
export const PARTNER_UPDATE_DIALOG = 'PARTNER_UPDATE_DIALOG'
export const PARTNERS_UPDATE_CLEAR = 'PARTNERS_UPDATE_CLEAR'

export const FILTERS_EDIT_DATE = 'FILTERS_EDIT_DATE'
export const FILTERS_EDIT_PAYLOAD = 'FILTERS_EDIT_PAYLOAD'
export const FILTERS_SET_FILTERS = 'FILTERS_SET_FILTERS'

// COMMITEVOLUNTEERACTION
export const COMMITEVOLUNTEERACTION_UPDATE = 'COMMITEVOLUNTEERACTION_UPDATE'

// HEART_BUTTON
export const HEARTBUTTON_CLICK_LIKE = 'HEARTBUTTON_CLICK_LIKE'
export const HEARTBUTTON_SET_LIKE_STATUS = 'HEARTBUTTON_SET_LIKE_STATUS'

// CITY_SELECT
export const CITY_SELECT_UPDATE = 'CITY_SELECT_UPDATE'

// CITY
export const CITY_FETCH = 'CITY_FETCH'
export const CITY_EDIT = 'CITY_EDIT'
export const CITY_SEARCH = 'CITY_SEARCH'
export const CITY_CLEAR = 'CITY_CLEAR'

// CITY FILTERS
export const FILTERS_CITY_EDIT = 'FILTERS_CITY_EDIT'
export const FILTERS_CITY_ORDER = 'FILTERS_CITY_ORDER'
export const FILTERS_CITY_CLEAR = 'FILTERS_CITY_CLEAR'

// COMPANY FILTERS
export const FILTERS_COMPANY_EDIT = 'FILTERS_COMPANY_EDIT'
export const FILTERS_COMPANY_ORDER = 'FILTERS_COMPANY_ORDER'
export const FILTERS_COMPANY_CLEAR = 'FILTERS_COMPANY_CLEAR'

// COMPANIES
export const COMPANY_FETCH = 'COMPANY_FETCH'
export const COMPANY_EDIT = 'COMPANY_EDIT'
export const COMPANY_SEARCH = 'COMPANY_SEARCH'
export const COMPANY_CLEAR = 'COMPANY_CLEAR'

// ACTION TYPES ADMIN
export const ACTION_TYPES_FETCH = 'ACTION_TYPES_FETCH'
export const ACTION_TYPES_UPDATE = 'ACTION_TYPES_UPDATE'
export const ACTION_TYPES_GET_ONE = 'ACTION_TYPES_GET_ONE'
export const ACTION_TYPES_DELETE = 'ACTION_TYPES_DELETE'

// ACTION TYPES ADMIN FILTERS
export const FILTERS_ACTION_TYPES_EDIT = 'FILTERS_ACTION_TYPES_EDIT'
export const FILTERS_ACTION_TYPES_EDIT_NAME = 'FILTERS_ACTION_TYPES_EDIT_NAME'
export const FILTERS_ACTION_TYPES_ORDER = 'FILTERS_ACTION_TYPES_ORDER'
export const FILTERS_ACTION_TYPES_CLEAR = 'FILTERS_ACTION_TYPES_CLEAR'

// COMMITTEE_SELECTS
export const COMMITTEE_SELECTS_UPDATE = 'COMMITTEE_SELECTS_UPDATE'
export const COMMITTEE_SELECTS_CLEAR = 'COMMITTEE_SELECTS_CLEAR'
export const COMMITTEE_CITY = 'COMMITTEE_CITY'
