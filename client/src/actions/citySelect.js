import * as types from './types'
import CidadeService from '../services/CidadeService'

export const loadCities = () => (
  (dispatch) => {
    CidadeService
      .getList()
      .then((res) => {
        if (!res.ok) {
          return console.log('Erro ao buscar comites')
        }
        return dispatch({ type: types.CITY_SELECT_UPDATE, payload: { list: res.data } })
      })
  }
)

export const citySelectUpdate = payload => (
  { type: types.CITY_SELECT_UPDATE, payload }
)
