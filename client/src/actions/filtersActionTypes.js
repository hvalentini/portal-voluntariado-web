import * as types from './types'
import TipoDeAcaoService from '../services/TipoDeAcaoService'

export const editFilterActionTypes = content => (
  { type: types.FILTERS_ACTION_TYPES_EDIT, payload: content }
)

export const editFilterActionTypesName = content => (
  { type: types.FILTERS_ACTION_TYPES_EDIT_NAME, payload: content }
)

export const getFilterActionTypes = payload => (
  (dispatch) => {
    if (payload.filtro && payload.filtro.nomeNormalizado === '') {
      delete payload.filtro.nomeNormalizado
    }
    dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: true })
    TipoDeAcaoService
      .getFilteredActionTypes(payload)
      .then((res) => {
        if (res.ok) {
          const order = payload.ordenacao ? payload.ordenacao : '{ "nomeNormalizado" : 1 }'
          const resultado = res.data
          dispatch({ type: types.FILTERS_ACTION_TYPES_ORDER, payload: order })
          dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: false })
          return dispatch({ type: types.ACTION_TYPES_UPDATE, payload: { actions: resultado } })// apagar quando mudar no leaderdashbord
        }
        dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: false })
        return console.log('Erro ao buscar tipos de ações')
      })
  }
)

export const clearFilter = () => (
  { type: types.FILTERS_ACTION_TYPES_CLEAR }
)
