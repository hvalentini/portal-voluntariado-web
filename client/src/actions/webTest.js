import * as types from './types'
import WebTestService from '../services/WebTestService'

export const logout = () => (
  () => {
    window.location.href = '/logout-admin'
  }
)

export const volunteerLogout = () => (
  () => {
    window.location.href = '/logout'
  }
)

export const download = url => (
  () => {
    window.open(url)
  }
)

export const getWebStatus = (type, url) => (
  (dispatch) => {
    WebTestService
    .test()
    .then((res) => {
      if (res.ok) {
        dispatch({ type: types.WEB_TEST, payload: true })
        if (type === 'logout') {
          return dispatch(logout())
        }
        if (type === 'volunteer-logout') {
          return dispatch(volunteerLogout())
        }
        if (type === 'download') {
          return dispatch(download(url))
        }
      }
      dispatch({ type: types.WEB_TEST, payload: false })
      return dispatch({ type: types.API_WARNING, payload: true })
    })
  }
)
