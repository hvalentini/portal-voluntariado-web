import * as types from './types'
import LocalService from '../services/LocalService'
import { setBase } from './localAutocomplete'

export const handleChange = payload => (
  { type: types.NEW_LOCAL_CHANGE, payload }
)
export const editPhone = payload => (
  { type: types.NEW_LOCAL_EDIT_PHONE, payload }
)

export const addLocal = (payload, inModal) => (
  (dispatch, state) => {
    const user = state().loggedUser
    const routes = user.adminLogin
                 ? '/admin/instituicoes-parceiras'
                 : '/lider/instituicoes-parceiras'

    const query = {
      _id:  payload._id,
      nome: payload.nome,
      endereco: payload.endereco,
      telefone: payload.telefone,
      responsavelNome: payload.responsavelNome,
      responsavelEmail: payload.responsavelEmail,
      responsavelNome2: payload.responsavelNome2,
      responsavelEmail2: payload.responsavelEmail2,
      observacao: payload.observacao,
      qtdeSalas: payload.qtdeSalas,
      qtdeAlunos: payload.qtdeAlunos,
      escola: payload.escola,
      serie: payload.serie,
    }

    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })

    if (payload._id) {
      query.editadoPor = payload.editadoPor

      LocalService
      .editLocal(query)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'locais',
            msg: `A alteração da instituição: ${payload.nome} foi concluida com sucesso.`,
            rotaPagina: routes,
            typeAction: 'edit',
          }
        } else {
          newModal = {
            tipo: 'warning',
            entidade: 'locais',
            msg: 'Oops... Algo deu errado na edição da instituição',
          }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
    } else {
      query.criadoPor = payload.criadoPor

      LocalService
      .addLocal(query)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          // dispatch({ type: types.NEW_LOCAL_ADD_LOCAL, payload: res.data.data })
          dispatch({ type: types.LOCALSAUTOCOMPLETE_UPDATE_SELECTED, payload: res.data.data })
          dispatch({ type: types.LOCALSAUTOCOMPLETE_CLOSE_MODAL, payload: false })
          dispatch({ type: types.NEW_LOCAL_CLEAR })
          dispatch(setBase())
          newModal = {
            tipo: inModal ? 'successOne' : 'success',
            entidade: 'locais',
            msg: 'Instituição cadastrada com sucesso.',
            rotaPagina: inModal ? '#' : routes,
            typeAction: 'add',
          }
        } else {
          newModal = {
            tipo: 'warning',
            entidade: 'locais',
            msg: `Oops... Algo deu errado no cadastro da instituição. "${res.data.message}"`,
          }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
    }
  }
)

export const closeDialog = () => (
  dispatch => dispatch({ type: types.NEW_LOCAL_CLOSE_DIALOG, payload: false })
)

export const clearState = () => (
  dispatch => dispatch({ type: types.NEW_LOCAL_CLEAR })
)

export const getOneLocal = payload => (
  (dispatch) => {
    dispatch({
      type: types.NEW_LOCAL_LOADER,
      payload: true,
    })
    LocalService
    .getOneLocal(payload)
    .then((res) => {
      if (res.ok) {
        dispatch({
          type: types.NEW_LOCAL_LOADER,
          payload: false,
        })
        return dispatch({ type: types.NEW_LOCAL_GET, payload: res.data })
      }
    })
  }
)

export const deleteInstitution = _id => (
  (dispatch) => {
    LocalService
      .handleDeleteInstitution(_id)
      .then((res) => {
        if (res.ok) {
          const newRes = {
            deleteSuccess: true
          }
          return dispatch({ type: types.MODAL_CLOSE, payload: newRes })
        }
        // ABRIR MODAL DE RETORNO DE ERRO AO DELETAR UMA MODAL.
        const newModal = { tipo: 'warningDelete', entidade: 'locais', msg: res.data.message }
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)
