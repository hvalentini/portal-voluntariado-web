import * as types from './types'
import LocalService from '../services/LocalService'

export const loadPartnerInstitutions = (filter, orderBy) => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    LocalService.getLocals(filter, orderBy).then((res) => {
      if (res.ok) {
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        return dispatch({ type: types.PARTNERS_UPDATE, payload: { list: res.data, loader: false } })
      }
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
      return console.error('ERRO AO BUSCAR LOCAIS')
    })
  }
)

export const openModalEdit = () => (
  dispatch => dispatch({ type: types.PARTNERS_OPEN_MODAL, payload: true })
)

export const closeModalEdit = () => (
  dispatch => dispatch({ type: types.PARTNERS_CLOSE_MODAL, payload: false })
)

export const closeDialog = () => (
  dispatch => dispatch({ type: types.PARTNER_UPDATE_DIALOG, payload: false })
)

export const bar = () => null
