import * as types from './types'
import VoluntarioService from '../services/VoluntarioService'
import phoneMask from '../utils/phoneMask'
import storeLogged from '../utils/storeLoggedUser'

export const getSessionVolunteer = () => (
  (dispatch) => {
    VoluntarioService
      .handleGetSessionVolunteers()
      .then((res) => {
        if (res.ok && res.data) {
          if (res.data.comite) {
            const comites = {
              list: [res.data.comite],
              byCompany: [res.data.comite.empresa],
              byCity: [res.data.comite],
              byUnit: [res.data.comite],
              selectedCompany: res.data.comite.empresa,
              selectedCity: res.data.comite,
              selectedUnit: res.data.comite,
            }
            dispatch({ type: types.COMMITTEE_UPDATE, payload: comites })
          }
          return dispatch({ type: types.REGISTER_FORM_SESSION_VOLUNTEER, payload: { ...res.data } })
        }
        if (!res.ok) { return console.log('Erro ao buscar voluntario na sessão') }
        return null
      })
      .catch(err => console.log(err))
  }
)

export const getStoreVolunteer = () => (
  (dispatch) => {
    const voluntarioCompleto = storeLogged.get()
    return dispatch({ type: types.REGISTER_FORM_UPDATE, payload: { voluntarioCompleto } })
  }
)

export const validateCPF = (payload, usuarioIncompleto, cpfMasked) => (
  (dispatch) => {
    dispatch({ type: types.REGISTER_FORM_UPDATE, payload: { loader: true } })
    VoluntarioService
      .validateCPF(payload, usuarioIncompleto)
      .then((res) => {
        if (res.ok) {
          // res.data.cpf = payload
          dispatch({ type: types.REGISTER_FORM_UPDATE, payload: { loader: false } })
          return dispatch({ type: types.VOLUNTEERS_VALIDATE_CPF,
            payload: { ...res.data, ...{ cpf: payload, cpfMasked } } })
        }
        dispatch({ type: types.REGISTER_FORM_UPDATE, payload: { loader: false } })
        dispatch({ type: types.REGISTER_FORM_CLEAR_NAME })
        return dispatch({ type: types.VOLUNTEERS_ERROR_CPF, payload: res.data.message })
      })
  }
)

export const clearName = () => (
  { type: types.REGISTER_FORM_CLEAR_NAME }
)

export const updateErrorCPF = payload => (
  { type: types.VOLUNTEERS_ERROR_CPF, payload }
)

export const updateStep = payload => (
  { type: types.REGISTER_FORM_UPDATE_STEP, payload }
)

export const updateStep1 = payload => (
  { type: types.REGISTER_FORM_UPDATE_STEP1, payload }
)

export const updateStep2 = payload => (
  { type: types.REGISTER_FORM_UPDATE_STEP2, payload }
)

export const updateStep2Phone = (prop, phone) => {
  const payload = {}

  payload[prop] = phoneMask(phone)
  return { type: types.REGISTER_FORM_UPDATE_STEP2, payload }
}

export const saveData = () => (
  (dispatch, state) => {
    const register = state().registerForm
    if (register.errorMsg || (register.step + 1) < 4) {
      return dispatch({ type: types.REGISTER_FORM_UPDATE_STEP, payload: 3 })
    }
    dispatch({ type: types.REGISTER_FORM_UPDATE, payload: { loader: true } })
    const data = { ...register.step1, ...register.step2, ...register.step3 }

    data.nome = data.nomeCompleto
    data.cadastradoPeloLider = false
    data.aceitouTermo = true
    data.aceitouTermoEsseAno = true
    data.comite = data.comite ? data.comite._id : null
    data.cidade = data.cidade ? data.cidade._id : null
    data.usuarioSharePoint = data.usuarioSharePoint || false
    data.centroResultadoId = data.centroResultadoId || null
    delete data.cpfMasked
    delete data.voluntarioUI
    delete data.voluntarioIncompleto
    delete data.nomeCompleto

    return VoluntarioService
      .save(data)
      .then((res) => {
        if (res.ok) {
          storeLogged.save(res.data.data || data)
          return dispatch(
            {
              type: types.REGISTER_FORM_UPDATE,
              payload: { voluntarioCompleto: res.data.data || data, success: true },
            },
          )
        }
        const msg = res.data ? res.data.message : 'Sistema indisponível no momento, tente novamente ou entre em contato com o administrador do sistema.'
        console.log('Erro ao salvar voluntario', msg)
        return dispatch({ type: types.REGISTER_FORM_UPDATE,
          payload: { errorMsg: msg, loader: false } })
      })
  }
)

export const updateSession = payload => (
  (dispatch) => {
    VoluntarioService
      .handleUpdateSessionVolunteers(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch(
            {
              type: types.REGISTER_FORM_UPDATE,
              payload: { success: true, finish: true },
            },
          )
        }
        console.log('Erro ao salvar voluntario', res.data.message)
        return { type: types.REGISTER_FORM_UPDATE, payload: { errorMsg: res.data.message } }
      })
  }
)

export const resetRegisterForm = () => (
  { type: types.REGISTER_FORM_UPDATE, payload: { success: false } }
)

export const updateLoader = payload => (
  { type: types.REGISTER_FORM_UPDATE, payload: { loader: payload } }
)

export const clearErrorMsg = () => (
  { type: types.REGISTER_FORM_UPDATE, payload: { errorMsg: null } }
)

export const Step3ChangeForm = payload => (
  { type: types.REGISTER_FORM_CHANGE_STEP3, payload }
)
