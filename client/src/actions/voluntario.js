import * as types from './types'
import VoluntarioService from '../services/VoluntarioService'
import AcoesService from '../services/AcoesService'
import storeAvatarsLeft from '../utils/storeAvatarsLeft'
import storeAvatarsRight from '../utils/storeAvatarsRight'

export const getVolunteerCreditHour = payload => (
  () => (
    AcoesService.getCreditHour(payload).then(res => res.data.hoursAvailable)
  )
)

export const searchAvatarsLeft = qtd => (
  (dispatch) => {
    const avatars = storeAvatarsLeft.get()
    if (avatars) {
      return dispatch({ type: types.LANDING_AVATARS_RANDOM_LEFT, payload: avatars })
    }
    return VoluntarioService
      .getAvatars(qtd)
      .then((res) => {
        if (res.ok) {
          storeAvatarsLeft.save(res.data.data)
          return dispatch({ type: types.LANDING_AVATARS_RANDOM_LEFT, payload: res.data.data })
        }
        return console.log('Erro ao buscar avatars')
      })
  }
)

export const searchAvatarsRight = qtd => (
  (dispatch) => {
    const avatars = storeAvatarsRight.get()
    if (avatars) {
      return dispatch({ type: types.LANDING_AVATARS_RANDOM_RIGHT, payload: avatars })
    }
    return VoluntarioService
      .getAvatars(qtd)
      .then((res) => {
        if (res.ok) {
          storeAvatarsRight.save(res.data.data)
          return dispatch({ type: types.LANDING_AVATARS_RANDOM_RIGHT, payload: res.data.data })
        }
        return console.log('Erro ao buscar avatars')
      })
  }
)

export const updateVolinteer = payload => (
  { type: types.VOLUNTEERS_ENT_UPDATE, payload }
)

export const getAmountVolunteers = payload => (
  (dispatch) => {
    VoluntarioService
      .handleGetAmountVolunteers(payload)
      .then((res) => {
        if (res.ok) {
          const resultado = { ...res.data }
          return dispatch({ type: types.VOLUNTEERS_FETCH_AMOUNT, payload: resultado })
        }
        return console.log('Erro ao buscar total de voluntarios')
      })
  }
)

export const getVoluntariosComite = idComite => (
  (dispatch) => {
    dispatch({ type: types.STATISTIC_SET_LOADER_QTD_VOLUNTEER, payload: true })
    VoluntarioService
      .getVoluntariosComite(idComite)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.STATISTIC_VOLUNTEERS_COMMITTEES, payload: res.data[0].comites })
        }
        dispatch({ type: types.STATISTIC_SET_LOADER_QTD_VOLUNTEER, payload: false })
        return console.log('Erro ao buscar total de voluntarios em um comite')
      })
  }
)

export const getVoluntariosAutoComplete = payload => (
  (dispatch, state) => {
    const form = state().acoes
    VoluntarioService
      .getVoluntariosAutoComplete(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.USERSAUTOCOMPLETE_UPDATE_DATASOURCE, payload: res.data })
        }
        return console.log('Erro ao buscar total de voluntarios em um comite')
      })
  }
)

export const login = payload => (
  (dispatch) => {
    VoluntarioService
      .login({ ...payload })
      .then((res) => {
        if (res.ok) {
        }
        return console.log('Erro ao realizar login')
      })
  }
)


export const fetchVolunteers = idComite => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    VoluntarioService
      .getFetchVolunteers(idComite)
      .then((res) => {
        if (res.ok) {
          const result = {}
          result.data = res.data
          result.loader = false
          dispatch({ type: types.UI_LOADING_LIST, payload: false }) // dispatch leader
          return dispatch({ type: types.VOLUNTEERS_FETCH_VOLUNTEERS, payload: result })
        }
        dispatch({ type: types.UI_LOADING_LIST, payload: false })
        dispatch({ type: types.VOLUNTEERS_FETCH_VOLUNTEERS_FAIL }) // dispatch leader
        return console.log('Erro ao buscar voluntarios para esse comite')
      })
  }
)

export const handleSearchVolunteers = payload => (
  { type: types.VOLUNTEERS_SEARCH_VOLUNTEERS, payload }
)

export const handleFilterSelected = payload => (
  { type: types.VOLUNTEERS_SELECTED_FILTER, payload }
)

export const handleSearchTermVolunteers = payload => (
  { type: types.VOLUNTEERS_SEARCH_TERM_VOLUNTEERS, payload }
)

export const getVolunterOrder = payload => (
  { type: types.VOLUNTEERS_SORTBY, payload }
)

// tem que alterar a função para mandar o filtro (conferir no comp VolunteerFilters )
export const filterVolunteers = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    const query = { ...payload }
    const order = '{ "nomeNormalizado" : 1 }'
    if (query.options === 'Participou de alguma ação') {
      delete query.options
      query.jaParticipouAcao = true
    }
    if (query.options === 'Ja participou de ação este ano') {
      delete query.options
      query.jaParticipouAcaoEsseAno = true
    }
    if (query.options === 'Nunca participou de ação') {
      delete query.options
    }

    const filtro = { ...query }
    dispatch({ type: types.VOLUNTEERS_LAST_FILTER, payload: filtro })
    VoluntarioService
      .getFilterVolunteers(query, order)
      .then((res) => {
        if (res.ok) {
          const result = {}
          if (payload.options === 'Nunca participou de ação') {
            result.data = res.data.filter(item => (
              item.quantidadeDeParticipacoes === 0
            ))
            result.loader = false
          } else {
            result.data = res.data
            result.loader = false
          }
          dispatch({ type: types.UI_SET_LOADER_BTN, payload: false }) // dispatch leader
          return dispatch({ type: types.VOLUNTEERS_FETCH_VOLUNTEERS, payload: result })
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        dispatch({ type: types.VOLUNTEERS_FETCH_VOLUNTEERS_FAIL }) // dispatch leader
        return console.log('Erro ao buscar voluntarios para esse comite')
      })
  }
)
