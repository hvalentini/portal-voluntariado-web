import * as types from './types'

export const addSelectedUsers = (content, parametro) => (
  dispatch => {
    if(parametro) {
      dispatch({ type: types.USERSAUTOCOMPLETE_CLEAN_SELECTED })
    }
    dispatch({ type: types.USERSAUTOCOMPLETE_UPDATE_SELECTED, payload: content })
  }
)

export const removeSelectedUsers = content => (
  dispatch => (
    dispatch({ type: types.USERSAUTOCOMPLETE_REMOVE_SELECTED, payload: content })
  )
)

export const handleErrorTextUser = payload => (
  { type: types.USERSAUTOCOMPLETE_ERROR_TEXT, payload }
)

export const editSelectedUsers = content => (
  dispatch => (
    content.map(item => dispatch({ type: types.USERSAUTOCOMPLETE_EDIT_SELECTED, payload: item }))
  )
)

export const cleanEditUser = () => (
  { type: types.USERSAUTOCOMPLETE_CLEAN_SELECTED }
)

export const changeUsers = () => (
  { type: types.USERSAUTOCOMPLETE_CHANGE_USERS }
)

export const filterDataSource = payload => (
  dispatch => (
    dispatch({ type: types.USERSAUTOCOMPLETE_FILTER_DATASOURCE, payload })
  )
)
