import * as types from './types'
import LocalService from '../services/LocalService'

export const getLocalsAutoComplete = payload => (
    (dispatch) => {
      LocalService
      .getLocalsAutoComplete(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.LOCALSAUTOCOMPLETE_UPDATE_DATASOURCE, payload: res.data })
        }
        return console.log('Erro ao buscar total de voluntarios em um comite')
      })
    }
)

export const handleErrorTextLocal = payload => (
  { type: types.LOCALSAUTOCOMPLETE_UPDATE_ERROR_TEXT, payload }
)
