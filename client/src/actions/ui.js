import * as types from './types'

export const closeGalleryModal = () => (
  dispatch => (
    dispatch({ type: types.UI_GALLERY_MODAL_CLOSE })
  )
)

export const setGalleryModalContent = content => (
  dispatch => (
    dispatch({ type: types.UI_SET_GALLERY_MODAL_CONTENT, payload: content })
  )
)

export const setGalleryModalCurrentSlide = (direction, slide) => {
  if (direction) {
    return dispatch => (
      dispatch({ type: types.UI_SET_GALLERY_MODAL_CURRENTSLIDE, payload: slide - 1 })
    )
  }
  return dispatch => (
      dispatch({ type: types.UI_SET_GALLERY_MODAL_CURRENTSLIDE, payload: slide + 1 })
    )
}


export const clearMsg = () => (
  { type: types.UI_CLEAR_MSG }
)
