import * as types from './types'
import AcoesService from '../services/AcoesService'
import updateActions from './commiteVolunteerAction'

export const getAmountBeneficiated = (committeeId, year, volunteerId) => (
  (dispatch) => {
    AcoesService
      .handleGetAmountBeneficiated(committeeId, year, volunteerId)
      .then((res) => {
        if (res.ok) {
          const resultado = { ...res.data[0] }
          return dispatch({ type: types.ACTION_FETCH_AMOUNT_BENEFICIATED, payload: resultado })
        }
        return console.log('Erro ao buscar quantidade de beneficiados')
      })
  }
)

export const getAmountActions = payload => (
  (dispatch) => {
    dispatch({ type: types.ACTIONS_SET_LOADER, payload: true })
    AcoesService
      .handleGetAmountActions(payload)
      .then((res) => {
        if (res.ok) {
          const resultado = { ...res.data }
          return dispatch({ type: types.ACTION_FETCH_AMOUNT_ACTIONS, payload: resultado })
        }
        dispatch({ type: types.ACTIONS_SET_LOADER, payload: false })
        return console.log('Erro ao buscar quantidade de ações')
      })
  }
)

export const getGallery = payload => (
  (dispatch) => {
    AcoesService
      .handleGetgetGallery(payload)
      .then((res) => {
        if (res.ok) {
          const resultado = { ...res.data }
          return dispatch({ type: types.ACTION_FETCH_GALLERY, payload: resultado })
        }
        return console.log('Erro ao buscar imagens')
      })
  }
)

export const getAcoesRealizadas = (idComite, ano, idVoluntario) => (
  (dispatch) => {
    dispatch({ type: types.STATISTIC_SET_LOADER_QTD_ACTIONS, payload: true })
    AcoesService
      .getAcoesRealizadas(idComite, ano, idVoluntario)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.STATISTIC_ACTIONS_COMMITTEES,
            payload: res.data.quantidade })
        }
        dispatch({ type: types.STATISTIC_SET_LOADER_QTD_ACTIONS, payload: false })
        return console.log('Erro ao buscar imagens')
      })
  }
)

export const handleLikeAction = payload => (
  () => {
    AcoesService
      .handleLikeAction({ idVoluntario: payload.idVoluntario, idAcao: payload.idAcao, type: payload.type })
      .then((res) => {
        if (res.ok) {
          return res
        }
        return console.log(res)
      })
  }
)

export const handleInterestAction = (payload, filter) => (
  (dispatch) => {
    AcoesService
      .handleInterestAction({ idVoluntario: payload.idVoluntario, idAcao: payload.idAcao })
      .then((res) => {
        if (res.ok) {
          if (payload.comiteId) {
            dispatch(updateActions(filter || `{ "comite": "${payload.comiteId}", "dataFinal": "${new Date()}", "statusAcao": "andamento"}`, false))
          }
          return res
        }
        return console.log(res)
      })
  }
)
/*eslint-disable*/
export const getAcoesFiltros = payload => (
  (dispatch) => {
    if (!payload.notLoader) {
      dispatch({ type: types.ACTIONS_SET_LOADING, payload: true })
    }
    if (payload.type === 'ACTIONS-COMITE') {
      dispatch({ type: types.STATISTIC_SET_LOADER_MY_COMITE, payload: true })
    }
    if (payload.type === 'ALL-PROGRAM') {
      dispatch({ type: types.STATISTIC_SET_LOADER_ALL_PROGRAM, payload: true })
    }
    const exclusivoParaLider = payload.exclusivoparaLider ? payload.exclusivoparaLider : false
    const ordenacao = payload.ordenacao ? payload.ordenacao : '{ "dataCadastro" : -1}'

    AcoesService
      .handlGetAcoesFiltros(payload.filtro, ordenacao ,exclusivoParaLider)
      .then((res) => {
        if (res.ok) {
          let resultado
          dispatch({ type: types.ACTIONS_SET_LOADING, payload: false })
          switch (payload.type) {
            case 'ACTIONS-MADE':
              resultado = res.data
              return dispatch({ type: types.ACTIONS_FILTERS, payload: resultado })
            case 'ALL-PROGRAM':
              resultado = res.data.length
              return dispatch({ type: types.STATISTIC_ALL_PROGRAM, payload: resultado })
            case 'ACTIONS-COMITE':
              resultado = res.data.length
              return dispatch({ type: types.STATISTIC_ACTIONS_MY_COMITE, payload: resultado })
            default:
          }
        }
        dispatch({ type: types.STATISTIC_SET_LOADER_ALL_PROGRAM, payload: false })
        dispatch({ type: types.STATISTIC_SET_LOADER_MY_COMITE, payload: false })
        dispatch({ type: types.ACTIONS_SET_LOADING, payload: false })
        return console.log('Erro ao buscar ação')
      })
  }
)

export const getAcoesOrder = payload => (
  (dispatch) => {

    const nome = payload.nomeNormalizado
    if(payload.filtro.nomeNormalizado == '') {
      delete payload.nomeNormalizado
    }

    const ordenacao = payload.ordenacao ? payload.ordenacao : '{ "dataCadastro" : -1}'

    dispatch({ type: types.ACTIONS_SET_LOADING, payload: true })
    dispatch({ type: types.ACTIONS_SET_ORDER, payload: ordenacao })
    AcoesService
      .handlGetAcoesFiltros(payload.filtro, ordenacao, true)
      .then((res) => {
        if (res.ok) {
          let resultado = {}
          resultado.data = res.data.length > 6 ? res.data.slice(0,6) : res.data
          resultado.order = ordenacao
          resultado.qtdActions = res.data.length
          return dispatch({ type: types.ACTIONS_BY_LEADER, payload: resultado })
          }
        }
      )}
)

export const getAction = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    AcoesService
      .handleGetAction(payload)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.USERSAUTOCOMPLETE_SET_SELECTED, payload:res.data[0].participantes })
          dispatch({ type: types.LOCALSAUTOCOMPLETE_UPDATE_SELECTED, payload: res.data[0].local })
          dispatch({ type: types.UI_LOADING_LIST, payload: false })
          return dispatch({ type: types.ACTIONS_EDITION, payload: res.data[0] })
        }
      })
  }
)

/*eslint-enable*/

export const uploadFiles = (files, id) => (
  (dispatch) => {
    const filesUp = files
    const data = new FormData()
    const gallery = { _id: id, galeria: [] }

    filesUp.forEach((file) => {
      const files2 = file
      const fileID = Date.now() + files2.name.replace(/(\[|\])/gi, '') + '@' + id
      files2.id = fileID
      data.append(fileID, files2, id)
      gallery.galeria.push({ urlFoto: file.preview })
    })

    AcoesService
      .upload(data)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.ACTION_PREVIEW_GALLERY, payload: gallery })
        }
      })
  }
)

export const cleanEditAction = () => (
  dispatch => (
    dispatch({ type: types.ACTIONS_EDITION, payload: {} })
  )
)

export const handleInfoActionParticipated = payload => (
  (dispatch) => {
    AcoesService
      .handleParticipated({ idVoluntario: payload.idVoluntario, idAcao: payload.idAcao })
      .then((res) => {
        if (res.ok) {
          if (payload.type || payload.filtro) {
            dispatch(getAcoesFiltros({ type: payload.type, filtro: payload.filtro, notLoader: true }))
          }
          return res
        }
        return console.log(res)
      })
  }
)

export const handlePrintActions = (payload, order, callback) => (
  (dispatch) => {
    AcoesService
      .handlGetAcoesFiltros(JSON.stringify(payload), order, true)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.ACTIONS_SET_PRINT, payload: res.data })
        }
        return callback()
      })
  }
)

export const deleteAction = id => (
  (dispatch) => {
    AcoesService
      .deleteAction(id)
      .then((res) => {
        if (res.ok) {
          const newRes = {
            deleteSuccess: true,
          }
          return dispatch({ type: types.MODAL_CLOSE, payload: newRes })
        }
        //  ABRIR MODAL DE RETORNO DE ERRO AO DELETAR UMA MODAL.
        const newModal = { tipo: 'warningDelete', entidade: 'acoes', msg: res.data.message }
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)

export const getAcoes = payload => (
  (dispatch) => {
    if (!payload.order) {
      dispatch({ type: types.FILTER_SET_LOADER, payload: true })
    } else {
      dispatch({ type: types.LIST_ACTIONS_LOADING, payload: true })
    }
    AcoesService
      .handlGetAcoesFiltros(payload.filtro, payload.order, true)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.FILTER_SET_LOADER, payload: false })
          return dispatch({ type: types.LIST_ACTIONS_FETCH, payload: res.data })
        }
        dispatch({ type: types.FILTER_SET_LOADER, payload: false })
        dispatch({ type: types.LIST_ACTIONS_LOADING, payload: false })
        return console.error('Erro ao buscar ações')
      })
  }
)

export const paginateAction = payload => (
  (dispatch) => {
    dispatch({ type: types.LIST_ACTIONS_LOADING, payload: true })
    AcoesService
      .handlGetAcoesPaginacao(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.LIST_ACTIONS_PAGINATE, payload: res.data })
        }
        dispatch({ type: types.LIST_ACTIONS_LOADING, payload: false })
        return console.error('Erro ao buscar ações')
      })
  }
)
