import * as types from './types'
import ComiteService from '../services/ComiteService'

const getFilterCity = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    ComiteService
      .getListCommmittee(payload)
      .then((res) => {
        if (res.ok) {
          dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
          return dispatch({ type: types.LIST_COMMITTEES_FETCH, payload: res.data })
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        return console.error('Erro ao buscar commites')
      })
  }
)

export default getFilterCity
