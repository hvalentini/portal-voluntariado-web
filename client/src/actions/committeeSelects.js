import { uniq, sortBy } from 'underscore'
import * as types from './types'
import ComiteService from '../services/ComiteService'

export const selectCity = (selectedCity, direct) => (
  (dispatch, state) => {
    let selectedUnit = state().registerForm.step2.comite || {}
    const comites = state().committeeSelects
    let byCity = comites.byCity
    let selectedCompany = comites.selectedCompany
    if (direct) {
      byCity = byCity && byCity.length >= 1 ? byCity : [selectedCity]
      selectedCompany = selectedCity.empresa
    }
    const list = sortBy(comites.list.filter(item => (item.cidade === selectedCity.cidade
        && item.empresa._id === selectedCity.empresa._id)), 'unidadeNormalizada')
    if (list.length <= 1) {
      selectedUnit = list[0] || selectedCity
      dispatch({ type: types.REGISTER_FORM_UPDATE_STEP2, payload: { comite: selectedUnit } })
    }
    return dispatch({ type: types.COMMITTEE_SELECTS_UPDATE,
      payload: { byUnit: list, selectedCity, selectedUnit, byCity, selectedCompany } })
  }
)

export const differentCompanies = comites => (
  (dispatch, state) => {
    const step2 = state().registerForm.step2
    const empresas = []
    comites.map((item) => {
      if (item.empresa) empresas.push(item.empresa)
      return null
    })
    dispatch({ type: types.COMMITTEE_SELECTS_UPDATE,
      payload: { byCompany: sortBy(uniq(empresas, 'nome'), 'nomeNormalizado'), list: comites } })
    if (step2.comite && step2.comite.unidade) {
      dispatch(selectCity(step2.comite, true))
    }
  }
)

export const loadCommittee = () => (
  (dispatch) => {
    // const clearComite = () => {
    //   dispatch({ type: types.COMMITTEE_SELECTS_UPDATE,
    //     payload: { byCity: [], selectedCompany: {}, selectedCity: {}, selectedUnit: {} } })
    //   dispatch({ type: types.REGISTER_FORM_UPDATE_STEP2, payload: { comite: {} } })
    // }
    ComiteService
      .getList()
      .then((res) => {
        if (!res.ok) {
          return console.log('Erro ao buscar comites')
        }
        dispatch(differentCompanies(res.data))
        // const step2 = state().registerForm.step2
        // if (step2.comite && !step2.comite._id) { return clearComite() }
        // const comite = find(res.data, item => (item.cidade === step2.comite))
        // if (comite && comite.unidade && !step2.comite) {
        //   return clearComite()
        // }
        return null
      })
  }
)

export const selectCompany = selectedCompany => (
  (dispatch, state) => {
    let selectedCity = state().committeeSelects.selectedCity || {}
    const comites = state().committeeSelects.list
      .filter(item => item.empresa._id === selectedCompany._id)
    if (comites.length === 1) {
      dispatch({ type: types.REGISTER_FORM_UPDATE_STEP2, payload: { comite: comites[0] } })
      selectedCity = comites[0]
      return dispatch(selectCity(comites[0], true))
    }
    const payload = { byCity: sortBy(uniq(comites, 'cidade'), 'cidadeNome'), selectedCompany, selectedCity }
    const company = state().comites.selectedCompany
    if (company && company._id !== selectedCompany._id) {
      payload.selectedUnit = null
      payload.byUnit = []
      dispatch({ type: types.REGISTER_FORM_UPDATE_STEP2, payload: { comite: null } })
    }
    return dispatch({ type: types.COMMITTEE_SELECTS_UPDATE, payload })
  }
)

export const selectUnit = selectedUnit => (
  (dispatch) => {
    dispatch({ type: types.REGISTER_FORM_UPDATE_STEP2, payload: { comite: selectedUnit } })
    return dispatch({ type: types.COMMITTEE_SELECTS_UPDATE, payload: { selectedUnit } })
  }
)


export const clearCommitteeSelected = () => (
  { type: types.COMMITTEE_SELECTS_CLEAR }
)
