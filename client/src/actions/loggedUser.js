import * as types from './types'
import LoggedUserServices from '../services/LoggedUserServices'

export const getLoggedUser = () => (
  (dispatch) => {
    LoggedUserServices
      .getLoggedUser()
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.LOGGED_USER_UPDATE, payload: res.data })
        }
        return console.log('Erro ao buscar os dados do usuário logado ')
      })
  }
)

export const foo = () => null
