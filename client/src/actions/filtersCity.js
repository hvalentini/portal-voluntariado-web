import noAccents from 'remove-accents'
import * as types from './types'
import CidadeService from '../services/CidadeService'

export const editFilterCity = content => (
  { type: types.FILTERS_CITY_EDIT, payload: content }
)

export const getFilterCity = payload => (
  (dispatch) => {
    //Disparar Loader - verificar se é no botao ou na pagina
    dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: true })
    CidadeService
      .handleCities(payload.filtro, payload.ordenacao)
      .then((res) => {
        if (res.ok) {
          let order = payload.ordenacao ? payload.ordenacao : { nomeNormalizado: 1 }

          if (order.nomeNormalizado === 1 && !order.uf) {
            order = '{ "nomeNormalizado" : 1 }'
          }
          if (order.nomeNormalizado === -1 && !order.uf) {
            order = '{ "nomeNormalizado": -1 }'
          }
          if (order.uf && order.uf === 1) {
            order = '{ "uf" : 1, "nomeNormalizado" : 1 }'
          }
          if (order.uf && order.uf === -1) {
            order = '{ "uf": -1, "nomeNormalizado" : 1 }'
          }

          dispatch({ type: types.FILTERS_CITY_ORDER, payload: order })
          dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: false })
          return dispatch({ type: types.LIST_CITY_FETCH, payload: res.data })
        }
        //Disparar Loader - verificar se é no botao ou na pagina
        dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: false })
        return console.log('Erro ao buscar cidades')
      })
  }
)

export const clearFilter = () => (
  { type: types.FILTERS_CITY_CLEAR }
)
