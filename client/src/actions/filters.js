import noAccents from 'remove-accents'
import * as types from './types'
import AcoesService from '../services/AcoesService'
import LocalService from '../services/LocalService'


export const editStartDate = content => (
  { type: types.FILTERS_EDIT_START_DATE, payload: content }
)

export const editEndDate = content => (
  { type: types.FILTERS_EDIT_END_DATE, payload: content }
)

export const updateFilters = payload => (
  { type: types.FILTERS_UPDATE, payload }
)

export const editDate = (content, dataInicial, dataFinal) => (
  (dispatch) => {
    if (content === 'programadas') {
      const obj = {
        status: 'programadas',
        dataInicial: new Date(),
        dataFinal: '',
      }
      return dispatch({ type: types.FILTERS_EDIT_DATE, payload: obj })
    }
    if (content === 'concluidas') {
      const obj = {
        status: 'concluidas',
        dataInicial,
        dataFinal,
      }
      return dispatch({ type: types.FILTERS_EDIT_DATE, payload: obj })
    }
    if (content === 'andamento') {
      const obj = {
        status: 'andamento',
        dataInicial: new Date(),
        dataFinal: new Date(),
      }
      return dispatch({ type: types.FILTERS_EDIT_DATE, payload: obj })
    }
    const obj = {
      status: 'todas',
      dataInicial: '',
      dataFinal: '',
    }
    return dispatch({ type: types.FILTERS_EDIT_DATE, payload: obj })
  }
)

export const editType = content => (
  (dispatch) => {
    const acoesRes = content
    dispatch({ type: types.FILTERS_EDIT_TYPE, payload: acoesRes })
  }
)

export const editFilterVoluntary = content => (
  { type: types.FILTERS_EDIT_VOLUNTARY, payload: content }
)

export const editFilterVoluntaryName = content => (
  { type: types.FILTERS_EDIT_VOLUNTARY_NAME, payload: content }
)


export const updatePartnersFilter = payload => (
  { type: types.FILTERS_UPDATE_PARTNERS, payload }
)

export const updatePartnersFilterOrderBy = payload => (
  (dispatch) => {
    dispatch({ type: types.UI_LOADING_LIST, payload: true })
    return dispatch({ type: types.PARTNERS_UPDATE, payload })
  }
)

export const filterPartners = () => (
  (dispatch, state) => {
    const partners = state().filters.partners
    const orderBy = state().partners.orderBy
    const loading = state().ui.loading

    if (!loading) {
      dispatch({ type: types.NEW_LOCAL_LOADER, payload: true })
    }

    const filter = {}
    if (partners.nome) {
      filter.nomeNormalizado = noAccents(partners.nome)
    }

    if (partners.escola !== 'null') {
      filter.escola = JSON.parse(partners.escola)
    }
    dispatch({ type: types.FILTER_SET_LOADING, payload: true })
    LocalService
      .getLocals(filter, orderBy)
      .then((res) => {
        if (res.ok) {
          if (!loading) {
            dispatch({ type: types.NEW_LOCAL_LOADER, payload: false })
          }
          dispatch({ type: types.UI_LOADING_LIST, payload: false })
          return dispatch({ type: types.PARTNERS_UPDATE,
            payload: { list: res.data, loader: false } })
        }
        return console.error('ERRO AO BUSCAR LOCAIS')
      })
  }
)

export const getFilterActions = payload => (
  (dispatch) => {
    dispatch({ type: types.LIST_ACTIONS_LOADING, payload: true })
    const query = payload
    if (query.nomeNormalizado === '') {
      delete query.nomeNormalizado
    }
    // delete query.status
    // delete query.statusAcao
    query.desativada = false
    const jsonPayload = JSON.stringify(query)
    const ordenacao = '{ "nomeNormalizado" : 1}'
    AcoesService
    .handlGetAcoesFiltros(jsonPayload, ordenacao, true)
    .then((res) => {
      const resultado = {}
      resultado.data = res.data.length > 6 ? res.data.slice(0.6) : res.data
      resultado.order = ordenacao
      resultado.qtdActions = res.data.length
      dispatch({ type: types.LIST_ACTIONS_LOADING, payload: false })
      return dispatch({ type: types.ACTIONS_BY_LEADER, payload: resultado })
    })
  }
)

export const setFilters = () => ({ type: types.FILTERS_SET_FILTERS })

export const filterPagination = payload => (
  (dispatch) => {
    dispatch({ type: types.ACTIONS_SET_LOADING, payload: true })
    if (!payload['filters'].comite) payload['filters'].comite = payload['comite']

    const jsonPayload = JSON.stringify(payload['filters'])
    payload['filters'] = jsonPayload
    AcoesService
    .handlGetAcoesPaginacao(payload)
    .then((res) => {
      const resultado = {}
      resultado.data = res.data
      resultado.order = payload.order
      return dispatch({ type: types.ACTIONS_BY_LEADER, payload: resultado })
    })
  }
)

export const clearFilter = () => (
  { type: types.PARTNERS_UPDATE_CLEAR }
)

export const cleanFilter = () => (
  { type: types.FILTERS_CLEAN_PARTNERS }
)
