import * as types from './types'

export const apiWarningUpdate = payload => (
  (dispatch) => {
    dispatch({ type: types.API_WARNING, payload })
  }
)
