import * as types from './types'
import TipoDeAcaoService from '../services/TipoDeAcaoService'

// Página admin
export const actionClear = () => (
  dispatch => dispatch({ type: types.ACTION_TYPES_CLEAR })
)

export const updateReducerAction = payload => (
  dispatch => dispatch({ type: types.ACTION_TYPES_GET_ONE, payload })
)

export const sendDeleteActionType = _id => (
  (dispatch) => {
    dispatch({
      type: types.ACTION_TYPES_UPDATE,
      payload: { deleteRequestFinished: false, deleteRequestFinishedOk: true },
    })
    TipoDeAcaoService
    .deleteActionsTypes(_id)
    .then((res) => {
      if (res.ok) {
        dispatch({ type: types.ACTION_TYPES_DELETE, payload: { _id } })
        const newRes = { deleteSuccess: true }
        return dispatch({ type: types.MODAL_CLOSE, payload: newRes })
      }
      // ABRIR MODAL DE RETORNO DE ERRO AO DELETAR UMA MODAL.
      const newModal = { tipo: 'warningDelete', entidade: 'tipoDeAcoes', msg: res.data.message }
      return dispatch({ type: types.MODAL_SET, payload: newModal })
    })
  }
)


export const getOneAction = id => (
  (dispatch) => {
    TipoDeAcaoService
    .getOneActionType(id)
    .then((res) => {
      if (res.ok) {
        dispatch({ type: types.ACTION_TYPES_GET_ONE, payload: res.data })
      } else {
        dispatch({ type: types.ACTION_TYPES_UPDATE, payload: res.data })
      }
    })
  }
)

export const getActions = payload => (
  (dispatch) => {
    dispatch({ type: types.ACTION_TYPES_UPDATE, payload: { loader: true } })
    TipoDeAcaoService
      .handleGetFetchActionsTypes(payload)
      .then((res) => {
        if (res.ok) {
          return dispatch({
            type: types.ACTION_TYPES_UPDATE,
            payload: { actions: res.data, loader: false },
          })
        }
        dispatch({ type: types.ACTION_TYPES_UPDATE, payload: { loader: false } })
        return console.log('Erro ao buscar tipos de ações')
      })
  }
)

export const putUpdateActions = payload => (
  (dispatch) => {
    dispatch({ type: types.ACTION_TYPES_UPDATE, payload: { loader: true } })
    dispatch({ type: types.UI_SET_LOADER_BTN, payload: true })
    if (payload._id) {
      TipoDeAcaoService
      .putActionsTypes(payload)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'tipoDeAcoes',
            msg: `A alteração foi efetuada com sucesso para ${payload.nome}`,
            rotaPagina: '/admin/tipos-de-acoes',
            typeAction: 'edit',
          }
        } else {
          newModal = {
            tipo: 'warning',
            entidade: 'tipoDeAcoes',
            msg: `Oops... Algo deu errado na Alteração do tipo de açao ${payload.nome}`,
          }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        dispatch({
          type: types.ACTION_TYPES_UPDATE,
          payload: { message: res.data, loader: false },
        })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
    } else {
      const postData = { ...payload }
      delete postData._id
      TipoDeAcaoService
      .postActionTypes(postData)
      .then((res) => {
        let newModal = {}
        if (res.ok) {
          newModal = {
            tipo: 'success',
            entidade: 'tipoDeAcoes',
            msg: 'O cadastro do novo tipo de ação foi realizado com sucesso.',
            rotaPagina: '/admin/tipos-de-acoes',
            typeAction: 'add',
          }
        } else {
          newModal = {
            tipo: 'warning',
            entidade: 'tipoDeAcoes',
            msg: 'Oops... Algo deu errado no cadastro do tipo de açao',
          }
        }
        dispatch({ type: types.UI_SET_LOADER_BTN, payload: false })
        dispatch({
          type: types.ACTION_TYPES_UPDATE,
          payload: { message: res.data, loader: false },
        })
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
    }
  })
