import * as types from './types'
import LocalService from '../services/LocalService'

export const addSelectedLocal = content => (
  dispatch => (
    dispatch({ type: types.LOCALSAUTOCOMPLETE_UPDATE_SELECTED, payload: content })
  )
)

export const setBase = () => (
  (dispatch) => {
    LocalService
    .getLocals(null, { nomeNormalizado: 1 })
    .then((res) => {
      if (res.ok) {
        return dispatch({ type: types.LOCALSAUTOCOMPLETE_SET_BASE, payload: res.data })
      }
      return console.log('Erro ao buscar total de voluntarios em um comite')
    })
  }
)

export const searchOnBase = content => (
  dispatch => (
    dispatch({ type: types.LOCALSAUTOCOMPLETE_UPDATE_DATASOURCE, payload: content })
  )
)

export const openModal = () => (
  dispatch => (
    dispatch({ type: types.LOCALSAUTOCOMPLETE_OPEN_MODAL, payload: true })
  )
)

export const closeModal = () => (
  dispatch => (
    dispatch({ type: types.LOCALSAUTOCOMPLETE_CLOSE_MODAL, payload: false })
  )
)

export const cleanEditLocal = () => (
  dispatch => (
    dispatch({ type: types.LOCALSAUTOCOMPLETE_UPDATE_SELECTED, payload: {} })
  )
)

export const handleUpdateLocal = payload => (
  { type: types.LOCALSAUTOCOMPLETE_UPDATE, payload }
)
