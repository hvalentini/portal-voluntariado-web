import * as types from './types'

export const handleChangeName = payload => (
  { type: types.NEW_ACTION_CHANGE_NAME, payload }
)
