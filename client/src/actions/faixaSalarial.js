import * as types from './types'
import FaixaSalarialService from '../services/FaixaSalarialService'

const getSalaryRange = payload => (
  (dispatch) => {
    FaixaSalarialService
      .handleGetSalaryRange(payload)
      .then((res) => {
        if (res.ok) {
          const resultado = [...res.data]
          return dispatch({ type: types.SALARY_FETCH_SALARY_RANGE, payload: resultado })
        }
        return console.log('Erro ao buscar faixa salarial ')
      })
  }
)

export default getSalaryRange
