import * as types from './types'
import EmpresaService from '../services/EmpresaService'

export const editFilterCompany = content => (
  { type: types.FILTERS_COMPANY_EDIT, payload: content }
)

export const getFilterCompany = payload => (
  (dispatch) => {
    //Disparar Loader - verificar se é no botao ou na pagina
    dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: true })
    EmpresaService
      .filterCompany(payload.filtro, payload.ordenacao)
      .then((res) => {
        if (res.ok) {
          const order = payload.ordenacao ? payload.ordenacao : '{ "nomeNormalizado" : 1 }'
          dispatch({ type: types.FILTERS_COMPANY_ORDER, payload: order })
          dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: false })
          return dispatch({ type: types.LIST_COMPANY_FETCH, payload: res.data })
        }
        dispatch({ type: payload.ordenacao ? types.UI_LOADING_LIST : types.UI_SET_LOADER_BTN, payload: false })
        return console.log('Erro ao buscar empresas')
      })
  }
)

export const clearFilter = () => (
  { type: types.FILTERS_COMPANY_CLEAR }
)
