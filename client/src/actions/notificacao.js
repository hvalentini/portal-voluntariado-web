import * as types from './types'
import NotificacoesService from '../services/NotificacoesService'
import VoluntarioService from '../services/VoluntarioService'

export const getNotifications = payload => (
  (dispatch) => {
    NotificacoesService
      .handleGetNotifications({
        voluntarioId: payload.voluntarioId })
      .then((res) => {
        if (res.ok) {
          return dispatch({
            type: types.NOTIFICATIONS_GET_ALL,
            payload: {
              notifications: res.data.notificacao,
              amountNotifications: res.data.quantidade,
            },
          })
        }
        return console.log('Erro ao buscar avatars')
      })
  }
)

export const getAdminNotifications = () => (
  (dispatch) => {
    const filter = { $or: [
        { tipo: 'Mensagem Admin' },
        { tipo: 'Mensagem Admin Somente Líderes' },
    ] }
    NotificacoesService
      .getNotifications(filter, { dataCadastro: 1 })
      .then((res) => {
        if (res.ok) {
          return dispatch({
            type: types.NOTIFICATIONS_UPDATE,
            payload: {
              notificacoes: res.data,
              amountNotifications: res.data.length,
            },
          })
        }
        return console.log('Erro ao buscar notificações do admin')
      })
  }
)

export const readNotifications = () => (
  (dispatch, state) => {
    dispatch({ type: types.NOTIFICATIONS_READ, payload: { readNotifications: true } })
    VoluntarioService
      .handleUpdateLastAcess(state().loggedUser._id)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.NOTIFICATIONS_READ, payload: { readNotifications: true } })
        }
        return console.log('Erro ao atualizar a sessão')
      })
  }
)

export const updateNotificationEntity = payload => ({
  type: types.NOTIFICATIONS_UPDATE_ENTIDADE, payload,
})

export const sendNotification = () => (
  (dispatch, state) => {
    const notification = state().notificacao
    // dispatch({ type: types.NOTIFICATIONS_READ, payload: { readNotifications: true } })
    if (notification.entidade.texto.trim() < 1) {
      const newModal = { tipo: 'warning', entidade: 'parametroSistema', msg: 'A mensagem é menor que o permitido.' }
      return dispatch({ type: types.MODAL_SET, payload: newModal })
    }
    return NotificacoesService
      .postNotification(notification.entidade)
      .then((res) => {
        if (res.ok) {
          return dispatch({ type: types.NOTIFICATIONS_UPDATE,
            payload: { notificacoes: [...notification.notificacoes, res.data.notificacao],
              entidade: { publicoAlvo: [], texto: '', tipo: notification.entidade.tipo } } })
        }
        const msg = res.data ? res.data.message : 'Sistema indisponível no momento, tente novamente ou entre em contato com o administrador do sistema.'
        const newModal = { tipo: 'warning', entidade: 'parametroSistema', msg }
        return dispatch({ type: types.MODAL_SET, payload: newModal })
      })
  }
)
