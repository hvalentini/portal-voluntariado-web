import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import { get, find, debounce } from 'lodash'
import styles from './CommiteCard.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import AvatarGroup from '../AvatarGroup/AvatarGroup'
import PillButtonAnimated from '../PillButtonAnimated/PillButtonAnimated'
import HeartButton from '../HeartButton/HeartButton.react'
import ModalListParticipants from '../ModalListParticipants/ModalListParticipants'

class CommiteCard extends React.Component {
  static formatDate(date) {
    let day = date.getUTCDate()
    let month = date.getMonth() + 1
    const year = date.getFullYear()

    day = day > 9 ? day : `0${day}`
    month = month > 9 ? month : `0${month}`
    return `${day}/${month}/${year}`
  }

  constructor(props) {
    super(props)

    this.state = {
      modalOpen: false,
      users: [],
      title: '',
      isInterested: false,
      like: false,
      likeLength: 0,
      initialLike: false,
    }

    this.likeAction = debounce(this.likeAction.bind(this), 600)
    this.interestAction = this.interestAction.bind(this)
    this.renderLocal = this.renderLocal.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleLikeCount = this.handleLikeCount.bind(this)
  }

  componentWillMount() {
    const queremParticipar = this.props.action.queremParticipar
    const voluntarioId = this.props.voluntarioId
    const isInterested = find(queremParticipar, el => el.voluntarioId === voluntarioId) != null
    const like = find(this.props.action.gostaram, el => el.voluntarioId === voluntarioId)
               ? true
               : false
    const likeLength = this.props.action.gostaram.length

    this.setState({
      isInterested,
      like,
      likeLength,
      initialLike: like,
    })
  }

  componentWillReceiveProps(nextProps) {
    const queremParticipar = nextProps.action.queremParticipar
    const voluntarioId = this.props.voluntarioId
    const isInterested = find(queremParticipar, el => el.voluntarioId === voluntarioId) != null

    this.setState({
      isInterested,
    })

    if (nextProps.action.gostaram !== this.props.action.gostaram) {
      const like = find(nextProps.action.gostaram,
        el => el.voluntarioId === this.props.voluntarioId) ? true : false
      this.setState({
        like,
        likeLength: nextProps.action.gostaram.length,
        initialLike: like,
      })
    }
  }

  openModal(ev, users, title) {
    ev.preventDefault()
    const open = users.length > 0
    this.setState({
      modalOpen: open,
      users,
      title,
    })
  }

  closeModal() {
    this.setState({
      modalOpen: false,
      users: [],
    })
  }

  handleLikeCount() {
    if (!this.state.like) {
      this.setState({
        likeLength: this.state.likeLength + 1,
        like: !this.state.like,
      })
    }
    if (this.state.like) {
      this.setState({
        likeLength: this.state.likeLength - 1,
        like: !this.state.like,
      })
    }
    this.likeAction()
  }

  likeAction() {
    if (this.state.like === this.state.initialLike) { return }
    const query = {}
    query.idAcao = get(this.props.action, '_id')
    query.idVoluntario = this.props.voluntarioId
    query.comiteId = this.props.comiteId
    query.type = this.state.like

    this.props.likeAction(query)
    this.setState({ initialLike: !this.state.initialLike })
  }

  interestAction() {
    const query = {}
    query.idAcao = get(this.props.action, '_id')
    query.idVoluntario = this.props.voluntarioId
    query.comiteId = this.props.comiteId

    this.props.interestAction(query)
  }

  renderLocal() {
    if (!this.props.action.local) { return '' }
    return (
      <p>
        <SvgIcon
          icon="icon-icon-41"
        />
        {this.props.action.local.nome}
      </p>
    )
  }

  render() {
    const gostaram = this.props.action.gostaram
    const isLiked = find(gostaram, el => el.voluntarioId === this.props.voluntarioId) != null
    const dataInicial = new Date(this.props.action.dataInicial)
    const dataFinal = new Date(this.props.action.dataFinal)

    const verify = isLiked && this.state.likeLength !== 0
                ? `${this.state.likeLength}`
                : 'Gostei'

    const text = verify === 'Gostei' && this.state.likeLength > 0
               ? `${this.state.likeLength}`
               : verify

    const customStyle = {
      overlay: {
        zIndex: 9,
        backgroundColor: 'rgba(25, 20, 20, 0.75)',
      },
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderRadius: '10px',
        padding: '15px',
      },
    }

    return (
      <div className={styles.CommiteCard}>
        <h2 className={`${styles.title}`}>{this.props.action.nome}</h2>
        <h3 className={`${styles.subTitle}`}>{this.props.action.nomeTipoAcao}</h3>
        {this.renderLocal()}
        <p>
          <SvgIcon
            icon="icon-icon-32"
          />
          {CommiteCard.formatDate(dataInicial)} a {CommiteCard.formatDate(dataFinal)}
        </p>
        <p>
          <SvgIcon
            icon="icon-icon-49"
          />
          {this.props.action.acaoPontual ? 'Essa ação é pontual' : 'Essa ação é continua'}
        </p>
        <p>
          {this.props.action.descricao}
        </p>
        <a
          role="button"
          tabIndex="0"
          className={styles.avatars}
          onClick={e => this.openModal(e, this.props.action.queremParticipar, 'Pessoas interessadas em participar')}
        >
          <p>Pessoas interessadas em participar:</p>
          <AvatarGroup
            title=""
            avatars={this.props.action.queremParticipar}
            qtdMinima={4}
            sizeAvatar="smaller"
          />
        </a>
        <div className={styles.holderBtn}>
          <HeartButton
            className="Grid-cell u-size4of12 u-textCenter"
            clickHeart={this.handleLikeCount}
            clickText={e => this.openModal(e, this.props.action.gostaram, 'Pessoas que gostaram da ação')}
            text={text}
            isLiked={this.state.like}
          />
          <PillButtonAnimated
            key={this.props.action._id}
            onClick={this.interestAction}
            text="Tenho interesse"
            animate={this.state.isInterested}
            className={styles.bt}
          />
        </div>
        <Modal
          isOpen={this.state.modalOpen}
          onRequestClose={this.closeModal}
          style={customStyle}
          contentLabel={''}
        >
          <div className={styles.modalHeader} id="VolunteerInfoIcon">
            <SvgIcon
              className={styles.iconClose}
              icon="icon-icon-11"
              onClick={this.closeModal}
            />
          </div>
          <ModalListParticipants
            users={this.state.users}
            title={this.state.title}
          />
        </Modal>
      </div>
    )
  }
}

CommiteCard.propTypes = {
  action: PropTypes.object,
  voluntarioId: PropTypes.string,
  comiteId: PropTypes.string,
  likeAction: PropTypes.func,
  interestAction: PropTypes.func,
}

export default CommiteCard
