import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './CarouselCommiteeActions.css'
import CommiteCard from './CommiteeCard.react'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import updateActions from '../../actions/commiteVolunteerAction'
import {
  handleLikeAction,
  handleInterestAction,
} from '../../actions/acoes'

class _CarouselCommiteeActions extends React.Component {

  constructor(props) {
    super(props)

    this.likeAction = this.likeAction.bind(this)
    this.interestAction = this.interestAction.bind(this)
  }

  componentWillMount() {
    this.props.updateActions(`{ "comite": "${this.props.comiteId}", "dataFinal": "${new Date()}", "statusAcao": "andamento" }`)
  }

  likeAction(query) {
    this.props.handleLikeAction(query)
  }

  interestAction(query) {
    this.props.handleInterestAction(query, this.props.query)
  }

  render() {
    if (this.props.loader) {
      return (
        <div className={styles.containerLoader}>
          <LoaderAdmin />
        </div>
      )
    }

    if (!this.props.actions || this.props.actions.length < 1) {
      return (
        <div className={styles.emptyState}>
          <div className={styles.img}>
            <img src="/images/empty_action.svg" alt="" />
          </div>
          <div className={styles.text}>
            <h1>Aaaah...</h1>
            <h2>Nenhuma ação está programada.</h2>
          </div>
        </div>
      )
    }
    const cards = this.props.actions && this.props.actions.map((item, i) => (
      <CommiteCard
        key={`card-${i}`}
        action={item}
        voluntarioId={this.props.userId}
        likeAction={this.likeAction}
        interestAction={this.interestAction}
        comiteId={this.props.comiteId}
      />
    ))
    return (
      <div className={styles.CarouselCommiteeActions}>
        {cards}
      </div>
    )
  }
}

_CarouselCommiteeActions.propTypes = {
  updateActions: PropTypes.func,
  handleLikeAction: PropTypes.func,
  handleInterestAction: PropTypes.func,
  actions: PropTypes.array,
  comiteId: PropTypes.string,
  userId: PropTypes.string,
  loader: PropTypes.bool,
  query: PropTypes.string,
}

const mapStateToProps = state => ({
  actions: state.commiteVolunteerAction.actions,
  comiteId: state.loggedUser.comite._id,
  userId: state.loggedUser._id,
  loader: state.acoes.loadingCarousel,
  query: state.filters.query,
})

const mapActionsToProps = {
  updateActions,
  handleLikeAction,
  handleInterestAction,
}

const CarouselCommiteeActions = connect(
  mapStateToProps,
  mapActionsToProps,
)(_CarouselCommiteeActions)

export default CarouselCommiteeActions
