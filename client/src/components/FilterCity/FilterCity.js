import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  TextField,
  SelectField,
  MenuItem,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import muiTheme from '../../muiTheme'
import { editFilterCity, getFilterCity } from '../../actions/filtersCity'
import { getStates } from '../../actions/cidades'
import PillButton from '../PillButton/PillButton'
import styles from './FilterCity.css'

const style = {}


class _FilterCity extends React.Component {

  constructor(props) {
    super(props)

    this.state = {}

    this.getFilterCity = this.getFilterCity.bind(this)
    this.onChangeUF = this.onChangeUF.bind(this)
  }

  componentWillMount() {
    this.props.getStates()
  }

  onChangeUF(ev, index, value) {
    const filter = { ...this.props.filter, uf: value }
    this.props.editFilterCity(filter)
  }

  getFilterCity() {
    const filtro = this.props.filter.uf || this.props.filter.nomeNormalizado
    ? { nomeNormalizado: this.props.filter.nomeNormalizado, uf: this.props.filter.uf }
    : { uf: this.props.filter.uf }

    this.props.getFilterCity({ filtro })
  }

  handleChangeName(prop, value) {
    const filter = { ...this.props.filter }
    filter[prop] = value.replace(/^\s+/gm, '')
    this.props.editFilterCity(filter)
  }

  render() {
    return (
      <div>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <form>
            <div
              className={styles.wrapperFilter}
              style={{ maxHeight: `${window.innerHeight - 230}px` }}
            >
              <TextField
                name="nome"
                floatingLabelText="Nome da cidade"
                underlineStyle={style.line}
                value={this.props.filter.nome}
                onChange={e => this.handleChangeName('nome', e.target.value)}
                maxLength={100}
                inputStyle={{ marginLeft: '27px' }}
                hintStyle={{ marginLeft: '27px' }}
                style={{ marginTop: '-15px' }}
                floatingLabelStyle={{ marginLeft: '27px' }}
                floatingLabelFocusStyle={{ marginLeft: '0' }}
                floatingLabelShrinkStyle={{ marginLeft: '0' }}
                fullWidth
                autoComplete="off"
              />
              <SvgIcon
                icon="icon-icon-29"
                className={styles.iconSearch}
              />

              <p className={styles.text}>Estado: </p>
              <SelectField
                value={this.props.filter.uf}
                onChange={this.onChangeUF}
                // labelStyle={style.menuStyle}
                // underlineStyle={style.line}
                style={{ top: '-10px' }}
                fullWidth
              >
                <MenuItem
                  className="SelectBkg"
                  value=""
                  primaryText="Selecione um estado"
                />
                {
                  this.props.estados.map((item, i) => (
                    <MenuItem
                      className="SelectBkg"
                      value={item.uf}
                      primaryText={item.uf}
                      key={i}
                    />

                ))
                }
              </SelectField>

              <div
                style={{ padding: '15px 0' }}
                className={styles.btn}
              >
                <PillButton
                  text="FILTRAR"
                  onClick={this.getFilterCity}
                  loader={this.props.loader}
                />
              </div>
            </div>
          </form>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.menuStyle = {
  marginBottom: '-12px',
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterCity.propTypes = {
  filter: PropTypes.object,
  loader: PropTypes.bool,
  estados: PropTypes.array,
  getStates: PropTypes.func,
  editFilterCity: PropTypes.func,
  getFilterCity: PropTypes.func,
}

const mapStateToProps = state => ({
  filter: state.filtersCity,
  estados: state.list.estados,
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  editFilterCity,
  getFilterCity,
  getStates,
}

const FilterCity = connect(mapStateToProps, mapActionsToProps)(_FilterCity)
export default FilterCity
