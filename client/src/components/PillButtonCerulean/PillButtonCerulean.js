import React from 'react'
import PropTypes from 'prop-types'
import PillButton from '../../components/PillButton/PillButton'

const PillButtonCerulean = props => (
  <div>
    <PillButton
      {...props}
      style={{
        backgroundColor: '#00BEE7',
        textTransform: 'none',
        fontSize: '22px',
        lineHeight: '26px',
        ...props.style,
      }}
    />

  </div>
)

PillButtonCerulean.propTypes = {
  style: PropTypes.object,
}

export default PillButtonCerulean
