import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { updateActions, getOneAction, sendDeleteActionType } from '../../actions/actionTypes'
import { setModal } from '../../actions/modal'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './BoxActionTypes.css'

class _BoxActionTypes extends React.Component {

  constructor(props) {
    super(props)
    this.handleEditAction = this.handleEditAction.bind(this)
    this.handleDeleteAction = this.handleDeleteAction.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.getModalidade = this.getModalidade.bind(this)
  }

  getModalidade() {
    switch (this.props.actionTypes.tipo) {
      case (0):
        return 'Pontual'
      case (1):
        return 'Contínua'
      case (2):
        return 'Gerencial'
      default:
        return ''
    }
  }

  handleEditAction(_id) {
    this.props.getOneAction(_id)
  }

  handleDeleteAction(_id) {
    this.props.sendDeleteActionType(_id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina, id) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina, id)
  }

  render() {
    const _id = this.props.actionTypes._id
    const nome = this.props.actionTypes.nome
    const modalidade = this.getModalidade()
    const informarQtdeCartas = this.props.actionTypes.informarQtdeCartas ? 'Sim' : 'Não'
    const msg = `Você tem certeza que deseja excluir o Tipo de Ação ${nome}?`

    return (
      <div className={`Grid ${styles.root} print`}>
        <div className={`Grid-cell u-size6of12 ${styles.nome}`}>
          {nome}
        </div>
        <div className="Grid-cell u-size2of12">
          {modalidade}
        </div>
        <div className="Grid-cell u-size2of12">
          <div className={styles.toggleExisteCarta}>
            {informarQtdeCartas}
          </div>
        </div>
        <div className={`Grid-cell u-size2of12 ${styles.divIcons}`}>
          <Link to={`/admin/tipos-de-acoes/editar/${_id}`}>
            <SvgIcon
              icon="icon-icon-58"
              className={`${styles.icon} noPrint`}
              key={`${this.props.keyProp}edit`}
            />
          </Link>
          <button
            onClick={() => this.handleOpenModal(
              'delete', // TIPO DE AÇÃO
              'tipoDeAcoes', // ENTIDADE RELACIONADA NO BANCO
              msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM VENHA DO BANCO ENVIAR NULL
              this.handleDeleteAction, // AÇÃO PRINCIPAL
              '/admin/tipos-de-acoes', // ROTA DA PAGINA PRINCIPAL
              _id,
            )}
          >
            <SvgIcon
              icon="icon-icon-57"
              className={`${styles.icon} noPrint`}
              key={`${this.props.keyProp}del`}
            />
          </button>
        </div>
        <hr className={`Grid-cell u-size12of12 ${styles.separator}`} />
      </div>
    )
  }
}

_BoxActionTypes.propTypes = {
  actionTypes: PropTypes.object,
  getOneAction: PropTypes.func,
  sendDeleteActionType: PropTypes.func,
  setModal: PropTypes.func.isRequired,
  key: PropTypes.string,
}

const mapStateToProps = () => ({})

const mapActionsToProps = {
  updateActions,
  getOneAction,
  sendDeleteActionType,
  setModal,
}

const BoxActionTypes = connect(mapStateToProps,
  mapActionsToProps)(_BoxActionTypes)

export default BoxActionTypes
