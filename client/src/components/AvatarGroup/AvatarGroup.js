import React from 'react'
import PropTypes from 'prop-types'
import Avatar from '../Avatar/Avatar'
import styles from './AvatarGroup.css'

class AvatarGroup extends React.Component {

  handleAvatars() {
    if (this.props.avatars.length) {
      const newAvatars = this.props.avatars.slice(0, this.props.qtdMinima)
      if (this.props.qtdMinima > this.props.avatars.length) {
        return newAvatars.map((item, i) => (
          <div className={styles.divAvatarUn} key={i}>
            <Avatar
              url={item.urlAvatar}
              size={this.props.sizeAvatar}
            />
          </div>
        ))
      }
      return newAvatars.map((item, i) => (
        <div className={styles.divAvatar} key={i}>
          <Avatar
            url={item.urlAvatar}
            size={this.props.sizeAvatar}
          />
        </div>
      ))
    }

    return null
  }

  handleNumber() {
    if (this.props.avatars.length === 0) {
      return (
        <span className={styles.empty}>-</span>
      )
    }
    if (this.props.avatars.length > this.props.qtdMinima) {
      return (
        <span className={styles.number}>{this.props.avatars.length}</span>
      )
    }
    return null
  }

  render() {

    return (
      <div>
        <p className={this.props.titleClass || styles.title}>{this.props.title}</p>
        {this.handleAvatars()}
        {this.handleNumber()}
      </div>
    )
  }
}

AvatarGroup.propTypes = {
  titleClass: PropTypes.string,
  title: PropTypes.string,
  qtdMinima: PropTypes.number,
  avatars: PropTypes.array,
  sizeAvatar: PropTypes.string,
}

export default AvatarGroup
