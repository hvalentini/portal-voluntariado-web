import React from 'react'
import PropTypes from 'prop-types'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { TextField } from 'material-ui'
import { connect } from 'react-redux'
import PillButtonCerulean from '../../components/PillButtonCerulean/PillButtonCerulean'
import { updateNewPass, updateNewPassData, sendChangePass, closeDialog } from '../../actions/newPass'
import styles from './NewPassword.css'

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#484848',
    accent3Color: '#484848',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#484848',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  checkbox: {
    checkedColor: '#01BEE8',
    boxColor: '#484848',
  },
  fontFamily: 'Chantilly-Serial',
})

class _NewPassword extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      msgErro: {
        errorTextSenhaAtual: '',
        errorTextsenhaNova: '',
        errorTextsenhaNovaConfirma: '',
      },
    }
    this.handleChangePass = this.handleChangePass.bind(this)
  }

  componentWillMount() {
    this.props.updateNewPass({ data: { email: this.props.loggedUser.email } })
  }

  componentDidMount() {
    _NewPassword.scrollTo()
  }

  handleChangePass(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateNewPassData(obj)
  }

  checkPassMatch() {
    const msgErro = {}
    if (!this.props.newPass.data.senhaAtual) {
      msgErro.errorTextSenhaAtual = 'Insira a sua senha atual.'
    }

    if (!this.props.newPass.data.senhaNova) {
      msgErro.errorTextsenhaNova = 'Insira a sua nova senha.'
    }
    if (this.props.newPass.data.senhaNova && this.props.newPass.data.senhaNova.length < 6) {
      msgErro.errorTextsenhaNova = 'Insira pelo menos 6 dígitos'
    }

    if (!this.props.newPass.data.senhaNovaConfirma) {
      msgErro.errorTextsenhaNovaConfirma = 'Insira a confirmação da nova senha.'
    }
    if (this.props.newPass.data.senhaNova && this.props.newPass.data.senhaNovaConfirma.length < 6) {
      msgErro.errorTextsenhaNovaConfirma = 'Insira pelo menos 6 dígitos'
    }

    if (this.props.newPass.data.senhaNovaConfirma !== this.props.newPass.data.senhaNova) {
      msgErro.errorTextsenhaNova = 'As senhas não são iguais'
      msgErro.errorTextsenhaNovaConfirma = 'As senhas não são iguais'
    }

    if (Object.keys(msgErro).length > 0) {
      return this.setState({ msgErro })
    }

    msgErro.errorTextSenhaAtual = ''
    msgErro.errorTextsenhaNova = ''
    msgErro.errorTextsenhaNovaConfirma = ''

    this.setState({ msgErro })

    return this.props
      .sendChangePass(this.props.newPass.data, this.props.loggedUser)
  }

  render() {
    return (
      <div className={styles.root}>
        <h1 className={styles.title}>Troca de senha</h1>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <div className="Grid-cell u-size6of12 u-textLeft u-inlineBlock">
              <TextField
                floatingLabelText="Senha atual*"
                name="senhaAtual"
                errorText={this.state.msgErro.errorTextSenhaAtual}
                onChange={e => this.handleChangePass(e.target.name, e.target.value)}
                maxLength={100}
                type="password"
                fullWidth
              />
              <TextField
                floatingLabelText="Nova senha*"
                name="senhaNova"
                errorText={this.state.msgErro.errorTextsenhaNova}
                onChange={e => this.handleChangePass(e.target.name, e.target.value)}
                maxLength={100}
                type="password"
                fullWidth
              />
              <TextField
                floatingLabelText="Confirmar nova senha*"
                name="senhaNovaConfirma"
                onChange={e => this.handleChangePass(e.target.name, e.target.value)}
                errorText={this.state.msgErro.errorTextsenhaNovaConfirma}
                maxLength={100}
                type="password"
                fullWidth
              />
            </div>
          </div>
        </MuiThemeProvider>
        <div className={styles.obrigatorio}><small>*Campo Obrigatório</small></div>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div className={styles.button}>
            <PillButtonCerulean
              text="Alterar a senha"
              color="white"
              opacity="1"
              onClick={() => this.checkPassMatch()}
              loader={this.props.newPass.loader}
            />
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}


_NewPassword.propTypes = {
  loggedUser: PropTypes.string,
  newPass: PropTypes.object,
  isDialogOpen: PropTypes.bool,
  closeDialog: PropTypes.func,
  sendChangePass: PropTypes.func,
  updateNewPassData: PropTypes.func,
  updateNewPass: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
  newPass: state.newPass,
})

const mapActionsToProps = {
  sendChangePass,
  closeDialog,
  updateNewPassData,
  updateNewPass,
}

const NewPassword = connect(mapStateToProps, mapActionsToProps)(_NewPassword)


export default NewPassword
