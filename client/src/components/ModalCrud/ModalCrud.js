import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { closeModal } from '../../actions/modal'
// import styles from './ModalCrud'
import { clearMsg } from '../../actions/ui'
import ModalDialog from '../../components/ModalDialog/ModalDialog'

class _ModalCrud extends React.Component {

  constructor(props) {
    super(props)
    this.handleCloseModal = this.handleCloseModal.bind(this)
    this.handleNew = this.handleNew.bind(this)
    this.handleCloseModalSuccess = this.handleCloseModalSuccess.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modal.deleteSuccess) {
      this.props.closeModal(this.props.modal.tipo)
      this.props.history.push(this.props.modal.rotaPagina)
    }
  }

  handleCloseModal() {
    this.props.closeModal(this.props.modal.tipo)
  }

  handleCloseModalSuccess() {
    this.props.closeModal(this.props.modal.tipo)
    // this.props.history.push(this.props.modal.rotaPagina)
    if(!this.props.modal.rotaPagina) {
      this.props.history.goBack()
    } else {
      if (this.props.modal.rotaPagina.indexOf('redirect') !== -1) {
        window.location = this.props.modal.rotaPagina
      }
      this.props.history.push(this.props.modal.rotaPagina) 
    }
  }

  handleNew() {
    this.handleCloseModal()
  }

  render() {
    const modal = this.props.modal
    let texto = 'Novo Cadastro'
    if (modal.typeAction === 'edit') texto = 'Alterar Novamente'

    return (
      <div>
        <ModalDialog
          type="delete"
          open={modal.openDelete}
          textFirst="Cancelar"
          actionFirst={this.handleCloseModal}
          textSecond="Excluir"
          actionSecond={() => modal.action(modal.id)}
          msg={modal.msg}
        />

        <ModalDialog
          type="warningDelete"
          open={modal.openWarningDelete}
          textFirst="Ok"
          actionFirst={this.handleCloseModal}
          msg={modal.msg}
        />

        <ModalDialog
          type="warning"
          open={modal.openWarning}
          textFirst="Ok"
          actionFirst={this.handleCloseModal}
          msg={modal.msg}
        />

        <ModalDialog
          type="success"
          open={modal.openSuccess}
          textFirst={texto}
          actionFirst={this.handleNew}
          textSecond="Continuar"
          actionSecond={this.handleCloseModalSuccess}
          msg={modal.msg}
        />

        <ModalDialog
          type="successOne"
          open={modal.openSuccessOne}
          textFirst="Ok"
          actionFirst={this.handleCloseModalSuccess}
          msg={modal.msg}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  modal: state.modal,
})

const mapActionsToProps = {
  clearMsg,
  closeModal,
}

const ModalCrud = connect(mapStateToProps, mapActionsToProps)(_ModalCrud)
export default withRouter(ModalCrud)
