import React from 'react'
import PropTypes from 'prop-types'
import styles from './UserAvatar.css'
import Avatar from '../Avatar/Avatar'
// import IconBalls from '../IconBalls/IconBalls'

const UserAvatar = props => (
  <div className={styles.boxUserAvatar}>
    <Avatar
      url={props.url || '/images/user-avatar.svg'}
      size={props.size}
    />
    {/* <IconBalls
      background={'#f2c021'}
      icon={'yellow'}
      size={props.size}
    />
    <IconBalls
      background={'#099949'}
      icon={'green'}
      size={props.size}
    /> */}
  </div>
)

UserAvatar.propTypes = {
  size: PropTypes.string.isRequired,
  url: PropTypes.string,
}

UserAvatar.defaultProps = {
  size: 'large',
}

export default UserAvatar
