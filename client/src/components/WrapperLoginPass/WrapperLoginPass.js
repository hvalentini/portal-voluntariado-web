import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './WrapperLoginPass.css'
import {
  updateLogin,
  updateLoginUser,
  loginAuth,
} from '../../actions/login'

class _WrapperLoginPass extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className={styles.formBg}>
        <div className={styles.divAnimation}>
          <img
            className={styles.logo}
            src="/images/logo-223x152.png"
            alt="Logo Portal do Voluntariado"
          />
        </div>
        {this.props.children}
      </div>
    )
  }
}

_WrapperLoginPass.displayName = 'WrapperLoginPass'

_WrapperLoginPass.propTypes = {
}

const mapStateToProps = state => ({
  login: state.login,
})

const mapActionsToProps = {
  updateLogin,
  updateLoginUser,
  loginAuth,
}

const WrapperLoginPass = connect(mapStateToProps, mapActionsToProps)(_WrapperLoginPass)

export default WrapperLoginPass
