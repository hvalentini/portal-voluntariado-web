import React from 'react'
import styles from './SpinnerSuccess.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class SpinnerSuccess extends React.Component {

  render() {

    if (((navigator.appName === 'Netscape') ||
    (navigator.appVersion.indexOf('MSIE') !== -1) ||
    (navigator.appName === 'Microsoft Internet Explorer')) && (navigator.userAgent.indexOf('Chrome') === -1)) {
      return (
        <div className={styles.loaderIE}>
          <SvgIcon
            className={styles.iconIE}
            icon="icon-icon-48"
          />
        </div>
      )
    }

    return (
      <div className={styles.loader}>
        <svg className={styles.circular}>
          <circle
            className={styles.path}
            cx="50"
            cy="50"
            r="20"
            fill="none"
          />
        </svg>
        <svg className={styles.suc}>
          <path className={styles.checkmarkCheck} fill="none" d="M10.7,20.4l5.3,5.3l12.4-12.5" />
        </svg>
      </div>
    )
  }
}
export default SpinnerSuccess
