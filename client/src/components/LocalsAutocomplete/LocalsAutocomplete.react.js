import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import get from 'lodash.get'
import { AutoComplete, MenuItem, ListItem } from 'material-ui'
import noAccents from 'remove-accents'
import styles from './LocalsAutocomplete.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import EditInstitutionsPage from '../../pages/EditInstitutionsPage/EditInstitutionsPage'
import {
  addSelectedLocal,
  setBase,
  searchOnBase,
  openModal,
  closeModal,
  handleUpdateLocal,
} from '../../actions/localAutocomplete'
import { clearState } from '../../actions/newLocal'

const menuItemStyle = { borderBottom: '1px solid #b1b1b1' }

const customStyle = {
  overlay: {
    zIndex: 9,
    backgroundColor: 'rgba(25, 20, 20, 0.75)',
  },
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
}

const style = {}

class _LocalsAutocomplete extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      update: true,
      errorText: '',
    }

    this.handleDataSource = this.handleDataSource.bind(this)
    this.updateSearchText = this.updateSearchText.bind(this)
    this.chosenLocal = this.chosenLocal.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  componentWillMount() {
    this.props.setBase()
    this.props.clearState()
  }

  componentDidUpdate() {
    const localEdit = get(this.props.acaoEdit, 'local')
    if (this.state.update && localEdit && !this.props.selected._id) {
      this.props.addSelectedLocal(localEdit)
    }
  }

  handleDataSource() {
    const data = this.props.dataSource
    if (data && data.length) {
      const autocompleteList = data.map((item, i) => {
        const obj = item
        obj.text = `${item.nome}`
        obj.value = (
          <MenuItem
            style={i !== (data.length - 1) ? menuItemStyle : {}}
            key={item._id}
            value={item._id}
          >
            <ListItem
              primaryText={item.nome}
              secondaryText={item.endereco}
            />
          </MenuItem>
        )
        return obj
      })
      return autocompleteList
    }
    return []
  }

  handleBlur(e) {
    let textAutoComplete = ''
    const textNormalized = noAccents(e.target.value).toLowerCase().trim()
    const reg = new RegExp(textNormalized, 'gi')
    const result = this.props.dataSource.filter(element => (
      element.nomeNormalizado.match(reg)
    ))

    if (result.length === 1) {
      textAutoComplete = result[0].nome
      this.props.addSelectedLocal(result[0])
    }

    this.setState({
      textAutoComplete,
      errorText: textAutoComplete ? '' : 'Favor selecionar o local da ação',
    })

    this.props.onBlur(textAutoComplete)
  }

  handleClose() {
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    if (acao.tipoAcao && acao.tipoAcao.informarQtdeCartas) {
      document.getElementById('qtdeCartas').focus()
    } else if (document.getElementById('valorGasto')) {
      document.getElementById('valorGasto').focus()
    }
  }

  updateSearchText(text) {
    const textNormalized = noAccents(text).toLowerCase()
    this.props.searchOnBase(textNormalized.trim())
    this.props.handleUpdateLocal({ selected: {} })

    this.setState({
      update: false,
      textAutoComplete: text,
    })
  }

  chosenLocal(chosenRequest) {
    this.props.addSelectedLocal(chosenRequest)
  }

  render() {
    const search = this.props.selected && this.props.selected.nome
                  ? this.props.selected.nome
                  : this.state.textAutoComplete

    const searchTextEdit = get(this.props.acaoEdit, 'local.nome')
    const searchText = search == null ? searchTextEdit : search
    const searchFinal = searchText || undefined

    const errorText = this.props.required ? this.state.errorText || this.props.errorTextLocal : ''

    return (
      <div>
        <Modal
          isOpen={this.props.modalOpen}
          onRequestClose={() => this.props.closeModal()}
          style={customStyle}
          contentLabel={'Novo Local'}
        >
          <div className={styles.modalHeader} id="LocalsAutocompleteIcon">
            Nova instituição
            <SvgIcon
              icon="icon-icon-11"
              onClick={() => this.props.closeModal()}
            />
          </div>
          <div className={styles.holder}>
            <EditInstitutionsPage inModal />
          </div>
        </Modal>
        <div className={styles.localAutocomplete}>
          <div className={styles.searchIcon} id="LocalsAutocompleteIcon">
            <SvgIcon icon="icon-icon-29" />
          </div>
          <AutoComplete
            dataSource={this.handleDataSource()}
            filter={AutoComplete.noFilter}
            floatingLabelText="Selecione o local"
            searchText={searchFinal}
            onNewRequest={this.chosenLocal}
            fullWidth
            onFocus={e => this.updateSearchText((!searchFinal ? '' : searchFinal), e)}
            onBlur={this.handleBlur}
            onUpdateInput={this.updateSearchText}
            openOnFocus
            errorText={errorText}
            floatingLabelStyle={style.flotLabel}
            listStyle={{ maxHeight: '230px', overflowY: 'auto' }}
            onClose={this.handleClose}
          />
        </div>
        <a
          role="button"
          tabIndex="0"
          className={styles.addLocal}
          onClick={() => this.props.openModal()}
        >
          <SvgIcon
            icon="icon-icon-30"
            className={styles.icon}
          />
        </a>
      </div>
    )
  }
}

style.flotLabel = {
  fontSize: '17px',
  color: '#707070',
}

style.itemMenu = {
  position: 'absolute',
  bottom: '-15px',
}

_LocalsAutocomplete.propTypes = {
  dataSource: PropTypes.array.isRequired,
  required: PropTypes.bool,
  acaoEdit: PropTypes.object,
  action: PropTypes.object,
  addSelectedLocal: PropTypes.func,
  searchOnBase: PropTypes.func,
  errorTextLocal: PropTypes.string,
  setBase: PropTypes.func,
  handleUpdateLocal: PropTypes.func,
  modalOpen: PropTypes.bool,
  openModal: PropTypes.func,
  closeModal: PropTypes.func,
  selected: PropTypes.object,
  clearState: PropTypes.func,
  onBlur: PropTypes.func,
}

const mapStateToProps = state => ({
  ...state.localsAutocomplete,
  searchText: state.localsAutocomplete.searchText,
  newLocal: state.newLocal.new,
  acaoEdit: state.acoes.acaoEdit,
  action: state.registerFormAction,
  local: state.localsAutocomplete,
})

const mapActionsToProps = {
  searchOnBase,
  addSelectedLocal,
  setBase,
  openModal,
  closeModal,
  handleUpdateLocal,
  clearState,
}

const LocalsAutocomplete = connect(mapStateToProps, mapActionsToProps)(_LocalsAutocomplete)
export default LocalsAutocomplete
