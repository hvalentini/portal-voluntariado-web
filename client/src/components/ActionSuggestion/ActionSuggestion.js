import React from 'react'
import PropTypes from 'prop-types'
import styles from './ActionSuggestion.css'
import PillButtonKeyLimePie from '../PillButtonKeyLimePie/PillButtonKeyLimePie'

const ActionSuggestion = props => (
  <div
    className={`Grid Grid--alignMiddle ${styles.action}`}
    style={{
      boxShadow: '1px 1px 5px 1px rgba(0, 0, 0, 0.4)',
    }}
  >
    <div className="Grid-cell u-md-size2of3 u-lg-size2of3 u-sm-size3of3">
      <h2>Proponha ações voluntárias</h2>
      <p>Além de participar das atividades, você também pode sugerir novas ideias.</p>
    </div>
    <div className="Grid-cell u-md-size1of3 u-lg-size1of3 u-sm-size3of3">
      <a href={`mailto:${props.email}?subject=Portal%20Voluntariado%20-%20Sugestão%20de%20ação`}>
        <PillButtonKeyLimePie
          text="Tenho uma ideia"
          style={{
            color: '#4a4849',
            padding: '15px 40px',
            fontSize: 23,
          }}
        />
      </a>
    </div>
  </div>
)

ActionSuggestion.propTypes = {
  email: PropTypes.string,
}

export default ActionSuggestion
