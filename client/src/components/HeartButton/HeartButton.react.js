import React from 'react'
import PropTypes from 'prop-types'

import styles from './HeartButton.css'

class HeartButton extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      is_animating: false,
    }

    this.handleClick = this.handleClick.bind(this)
  }

  componentWillMount() {
    this.setState({
      liked: this.props.isLiked,
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLiked === this.props.isLiked) return

    this.setState({
      liked: nextProps.isLiked,
    })
  }

  handleClick() {
    this.setState({
      liked: !this.state.liked,
      is_animating: !this.state.liked,
    })

    this.props.clickHeart()
  }

  render() {
    let likeStyle = this.state.liked ? `${styles.heart} ${styles.heartOn}` : `${styles.heart}`
    likeStyle = this.state.is_animating ? `${styles.heart} ${styles.is_animating}` : likeStyle

    return (
      <span>
        <button
          onClick={this.handleClick}
          className={`${this.props.className} ${styles.heartB}`}
        >
          <div className={`u-posRelative ${styles.heartAlt}`}>
            <div className={styles.icons}>
              <div className={likeStyle} />
            </div>
          </div>
        </button>
        <button
          className={styles.textB}
          onClick={this.props.clickText}
        >
          {this.props.text}
        </button>
      </span>
    )
  }
}

HeartButton.propTypes = {
  isLiked: PropTypes.bool,
  clickHeart: PropTypes.func,
  clickText: PropTypes.func,
  className: PropTypes.string,
  text: PropTypes.string,
}

export default HeartButton
