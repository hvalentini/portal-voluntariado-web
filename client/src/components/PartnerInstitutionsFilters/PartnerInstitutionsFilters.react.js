import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { RadioButton, RadioButtonGroup, TextField } from 'material-ui'
import styles from './PartnerInstitutionsFilters.css'
import { updatePartnersFilter, filterPartners } from '../../actions/filters'
import PillButtonCerulean from '../PillButtonCerulean/PillButtonCerulean'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const style = {}

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  checkbox: {
    checkedColor: '#01BEE8',
    boxColor: '#636363',
  },
  fontFamily: 'Chantilly-Serial',
})

class _PartnerInstitutionsFilters extends React.Component {
  constructor(props) {
    super(props)
    this.setSearchTerm = this.setSearchTerm.bind(this)
    this.changeRadio = this.changeRadio.bind(this)
  }

  setSearchTerm(ev) {
    const obj = {}
    obj[ev.target.name] = ev.target.value
    this.props.updatePartnersFilter(obj) // nome
  }

  changeRadio(ev) {
    const obj = {}
    obj[ev.target.name] = ev.target.value
    this.props.updatePartnersFilter(obj) // escola
  }


  render() {
    const partnersFilter = this.props.filters.partners
    return (
      <div className="formBg">
        <h3 className={styles.title}>Filtros</h3>
        <hr className={styles.separator} />
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <TextField
              hintText="Pesquisar Instituições"
              name="nome"
              underlineStyle={style.line}
              onChange={ev => this.setSearchTerm(ev)}
              maxLength={100}
              inputStyle={{ marginLeft: '38px' }}
              hintStyle={{ marginLeft: '38px' }}
              fullWidth
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />
            <div className={styles.containerCheckbox}>
              <p className={styles.text}>Tipos de instituições</p>
              <RadioButtonGroup
                name="escola"
                valueSelected={partnersFilter.escola}
                onChange={ev => this.changeRadio(ev)}
              >
                <RadioButton
                  value="null"
                  label="Todas"
                  className={`${style.radioButton} u-inlineBlock`}
                />
                <RadioButton
                  value="true"
                  label="Instituições Escolares"
                  className={`${style.radioButton} u-inlineBlock`}
                />
                <RadioButton
                  value="false"
                  label="Instituições não-escolares"
                  className={`${style.radioButton} u-inlineBlock`}
                />
              </RadioButtonGroup>
            </div>
            <div className="Grid-cell u-textCenter">
              <PillButtonCerulean
                text="Filtrar Instituições"
                color="#FFFFFF"
                opacity="1"
                onClick={() => this.props.filterPartners()}
                className={styles.button}
                loader={this.props.filters.loader}
              />
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.radioButton = {
  margin: '4px 0',
}

style.menuStyle = {
  marginBottom: '-12px',
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_PartnerInstitutionsFilters.propTypes = {
  partners: PropTypes.object,
  filters: PropTypes.object,
  filterPartners: PropTypes.func,
  updatePartnersFilter: PropTypes.func,
}

const mapStateToProps = state => ({ filters: state.filters })

const mapActionsToProps = {
  updatePartnersFilter,
  filterPartners,
}

const PartnerInstitutionsFilters = connect(mapStateToProps,
  mapActionsToProps)(_PartnerInstitutionsFilters)

export default PartnerInstitutionsFilters
