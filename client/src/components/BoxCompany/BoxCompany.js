import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './BoxCompany.css'
import { deleteCompany } from '../../actions/empresas'
import { setModal } from '../../actions/modal'
let key = ''

class _BoxCompany extends React.Component {

  constructor(props) {
    super(props)
    this.handleDeleteCompany = this.handleDeleteCompany.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
  }

  handleDeleteCompany(id) {
    this.props.deleteCompany(id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina, id) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina, id)
  }

  handleCompanies() {
    key=''
    return (
      this.props.empresas.map((item) => {
        const msg = `Você tem certeza que deseja excluir a empresa ${item.nome}?`
        key = `${key}${item._id}`
        return (
          <div className={`${styles.root} print`} key={item._id}>
            <p className={styles.nomeEmpresa}>{item.nome}</p>
            <div className={styles.divIcons}>
              <Link to={`/admin/empresas/editar/${item._id}`}>
                <SvgIcon
                  icon="icon-icon-58"
                  className={`${styles.icon} noPrint`}
                  key={`${key}edit`}
                />
              </Link>
              <button
                onClick={() =>
                  this.handleOpenModal(
                    'delete', // TIPO DE AÇÃO
                    'empresas', // ENTIDADE RELACIONADA NO BANCO
                    msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM VENHA DO BANCO ENVIAR NULL
                    this.handleDeleteCompany, // AÇÃO PRINCIPAL
                    '/admin/empresas', // ROTA DA PAGINA PRINCIPAL
                    item._id,
                  )
                }
              >
                <SvgIcon
                  icon="icon-icon-57"
                  className={`${styles.icon} noPrint`}
                  key={`${key}del`}
                />
              </button>
            </div>
            <hr className={styles.separator} />
          </div>
        )
      })
    )
  }

  render() {
    return (
      <div>
        {this.handleCompanies()}
      </div>
    )
  }
}

_BoxCompany.propTypes = {
  deleteCompany: PropTypes.func,
  setModal: PropTypes.func,
  empresas: PropTypes.array,
}

const mapStateToProps = state => ({
  empresas: state.list.empresas,
})

const mapActionsToProps = {
  deleteCompany,
  setModal,
}

const BoxCompany = connect(mapStateToProps, mapActionsToProps)(_BoxCompany)
export default BoxCompany
