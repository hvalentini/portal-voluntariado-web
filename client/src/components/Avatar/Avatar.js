import React from 'react'
import PropTypes from 'prop-types'
import styles from './Avatar.css'


const Avatar = (props) => {
  let styleProp = {}
  let boxImage = { height: '106px', width: '106px' }
  const url = props.url || '/images/user-avatar.svg'

  if (props.borderLess && !props.disabled) {
    styleProp = {
      ...styleProp,
      border: '5px solid rgb(255, 255, 255)',
      boxShadow: '0px 0px 5px 1px #bfbfbf',
    }
  }

  if (props.borderLess && !props.disabled && props.size === 'smaller') {
    styleProp = {
      ...styleProp,
      border: '2px solid rgb(255, 255, 255)',
      boxShadow: '0px 0px 5px 1px #bfbfbf',
    }
  }

  if (props.disabled) {
    styleProp = { ...styleProp, opacity: 0.5 }
    boxImage = { height: '77px', width: '77px' }
  }

  if (props.opacity && !props.disabled) { styleProp = { ...styleProp, opacity: props.opacity } }
  if (props.size === 'smaller') { boxImage = { height: '36px', width: '36px' } }
  if (props.size === 'small') { boxImage = { height: '72px', width: '72px' } }
  if (props.size === 'exMedium') { boxImage = { height: '140px', width: '140px' } }
  if (props.size === 'another') { boxImage = { height: '198px', width: '198px' } }
  if (props.size === 'large') { boxImage = { height: '206px', width: '206px' } }

  return (
    <div style={{ margin: '0 auto', ...boxImage }}>
      <img
        src={url}
        className={styles.Avatar}
        style={{ ...styleProp, ...props.style }}
        alt="avatar"
      />
    </div>
  )
}

Avatar.propTypes = {
  opacity: PropTypes.number.isRequired,
  borderLess: PropTypes.bool.isRequired,
  size: PropTypes.string.isRequired,
  url: PropTypes.string,
  style: PropTypes.object,
  disabled: PropTypes.bool.isRequired,
}

Avatar.defaultProps = {
  disabled: false,
  opacity: 1,
  borderLess: true,
  size: 'medium',
  style: {},
  url: '/images/user-avatar.svg',
}

export default Avatar
