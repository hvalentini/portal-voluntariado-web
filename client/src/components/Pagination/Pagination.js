import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import { filterPagination } from '../../actions/filters'
import styles from './Pagination.css'

class _Pagination extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      500,
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      pageSelected: 0,
      scroll: 0,
    }
    this.handlePage = this.handlePage.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.order !== nextProps.order) {
      this.setState({
        pageSelected: 0,
      })
    }

    if (this.props.actions !== nextProps.actions) {
      if (this.state.scroll === 0) {
        _Pagination.scrollTo()
        return this.setState({ scroll: 1 })
      }
    }

    return this.setState({ scroll: 0 })
  }

  handlePage(ev) {
    const obj = {
      filters: this.props.filters,
      comite: this.props.comite._id,
      order: this.props.order,
      page: ev.target.value,
    }
    this.props.filterPagination(obj)
    this.setState({
      pageSelected: ev.target.value,
    })
  }

  handleDivPages() {
    // OBS: DEFINIDO 6 REGISTROS POR PAGINA
    const qtdPages = Math.ceil(this.props.qtdActions / 6)

    const objectRows = []
    for (let i = 0; i < qtdPages; i += 1) {
      const classe = (i === this.state.pageSelected) ? styles.pageActive : styles.page
      objectRows.push(
        <button
          key={i}
          value={i}
          className={classe}
          onClick={this.handlePage}
        >
          { i + 1 }
        </button>,
      )
    }
    return objectRows
  }

  render() {
    if (this.props.qtdActions <= 6 || !this.props.actions.length || this.props.loading) return null

    return (
      <div className={styles.pagination} key={this.props.actions[0]._id}>
        {this.handleDivPages()}
      </div>
    )
  }
}

_Pagination.propTypes = {
  filters: PropTypes.object,
  comite: PropTypes.object,
  order: PropTypes.string,
  filterPagination: PropTypes.func,
  qtdActions: PropTypes.number,
  loading: PropTypes.bool,
  actions: PropTypes.array,
}

const mapStateToProps = state => ({
  actions: state.acoes.acoesLeader,
  filters: state.filters.acoes,
  comite: state.loggedUser.comite,
  qtdActions: state.acoes.qtdActions,
  loading: state.acoes.loading,
})

const mapActionsToProps = {
  filterPagination,
}

const Pagination = connect(mapStateToProps, mapActionsToProps)(_Pagination)
export default Pagination
