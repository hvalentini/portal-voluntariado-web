import React from 'react'
import styles from './HeaderAdmin.css'
import HeaderAdminButton from '../../components/HeaderAdminButton/HeaderAdminButton'
import HeaderAdminUser from '../../components/HeaderAdminUser/HeaderAdminUser'

const HeaderAdmin = () => (
  <div className={styles.root}>
    <div className={styles.menuBlock}>
      <div className={styles.menu}>
        <HeaderAdminButton
          // text={
          //   <SvgIcon
          //     icon="icon-icon-62"
          //     style={{ fontSize: '24px', verticalAlign: 'middle' }}
          //   />
          // }
          exact
          to="/admin"
          text="Home"
        />
        <HeaderAdminButton text="Cidades" to="/admin/cidades" />
        <HeaderAdminButton text="Empresas" to="/admin/empresas" />
        <HeaderAdminButton text="Comitês" to="/admin/comites" />
        <HeaderAdminButton text="Tipos de ações" to="/admin/tipos-de-acoes" />
        <HeaderAdminButton text="Ações" to="/admin/acoes" />
        <HeaderAdminButton text="Instituições" to="/admin/instituicoes-parceiras" />
      </div>
    </div>
    <div className={styles.divHeaderAdminUser}>
      <HeaderAdminUser />
    </div>
  </div>
)

export default HeaderAdmin
