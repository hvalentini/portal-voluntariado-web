import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TextField, Dialog, FlatButton } from 'material-ui'
import { Link } from 'react-router-dom'
import PillButtonWhite from '../PillButtonWhite/PillButtonWhite'
import styles from './LoginPassRecovery.css'
import {
  updateLogin,
  updateLoginUser,
  sendRecoverRequest,
  closeDialog,
} from '../../actions/login'
import ErrorMessage from '../ErrorMessage/ErrorMessage.react'


class _LoginPassRecovery extends React.Component {

  constructor(props) {
    super(props)
    this.handleChangeUser = this.handleChangeUser.bind(this)
    this.sendRecoverRequest = this.sendRecoverRequest.bind(this)
    this.clearError = this.clearError.bind(this)
  }

  handleChangeUser(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateLoginUser(obj)
  }

  sendRecoverRequest() {
    const emailCpf = this.props.login.user.username
    this.props.sendRecoverRequest(emailCpf)
  }

  clearError() {
    this.props.updateLogin({ error: null })
  }

  renderAction(disableSubmit) {
    if (this.props.login.error) {
      return (
        <ErrorMessage close={this.clearError} style={{ marginTop: '20px' }}>
          {this.props.login.error}
        </ErrorMessage>
      )
    }

    return (
      <div className={styles.buttons}>
        <div className={styles.buttonBox}>
          <PillButtonWhite
            text="Acessar"
            color="#01BEE8"
            className={styles.actionBtn}
            onClick={() => this.sendRecoverRequest()}
            loader={this.props.login.loader}
            disabled={disableSubmit}
          />
        </div>
        <p className={styles.action}>
          Novo por aqui? <Link to="/cadastro/passo-1">Cadastre-se</Link>
        </p>
      </div>
    )
  }

  render() {
    const user = this.props.login.user
    const disableSubmit = !(user.username)
    return (
      <div>
        <h1>Perdeu a senha?</h1>
        <h1>Digite o seu email para continuar</h1>
        <div>
          <div className="Grid Grid--withGutter">
            <div className="Grid-cell u-posRelative u-textLeft">
              <Dialog
                actions={
                  <FlatButton
                    label="Ok"
                    secondary
                    className={styles.button}
                    onTouchTap={this.props.closeDialog}
                    keyboardFocused
                  />
                }
                modal={false}
                open={this.props.login.isDialogOpen}
                onRequestClose={this.props.closeDialog}
                bodyStyle={{ textAlign: 'center', color: '#484848', fontSize: '18px' }}
              >
                {this.props.login.message}
              </Dialog>
              <TextField
                floatingLabelText="Seu E-mail/CPF"
                floatingLabelStyle={{ color: '#fff' }}
                name="username"
                value={user.username}
                onChange={e => this.handleChangeUser(e.target.name, e.target.value)}
                maxLength={100}
                fullWidth
              />
            </div>
          </div>
        </div>
        {this.renderAction(disableSubmit)}
      </div>
    )
  }
}

_LoginPassRecovery.displayName = 'LoginPassRecovery'

_LoginPassRecovery.propTypes = {
  isDialogOpen: PropTypes.bool,
  closeDialog: PropTypes.func,
  login: PropTypes.object.isRequired,
  updateLogin: PropTypes.func.isRequired,
  updateLoginUser: PropTypes.func.isRequired,
  sendRecoverRequest: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  login: state.login,
})

const mapActionsToProps = {
  updateLoginUser,
  sendRecoverRequest,
  updateLogin,
  closeDialog,
}

const LoginPassRecovery = connect(mapStateToProps, mapActionsToProps)(_LoginPassRecovery)

export default LoginPassRecovery
