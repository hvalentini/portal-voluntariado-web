import React from 'react'
import PropTypes from 'prop-types'
import { TextField } from 'material-ui'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Scrollbars } from 'react-custom-scrollbars'
import styles from './NotificationsAdmin.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import muiTheme from '../../muiTheme'
import LoaderAdmin from '../LoaderAdmin/LoaderAdmin'
import { updateNotificationEntity, sendNotification, getAdminNotifications } from '../../actions/notificacao'
import ModalDialog from '../../components/ModalDialog/ModalDialog'

class _Component extends React.Component {

  static scrollGlue() {
    setTimeout(() => {
      const objDiv = document.getElementById('scrollAdminNotif') ? document.getElementById('scrollAdminNotif').getElementsByTagName('div')[0] : {}
      objDiv.scrollTop = objDiv.scrollHeight
    }, 0)
  }

  constructor(props) {
    super(props)
    this.state = {
      openModal: false,
    }
    this.handleChange = this.handleChange.bind(this)
    this.sendMsg = this.sendMsg.bind(this)
    this.renderNotifications = this.renderNotifications.bind(this)
    this.handleKeyPress = this.handleKeyPress.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.confirmSend = this.confirmSend.bind(this)
  }

  componentWillMount() {
    this.props.getAdminNotifications()
  }

  componentWillReceiveProps(newProps) {
    if (this.props.notificacao.notificacoes !== newProps.notificacao.notificacoes
      || this.props.notificacao.entidade.tipo !== newProps.notificacao.entidade.tipo) {
      _Component.scrollGlue()
    }
  }

  handleChange(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateNotificationEntity(obj)
  }

  handleKeyPress(e) {
    if (e.key === 'Enter' && !e.shiftKey) {
      this.confirmSend(e)
    }
  }

  confirmSend(e) {
    e.preventDefault()
    if (this.props.notificacao.entidade.texto.trim()) {
      this.setState({ openModal: true })
    } else {
      this.sendMsg()
    }
  }

  sendMsg() {
    this.props.sendNotification()
    this.setState({ openModal: false })
    setTimeout(() => {
      this.algumaCoisa.focus()
    }, 0)
  }

  closeModal() {
    this.setState({ openModal: false })
    setTimeout(() => {
      this.algumaCoisa.focus()
    }, 0)
  }

  renderNotifications() {
    const notes = this.props.notificacoes || []
    if (!notes.length > 0) {
      return (<LoaderAdmin />)
    }
    return notes.map((item) => {
      const datetime = item.dataCadastro.split('T')
      const date = datetime[0].split('-').reverse().join('/')
      const time = datetime[1].substring(0, 5)
      const texto = item.texto.replace(/(\r\n|\n|\r)/gm, '<br />').split('<br />').map((tag, i) => (
        <span key={i}>{tag}<br /></span>
      ))
      return (
        <div key={item._id} className={styles.ballon}>
          <p className={styles.texto}>
            <span className={styles.textoSpan}>
              {texto}
              <SvgIcon
                className={styles.iconCheck}
                icon="icon-icon-19"
              />
            </span>
          </p>
          <p className={styles.datetime}>
            Enviado: {date} - {time}
          </p>
        </div>
      )
    })
  }

  renderModal() {
    if (this.state.openModal) {
      const contact = this.props.notificacao.entidade.tipo === 'Mensagem Admin Somente Líderes'
                      ? 'todos os líderes?'
                      : 'todos voluntários?'
      return (
        <ModalDialog
          type="warning"
          open={this.state.openModal}
          textFirst="Cancelar"
          actionFirst={this.closeModal}
          textSecond="Continuar"
          actionSecond={this.sendMsg}
          msg={`Tem certeza que deseja enviar esta mensagem para ${contact}?`}
        />
      )
    }

    return null
  }

  render() {
    const height = window.innerHeight - 384
    if (!this.props.notificacao.entidade.tipo) {
      return (
        <div>
          <div className={styles.container}>
            <h3 className={styles.title}>Notificações</h3>
            <hr className={styles.separator} />
          </div>
          <div className={styles.root} style={{ height: `${height}px` }}>
            <div className={styles.empty}>
              <SvgIcon
                className={styles.iconConversation}
                icon="icon-icon-69"
              />
              <p>Selecione um dos contatos ao lado para enviar notificações.</p>
            </div>
          </div>
        </div>
      )
    }
    return (
      <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
        <div>
          <div className={styles.container}>
            <h3 className={styles.title}>Notificações</h3>
            <hr className={styles.separator} />
          </div>
          <form className={styles.container}>
            <div className={styles.notificacoes} style={{ height: `${height}px` }}>
              <Scrollbars autoHide id="scrollAdminNotif">
                {this.renderNotifications()}
              </Scrollbars>
            </div>
            {/* campo para escrever a mensagem */}
            <div className="u-posRelative">
              <TextField
                hintText={`NOTIFICAÇÃO PARA ${this.props.notificacao.entidade.tipo === 'Mensagem Admin' ? 'TODOS' : 'LÍDERES'}`}
                className={styles.textField}
                hintStyle={{ padding: '0 10px', color: '#cbcbcb' }}
                name="texto"
                underlineShow={false}
                value={this.props.notificacao.entidade.texto}
                onChange={e => this.handleChange(e.target.name, e.target.value)}
                style={{ height: '52px', width: 'calc(100%)', border: '0.6px solid #cbcbcb', padding: '0 10px 10px' }}
                multiLine
                rows={1}
                rowsMax={2}
                fullWidth
                onKeyPress={this.handleKeyPress}
                ref={(c) => { this.algumaCoisa = c }}
              />
              <button
                className={styles.sendBtn}
                onClick={this.confirmSend}
              >
                <SvgIcon
                  className={styles.submit}
                  icon="icon-icon-70"
                />
              </button>
            </div>

          </form>
          {this.renderModal()}
        </div>
      </MuiThemeProvider>
    )
  }
}

_Component.displayName = 'NotificationsAdmin'

_Component.propTypes = {
  notificacao: PropTypes.object,
  updateNotificationEntity: PropTypes.func,
  sendNotification: PropTypes.func,
  getAdminNotifications: PropTypes.func,
  notificacoes: PropTypes.array,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
  notificacao: state.notificacao,
})

const mapActionsToProps = {
  updateNotificationEntity,
  sendNotification,
  getAdminNotifications,
}

const Componet = connect(mapStateToProps, mapActionsToProps)(_Component)

export default Componet
