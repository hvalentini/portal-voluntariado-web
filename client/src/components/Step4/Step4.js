import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Dialog, FlatButton } from 'material-ui'
import styles from './Step4.css'
import UserAvatar from '../UserAvatar/UserAvatar'
import PillButtonWhite from '../PillButtonWhite/PillButtonWhite'
// import PillButtonCerulean from '../PillButtonCerulean/PillButtonCerulean'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import {
  updateStep,
  saveData,
  clearErrorMsg,
  updateSession } from '../../actions/registerForm'

class _Step4 extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      habilitar: false,
    }

    this.scrollDiv = this.scrollDiv.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleClearErrorMsg = this.handleClearErrorMsg.bind(this)
  }

  componentWillMount() {
    this.props.updateStep(4)
  }

  componentDidMount() {
    document.getElementById('term').addEventListener('scroll', this.scrollDiv)
  }

  scrollDiv() {
    const object = document.getElementById('term')
    if ((object.scrollTop + object.offsetHeight) >= object.scrollHeight) {
      this.setState({
        habilitar: true,
      })
    }
  }

  handleSave() {
    this.props.saveData()
  }

  handleClearErrorMsg() {
    this.props.clearErrorMsg()
  }

  render() {
    const errorMsg = this.props.registerForm.errorMsg
    const loader = this.props.registerForm.loader
    const opacity = this.state.habilitar ? '1' : '0.5'
    const disabled = (!this.state.habilitar)
    const size = window.innerHeight > 800
                ? 'large'
                : 'medium'
    const nome = this.props.registerForm.step1.nomeCompleto
    const step2 = this.props.registerForm.step2
    const cidade = step2.cidade ? `${step2.cidade.nome} - ${step2.cidade.uf}` : null
    const cpf = this.props.registerForm.step1.cpfMasked || this.props.registerForm.step1.cpf
    const data = new Date()
    const date = ("0" + data.getDate()).substr(-2) + "/"
      + ("0" + (data.getMonth() + 1)).substr(-2) + "/" + data.getFullYear()

    if (this.props.registerForm.success) {
      window.location.assign('/welcome')
      // this.props.updateSession({
      //   nome: this.props.registerForm.step1.nome,
      //   email: this.props.registerForm.step2.email,
      // })
      // if (this.props.registerForm.finish) {
      //   window.location.assign('/welcome')
      // }
    }
    const actions = [
      <FlatButton
        label="Ok"
        secondary
        className={styles.buttonDialog}
        onTouchTap={this.handleClearErrorMsg}
        keyboardFocused
      />,
    ]

    return (
      <div className={styles.formBg}>
        <Dialog
          actions={actions}
          modal={false}
          open={(errorMsg !== null)}
          onRequestClose={this.handleClearErrorMsg}
          bodyStyle={{ textAlign: 'center', color: '#484848', fontSize: '18px' }}
        >
          {errorMsg}
        </Dialog>
        <div className={styles.container}>
          <div className={styles.avatar}>
            <UserAvatar
              url={this.props.step1.urlAvatar}
              size={size}
            />
          </div>
          <h1>{this.props.step1.nome}</h1>
          <div className={styles.divAnimation}>
            {/* eslint-disable */}
            <div className={styles.containerTerm} id="term">
              <p className={styles.title}><span><b>Termo de adesão ao trabalho voluntário</b></span></p>
              <p className={styles.title}><span><b>Programa de Voluntariado do Grupo Algar</b></span></p>
              <p><span>Que entre si fazem, de um lado INSTITUTO ALGAR, inscrito no CNPJ sob o n&uacute;mero 04.893.499/0001-64, com endere&ccedil;o na Rua Lapa do Lobo, 800, Bairro Granja Marileusa, na cidade de Uberl&acirc;ndia-MG, neste ato representada nos termos de seu Estatuto Social, doravante denominada <strong>INSTITUTO</strong>, e de outro lado o Sr. (a) <span>{nome}</span>, inscrito no CPF sob n&uacute;mero <span>{cpf}</span> e com endere&ccedil;o na <span>{step2.endereco}</span>, Bairro <span>{step2.bairro}</span>, na cidade de <span>{cidade}</span>, doravante denominado <strong>VOLUNT&Aacute;RIO, </strong>resolvem firmar o presente termo de ades&atilde;o ao PROGRAMA DE VOLUNTARIADO DO GRUPO ALGAR, doravante denominado <strong>PROGRAMA.</strong></span></p>
              <br />
              <p><strong><u><span>Cl&aacute;usula 1&ordf;. &ndash; Ades&atilde;o ao Programa de Voluntariado do Grupo Algar</span></u></strong></p>
              <p><span>Por meio do presente, o <strong>VOLUNT&Aacute;RIO</strong> por sua livre e espont&acirc;nea vontade adere ao <strong>PROGRAMA </strong>a fim de contribuir com as atividades desenvolvidas de voluntariado coordenadas pelo <strong>INSTITUTO.</strong></span></p>
              <br />
              <p><strong><u><span>Cl&aacute;usula 2&ordf;. &ndash; Operacionaliza&ccedil;&atilde;o e Fundamenta&ccedil;&atilde;o legal</span></u></strong></p>
              <p><strong><span>O PROGRAMA </span></strong><span>ser&aacute; rege-se pela Pol&iacute;tica de Voluntariado do Grupo Algar, doravante denominada <strong>POL&Iacute;TICA</strong> e em observ&acirc;ncia na Lei n&ordm;. 9.608/98 de 18/02/98, que regula as condi&ccedil;&otilde;es do Servi&ccedil;o Volunt&aacute;rio no Brasil e nestes termos n&atilde;o gera para as partes qualquer tipo de obriga&ccedil;&atilde;o de retribui&ccedil;&atilde;o financeira, remunera&ccedil;&atilde;o, obriga&ccedil;&otilde;es trabalhistas ou previdenci&aacute;rias.</span></p>
              <br />
              <p><strong><u><span>Cl&aacute;usula 3&ordf;. -&nbsp; Conduta esperado do VOLUNT&Aacute;RIO</span></u></strong></p>
              <p><span>Compete ao <strong>VOLUNT&Aacute;RIO</strong> participar das a&ccedil;&otilde;es propostas <strong>INSTITUTO</strong> e cumprir com empenho todas as atividades que se propuser. Atos ou omiss&otilde;es praticadas pelo <strong>VOLUNT&Aacute;RIO</strong> que contrariem a <strong>POL&Iacute;TICA </strong>a finalidade do <strong>PROGRAMA </strong>poder&atilde;o acarretar seu afastamento ou desligamento do <strong>PROGRAMA</strong>. Compete exclusivamente ao Coordenador do <strong>INSTITUTO</strong> decidir sobre estes casos. Compete ainda ao <strong>VOLUNT&Aacute;RIO </strong>zelar para imagem do <strong>INSTITUTO</strong>, n&atilde;o praticando nenhum ato que contrarie os princ&iacute;pios do <strong>INSTITUTO</strong>, tais como, mas n&atilde;o limitando, a intoler&acirc;ncia a qualquer tipo de explora&ccedil;&atilde;o de trabalho ou qualquer tipo de discrimina&ccedil;&atilde;o.</span></p>
              <br />
              <p><strong><u><span>Cl&aacute;usula 4&ordf;. -&nbsp; Uso da imagem</span></u></strong></p>
              <p><span>O <strong>VOLUNT&Aacute;RIO</strong> autoriza o uso de sua imagem e voz em car&aacute;ter irrevog&aacute;vel, irretrat&aacute;vel e de forma gratuita para o <strong>INSTITUTO</strong> e para todas as empresas que comp&otilde;em o Grupo Econ&ocirc;mico ALGAR. O I<strong>NSTITUTO</strong> por sua vez garante ao <strong>VOLUNT&Aacute;RIO</strong> que seu nome, voz e imagem quando utilizados, ser&atilde;o exclusivamente para fins de divulga&ccedil;&atilde;o, observada a moral e os bons costumes.</span></p>
              <br />
              <p><strong><u><span>Cl&aacute;usula 5&ordf;. -&nbsp; Desligamento do PROGRAMA</span></u></strong></p>
              <p><span>O desligamento por inciativa do <strong>VOLUNT&Aacute;RIO </strong>poder&aacute; ocorrer a qualquer momento, sem qualquer &ocirc;nus para as partes, bastando que comunique expressamente ao L&iacute;der Social da Empresa em que trabalha.</span></p>
              <br />
              <p><strong><u><span>Cl&aacute;usula 6&ordf;. -&nbsp; Disposi&ccedil;&otilde;es Gerais</span></u></strong></p>
              <ol style={{ marginBottom: '0cm' }} type="a">
              <li><span>Por se tratar de uma rela&ccedil;&atilde;o de voluntariado, cuja participa&ccedil;&atilde;o decorre exclusivamente da vontade do <strong>VOLUNT&Aacute;RIO</strong>, desde j&aacute; o <strong>VOLUNT&Aacute;RIO</strong> isenta o <strong>INSTITUTO</strong>, as Institui&ccedil;&otilde;es e ou Parceiros do <strong>INSTITUTO</strong> no <strong>PROGRAMA</strong> e tamb&eacute;m as Empresas que comp&otilde;em o Grupo Algar de qualquer responsabilidade civil, trabalhista ou criminal e eventual pretens&atilde;o indenizat&oacute;ria decorrente de acidentes ou eventuais danos materiais ocorridos no exerc&iacute;cio de suas atividades como <strong>VOLUNT&Aacute;RIO.</strong></span></li>
              <li><span>Eventuais despesas com alimenta&ccedil;&atilde;o, deslocamento, compra de materiais e outros custos para realiza&ccedil;&atilde;o de atividades do <strong>PROGRAMA, ser&atilde;o</strong> ressarcidas pelo <strong>INSTITUTO</strong>, desde que sejam comprovadas e previamente e expressamente autorizados por este.</span></li>
              </ol>
              <br />
              <p><strong><u><span>Cl&aacute;usula 7&ordf;. - Aceite</span></u></strong></p>
              <p><strong><span>O VOLUNT&Aacute;RIO </span></strong><span>declara estar ciente e ter conhecimento integral da Legisla&ccedil;&atilde;o especifica, normas e regulamentos estabelecidos pelo <strong>INSTITUTO,</strong> aceitando de forma expressa aderir <strong>PROGRAMA</strong> nos exatos termos constantes do presente instrumento. O aceite poder&aacute; ser feito de forma expressa neste documento ou por assinatura digital por meio do uso de recursos da WEB ou aplicativos de correio eletr&ocirc;nico.</span></p>
              <p><strong><span>&nbsp;</span></strong></p>
              <p><span>{cidade}, {date}</span></p>
              <p><span>&nbsp;</span></p>
            </div>
            {/* eslint-active */}
            <p className={styles.note}>Para prosseguir é necessário ler todo o termo. Ao escolher "Aceito os termos" abaixo, você concorda completamente com o Termo de Adesão ao Trabalho Voluntário do <b>Grupo Algar</b>.</p>
          </div>
          <div className={styles.buttons}>
            <Link
              to="passo-3"
              className={styles.btnBack}
            >
              <SvgIcon
                icon="icon-icon-10"
                className={styles.icon}
              />
              Voltar
            </Link>
            <div className={styles.btnAccept}>
              <PillButtonWhite
                text="Aceito os termos"
                color="#01BEE8"
                disabled={disabled}
                opacity={opacity}
                onClick={this.handleSave}
                loader={loader}
                style={{ margin: 0 }}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

_Step4.propTypes = {
  step1: PropTypes.object,
  registerForm: PropTypes.object,
  updateStep: PropTypes.func,
  saveData: PropTypes.func,
  updateSession: PropTypes.func,
  clearErrorMsg: PropTypes.func,
}

const mapStateToProps = state => ({
  step1: state.registerForm.step1,
  registerForm: state.registerForm,
})

const mapActionsToProps = {
  updateStep,
  saveData,
  updateSession,
  clearErrorMsg,
}

const Step4 = connect(mapStateToProps, mapActionsToProps)(_Step4)

export default Step4
