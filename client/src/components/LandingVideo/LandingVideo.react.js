import React from 'react'
import PropTypes from 'prop-types'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './LandingVideo.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class LandingVideo extends React.Component {

  static calculateRatio(video) {
    const ratio = video.videoHeight / video.videoWidth
    return {
      width: parseInt(ratio * window.innerWidth, 10),
      height: parseInt(ratio * window.innerHeight, 10),
    }
  }

  static playButton(playBtn, vidPlayer) {
    const playButton = playBtn
    const videoPlayer = vidPlayer
    const scroll = document.getElementsByClassName('pv-SectionVideoAboutUs-landingVideo')[0].offsetTop
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
            ? document.querySelector('html')
            : document.querySelector('body')
    animatedScrollTo(
      el,
      scroll,
      1000,
    )
    playButton.style.display = 'none'
    videoPlayer.setAttribute('controls', true)
    videoPlayer.play()
  }

  static videoPlayerPlay(el) {
    const playButton = el
    playButton.style.display = 'none'
  }

  static videoPlayerPause(el) {
    const playButton = el
    playButton.style.display = 'block'
  }

  static videoPlayerClick(playBtn, vidPlayer) {
    const playButton = playBtn
    const videoPlayer = vidPlayer
    playButton.style.display = 'block'
    videoPlayer.pause()
    videoPlayer.setAttribute('controls', false)
  }

  static windowResize(el) {
    const videoPlayer = el
    const ratio = LandingVideo.calculateRatio(videoPlayer)
    videoPlayer.style.width = `${ratio.width}px`
    videoPlayer.style.height = `${ratio.height}px`
  }

  constructor(props) {
    super(props)

    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount() {
    const playButton = document.getElementsByClassName('pv-LandingVideo-playButton')[0]
    const videoPlayer = document.getElementById('landingVideo')

    playButton.addEventListener('click', () => LandingVideo.playButton(playButton, videoPlayer))

    videoPlayer.addEventListener('play', () => LandingVideo.videoPlayerPlay(playButton))

    videoPlayer.addEventListener('pause', () => LandingVideo.videoPlayerPause(playButton))

    videoPlayer.addEventListener('click', () => LandingVideo.videoPlayerClick(playButton, videoPlayer), false)

    window.addEventListener('resize', () => LandingVideo.windowResize(videoPlayer))
  }

  componentWillUnmount() {
    const playButton = document.getElementsByClassName('pv-LandingVideo-playButton')[0]
    const videoPlayer = document.getElementById('landingVideo')

    try {
      playButton.removeEventListener('click', LandingVideo.playButton)
      videoPlayer.removeEventListener('click', LandingVideo.videoPlayerClick)
      videoPlayer.removeEventListener('play', LandingVideo.videoPlayerPlay)
      videoPlayer.removeEventListener('pause', LandingVideo.videoPlayerPause)
      window.removeEventListener('resize', LandingVideo.windowResize)
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    return (
      <div
        className={styles.container}
      >
        <div
          className={styles.playButton}
        >
          <SvgIcon
            icon="icon-icon-6"
          />
        </div>
        <video
          poster={this.props.cover}
          id="landingVideo"
          className={styles.landingVideo}
          style={{
            width: parseInt((360 / 640) * window.innerWidth, 10),
            height: parseInt((360 / 640) * window.innerHeight, 10),
          }}
        >
          <source src={`${this.props.url}.mp4`} type="video/mp4" />
          <source src={`${this.props.url}.ogv`} type="video/ogg" />
        </video>
      </div>
    )
  }
}

LandingVideo.propTypes = {
  url: PropTypes.string.isRequired,
  cover: PropTypes.string.isRequired,
}

export default LandingVideo
