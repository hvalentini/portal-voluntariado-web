import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  TextField,
  SelectField,
  MenuItem,
  RadioButton,
  RadioButtonGroup,
  DatePicker,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import noAccents from 'remove-accents'
import { Scrollbars } from 'react-custom-scrollbars'
import muiTheme from '../../muiTheme'
import PillButton from '../PillButton/PillButton'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { getAcoes } from '../../actions/acoes'
import { getCities } from '../../actions/cidades'
import { handleCompanies } from '../../actions/empresas'
import { setBase } from '../../actions/localAutocomplete'
import { handleFetchActionsTypes } from '../../actions/registerFormAction'
import styles from './FilterActionsAdmin.css'
import maskMoney from '../../utils/maskMoney'
import moneyToFloat from '../../utils/moneyToFloat'
import numericMask from '../../utils/numericMask'

const IntlPolyfill = require('intl')

const DateTimeFormat = IntlPolyfill.DateTimeFormat
require('intl/locale-data/jsonp/pt-BR')

const style = {
  date: {
    width: '50%',
  },
  black: {
    color: '#484848',
    fontSize: '18px',
  },
  radioButton: {
    margin: '4px 0',
  },
  menuStyle: {
    marginBottom: '-12px',
  },
  menuStyleSelect: {
    marginBottom: '0',
  },
  line: {
    borderColor: 'rgb(170, 170, 170)',
  },
}

class _FilterActionsAdmin extends React.Component {
  static isMSIE() {
    let isMSIE = false

    if (navigator.appName === 'Microsoft Internet Explorer') {
      const ua = navigator.userAgent
      const re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})')

      if (re.exec(ua) !== null) {
        isMSIE = true
      }
    } else if (navigator.appName === 'Netscape') {
      if (navigator.appVersion.indexOf('Trident') > -1) isMSIE = true
      else if (navigator.appVersion.indexOf('Edge') > -1) isMSIE = true
    }

    return isMSIE
  }

  constructor(props) {
    super(props)
    this.state = {
      btnBottom: 0,
      filtro: {
        nomeNormalizado: '',
        verbaExtra: '',
        cidade: '',
        empresa: '',
        local: '',
        statusAcao: '',
        totalParticipantes: '',
        totalBeneficiados: '',
        valorGasto: '',
        tipoAcao: '',
      },
    }
    this.getFilterActions = this.getFilterActions.bind(this)
    this.onChangeTypeActions = this.onChangeTypeActions.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
    this.onChangeCidade = this.onChangeCidade.bind(this)
    this.onChangeEmpresa = this.onChangeEmpresa.bind(this)
    this.onChangeLocal = this.onChangeLocal.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeMoney = this.handleChangeMoney.bind(this)
    this.handleChangeNumeric = this.handleChangeNumeric.bind(this)
    this.handleChangeStatus = this.handleChangeStatus.bind(this)
    this.renderDate = this.renderDate.bind(this)
  }

  componentWillMount() {
    this.props.handleFetchActionsTypes()
    this.props.getCities()
    this.props.handleCompanies()
    this.props.setBase()
  }

  onChangeTypeActions(ev, index, value) {
    const filtro = { ...this.state.filtro, tipoAcao: value }
    this.setState({ ...this.state, filtro })
  }

  onChangeCidade(ev, index, value) {
    const filtro = { ...this.state.filtro, cidade: value }
    this.setState({ ...this.state, filtro })
  }

  onChangeEmpresa(ev, index, value) {
    const filtro = { ...this.state.filtro, empresa: value }
    this.setState({ ...this.state, filtro })
  }

  onChangeLocal(ev, index, value) {
    const filtro = { ...this.state.filtro, local: value }
    this.setState({ ...this.state, filtro })
  }

  getFilterActions() {
    const filtro = { ...this.state.filtro }

    filtro.nomeNormalizado = noAccents(filtro.nomeNormalizado).trim()

    Object.keys(filtro).forEach((key) => {
      if (!filtro[key] || filtro[key] === '') delete filtro[key]
    })

    filtro.valorGasto = filtro.valorGasto ? moneyToFloat(filtro.valorGasto) : filtro.valorGasto
    filtro['comite.verbaExtra'] = filtro.verbaExtra ? moneyToFloat(filtro.verbaExtra) : filtro.verbaExtra
    filtro['comite.empresa'] = filtro.empresa
    filtro['comite.cidade'] = filtro.cidade
    filtro['tipoAcao'] = filtro.tipoAcao
    filtro['local'] = filtro.local
    filtro.comite = this.props.comite

    delete filtro.verbaExtra
    delete filtro.empresa
    delete filtro.cidade
    // delete filtro.local
    // delete filtro.tipoAcao
    filtro.desativada = false

    this.props.getAcoes({ filtro: JSON.stringify(filtro) })

    this.props.handleFilter(filtro)
  }

  handleChange(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    this.setState({ ...this.state, filtro })
  }

  handleChangeStatus(e) {
    const filtro = { ...this.state.filtro }
    const date = new Date()
    switch (e.target.value) {
      case ('programadas'):
        filtro.statusAcao = e.target.value
        filtro.dataInicial = date
        filtro.dataFinal = ''
        break
      case ('concluidas'):
        filtro.statusAcao = e.target.value
        filtro.dataInicial = ''
        filtro.dataFinal = ''
        break
      case ('andamento'):
        filtro.statusAcao = e.target.value
        filtro.dataInicial = date
        filtro.dataFinal = date
        break
      default:
        filtro.statusAcao = ''
        filtro.dataInicial = ''
        filtro.dataFinal = ''
        break
    }

    this.setState({ ...this.state, filtro })
  }

  handleChangeMoney(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value === '' ? '' : maskMoney(moneyToFloat(e.target.value))
    this.setState({ ...this.state, filtro })
  }

  handleChangeNumeric(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value === '' ? '' : numericMask(e.target.value)
    this.setState({ ...this.state, filtro })
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  adminFilters() {
    if (!this.props.isAdmin) { return null }
    return (
      <div style={{ width: '100%' }}>
        <TextField
          floatingLabelText="Verba Extra"
          floatingLabelShrinkStyle={{ fontSize: '18px' }}
          name="verbaExtra"
          underlineStyle={style.line}
          value={this.state.filtro.verbaExtra}
          onChange={this.handleChangeMoney}
          maxLength={100}
          fullWidth
        />
        <p className={styles.text}>Selecione uma cidade: </p>
        <SelectField
          name="cidade"
          value={this.state.filtro.cidade}
          onChange={this.onChangeCidade}
          labelStyle={style.menuStyle}
          underlineStyle={style.line}
          style={{ top: '-10px', marginBottom: '-10px' }}
          fullWidth
        >
          <MenuItem
            className="SelectBkg"
            value=""
            primaryText="Todos"
          />
          {
            this.props.cidades.map((item, i) => (
              <MenuItem
                className="SelectBkg"
                value={item._id}
                primaryText={`${item.nome}/${item.uf}`}
                key={i}
              />

            ))
          }
        </SelectField>
        <p className={styles.text}>Selecione uma empresa: </p>
        <SelectField
          value={this.state.filtro.empresa}
          onChange={this.onChangeEmpresa}
          labelStyle={style.menuStyle}
          underlineStyle={style.line}
          style={{ top: '-10px', marginBottom: '-10px' }}
          fullWidth
        >
          <MenuItem
            className="SelectBkg"
            value=""
            primaryText="Todos"
          />
          {
            this.props.empresas.map((item, i) => (
              <MenuItem
                className="SelectBkg"
                value={item._id}
                primaryText={item.nome}
                key={i}
              />

            ))
          }
        </SelectField>
      </div>
    )
  }

  renderDate() {
    if (this.state.filtro.statusAcao !== 'concluidas') return null

    const minDate = this.state.filtro.dataInicial
                  ? { minDate: this.state.filtro.dataInicial }
                  : {}
    const maxDateInicial = this.state.filtro.dataFinal
                          ? { maxDate: this.state.filtro.dataFinal }
                          : { maxDate: new Date() }
    const maxDateFinal = { maxDate: new Date() }
    return (
      <div>
        <div className={styles.date}>
          <DatePicker
            name="dataInicial"
            floatingLabelText="Data inicial"
            cancelLabel="Cancelar"
            hintStyle={style.black}
            textFieldStyle={{ width: '100%', zIndex: 2, cursor: 'pointer' }}
            DateTimeFormat={DateTimeFormat}
            onChange={this.handleChange}
            locale="pt-BR"
            {...maxDateInicial}
            firstDayOfWeek={0}
          />
          <SvgIcon
            icon="icon-icon-32"
            className={styles.iconDate}
          />
        </div>
        <div className={styles.date}>
          <DatePicker
            name="dataFinal"
            floatingLabelText="Data final"
            cancelLabel="Cancelar"
            hintStyle={style.black}
            textFieldStyle={{ width: '100%', zIndex: 2, cursor: 'pointer' }}
            DateTimeFormat={DateTimeFormat}
            onChange={this.handleChange}
            locale="pt-BR"
            {...minDate}
            {...maxDateFinal}
            firstDayOfWeek={0}
          />
          <SvgIcon
            icon="icon-icon-32"
            className={styles.iconDateLast}
          />
        </div>
      </div>
    )
  }

  render() {
    const stylesScroll = {
      overflowX: 'hidden',
      position: 'relative',
      display: 'flex',
      flexWrap: 'wrap',
      padding: '0 0 70px',
    }

    if (_FilterActionsAdmin.isMSIE()) stylesScroll.height = '80vh'
    return (
      <div style={{ height: '100%' }}>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <Scrollbars
            scrollStyle={{ overflowX: 'hidden' }}
            autoHide
            style={stylesScroll}
            renderTrackHorizontal={props => <div {...props} className="track-horizontal" style={{ display: 'none' }} />}
            renderThumbHorizontal={props => <div {...props} className="thumb-horizontal" style={{ display: 'none' }} />}
          >
            <TextField
              floatingLabelText="Procure uma ação"
              name="nomeNormalizado"
              underlineStyle={style.line}
              value={this.state.filtro.nomeNormalizado}
              onChange={this.handleChange}
              maxLength={100}
              inputStyle={{ marginLeft: '27px' }}
              hintStyle={{ marginLeft: '27px' }}
              style={{ marginTop: '-15px' }}
              floatingLabelStyle={{ marginLeft: '27px' }}
              floatingLabelFocusStyle={{ marginLeft: '0' }}
              floatingLabelShrinkStyle={{ marginLeft: '0' }}
              fullWidth
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />
            {this.adminFilters()}
            <div className={styles.containerCheckbox}>
              <p className="filterLeaderText">Status das ações</p>
              <RadioButtonGroup
                name="statusAcao"
                onChange={this.handleChangeStatus}
                valueSelected={this.state.filtro.statusAcao}
              >
                <RadioButton
                  value=""
                  label="Todas as ações"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="programadas"
                  label="Agendadas"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="andamento"
                  label="Em andamento"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="concluidas"
                  label="Já ocorreu"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
              </RadioButtonGroup>
              {this.renderDate()}
            </div>
            <p
              style={{ marginBottom: '-10px' }}
              className="filterLeaderText"
            >
              Tipo de Ação:
            </p>
            <SelectField
              name="tipoAcao"
              value={this.state.filtro.tipoAcao}
              onChange={this.onChangeTypeActions}
              labelStyle={style.menuStyleSelect}
              underlineStyle={style.line}
              style={{ marginBottom: '-15px' }}
              fullWidth
            >
              <MenuItem
                className="SelectBkg"
                value=""
                primaryText="Todos"
              />
              {
                this.props.tipoAcao.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item._id}
                    primaryText={item.nome}
                    key={i}
                  />

              ))
              }
            </SelectField>
            <TextField
              floatingLabelText="Qtde. Participantes"
              floatingLabelShrinkStyle={{ fontSize: '18px' }}
              name="totalParticipantes"
              underlineStyle={style.line}
              value={this.state.filtro.totalParticipantes}
              onChange={this.handleChangeNumeric}
              maxLength={5}
              fullWidth
              disabled={this.state.filtro.status === 'andamento'}
              // type="number"
            />

            <TextField
              floatingLabelText="Qtde. Beneficiados"
              floatingLabelShrinkStyle={{ fontSize: '18px' }}
              name="totalBeneficiados"
              underlineStyle={style.line}
              value={this.state.filtro.totalBeneficiados}
              onChange={this.handleChangeNumeric}
              maxLength={6}
              fullWidth
              disabled={this.state.filtro.status === 'andamento'}
              // type="number"
            />

            <TextField
              floatingLabelText="Valor Gasto"
              floatingLabelShrinkStyle={{ fontSize: '18px' }}
              name="valorGasto"
              underlineStyle={style.line}
              value={this.state.filtro.valorGasto}
              onChange={this.handleChangeMoney}
              onBlur={this.format}
              fullWidth
              disabled={this.state.filtro.status === 'andamento'}
            />

            <p className={styles.text}>Selecione um Local: </p>
            <SelectField
              name="local"
              value={this.state.filtro.local}
              onChange={this.onChangeLocal}
              labelStyle={style.menuStyle}
              underlineStyle={style.line}
              style={{ top: '-10px', marginBottom: '100px' }}
              fullWidth
            >
              <MenuItem
                className="SelectBkg"
                value=""
                primaryText="Todos"
              />
              {
                this.props.locais.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item._id}
                    primaryText={item.nome}
                    key={i}
                  />

              ))
              }
            </SelectField>
          </Scrollbars>
        </MuiThemeProvider>
        <div
          style={this.props.styleBnt}
          className={styles.btn}
        >
          <PillButton
            text="Filtrar"
            onClick={e => this.getFilterActions(e)}
            loader={this.props.loader}
          />
        </div>
      </div>
    )
  }
}

_FilterActionsAdmin.propTypes = {
  loader: PropTypes.bool,
  tipoAcao: PropTypes.array,
  cidades: PropTypes.array,
  empresas: PropTypes.array,
  locais: PropTypes.array,
  comite: PropTypes.string,
  getAcoes: PropTypes.func,
  handleFetchActionsTypes: PropTypes.func,
  handleCompanies: PropTypes.func.isRequired,
  setBase: PropTypes.func.isRequired,
  getCities: PropTypes.func.isRequired,
  handleFilter: PropTypes.func.isRequired,
  isAdmin: PropTypes.string,
  styleBnt: PropTypes.object,
}

_FilterActionsAdmin.defaultProps = {
  cidades: [],
  empresas: [],
  locais: [],
  styleBnt: {},
}

const mapStateToProps = state => ({
  actions: state.acoes.acoesLeader,
  tipoAcao: state.registerFormAction.tipoAcaoTemp,
  loader: state.filters.loader,
  cidades: state.list.cidades,
  empresas: state.list.empresas,
  locais: state.localsAutocomplete.base,
  isAdmin: state.loggedUser.adminLogin,
})

const mapActionsToProps = {
  getAcoes,
  handleFetchActionsTypes,
  handleCompanies,
  getCities,
  setBase,
}

const FilterActionsAdmin = connect(mapStateToProps, mapActionsToProps)(_FilterActionsAdmin)
export default FilterActionsAdmin
