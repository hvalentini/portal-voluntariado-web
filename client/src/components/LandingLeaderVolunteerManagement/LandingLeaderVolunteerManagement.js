import React from 'react'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import VolunteerManagement from '../../components/VolunteerManagement/VolunteerManagement.react'
import VolunteerFilters from '../../components/VolunteerFilters/VolunteerFilters.react'
import styles from './LandingLeaderVolunteerManagement.css'

class LandingLeaderVolunteerManagement extends React.PureComponent {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  componentDidMount() {
    LandingLeaderVolunteerManagement.scrollTo()
  }

  render() {
    return (
      <div className={styles.blocks}>
        <VolunteerFilters />
        <VolunteerManagement />
      </div>
    )
  }
}

export default LandingLeaderVolunteerManagement
