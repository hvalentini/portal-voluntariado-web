import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './LeaderAvatar.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import Avatar from '../Avatar/Avatar'
import { getWebStatus } from '../../actions/webTest'

const _LeaderAvatar = props => (
  <div className={styles.leaderAvatar}>
    <Avatar
      url={props.url}
      size={'another'}
    />
    <h2>{`${props.name.split(' ', 1)} ${props.name.split(' ').pop()}`}</h2>
    <div className={styles.divLogout}>
      <a
        href="/logout"
        onClick={(ev) => {
          ev.preventDefault()
          props.dispatch(getWebStatus('volunteer-logout'))
        }}
      >
        <p className={styles.logout}>Sair do portal</p>
      </a>
    </div>
    <div>
      <span className={styles.company}>
        <SvgIcon
          className={styles.firstIcon}
          icon="icon-icon-13"
        />
        {`${props.companyName} ${!props.companyUnit
                                ? ''
                                : `- ${props.companyUnit}`
        }`}
      </span>
      <span className={styles.category}>
        <SvgIcon
          className={styles.secondIcon}
          icon="icon-icon-3"
        />
        {props.category}
      </span>
    </div>
  </div>
)

_LeaderAvatar.propTypes = {
  url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  companyName: PropTypes.string.isRequired,
  companyUnit: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
}

_LeaderAvatar.defaultProps = {
  companyUnit: '',
}

const LeaderAvatar = connect(null, null)(_LeaderAvatar)

export default LeaderAvatar
