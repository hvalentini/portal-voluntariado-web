import React from 'react'
import PropTypes from 'prop-types'
import currencyFormatter from 'currency-formatter'
import styles from './AvailableBalance.css'

const AvailableBalance = (props) => {
  let saldoAtual = currencyFormatter.format(props.saldoAtual, { code: 'BRL' })
  saldoAtual = (props.saldoAtual < 0) ? saldoAtual.substring(4) : saldoAtual.substring(3)
  const symbol = (props.saldoAtual < 0) ? '-' : ''
  const style = (props.saldoAtual < 0) ? { worn: { color: '#FFB137' } } : {}

  return (
    <div className="Grid">
      <div className={styles.container}>
        <h3 className={styles.texto}>Saldo para ações</h3>
        <div className="Grid-cell">
          <div className="Grid Grid-withGutter Grid--alignMiddle" style={style.worn}>
            <p className={styles.cifrão}>{symbol}R$ </p>
            <p className={styles.saldo}>{saldoAtual}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

AvailableBalance.propTypes = {
  saldoAtual: PropTypes.number,
}

export default AvailableBalance
