import React from 'react'
import PropTypes from 'prop-types'
import Badge from 'material-ui/Badge'
import { Tabs, Tab } from 'material-ui/Tabs'
import styles from './LeaderTabs.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import Notification from '../Notification/Notification.react'
import DownloadsList from '../../components/DownloadsList/DownloadsList'
import Certificate from '../../components/Certificate/Certificate.react'

const style = {}


class LeaderTabs extends React.Component {
  constructor(props) {
    super(props)
    this.cleanNotifications = this.cleanNotifications.bind(this)
    this.readNotificationsFunc = this.readNotificationsFunc.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.readNotifications !== this.props.readNotifications) {
      this.cleanNotifications()
    }
  }

  cleanNotifications() {
    if (!this.props.readNotifications) {
      document.getElementsByClassName('pv-LeaderTabs-badge')[0].children[1].style.display = 'none'
    }
  }

  readNotificationsFunc() {
    this.props.readNotificationsFunction({
      email: this.props.loggedUser.email,
      nome: this.props.loggedUser.nome,
    })
  }

  render() {
    const numberNotifications = this.props.numberNotifications > 99
    ? '+99'
    : this.props.numberNotifications
    const badgeIndexof = this.props.numberNotifications ? style.badge : style.badgeNone
    return (
      <div className={styles.container}>
        <Tabs
          tabItemContainerStyle={{
            backgroundColor: '#01BEE8',
            borderBottom: '1px solid #49CFEC',
          }}
        >
          <Tab
            icon={<SvgIcon
              className={styles.icons}
              icon="icon-icon-40"
            />}
            label="Perfil"
            className={styles.tabs}
          >
            {this.props.children}
          </Tab>
          <Tab
            icon={<SvgIcon
              className={styles.icons}
              icon="icon-icon-14"
            />}
            label="Certificados"
            className={styles.tabs}
          >
            <Certificate />
          </Tab>
          <Tab
            icon={<SvgIcon
              className={styles.icons}
              icon="icon-icon-15"
            />}
            label="Downloads"
            className={styles.tabs}
          >
            <DownloadsList />
          </Tab>
          <Tab
            icon={<Badge
              badgeContent={numberNotifications}
              badgeStyle={badgeIndexof}
              className={styles.badge}
            >
              <SvgIcon
                className={styles.icons}
                icon="icon-icon-16"
              />
            </Badge>}
            label="Notificações"
            className={styles.tabs}
            onActive={this.readNotificationsFunc}
          >
            <Notification loggedUser={this.props.loggedUser} />
          </Tab>
        </Tabs>
      </div>
    )
  }
}

LeaderTabs.propTypes = {
  numberNotifications: PropTypes.number.isRequired,
  loggedUser: PropTypes.object,
  readNotifications: PropTypes.bool,
  readNotificationsFunction: PropTypes.func,
}

style.badgeTeste = {
  padding: 0,
}

style.label = {
  fontSize: '13px',
}
style.badge = {
  top: '-20%',
  right: '45%',
  transform: 'translate(55%, -20%)',
  width: 25,
  height: 25,
  backgroundColor: 'red',
  border: 'none',
}

style.badgeNone = {
  display: 'none',
}

export default LeaderTabs
