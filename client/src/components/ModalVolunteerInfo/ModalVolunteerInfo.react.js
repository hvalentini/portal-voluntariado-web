import React from 'react'
import get from 'lodash.get'
import styles from './ModalVolunteerInfo.css'
import Avatar from '../Avatar/Avatar'

const ModalVolunteerInfo = (props) => {
  const name = get(props, 'info.nome')
  const email = get(props, 'info.email')
  const cargo = get(props, 'info.cargoNome')
  const cr = get(props, 'info.centroResultadoNome')
  const shirtSize = get(props, 'info.tamanhoCamiseta')
  const chief = get(props, 'info.emailSuperior')

  const professionalPhone = get(props, 'info.telefoneProfissional')
        ? get(props, 'info.telefoneProfissional') : ''

  const personalPhone = get(props, 'info.telefonePessoal')
        ? get(props, 'info.telefonePessoal') : ''

  const showProfessionalPhone = professionalPhone ? '' : 'u-hidden'

  const showPersonalPhone = personalPhone ? '' : 'u-hidden'

  const showChief = chief ? '' : 'u-hidden'
  const showCR = cr ? '' : 'u-hidden'

  const avatar = get(props, 'info.urlAvatar')

  return (
    <div className={styles.boxInfo}>
      <div className="Grid Grid--withGutter">
        <div className="Grid-cell u-size3of12">
          <div className={styles.avatar}>
            <Avatar
              size="exMedium"
              url={avatar}
            />
          </div>
        </div>

        <div className="Grid-cell u-size9of12">
          <p className={styles.name}>{name}</p>
          <p className={styles.email}>Email: {email}</p>
          <p className={styles.cargo}>{cargo}</p>
          <p className={`${styles.cr} ${showCR}`}>CR: {cr}</p>
          <p className={styles.cr}>Camiseta: {shirtSize}</p>
        </div>
        <div className="Grid-cell">
          <div className="Grid">
            <div className={`${showPersonalPhone} Grid-cell u-size3of12 u-textLeft`}>
              <p className={styles.phoneLabel}>Celular pessoal</p>
              <span className={styles.phone}>{personalPhone}</span>
            </div>
            <div className={`${showProfessionalPhone} Grid-cell u-size3of12 u-textLeft`}>
              <p className={styles.phoneLabel}>Telefone profissional</p>
              <span className={styles.phone}>{professionalPhone}</span>
            </div>
            <div className={`${showChief} Grid-cell u-textLeft`}>
              <p className={styles.chief}>Email Superior imediato: {chief}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ModalVolunteerInfo
