import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './LandingFooter.css'
import PillButtonKeyLimePie from '../PillButtonKeyLimePie/PillButtonKeyLimePie'

class LandingFooter extends React.Component {

  constructor(props) {
    super(props)

    this.linkToRegister = this.linkToRegister.bind(this)
    this.handleButton = this.handleButton.bind(this)
  }

  linkToRegister() {
    this.props.history.push('/cadastro/passo-1')
  }

  handleButton() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      0,
      1000,
      this.linkToRegister,
    )
  }

  render() {
    return (
      <div className={`Grid Grid--alignMiddle ${styles.landingFooter}`}>
        <div className="Grid-cell u-md-size2of3 u-lg-size2of3 u-sm-size3of3">
          <h2>Interessado em ajudar?</h2>
          <p>Seja o primeiro do seu setor a entrar no programa de voluntariado da Algar.</p>
        </div>
        <div className="Grid-cell u-md-size1of3 u-lg-size1of3 u-sm-size3of3">
          <PillButtonKeyLimePie
            text="Quero ser voluntário"
            color="#4a4849"
            onClick={this.handleButton}
            style={{
              padding: '18px 37px',
              fontSize: '26px',
              lineHeight: '30px',
              marginRight: 0,
              color: '#484848',
            }}
          />
        </div>
      </div>
    )
  }
}

LandingFooter.propTypes = {
  history: PropTypes.object,
}

export default withRouter(LandingFooter)
