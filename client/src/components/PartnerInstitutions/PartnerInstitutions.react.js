import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import CircularProgress from 'material-ui/CircularProgress'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import styles from './PartnerInstitutions.css'
import PartnerInstitutionsHeader from '../PartnerInstitutionsHeader/PartnerInstitutionsHeader.react'
import PartnerInstitutionsCard from '../PartnerInstitutionsCard/PartnerInstitutionsCard.react'
import { handleChange, editPhone, addLocal } from '../../actions/newLocal'
import { openModalEdit, closeModalEdit, loadPartnerInstitutions, closeDialog } from '../../actions/partners'

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  fontFamily: 'Chantilly-Serial',
})

class _PartnerInstitutions extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  componentWillMount() {
    this.props.loadPartnerInstitutions({}, { nomeNormalizado: 1 })
  }

  componentDidMount() {
    setTimeout(() => {
      _PartnerInstitutions.scrollTo()
    }, 0)
  }

  renderInstitutionsCards() {
    let card
    if (this.props.partners.loader) {
      return (
        <div className="Grid Grid--withGutter">
          <CircularProgress
            style={{ margin: '10% auto' }}
          />
        </div>
      )
    }
    if (this.props.partners.list.length === 0) {
      card = <div className={styles.notFound}>Nenhuma instituição voluntária encontrada</div>
    } else {
      card = this.props.partners.list.map((item, i) => (
        <PartnerInstitutionsCard
          key={i}
          partnerInfo={item}
        />
      ))
    }
    return (
      <div>
        {card}
      </div>
    )
  }

  render() {
    return (
      <div className={`${styles.contentLeader} contentLeader contentMedia page-break`}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <PartnerInstitutionsHeader />
            <div>
              {this.renderInstitutionsCards()}
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

_PartnerInstitutions.propTypes = {
  partners: PropTypes.object,
  loadPartnerInstitutions: PropTypes.func,
}

const mapStateToProps = state => ({
  openDialog: state.newLocal.openDialog,
  partners: state.partners,
  serviceMessage: state.newLocal.serviceMessage,
})

const mapActionsToProps = {
  handleChange,
  editPhone,
  addLocal,
  loadPartnerInstitutions,
  closeModalEdit,
  closeDialog,
  openModalEdit,
}

const PartnerInstitutions = connect(mapStateToProps, mapActionsToProps)(_PartnerInstitutions)

export default PartnerInstitutions
