import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { TextField, SelectField, MenuItem } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { connect } from 'react-redux'
import muiTheme from '../../muiTheme'
import styles from './FormCommittee.css'
import BackButton from '../../components/BackButton/BackButton'
import PillButton from '../../components/PillButton/PillButton'
import { selectCompany, selectCity } from '../../actions/committeeSelects'
import {
  handleChangeCommittee,
  handleAddCommittee,
  handleSaveEditCommittee,
  clearCommittee,
  deleteCommittee,
  selectCityCommitee,
  searchCommittee,
} from '../../actions/comites'
import { setModal } from '../../actions/modal'
import LeaderAutocomplete from '../LeaderAutocomplete/LeaderAutocomplete.react'
import moneyMask from '../../utils/maskMoney'
import moneyToFloat from '../../utils/moneyToFloat'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import { handleCompanies } from '../../actions/empresas'
import { getCities } from '../../actions/cidades'

class _FormCommittee extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      errorEmpresa: '',
      errorCidade: '',
      errorVerbaInicial: '',
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeCompany = this.handleChangeCompany.bind(this)
    this.handleChangeCity = this.handleChangeCity.bind(this)
    this.handleAddCommittee = this.handleAddCommittee.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleChangeMoney = this.handleChangeMoney.bind(this)
    this.handleDeleteCommittee = this.handleDeleteCommittee.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillMount() {
    this.props.handleCompanies()
    this.props.getCities()
    if (this.props.match.params.id) {
      this.props.searchCommittee(this.props.match.params.id)
    }
  }

  handleChange(e) {
    const obj = { ...this.props.comiteEdit }
    obj[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    this.props.handleChangeCommittee(obj)
  }

  handleChangeMoney(e) {
    // CONVERTER EM INTEIRO PARA SALVAR
    const valor = moneyToFloat(e.target.value)
    const obj = { ...this.props.comiteEdit }
    obj[e.target.name] = valor
    this.props.handleChangeCommittee(obj)
  }

  handleBlur(e) {
    if (this.state.errorVerbaInicial && e.target.value) {
      this.setState({ errorVerbaInicial: '' })
    }
  }

  handleChangeCompany(x, i) {
    const value = this.props.empresas[i]._id
    const obj = { ...this.props.comiteEdit }
    obj.empresa = value
    if (obj.empresa && this.state.errorEmpresa) {
      this.setState({ errorEmpresa: '' })
    }
    this.props.handleChangeCommittee(obj)
  }

  handleChangeCity(x, i) {
    const payload = this.props.cidades[i]
    if (payload && this.state.errorCidade) {
      this.setState({ errorCidade: '' })
    }
    this.props.selectCityCommitee(payload)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina)
  }

  handleAddCommittee(e) {
    e.preventDefault()

    const verbaExtra = this.props.comiteEdit.verbaExtra
                      ? moneyToFloat(this.props.comiteEdit.verbaExtra) : 0

    const verbaInicial = this.props.comiteEdit.verbaInicial
                        ? moneyToFloat(this.props.comiteEdit.verbaInicial)
                        : null

    let errorVerbaInicial = (!verbaInicial)
                              ? 'Verba Inicial é obrigatória.' : ''

    errorVerbaInicial = this.props.comiteEdit.verbaInicial === '0.00'
                      ? 'Favor informar verba inicial maior que zero.'
                      : errorVerbaInicial

    const errorEmpresa = !this.props.comiteEdit.empresa
                         ? 'Selecione uma empresa.' : ''

    const errorCidade = !this.props.comiteEdit.cidade
                        ? 'Selecione uma cidade.' : ''

    if (errorVerbaInicial || errorEmpresa || errorCidade) {
      return this.setState({ errorVerbaInicial, errorEmpresa, errorCidade })
    }

    if (this.props.match.params.id) {
      const liderSocial = this.props.comiteEdit.liderSocial || {}

      const newCommittee = {
        _id: this.props.comiteEdit._id,
        unidade: this.props.comiteEdit.unidade,
        verbaExtra,
        verbaInicial,
        cidade: this.props.comiteEdit.cidade,
        empresa: this.props.comiteEdit.empresa,
        liderSocial,
      }
      return this.props.handleSaveEditCommittee(newCommittee)
    }

    const newCommittee = {
      unidade: this.props.comiteEdit.unidade,
      verbaExtra,
      verbaInicial,
      cidade: this.props.comiteEdit.cidade,
      empresa: this.props.comiteEdit.empresa,
    }
    return this.props.handleAddCommittee(newCommittee)
  }

  // AÇÕES DA MODAL
  handleDeleteCommittee() {
    this.props.deleteCommittee(this.props.match.params.id)
  }

  handleLeader() {
    if (this.props.match.params.id && this.props.comiteEdit._id) {
      return (
        <LeaderAutocomplete />
      )
    }

    return null
  }

  renderBtnDelete() {
    if (!this.props.match.params.id) { return null }

    const msg = 'Você tem certeza que deseja excluir o comitê?'

    return (
      <div className={styles.delete}>
        <PillButton
          text="excluir"
          negativeColors
          onClick={() =>
            this.handleOpenModal(
              'delete', // TIPO DE AÇÃO - PODE SER DELETE, WARNING, OU SUCCESS
              'comites', // ENTIDADE RELACIONADA NO BANCO
              msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM NAO VENHA DO BANCO ENVIAR
              this.handleDeleteCommittee, // AÇÃO PRINCIPAL
              '/admin/comites', // ROTA DA PAGINA PRINCIPAL
            )
          }
        />
      </div>
    )
  }

  renderBtnRegister() {
    const textBtn = this.props.match.params.id ? 'ALTERAR' : 'CADASTRAR'
    let ml = ''
    if (this.props.match.params.id) {
      ml = styles.ml
    }
    return (
      <div className={`${styles.register} ${ml}`}>
        <PillButton
          text={textBtn}
          loader={this.props.loader}
          onClick={e => this.handleAddCommittee(e)}
        />
      </div>
    )
  }

  render() {
    if ((this.props.match.params.id && !this.props.comiteEdit.cidade)) {
      return (
        <LoaderAdmin />
      )
    }

    const showLeader = !this.props.match.params.id ? 'u-hidden' : ''
    const company = this.props.comiteEdit ? this.props.comiteEdit.empresa : ''
    const city = this.props.comiteEdit ? this.props.comiteEdit.cidade : ''
    const und = this.props.comiteEdit ? this.props.comiteEdit.unidade : null
    let buttons = 'u-textRight'
    if (!this.props.match.params.id) { buttons = 'u-textCenter' }

    // MONEYMASK FAZER A MASCARA
    const verbaExtra = this.props.comiteEdit && this.props.comiteEdit.verbaExtra ? moneyMask(this.props.comiteEdit.verbaExtra) : ''
    const verbaInicial = this.props.comiteEdit && this.props.comiteEdit.verbaInicial ? moneyMask(this.props.comiteEdit.verbaInicial) : ''

    return (
      <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
        <div>
          <div className="Grid">
            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <SelectField
                name="empresa"
                floatingLabelText="Empresa"
                value={company}
                onChange={this.handleChangeCompany}
                errorText={this.state.errorEmpresa}
                fullWidth
              >
                {
                  this.props.empresas.map((item, i) => {
                    if (!item) return null
                    return (
                      <MenuItem
                        className="SelectBkg"
                        value={item._id}
                        primaryText={item.nome}
                        key={i}
                      />
                    )
                  })
                }
              </SelectField>
            </div>
            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <SelectField
                name="cidade"
                floatingLabelText="Cidade/UF"
                value={city}
                onChange={this.handleChangeCity}
                fullWidth
                errorText={this.state.errorCidade}
              >
                {
                  this.props.cidades.map((item, i) => (
                    <MenuItem
                      className="SelectBkg"
                      value={item._id}
                      primaryText={`${item.nome} / ${item.uf}`}
                      key={i}
                    />
                  ))
                }
              </SelectField>
            </div>

            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <TextField
                floatingLabelText="Unidade"
                name="unidade"
                value={und}
                onChange={this.handleChange}
                type="text"
                fullWidth
              />
            </div>

            {/* campo autoComplete LIDER quando for editar um comite */}
            <div className={`Grid-cell u-size1of1 u-textLeft u-inlineBlock ${showLeader}`}>
              {this.handleLeader()}
            </div>

            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <TextField
                floatingLabelText="Verba Inicial"
                name="verbaInicial"
                value={verbaInicial}
                errorText={this.state.errorVerbaInicial}
                onChange={this.handleChangeMoney}
                onBlur={this.handleBlur}
                maxLength={13}
                type="text"
                fullWidth
              />
            </div>

            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <TextField
                floatingLabelText="Verba Extra(Opcional)"
                name="verbaExtra"
                value={verbaExtra}
                onChange={this.handleChangeMoney}
                maxLength={13}
                type="text"
                fullWidth
              />
            </div>

          </div>
          <div className={`${styles.divBtn} ${buttons}`}>
            <div className={styles.backButton}>
              <BackButton history={this.props.history} />
            </div>
            {this.renderBtnDelete()}
            {this.renderBtnRegister()}
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

_FormCommittee.propTypes = {
  match: PropTypes.object,
  comiteEdit: PropTypes.object,
  history: PropTypes.object,
  handleChangeCommittee: PropTypes.func,
  setModal: PropTypes.func,
  handleSaveEditCommittee: PropTypes.func,
  handleAddCommittee: PropTypes.func,
  deleteCommittee: PropTypes.func,
  cidades: PropTypes.array,
  empresas: PropTypes.array,
  selectCityCommitee: PropTypes.func,
  searchCommittee: PropTypes.func,
  getCities: PropTypes.func,
  handleCompanies: PropTypes.func,
  loader: PropTypes.bool,
}

const mapStateToProps = state => ({
  deleteSuccess: state.ui.deleteSuccess,
  comiteEdit: state.comites,
  cidades: state.list.cidades,
  empresas: state.list.empresas,
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  selectCompany,
  selectCity,
  setModal,
  handleChangeCommittee,
  handleAddCommittee,
  handleSaveEditCommittee,
  clearCommittee,
  deleteCommittee,
  handleCompanies,
  getCities,
  selectCityCommitee,
  searchCommittee,
}

const FormCommittee = connect(mapStateToProps, mapActionsToProps)(_FormCommittee)
export default withRouter(FormCommittee)
