import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TextField, RadioButton, RadioButtonGroup } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import PillButton from '../PillButton/PillButton'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { updatePartnersFilter, filterPartners, cleanFilter } from '../../actions/filters'
import styles from './FilterInstitutions.css'
import muiTheme from '../../muiTheme'

const style = {}

class _FilterInstitutions extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      btnBottom: 0,
    }

    this.setSearchTerm = this.setSearchTerm.bind(this)
    this.changeRadio = this.changeRadio.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
  }

  componentWillUnmount() {
    this.props.cleanFilter()
  }

  setSearchTerm(ev) {
    const obj = {}
    obj[ev.target.name] = ev.target.value.replace(/^\s+/gm, '')
    this.props.updatePartnersFilter(obj) // nome
  }

  changeRadio(ev) {
    const obj = {}
    obj[ev.target.name] = ev.target.value
    this.props.updatePartnersFilter(obj) // escola
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  render() {
    const partnersFilter = this.props.filters.partners
    return (
      <div>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <div
            className={styles.wrapperFilter}
            style={{ maxHeight: `${window.innerHeight - 230}px` }}
            onScroll={this.adjustButtonPlacing}
          >
            <TextField
              floatingLabelText="Nome da instituição"
              name="nome"
              underlineStyle={style.line}
              onChange={ev => this.setSearchTerm(ev)}
              maxLength={100}
              inputStyle={{ marginLeft: '27px' }}
              hintStyle={{ marginLeft: '27px' }}
              style={{ marginTop: '-15px' }}
              floatingLabelStyle={{ marginLeft: '27px' }}
              floatingLabelFocusStyle={{ marginLeft: '0' }}
              floatingLabelShrinkStyle={{ marginLeft: '0' }}
              fullWidth
              autoComplete="off"
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />

            <div className={styles.containerCheckbox}>
              <p className={styles.text}>Tipos de instituições</p>
              <RadioButtonGroup
                name="escola"
                valueSelected={partnersFilter.escola}
                onChange={ev => this.changeRadio(ev)}
              >
                <RadioButton
                  value="null"
                  label="Todas"
                  className={`${style.radioButton} u-inlineBlock`}
                />
                <RadioButton
                  value="true"
                  label="Instituições Escolares"
                  className={`${style.radioButton} u-inlineBlock`}
                />
                <RadioButton
                  value="false"
                  label="Instituições não-escolares"
                  className={`${style.radioButton} u-inlineBlock`}
                />
              </RadioButtonGroup>
            </div>

            <div
              style={{ padding: '15px 0' }}
              className={styles.btn}
            >
              <PillButton
                text="Filtrar"
                onClick={() => this.props.filterPartners()}
                loader={this.props.newLocal.loader}
              />
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterInstitutions.propTypes = {
  filters: PropTypes.object,
  newLocal: PropTypes.object,
  updatePartnersFilter: PropTypes.func,
  filterPartners: PropTypes.func,
  cleanFilter: PropTypes.func,
}

const mapStateToProps = state => ({
  filters: state.filters,
  newLocal: state.newLocal,
})

const mapActionsToProps = {
  updatePartnersFilter,
  filterPartners,
  cleanFilter,
}

const FilterInstitutions = connect(mapStateToProps, mapActionsToProps)(_FilterInstitutions)
export default FilterInstitutions
