import React from 'react'
import PropTypes from 'prop-types'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import NewAction from '../../components/NewAction/NewAction.react'

class LandingLeaderNewAction extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  componentDidMount() {
    LandingLeaderNewAction.scrollTo()
  }

  render() {
    return (
      <NewAction id={this.props.match.params.id} />
    )
  }
}

LandingLeaderNewAction.propTypes = {
  match: PropTypes.object,
}

export default LandingLeaderNewAction
