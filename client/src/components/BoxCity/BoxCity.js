import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './BoxCity.css'
import { deleteCity } from '../../actions/cidades'
import { setModal } from '../../actions/modal'

class _BoxCity extends React.Component {

  constructor(props) {
    super(props)
    this.handleDeleteCity = this.handleDeleteCity.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleCities = this.handleCities.bind(this)
  }

  handleDeleteCity(id) {
    this.props.deleteCity(id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina, id) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina, id)
  }

  handleCities() {
    let key = ''
    return this.props.cidades.map((item) => {
      key = `${key}${item._id}`
      const msg = `Você tem certeza que deseja excluir a cidade ${item.nome}/${item.uf}?`
      return (
        <div className={`${styles.root} print`} key={item._id}>
          <span className={styles.nomeCidade}>{item.nome}</span>
          <span className={styles.ufCidade}>/{item.uf}</span>
          <div className={styles.divIcons}>
            <Link to={`/admin/cidades/editar/${item._id}`}>
              <SvgIcon
                icon="icon-icon-58"
                className={`${styles.icon} noPrint`}
                key={`${key}edit`}
              />
            </Link>
            <button
              onClick={() => this.handleOpenModal(
                'delete', // TIPO DE AÇÃO
                'cidades', // ENTIDADE RELACIONADA NO BANCO
                msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM VENHA DO BANCO ENVIAR NULL
                this.handleDeleteCity, // AÇÃO PRINCIPAL
                '/admin/cidades', // ROTA DA PAGINA PRINCIPAL
                item._id,
              )}
            >
              <SvgIcon
                icon="icon-icon-57"
                className={`${styles.icon} noPrint`}
                key={`${key}del`}
              />
            </button>
          </div>
          <hr className={styles.separator} />
        </div>
      )
    })
  }

  render() {
    return (
      <div>
        {this.handleCities()}
      </div>
    )
  }
}

_BoxCity.propTypes = {
  deleteCity: PropTypes.func,
  setModal: PropTypes.func,
  cidades: PropTypes.array,
  filter: PropTypes.object,
}

const mapStateToProps = state => ({
  cidades: state.list.cidades,
  filter: state.filtersCity,
})

const mapActionsToProps = {
  deleteCity,
  setModal,
}

const BoxCity = connect(mapStateToProps, mapActionsToProps)(_BoxCity)
export default BoxCity
