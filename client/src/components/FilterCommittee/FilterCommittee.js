import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  TextField,
  SelectField,
  MenuItem,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Scrollbars } from 'react-custom-scrollbars'
import noAccents from 'remove-accents'
import muiTheme from '../../muiTheme'
import { getCities } from '../../actions/cidades'
import { getCommittees } from '../../actions/comites'
import { handleCompanies } from '../../actions/empresas'
import PillButton from '../PillButton/PillButton'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './FilterCommittee.css'
import maskMoney from '../../utils/maskMoney'
import moneyToFloat from '../../utils/moneyToFloat'
import numericMask from '../../utils/numericMask'

const style = {}

class _FilterCommittee extends React.Component {
  static isMSIE() {
    let isMSIE = false

    if (navigator.appName === 'Microsoft Internet Explorer') {
      const ua = navigator.userAgent
      const re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})')

      if (re.exec(ua) !== null) {
        isMSIE = true
      }
    } else if (navigator.appName === 'Netscape') {
      if (navigator.appVersion.indexOf('Trident') > -1) isMSIE = true
      else if (navigator.appVersion.indexOf('Edge') > -1) isMSIE = true
    }

    return isMSIE
  }

  constructor(props) {
    super(props)

    this.state = {
      filtro: {
        unidade: '',
        unidadeNormalizada: '',
        cidade: '',
        empresa: '',
        voluntarios: '',
        beneficiadas: '',
        acoes: '',
        verbaInicial: '',
        verbaExtra: '',
        saldoAtual: '',
      },
      btnBottom: 0,
    }

    this.handleChange = this.handleChange.bind(this)
    this.onChangeCidade = this.onChangeCidade.bind(this)
    this.onChangeEmpresa = this.onChangeEmpresa.bind(this)
    this.handleChangeMoney = this.handleChangeMoney.bind(this)
    this.handleChangeNumeric = this.handleChangeNumeric.bind(this)
    this.filterCommittees = this.filterCommittees.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
  }

  componentWillMount() {
    this.props.getCities()
    this.props.handleCompanies()
  }

  onChangeCidade(ev, index, value) {
    const filtro = { ...this.state.filtro, cidade: value }
    this.setState({ ...this.state, filtro })
  }

  onChangeEmpresa(ev, index, value) {
    const filtro = { ...this.state.filtro, empresa: value }
    this.setState({ ...this.state, filtro })
  }

  handleChange(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    this.setState({ ...this.state, filtro })
  }

  handleChangeMoney(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value === '' ? '' : maskMoney(moneyToFloat(e.target.value))
    this.setState({ ...this.state, filtro })
  }

  handleChangeNumeric(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value === '' ? '' : numericMask(e.target.value)
    this.setState({ ...this.state, filtro })
  }

  filterCommittees() {
    const filtro = Object.assign({}, this.state.filtro)

    Object.keys(filtro).forEach((key) => {
      if (!filtro[key] || filtro[key] === '') delete filtro[key]
    })

    filtro['empresas._id'] = filtro.empresa

    filtro.voluntarios = filtro.voluntarios ? Number(filtro.voluntarios) : null
    filtro['beneficiadas.sum'] = filtro.beneficiadas ? Number(filtro.beneficiadas) : null
    filtro.acoes = filtro.acoes ? Number(filtro.acoes) : null
    filtro.verbaInicial = filtro.verbaInicial ? Number(moneyToFloat(filtro.verbaInicial)) : null
    filtro.verbaExtra = filtro.verbaExtra ? Number(moneyToFloat(filtro.verbaExtra)) : null
    filtro.saldoAtual = filtro.saldoAtual ? Number(moneyToFloat(filtro.saldoAtual)) : null
    filtro.unidadeNormalizada = filtro.unidade ? noAccents(filtro.unidade).toLowerCase() : null

    delete filtro.beneficiadas
    delete filtro.empresa
    delete filtro.unidade
    if (!filtro.unidadeNormalizada) {
      delete filtro.unidadeNormalizada
    }

    this.props.getCommittees({ filtro })
    this.props.handleFilter(filtro)
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  render() {
    const stylesScroll = {
      overflowX: 'hidden',
      position: 'relative',
      display: 'flex',
      flexWrap: 'wrap',
      padding: '0 0 70px',
    }

    if (_FilterCommittee.isMSIE()) stylesScroll.height = '80vh'

    return (
      <div style={{ height: '100%' }}>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <Scrollbars
            id="filterInternal"
            autoHide
            scrollStyle={{ overflowX: 'hidden' }}
            style={stylesScroll}
            renderTrackHorizontal={props => <div {...props} className="track-horizontal" style={{ display: 'none' }} />}
            renderThumbHorizontal={props => <div {...props} className="thumb-horizontal" style={{ display: 'none' }} />}
          >
            <TextField
              floatingLabelText="Nome da Unidade"
              name="unidade"
              underlineStyle={style.line}
              value={this.state.filtro.unidade}
              onChange={this.handleChange}
              maxLength={100}
              inputStyle={{ marginLeft: '27px' }}
              hintStyle={{ marginLeft: '27px' }}
              style={{ marginTop: '-15px' }}
              floatingLabelStyle={{ marginLeft: '27px' }}
              floatingLabelFocusStyle={{ marginLeft: '0' }}
              floatingLabelShrinkStyle={{ marginLeft: '0' }}
              fullWidth
              autoComplete="off"
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />

            <p className={styles.text}>Selecine uma cidade: </p>
            <SelectField
              name="cidade"
              value={this.state.filtro.cidade}
              onChange={this.onChangeCidade}
              labelStyle={style.menuStyle}
              underlineStyle={style.line}
              style={{ top: '-10px', marginBottom: '-10px' }}
              fullWidth
            >
              <MenuItem
                className="SelectBkg"
                value=""
                primaryText="Todos"
              />
              {
                this.props.cidades.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item._id}
                    primaryText={`${item.nome}/${item.uf}`}
                    key={i}
                  />

               ))
              }
            </SelectField>

            <p className={styles.text}>Selecione uma empresa: </p>
            <SelectField
              value={this.state.filtro.empresa}
              onChange={this.onChangeEmpresa}
              labelStyle={style.menuStyle}
              underlineStyle={style.line}
              style={{ top: '-10px', marginBottom: '-10px' }}
              fullWidth
            >
              <MenuItem
                className="SelectBkg"
                value=""
                primaryText="Todos"
              />
              {
                this.props.empresas.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item._id}
                    primaryText={item.nome}
                    key={i}
                  />

               ))
              }
            </SelectField>

            <TextField
              floatingLabelText="Número de Voluntários"
              name="voluntarios"
              underlineStyle={style.line}
              value={this.state.filtro.voluntarios}
              onChange={this.handleChangeNumeric}
              maxLength={100}
              fullWidth
              autoComplete="off"
            />


            <TextField
              floatingLabelText="Número de Beneficiados"
              name="beneficiadas"
              underlineStyle={style.line}
              value={this.state.filtro.beneficiadas}
              onChange={this.handleChangeNumeric}
              maxLength={100}
              fullWidth
              autoComplete="off"
            />

            <TextField
              floatingLabelText="Número de Ações"
              name="acoes"
              underlineStyle={style.line}
              value={this.state.filtro.acoes}
              onChange={this.handleChangeNumeric}
              fullWidth
              autoComplete="off"
            />

            <TextField
              floatingLabelText="Verba Inicial"
              name="verbaInicial"
              underlineStyle={style.line}
              value={this.state.filtro.verbaInicial}
              onChange={this.handleChangeMoney}
              fullWidth
              autoComplete="off"
            />

            <TextField
              floatingLabelText="Verba Extra"
              name="verbaExtra"
              underlineStyle={style.line}
              value={this.state.filtro.verbaExtra}
              onChange={this.handleChangeMoney}
              fullWidth
              autoComplete="off"
            />

            <TextField
              floatingLabelText="Saldo Atual"
              name="saldoAtual"
              underlineStyle={style.line}
              value={this.state.filtro.saldoAtual}
              onChange={this.handleChangeMoney}
              fullWidth
              style={{ marginBottom: '100px' }}
              autoComplete="off"
            />
          </Scrollbars>
        </MuiThemeProvider>
        <div
          className={styles.btn}
        >
          <PillButton
            text="Filtrar"
            onClick={this.filterCommittees}
            loader={this.props.loader}
          />
        </div>
      </div>
    )
  }
}

style.menuStyle = {
  marginBottom: '-12px',
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterCommittee.propTypes = {
  getCities: PropTypes.func.isRequired,
  handleCompanies: PropTypes.func.isRequired,
  loader: PropTypes.bool,
  cidades: PropTypes.array,
  empresas: PropTypes.array,
  handleFilter: PropTypes.func.isRequired,
  getCommittees: PropTypes.func,
}

_FilterCommittee.defaultProps = {
  cidades: [],
  empresas: [],
}

const mapStateToProps = state => ({
  cidades: state.list.cidades,
  empresas: state.list.empresas,
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  getCities,
  getCommittees,
  handleCompanies,
}

const FilterCommittee = connect(mapStateToProps, mapActionsToProps)(_FilterCommittee)

export default FilterCommittee
