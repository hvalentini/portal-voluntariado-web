import React from 'react'
import PropTypes from 'prop-types'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { connect } from 'react-redux'
import styles from './VolunteerManagementHeader.css'
import { getVolunterOrder } from '../../actions/voluntario'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const style = {}

const order = [
  {
    name: 'Nome de voluntário (A-Z)',
    value: 'nameasc',
  },
  {
    name: 'Nome de voluntário (Z-A)',
    value: 'namedesc',
  },
  {
    name: 'Participou em ação (0-9)',
    value: 'participationasc',
  },
  {
    name: 'Participou em ação (9-0)',
    value: 'participationdesc',
  },
]

const selected = 'nameasc'

class _VolunteerManagementHeader extends React.Component {
  constructor(props) {
    super(props)
    this.onOderChange = this.onOderChange.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  onOderChange(event, index, value) {
    let typeOrder = ''
    if (value === 'nameasc' || value === 'namedesc') { typeOrder = 'props.info.nomeNormalizado' }
    if (value === 'participationasc' || value === 'participationdesc') { typeOrder = 'props.info.quantidadeDeParticipacoes' }
    this.props.getVolunterOrder({ orderOfVolunteers: value, typeOrder })
  }

  onPrint() {
    if (this.props.voluntario.volunteers.length > 0
      || this.props.voluntario.filterVolunteers.length > 0
      || this.props.voluntario.searchTerm.length > 0) {
      window.print()
    }
  }


  render() {
    const selectedFilter = this.props.voluntario.selectedFilter
    const nome = (this.props.voluntario.term && this.props.voluntario.term.length > 0)
               ? (<p className="filter filterTerm">
                  Nomes que possuem termo: {`"${this.props.voluntario.term}"`}
               </p>)
               : null
    return (
      <div>
        <div className="Grid Grid-withGutter contentLeaderBottom">
          <div className="Grid-cell u-size7of12">
            <h3 className="contentLeaderTitle">Voluntários do seu comitê</h3>
            <p className="filter"> Filtro: {selectedFilter.length > 0 ? selectedFilter : 'Listar todos voluntários'}</p>
            {nome}
          </div>
          <div className="Grid-cell u-size4of12 noPrint">
            <p className={styles.order}> Ordenar por: </p>
            <SelectField
              value={
                this.props.voluntario.orderOfVolunteers
                ? this.props.voluntario.orderOfVolunteers
                : selected
              }
              onChange={this.onOderChange}
              labelStyle={style.menuStyle}
              fullWidth
            >
              {
                order.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item.value}
                    primaryText={item.name}
                    key={i}
                  />
                ))
              }
            </SelectField>
          </div>
          <div className="Grid-cell u-size1of12 u-textCenter noPrint">
            <button
              onClick={this.onPrint}
              className={styles.printButton}
            >
              <SvgIcon
                className={styles.printIcon}
                icon="icon-icon-45"
              />
            </button>
          </div>
        </div>
      </div>
    )
  }
}

style.menuStyle = {
  color: '#484848',
  fontSize: '18px',
}


_VolunteerManagementHeader.propTypes = {
  getVolunterOrder: PropTypes.func,
  voluntario: PropTypes.object,
}

const mapActionsToProps = {
  getVolunterOrder,
}

const mapStateToProps = state => ({
  voluntario: state.voluntario,
})

const VolunteerManagementHeader =
connect(mapStateToProps, mapActionsToProps)(_VolunteerManagementHeader)

export default VolunteerManagementHeader
