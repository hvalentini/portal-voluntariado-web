import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import get from 'lodash.get'
import find from 'lodash.find'
import Dropzone from 'react-dropzone'
import Shave from '../Shave'
import styles from './InfoActionsGallery.css'
import { handleLikeAction, uploadFiles, handleInfoActionParticipated } from '../../actions/acoes'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const style = {}

class _InfoActionsGallery extends React.Component {

  static convertDate(date) {
    const now = new Date(date)
    now.setTime((now.getTime() + (now.getTimezoneOffset() * 60000)))
    return now
  }

  constructor(props) {
    super(props)
    this.state = {
      like: '',
      participated: false,
    }
    this.likeAction = this.likeAction.bind(this)
    this.onOpenClick = this.onOpenClick.bind(this)
    this.onDrop = this.onDrop.bind(this)
  }

  onOpenClick() {
    this.dropzone.open()
  }

  onDrop(files) {
    if (!files.length) return
    const id = get(this.props.acao, '_id')
    this.props.uploadFiles(files, id)
  }

  handleParticipated() {
    const filtro = `{"participantes.voluntarioId":"${this.props.idVoluntario}"}`
    this.props.handleInfoActionParticipated({
      idAcao: this.props.acao._id,
      idVoluntario: this.props.idVoluntario,
      type: 'ALL-PROGRAM',
      filtro,
    })

    this.setState({
      participated: !this.state.participated,
    })
  }

  likeAction(likeStyle, type) {
    const query = {}
    query.idAcao = get(this.props.acao, '_id')
    query.idVoluntario = this.props.idVoluntario
    query.type = type
    this.props.handleLikeAction(query)

    if (likeStyle.indexOf('is_animating') !== -1 || likeStyle.indexOf('heartOn') !== -1) {
      this.setState({
        like: `${styles.heart}`,
      })
    } else {
      this.setState({
        like: `${styles.heart} ${styles.is_animating}`,
      })
    }
  }

  renderParticipated() {
    const queryUsers = find(this.props.acao.participantes, {
      voluntarioId: this.props.idVoluntario,
    })
    const acao = this.props.acao

    const styleParticipated = (queryUsers || this.state.participated)
                              && !(queryUsers && this.state.participated)
                            ? `${styles.iconCheckActive}`
                            : `${styles.iconCheck}`

    const iconParticipated = (queryUsers || this.state.participated)
                              && !(queryUsers && this.state.participated)
                            ? 'icon-icon-51'
                            : 'icon-icon-50'
    if (acao.comite._id === this.props.userComite && acao.acaoPontual === true) {
      return (
        <a
          role="button"
          tabIndex="0"
          className={`Grid-cell u-size4of12 u-textCenter ${styles.buttonHeart}`}
          onClick={() => this.handleParticipated()}
          style={{
            width: '27% !important',
          }}
        >
          <div className="u-posRelative">
            <SvgIcon
              className={styleParticipated}
              icon={iconParticipated}
              style={{ marginBottom: '9px' }}
            />
            <span>Participei</span>
          </div>
        </a>
      )
    }

    return null
  }

  render() {
    const query = find(this.props.acao.gostaram, { voluntarioId: this.props.idVoluntario })

    let likeStyle = query && !this.state.like ? `${styles.heart} ${styles.heartOn}` : this.state.like
    likeStyle = !likeStyle ? `${styles.heart}` : likeStyle

    const type = likeStyle.indexOf('heartOn') > 0 || likeStyle.indexOf('is_animating') > 0

    return (
      <div className={`${styles.container}`}>
        <h2
          className="u-textTruncate"
          title={get(this.props.acao, 'nome')}
        >{get(this.props.acao, 'nome')}
        </h2>
        <p
          className={`${styles.subTitle} u-textTruncate`}
          title={get(this.props.acao.tipoAcao, 'nome')}
        >
          {get(this.props.acao.tipoAcao, 'nome')}
        </p>

        <Shave
          maxHeight={100}
        >
          {/* <p
            className={styles.description}
            title={get(this.props.acao, 'descricao')}
          > */}
          {get(this.props.acao, 'descricao')}
          {/* </p> */}
        </Shave>
        <div className={`${styles.buttons} Grid`}>
          <a
            role="button"
            tabIndex="0"
            className={`Grid-cell u-size4of12 ${styles.buttonHeart}`}
            onClick={() => this.likeAction(likeStyle, !type)}
            style={{
              width: '27% !important',
            }}
          >
            <div className="u-posRelative">
              <div className={styles.icons}>
                <div className={likeStyle} />
                <span className={styles.linkLike}>Gostei</span>
              </div>
            </div>
          </a>

          {this.renderParticipated()}

          <button
            className="Grid-cell u-size4of12 u-textCenter"
            onClick={this.onOpenClick}
            style={{
              width: '27% !important',
            }}
          >
            <Dropzone
              accept=".jpg,.png,.jpeg,.gif"
              ref={(c) => { this.dropzone = c }}
              onDrop={this.onDrop}
              style={{ ...style.dropzonePartner }}
            >
              <div className="u-posRelative">
                <SvgIcon
                  className={styles.icons}
                  icon="icon-icon-1"
                />
                <span>Adicionar Foto</span>
              </div>
            </Dropzone>
          </button>
        </div>
      </div>
    )
  }
}

style.dropzonePartner = {
  height: '100%',
  width: '100%',
  borderWidth: '0',
}

_InfoActionsGallery.propTypes = {
  acao: PropTypes.object,
  idVoluntario: PropTypes.string,
  userComite: PropTypes.string,
  handleLikeAction: PropTypes.func,
  uploadFiles: PropTypes.func,
  handleInfoActionParticipated: PropTypes.func,
}

const mapActionsToProps = {
  handleLikeAction,
  uploadFiles,
  handleInfoActionParticipated,
}

const mapStateToProps = state => ({
  idComite: state.loggedUser.comite._id,
})

const InfoActionsGallery = connect(mapStateToProps, mapActionsToProps)(_InfoActionsGallery)
export default InfoActionsGallery
