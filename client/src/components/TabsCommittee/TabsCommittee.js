import React from 'react'
import PropTypes from 'prop-types'
import { Tabs, Tab } from 'material-ui/Tabs'
import currencyFormatter from 'currency-formatter'
import styles from './TabsCommittee.css'
import FormCommittee from '../../components/FormCommittee/FormCommittee'
import ActionsPage from '../../pages/ActionsPage/ActionsPage'
import LandingVolunteerPage from '../../pages/LandingVolunteerPage/LandingVolunteerPage'

class TabsCommittee extends React.Component {

  static handleForm() {
    return (
      <div>
        <FormCommittee {...this.props} />
      </div>
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      value: 'info',
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(value) {
    this.setState({
      value,
    })
  }

  handleInfos() {
    if (this.props.match.params.id && this.props.comite) {
      const beneficiadas = typeof this.props.comite.beneficiadas === 'object'
                          ? this.props.comite.beneficiadas.sum
                          : this.props.comite.beneficiadas

      let saldoAtual = currencyFormatter.format(this.props.comite.saldoAtual, { code: 'BRL' })
      saldoAtual = (this.props.comite.saldoAtual < 0)
                  ? saldoAtual.substring(4)
                  : saldoAtual.substring(3)
      const symbol = (this.props.comite.saldoAtual < 0) ? '-' : ''

      return (
        <div className={styles.infoRoot}>
          <div className="Grid-cell u-size3of12 u-textLeft">
            <span className={styles.infoTitle}>Voluntários</span>
            <span className={styles.info}>{this.props.comite.voluntarios}</span>
          </div>
          <div className="Grid-cell u-size3of12 u-textLeft">
            <span className={styles.infoTitle}>Ações</span>
            <span className={styles.info}>{this.props.comite.acoes}</span>
          </div>
          <div className="Grid-cell u-size3of12 u-textLeft">
            <span className={styles.infoTitle}>Beneficiados</span>
            <span className={styles.info}>{beneficiadas}</span>
          </div>
          <div className="Grid-cell u-size3of12 u-textLeft">
            <span className={styles.infoTitle}>Saldo Atual</span>
            <span className={styles.info}>{symbol}R$ {saldoAtual}</span>
          </div>
        </div>
      )
    }
    return null
  }

  render() {
    return (
      <Tabs
        value={this.state.value}
        onChange={this.handleChange}
        tabItemContainerStyle={{ width: '409px' }}
      >
        <Tab
          label="Informações Principais"
          value="info"
          buttonStyle={{
            color: '#00bee7',
            fontFamily: 'Chantilly-Serial',
            fontSize: '16px',
            fontWeight: '300',
            whiteSpace: 'normal',
            borderBottom: this.state.value === 'info' ? '2px solid #00bee7' : 'none',
            height: '25px',
          }}
          style={{
            width: '209px',
          }}
          className="noPrint"
        >
          <div className={styles.contentTabInfo}>
            {this.handleInfos()}
            {TabsCommittee.handleForm()}
          </div>
        </Tab>

        <Tab
          label="Voluntários"
          value="voluntarios"
          buttonStyle={{
            color: '#00bee7',
            fontFamily: 'Chantilly-Serial',
            fontSize: '16px',
            fontWeight: '300',
            whiteSpace: 'normal',
            borderBottom: this.state.value === 'voluntarios' ? '2px solid #00bee7' : 'none',
            height: '25px',
          }}
          style={{
            width: '120px',
          }}
          className="noPrint"
        >
          <div className={styles.contentTabVolunteer}>
            <LandingVolunteerPage idComite={this.props.match.params.id} />
          </div>
        </Tab>

        <Tab
          label="Ações"
          value="acoes"
          buttonStyle={{
            color: '#00bee7',
            fontFamily: 'Chantilly-Serial',
            fontSize: '16px',
            fontWeight: '300',
            whiteSpace: 'normal',
            borderBottom: this.state.value === 'acoes' ? '2px solid #00bee7' : 'none',
            height: '25px',
          }}
          style={{
            width: '68px',
          }}
          className="noPrint"
        >
          <div className={styles.contentTabActions}>
            <ActionsPage
              match={this.props.match}
              styleBnt={{ marginBottom: '-15px' }}
            />
          </div>
        </Tab>
      </Tabs>
    )
  }
}

TabsCommittee.propTypes = {
  comite: PropTypes.object,
  match: PropTypes.object,
}

export default TabsCommittee
