import React from 'react'
import PropTypes from 'prop-types'
import { MenuItem, DropDownMenu } from 'material-ui'
import { Link } from 'react-router-dom'
import styles from './HeaderInternalAdmin.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const style = {}

class HeaderInternalAdmin extends React.Component {

  renderAdd() {
    if (!this.props.to) {
      return null
    }

    return (
      <Link to={this.props.to} className={`${styles.linkAdd} noPrint`}>
        <div className={styles.buttonAdd}>
          <SvgIcon
            className={styles.iconAdd}
            icon="icon-icon-7"
          />
        </div>
        <span>ADICIONAR</span>
      </Link>
    )
  }

  renderPrint() {
    if (this.props.print) {
      return (
        <button
          onClick={this.props.onPrint}
          className={`${styles.printButton} noPrint`}
          style={{ display: 'inline-block' }}
        >
          <SvgIcon
            className={styles.printIcon}
            icon="icon-icon-45"
          />
        </button>
      )
    }

    return null
  }

  render() {
    const classTitleOrder = this.props.order.length ? '' : 'u-hidden'

    return (
      <div className={`${styles.container}`}>
        <div className="Grid Grid-withGutter">
          <div className="Grid-cell u-size5of12">
            <h3 className={styles.title}>{this.props.title}</h3>
            {this.renderAdd()}
          </div>
          <div className="Grid-cell u-size7of12">
            <div className={styles.order}>
              <p className={`${styles.orderTitle} ${classTitleOrder}`}> Ordenando por: </p>
              <DropDownMenu
                value={this.props.orderSelected}
                onChange={this.props.onChangeOrder}
                labelStyle={{ color: 'black', fontWeight: 'normal' }}
                style={{ ...style.select }}
                underlineStyle={{ display: 'none' }}
                className={`${styles.select}`}
              >
                {
                  this.props.order.map((item, i) => (
                    <MenuItem
                      className="SelectBkg"
                      value={item.value}
                      primaryText={item.name}
                      key={i}
                    />
                  ))
                }
              </DropDownMenu>
              {this.renderPrint()}
            </div>
          </div>
        </div>
        <hr className={styles.separator} />
      </div>
    )
  }
}

style.select = {
  overflow: 'hidden',
  top: '-10px',
  color: 'black',
}

HeaderInternalAdmin.propTypes = {
  title: PropTypes.string,
  order: PropTypes.array,
  to: PropTypes.string,
  orderSelected: PropTypes.string,
  onChangeOrder: PropTypes.func,
  print: PropTypes.bool,
  onPrint: PropTypes.func,
}

HeaderInternalAdmin.defaultProps = {
  order: [],
  print: false,
}

export default HeaderInternalAdmin
