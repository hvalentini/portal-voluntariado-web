import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './CommiteeCarouselFilter.css'
import updateActions from '../../actions/commiteVolunteerAction'

class _CommiteeCarouselFilter extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      selectedFilter: 1,
    }

    this.handleFilter = this.handleFilter.bind(this)
  }

  handleFilter(newFilter, selected) {
    const filter = newFilter || `"dataFinal": "${new Date()}", "statusAcao": "andamento"`
    const fullFilter = `{ "comite": "${this.props.comiteId}", ${filter} }`
    this.props.updateActions(fullFilter)

    this.setState({
      selectedFilter: selected,
    })

    const offsetY = (document.querySelector('.pv-LandingVolunteer-blocks:nth-child(2)') ||
      document.querySelector('.pv-LandingLeader-blocks:nth-child(2)'))
      .offsetTop

    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
              ? document.querySelector('html')
              : document.querySelector('body')
    animatedScrollTo(
      el,
      offsetY,
      1000,
    )

    this.props.selectTab(fullFilter)
  }

  render() {
    const date = new Date()
    return (
      <div className={`Grid Grid--alignMiddle ${styles.CommiteeCarouselFilter}`}>
        <div className={`Grid-cell u-size7of12 ${styles.title}`}>
          <h3 className={styles.titleDiv}>
            <SvgIcon
              icon="icon-icon-33"
            />
            Fique atento às ações do seu comitê
          </h3>
        </div>
        <div className={`Grid-cell u-size5of12 ${styles.filters}`}>
          <div className="Grid Grid--alignMiddle">
            <a
              role="button"
              tabIndex="0"
              className={`Grid-cell u-size1of3 ${styles.opts} ${this.state.selectedFilter === 1 ? styles.selected : ''}`}
              onClick={() => this.handleFilter(`"dataFinal": "${date}", "statusAcao": "andamento"`, 1)}
            >
              Todas
            </a>
            <a
              role="button"
              tabIndex="0"
              className={`Grid-cell u-size1of3 ${styles.opts} ${this.state.selectedFilter === 2 ? styles.selected : ''}`}
              onClick={() => this.handleFilter(`"dataInicial": "${date}", "dataFinal": "${date}", "statusAcao": "andamento"`, 2)}
            >
              Em Andamento
            </a>
            <a
              role="button"
              tabIndex="0"
              className={`Grid-cell u-size1of3 ${styles.opts} ${this.state.selectedFilter === 3 ? styles.selected : ''}`}
              onClick={() => this.handleFilter(`"dataInicial": "${date}"`, 3)}
            >
              Agendadas
            </a>
          </div>
        </div>
      </div>
    )
  }
}

_CommiteeCarouselFilter.propTypes = {
  updateActions: PropTypes.func,
  selectTab: PropTypes.func,
  comiteId: PropTypes.string,
}

const mapStateToProps = state => ({
  actions: state.commiteVolunteerAction.actions,
  comiteId: state.loggedUser.comite._id,
  userId: state.loggedUser._id,
})

const mapActionsToProps = {
  updateActions,
}

const CommiteeCarouselFilter = connect(
  mapStateToProps,
  mapActionsToProps,
)(_CommiteeCarouselFilter)

export default CommiteeCarouselFilter
