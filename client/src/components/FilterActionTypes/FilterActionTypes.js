import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  TextField,
  RadioButton,
  RadioButtonGroup,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import muiTheme from '../../muiTheme'
import PillButton from '../PillButton/PillButton'
import { getFilterActionTypes, editFilterActionTypes, editFilterActionTypesName } from '../../actions/filtersActionTypes'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './FilterActionTypes.css'

const style = {}


class _FilterActionTypes extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      btnBottom: 0,
    }

    this.handleChangeName = this.handleChangeName.bind(this)
    this.getFilterActionTypes = this.getFilterActionTypes.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
  }

  getFilterActionTypes() {
    const filtro = {
      nomeNormalizado: this.props.filtersActionTypes.nomeNormalizado,
    }

    if (this.props.filtersActionTypes.modalidade !== 'null') {
      filtro.tipo = parseInt(this.props.filtersActionTypes.modalidade, 10)
    }

    if (this.props.filtersActionTypes.informarQtdeCartas !== 'null') {
      filtro.informarQtdeCartas = this.props.filtersActionTypes.informarQtdeCartas === 'true'
    }

    this.props.getFilterActionTypes({ filtro, naoOrdernadarPersonalizado: true })
  }

  handleChangeName(value) {
    this.props.editFilterActionTypesName(value.replace(/^\s+/gm, ''))
  }

  changeRadio(e) {
    const obj = {}
    obj[e.target.name] = e.target.value
    this.props.editFilterActionTypes(obj)
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  render() {
    const nome = this.props.filtersActionTypes.nome
    return (
      <div>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <div
            className={styles.wrapperFilter}
            style={{ maxHeight: `${window.innerHeight - 172}px` }}
            onScroll={this.adjustButtonPlacing}
          >
            <TextField
              floatingLabelText="Nome do tipo de ação"
              name="nome"
              underlineStyle={style.line}
              value={nome}
              onChange={e => this.handleChangeName(e.target.value)}
              maxLength={100}
              inputStyle={{ marginLeft: '27px' }}
              hintStyle={{ marginLeft: '27px' }}
              style={{ marginTop: '-15px', marginBottom: '15px' }}
              floatingLabelStyle={{ marginLeft: '27px' }}
              floatingLabelFocusStyle={{ marginLeft: '0' }}
              floatingLabelShrinkStyle={{ marginLeft: '0' }}
              fullWidth
              autoComplete="off"
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />
            <div>
              <div className={styles.radioTitle}>Modalidade?</div>
              <RadioButtonGroup
                name="modalidade"
                className={styles.exclusivoParaLider}
                defaultSelected="null"
                onChange={e => this.changeRadio(e)}
              >
                <RadioButton value="null" label="Todas" />
                <RadioButton value={1} label="Contínua" />
                <RadioButton value={2} label="Gerencial" />
                <RadioButton value={0} label="Pontual" />
              </RadioButtonGroup>
              <div className={styles.radioTitle}>Existe cartas?</div>
              <RadioButtonGroup
                name="informarQtdeCartas"
                className={styles.existeCartas}
                defaultSelected="null"
                onChange={e => this.changeRadio(e)}
              >
                <RadioButton value="null" label="Ambas" />
                <RadioButton value="true" label="Sim" />
                <RadioButton value="false" label="Não" />
              </RadioButtonGroup>
            </div>
            <div
              style={{ bottom: '20px' }}
              className={styles.btn}
            >
              <PillButton
                text="Filtrar"
                onClick={() => this.getFilterActionTypes()}
                loader={this.props.loader}
              />
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterActionTypes.propTypes = {
  getFilterActionTypes: PropTypes.func,
  editFilterActionTypes: PropTypes.func,
  editFilterActionTypesName: PropTypes.func,
  filtersActionTypes: PropTypes.object,
  loader: PropTypes.bool,
}

const mapStateToProps = state => ({
  filtersActionTypes: state.filtersActionTypes,
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  editFilterActionTypes,
  getFilterActionTypes,
  editFilterActionTypesName,
}

const FilterActionTypes = connect(mapStateToProps, mapActionsToProps)(_FilterActionTypes)
export default FilterActionTypes
