import React from 'react'
import PropTypes from 'prop-types'
import styles from './PaginationComittee.css'

class PaginationComittee extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      pageSelected: 0,
      previousSelected: 0,
      scroll: 0,
      keysToDisplay: 3,
      maximumSize: 5,
    }
    this.handlePage = this.handlePage.bind(this)
    this.handlePagination = this.handlePagination.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.order !== nextProps.order) {
      this.setState({
        pageSelected: 0,
      })
    }
  }

  handlePage(ev) {
    if (Number(ev.target.value) === this.state.pageSelected) { return null }
    if (this.props.onClick) {
      this.setState({
        previousSelected: this.state.pageSelected,
        pageSelected: Number(ev.target.value),
      })
      return this.props.onClick(ev.target.value)
    }

    return this.setState({
      previousSelected: this.state.pageSelected,
      pageSelected: Number(ev.target.value),
    })
  }

  handlePagination(keys) {
    const actual = this.state.pageSelected
    const previous = this.state.previousSelected
    const visible = this.state.keysToDisplay
    const dots = (
      <button
        className={styles.page}
        key="dots"
        onClick={e => e.preventDefault()}
      >
        ...
      </button>
    )
    const dotsAfter = (
      <button
        className={styles.page}
        key="dotsAfter"
        onClick={e => e.preventDefault()}
      >
        ...
      </button>
    )

    if (actual > previous) {
      if (actual + visible >= keys.length) {
        return [dots, keys.slice(keys.length - visible, keys.length - 1)]
      }
      if (actual <= 2) {
        return [keys.slice(1, (actual - 1) + visible), dots]
      }
      return [dots, keys.slice(actual - 1, (actual - 1) + visible), dotsAfter]
    }

    if (actual >= keys.length - 2) {
      return [dots, keys.slice(actual - 1, keys.length - 1)]
    }
    if (actual - 1 > 0) {
      return [dots, keys.slice(actual - 1, (actual - 1) + visible), dotsAfter]
    }
    return [keys.slice(1, visible), dots]
  }

  handleDivPages() {
    const qtd = Math.ceil(this.props.totalItens / 6)
    if (qtd < 2) { return null }
    const keys = []
    const arrowLeft = (
      <button
        className={[styles.arrow, styles.arrowLeft].join(' ')}
        onClick={this.handlePage}
        value={this.state.pageSelected - 1}
        disabled={this.state.pageSelected < 1}
        key="arrowLeft"
      >
        &lt;
      </button>
    )
    const arrowRight = (
      <button
        className={[styles.arrow, styles.arrowRight].join(' ')}
        value={this.state.pageSelected + 1}
        onClick={this.handlePage}
        disabled={this.state.pageSelected >= qtd - 1}
        key="arrowRight"
      >
        &gt;
      </button>
    )
    for (let i = 0; i < qtd; i += 1) {
      const style = i === this.state.pageSelected ? styles.pageActive : styles.page
      keys.push(
        <button
          key={i}
          value={i}
          className={style}
          onClick={this.handlePage}
        >
          {i + 1}
        </button>,
      )
    }
    if (keys.length < 5) {
      return keys
    }
    return [
      arrowLeft,
      keys[0],
      ...this.handlePagination(keys),
      keys[keys.length - 1],
      arrowRight,
    ]
  }

  render() {
    return (
      <div className={[styles.pagination, this.props.className, 'noPrint'].join(' ')}>
        {this.handleDivPages()}
      </div>
    )
  }
}

PaginationComittee.propTypes = {
  onClick: PropTypes.func,
  totalItens: PropTypes.number,
  className: PropTypes.string,
  order: PropTypes.string,
}

export default PaginationComittee
