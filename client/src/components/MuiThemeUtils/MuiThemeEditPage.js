import getMuiTheme from 'material-ui/styles/getMuiTheme'

const MuiThemeEditPage = getMuiTheme({
  palette: {
    textColor: '#484848',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent2Color: '#484848',
    accent3Color: '#484848',
    disabledColor: '#484848',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  toggle: {
    thumbOnColor: '#0abce5',
    thumbOffColor: '#0abce5',
    thumbDisabledColor: '#47c4e0',
    thumbRequiredColor: '#47c4e0',
    trackOnColor: '#7eddf2',
    trackOffColor: '#7eddf2',
    trackDisabledColor: '#47c4e0',
    labelColor: '#484848',
    labelDisabledColor: '#484848',
    trackRequiredColor: '#47c4e0',
  },
  fontFamily: 'Chantilly-Serial',
})

export default MuiThemeEditPage
