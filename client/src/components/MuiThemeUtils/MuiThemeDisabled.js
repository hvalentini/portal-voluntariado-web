import getMuiTheme from 'material-ui/styles/getMuiTheme'

const MuiThemeDisabled = getMuiTheme({
  palette: {
    textColor: '#484848',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent2Color: '#484848',
    accent3Color: '#484848',
    disabledColor: '#484848',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  toggle: {
    thumbOnColor: '#44c6e2',
    thumbOffColor: '#44c6e2',
    thumbDisabledColor: '#44c6e2',
    thumbRequiredColor: '#44c6e2',
    trackOffColor: '#44c6e2',
    trackDisabledColor: '#44c6e2',
    labelColor: '#44c6e2',
    labelDisabledColor: '#44c6e2',
  },
  fontFamily: 'Chantilly-Serial',
})

export default MuiThemeDisabled
