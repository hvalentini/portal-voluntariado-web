import getMuiTheme from 'material-ui/styles/getMuiTheme'

const MuiThemeFilters = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  fontFamily: 'Chantilly-Serial',
})

export default MuiThemeFilters
