import React from 'react'
import styles from './MoreAboutUs.css'

const MoreAboutUs = () => (
  <div
    className={styles.body}
    style={{
      boxShadow: '0px 0px 10px 0px rgba(0, 0, 0, 0.5)',
      marginTop: '10px',
      border: 'none',
    }}
  >
    <h1 className={styles.title}>Conheça mais</h1>
    <p className={styles.text}>
      <spam className={styles.ident} />
      O Programa de Voluntariado do grupo Algar, foi lançado em abril de 2003.
      A Algar nasce com o intuito de "servir a comunidade".
      Neste programa, cada um pode contribuir com o seu tempo, trabalho e talento
      em prol de uma sociedade melhor.
    </p>
  </div>
)

export default MoreAboutUs
