import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import LinearProgress from 'material-ui/LinearProgress'
import CountUp from 'react-countup'
import styles from './VolunteerStatistic.css'
import { getAmountBeneficiated, getAmountActions, getAcoesRealizadas, getAcoesFiltros } from '../../actions/acoes'

const style = {}

class _VolunteerStatistic extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      actionsAllProgram: <CountUp
        start={0}
        end={this.props.actionsAllProgram}
        duration={0.5}
      />,
      actionMyComite: <CountUp
        start={0}
        end={this.props.actionMyComite}
        duration={0.5}
      />,
      amountBeneficiated: <CountUp
        start={0}
        end={this.props.amountBeneficiated || 0}
        duration={0.5}
      />,
    }
  }

  componentWillMount() {
    let filtro
    // first block
    filtro = `{"participantes.voluntarioId":"${this.props.idVoluntario}"}`
    this.props.getAcoesFiltros({ type: 'ALL-PROGRAM', filtro: `${filtro}` }) // participou de tantas acoes em todo o programa
    this.props.getAmountActions()

    // second block
    filtro = `{"participantes.voluntarioId":"${this.props.idVoluntario}","comite":"${this.props.idComite}"}`
    this.props.getAcoesFiltros({ type: 'ACTIONS-COMITE', filtro: `${filtro}` }) // participou de tatas acoes em seu comite
    this.props.getAcoesRealizadas(this.props.idComite)

    // third block
    this.props.getAmountBeneficiated(null, null, this.props.idVoluntario)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.actionsAllProgram !== nextProps.actionsAllProgram) {
      this.setState({
        actionsAllProgram: <CountUp
          start={this.props.actionsAllProgram}
          end={nextProps.actionsAllProgram}
          duration={0.5}
        />,
      })
    }
    if (this.props.actionMyComite !== nextProps.actionMyComite) {
      this.setState({
        actionMyComite: <CountUp
          start={this.props.actionMyComite}
          end={nextProps.actionMyComite}
          duration={0.5}
        />,
      })
    }
    if (this.props.amountBeneficiated !== nextProps.amountBeneficiated) {
      this.setState({
        amountBeneficiated: <CountUp
          start={this.props.amountBeneficiated}
          end={nextProps.amountBeneficiated}
          duration={0.5}
        />,
      })
    }
  }

  render() {
    const loader = this.props.loader
                 ? <LinearProgress mode="indeterminate" style={style.loader} />
                 : null

    const loaderActionMyComite = this.props.loaderMyComite
                 ? <LinearProgress mode="indeterminate" style={style.loader} />
                 : null

    const loaderBeneficiated = this.props.loaderBeneficiated
                 ? <LinearProgress mode="indeterminate" style={style.loader} />
                 : null

    return (
      <div className="Grid">
        <div className={`Grid-cell u-size2of6 ${styles.actions}`}>
          <div className={`${styles.qtd} u-posRelative`}>
            {this.state.actionsAllProgram}
            {loader}
          </div>
          <hr className={styles.divider} />
          <p className={styles.text}>
            ações das {this.props.amountActions}<br />do programa de voluntariado
          </p>
        </div>
        <div className={`Grid-cell u-size2of6 ${styles.actions}`}>
          <div className={`${styles.qtd} u-posRelative`}>
            {this.state.actionMyComite}
            {loaderActionMyComite}
          </div>
          <hr className={styles.divider} />
          <p className={styles.text}>
            ações de {this.props.totalActions} desenvolvidas pela {this.props.companyName}
          </p>
        </div>
        <div className={`Grid-cell u-size2of6 ${styles.actions}`}>
          <div className={`${styles.qtd} u-posRelative`}>
            {this.state.amountBeneficiated}
            {loaderBeneficiated}
          </div>
          <hr className={styles.divider} />
          <p className={styles.text}>Total de pessoas beneficiadas</p>
        </div>
      </div>
    )
  }
}

style.loader = {
  backgroundColor: 'rgba(255, 255, 255, .3)',
  top: '108px',
  left: '50%',
  marginLeft: '-35px',
  position: 'absolute',
  width: '70px',
  height: '3px',
}

_VolunteerStatistic.propTypes = {
  amountBeneficiated: PropTypes.number,
  amountActions: PropTypes.number.isRequired,
  companyName: PropTypes.string.isRequired,
  totalActions: PropTypes.number,
  actionsAllProgram: PropTypes.number.isRequired,
  actionMyComite: PropTypes.number.isRequired,
  idComite: PropTypes.string.isRequired,
  idVoluntario: PropTypes.string.isRequired,
  getAmountBeneficiated: PropTypes.func.isRequired,
  getAmountActions: PropTypes.func.isRequired,
  getAcoesRealizadas: PropTypes.func.isRequired,
  getAcoesFiltros: PropTypes.func.isRequired,
  loader: PropTypes.bool,
  loaderMyComite: PropTypes.bool,
  loaderBeneficiated: PropTypes.bool,
}

const mapStateToProps = state => ({
  amountBeneficiated: state.acoes.amountBeneficiated,
  amountActions: state.acoes.amountActions,
  companyName: state.loggedUser.comite.empresa.nome,
  totalActions: state.statistic.leader.actionsTaken,
  actionsAllProgram: state.statistic.volunteer.actionsAllProgram,
  actionMyComite: state.statistic.volunteer.actionMyComite,
  loader: state.statistic.loader,
  loaderMyComite: state.statistic.loaderMyComite,
  loaderBeneficiated: state.acoes.loaderBeneficiated,
})

const mapActionsToProps = {
  getAmountBeneficiated,
  getAmountActions,
  getAcoesRealizadas,
  getAcoesFiltros,
}

const VolunteerStatistic = connect(mapStateToProps, mapActionsToProps)(_VolunteerStatistic)
export default VolunteerStatistic
