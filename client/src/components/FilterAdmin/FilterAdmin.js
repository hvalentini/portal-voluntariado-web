import React from 'react'
import PropTypes from 'prop-types'
import { findDOMNode } from 'react-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { connect } from 'react-redux'
import styles from './FilterAdmin.css'
import muiTheme from '../../muiTheme'

require('./stickyfill.min')

class _FilterAdmin extends React.Component {
  static isMSIE() {
    let isMSIE = false

    if (navigator.appName === 'Microsoft Internet Explorer') {
      const ua = navigator.userAgent
      const re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})')

      if (re.exec(ua) !== null) {
        isMSIE = true
      }
    } else if (navigator.appName === 'Netscape') {
      if (navigator.appVersion.indexOf('Trident') > -1) isMSIE = true
      else if (navigator.appVersion.indexOf('Edge') > -1) isMSIE = true
    }

    return isMSIE
  }

  constructor(props) {
    super(props)

    this.state = {
      first: true,
    }

    this.handleScroll = this.handleScroll.bind(this)
  }

  componentDidMount() {
    if (_FilterAdmin.isMSIE()) {
      global.Stickyfill.add(findDOMNode(this))
    } else {
      document.addEventListener('scroll', this.handleScroll)
      document.addEventListener('resize', this.handleScroll)
      this.handleScroll()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.calcView && !this.props.calcView) this.handleScroll()
  }

  componentWillUnmount() {
    if (_FilterAdmin.isMSIE()) {
      global.Stickyfill.remove(findDOMNode(this))
    } else {
      document.removeEventListener('scroll', this.handleScroll)
      document.removeEventListener('resize', this.handleScroll)
    }
  }

  handleScroll() {
    const fator = 82
    let fatorOffset = 82
    const elFilter = findDOMNode(this)
    const parent = elFilter.offsetParent ? elFilter.offsetParent : document.body
    const el = elFilter.childNodes[2].childNodes[0]

    if (!el || !this.props.recalc) return

    if ((this.props.loader && this.props.loaderUI) && !this.state.first) return

    let offsetTop = parent.offsetTop + fator

    if (parent.tagName === 'BODY') offsetTop = elFilter.offsetTop + fator

    if (_FilterAdmin.isMSIE()) offsetTop += fator

    if (!this.state.offsetTop) {
      this.setState({
        offsetTop,
        first: false,
      })

      el.style.minHeight = '200px'
    }

    let parentFator = 0

    let calcElFilter = 90 - window.pageYOffset

    if (parent.childNodes[0] &&
        typeof parent.childNodes[0].className === 'string') {
      parentFator = parent.childNodes[0].className.indexOf('TabsCommittee') > -1
                    ? 250 : 0
      fatorOffset = 215
      calcElFilter = 245 - window.pageYOffset
    }

    offsetTop = this.state.offsetTop || offsetTop
    const vh = Math.ceil(100 - (((offsetTop - parentFator) * 100) / window.innerHeight))

    const footer = document.getElementById('footer')
    let posFotter = ((window.pageYOffset + window.innerHeight) - footer.offsetTop)
    posFotter = posFotter >= 0 ? posFotter : 0

    if (window.pageYOffset >= 0 && window.pageYOffset + 16 < offsetTop - fatorOffset) {
      el.style.setProperty('max-height', `calc(${vh}vh + ${window.pageYOffset}px)`, 'important')
      elFilter.style.setProperty('height', `calc(${100}vh - ${calcElFilter}px)`, 'important')
    } else {
      el.style.setProperty('max-height', `calc(90vh - ${posFotter}px)`, 'important')
      elFilter.style.setProperty('height', `calc(100vh - ${posFotter}px)`, 'important')
    }
  }

  render() {
    const children = React.cloneElement(this.props.children, { ...{ id: 'filtro' } })
    return (
      <div
        className={`${styles.filter} noPrint`}
      >
        <h3 className={styles.title}>{this.props.title}</h3>
        <hr className={styles.separator} />
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          {children}
        </MuiThemeProvider>
      </div>
    )
  }

}

_FilterAdmin.propTypes = {
  title: PropTypes.string,
  children: PropTypes.object,
  calcView: PropTypes.bool,
  loader: PropTypes.bool,
  loaderUI: PropTypes.bool,
  recalc: PropTypes.bool,
}

_FilterAdmin.defaultProps = {
  title: 'Filtros',
  children: {},
  calcView: false,
  loader: false,
  loaderUI: false,
  recalc: true,
}


const mapStateToProps = state => ({
  loader: state.list.loading,
  loaderUI: state.ui.loading,
})

const FilterAdmin = connect(mapStateToProps, null)(_FilterAdmin)
export default FilterAdmin
