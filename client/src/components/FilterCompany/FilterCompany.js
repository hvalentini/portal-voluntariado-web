import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TextField } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import muiTheme from '../../muiTheme'
import PillButton from '../PillButton/PillButton'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { editFilterCompany, getFilterCompany } from '../../actions/filtersCompany'
import styles from './FilterCompany.css'

const style = {}

class _FilterCompany extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      btnBottom: 0,
    }

    this.getFilterCompanies = this.getFilterCompanies.bind(this)
    this.handleChangeName = this.handleChangeName.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
  }

  getFilterCompanies() {
    const filtro = this.props.filter.nomeNormalizado
                    ? { nomeNormalizado: this.props.filter.nomeNormalizado }
                    : {}

    this.props.getFilterCompany({ filtro })
  }

  handleChangeName(value) {
    this.props.editFilterCompany(value.replace(/^\s+/gm, ''))
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  render() {
    return (
      <div>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <form>
            <div
              className={styles.wrapperFilter}
              style={{ maxHeight: `${window.innerHeight - 230}px` }}
              onScroll={this.adjustButtonPlacing}
            >
              <TextField
                // hintText="Nome da empresa"
                name="nome"
                floatingLabelText="Nome da empresa"
                className={styles.inputTxt}
                underlineStyle={style.line}
                value={this.props.filter.nome}
                onChange={e => this.handleChangeName(e.target.value)}
                maxLength={120}
                fullWidth
                inputStyle={{ marginLeft: '27px' }}
                hintStyle={{ marginLeft: '27px' }}
                style={{ marginTop: '-15px' }}
                floatingLabelStyle={{ marginLeft: '27px' }}
                floatingLabelFocusStyle={{ marginLeft: '0' }}
                floatingLabelShrinkStyle={{ marginLeft: '0' }}
                autoComplete="off"
              />
              <SvgIcon
                icon="icon-icon-29"
                className={styles.iconSearch}
              />

              <div
                style={{ padding: '15px 0' }}
                className={styles.btn}
              >
                <PillButton
                  text="FILTRAR"
                  onClick={e => this.getFilterCompanies(e)}
                  loader={this.props.loader}
                />
              </div>
            </div>
          </form>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterCompany.propTypes = {
  nome: PropTypes.string,
  loader: PropTypes.bool,
  filter: PropTypes.object,
  getFilterCompany: PropTypes.func,
  editFilterCompany: PropTypes.func,
}

const mapStateToProps = state => ({
  filter: state.filtersCompany,
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  editFilterCompany,
  getFilterCompany,
}

const FilterCompany = connect(mapStateToProps, mapActionsToProps)(_FilterCompany)
export default FilterCompany
