import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import styles from './HeaderAdminButton.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

// const HeaderAdminButton = props => (
class HeaderAdminButton extends React.Component {

  constructor(props) {
    super(props)
    this.renderText = this.renderText.bind(this)
  }

  renderText() {
    if(this.props.text === 'Home') {
      return(
          <SvgIcon
            icon="icon-icon-62"
            style={{ fontSize: '24px', verticalAlign: 'middle' }}
          />
      )
    }
    return this.props.text
  }

  render() {
    return(
      <NavLink
        to={this.props.to}
        exact={this.props.exact}
        className={[styles.button, this.props.className].join(' ')}
        activeClassName={styles.active}
      >
        <span className={styles.innerBtn}>{this.renderText()}</span>
      </NavLink>
    )
  }

}

HeaderAdminButton.propTypes = {
  text: PropTypes.string,
  to: PropTypes.string,
  className: PropTypes.string,
  exact: PropTypes.bool,
}

HeaderAdminButton.defaultProps = {
  className: null,
  to: '',
}

export default HeaderAdminButton
