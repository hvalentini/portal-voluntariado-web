import React from 'react'

const SvgIcon = (props) => {

  return {

    props,

    render() {
      return (
        <svg
          className={`icon ${this.props.icon} ${this.props.className || ''}`}
          onClick={this.props.onClick}
          style={this.props.style}
          dangerouslySetInnerHTML={{ __html: `<use xlink:href="#${this.props.icon}"></use>` }}
        />
      )
    },
  }
}

export default SvgIcon
