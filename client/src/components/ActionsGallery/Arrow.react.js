import React from 'react'
import PropTypes from 'prop-types'

const Arrow = (props) => {
  const ico = props.direction ? '<' : '>'
  return (
    <button onClick={props.onClick}>
      {ico}
    </button>
  )
}

Arrow.propTypes = {
  direction: PropTypes.bool,
  onClick: PropTypes.func,
}

export default Arrow
