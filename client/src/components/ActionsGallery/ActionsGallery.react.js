import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dropzone from 'react-dropzone'
import get from 'lodash.get'
import styles from './ActionsGallery.css'
import {
   setGalleryModalContent,
 } from '../../actions/ui'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { uploadFiles } from '../../actions/acoes'

const style = {}

class _ActionsGallery extends React.Component {

  static addBlur(e) {
    const target = e.target
    if (target.scrollLeft > 0 && (target.scrollWidth - target.scrollLeft) !== target.clientWidth) {
      target.parentNode.children[2].style.display = 'block'
      target.parentNode.children[1].style.display = 'block'
    } else if (target.scrollLeft === 0) {
      target.parentNode.children[2].style.display = 'none'
      target.parentNode.children[1].style.display = 'block'
    } else if ((target.scrollWidth - target.scrollLeft) === target.clientWidth) {
      target.parentNode.children[1].style.display = 'none'
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      modalOpen: false,
    }

    this.openModal = this.openModal.bind(this)
    this.onOpenClick = this.onOpenClick.bind(this)
    this.onDrop = this.onDrop.bind(this)
  }

  onOpenClick() {
    this.dropzone.open()
  }

  onDrop(files) {
    if (!files.length) return
    const id = get(this.props.acao, '_id')
    this.props.uploadFiles(files, id)
  }

  openModal(e) {
    return this.props.setGalleryModalContent({
      gallery: this.props.gallery,
      pos: Number(e.currentTarget.dataset.index),
    })
  }

  renderImg() {
    const flag = document.getElementsByClassName('pv-ActionsGallery-carouselOverflow').length
    if (this.props.gallery.length > 0) {
      return this.props.gallery.map((img, i) => (
        <div
          key={`MainGallery-${i}-${flag}`}
          className={styles.slides}
          onMouseUp={this.openModal}
          data-index={i}
        >
          <div className={styles.container}>
            <img
              src={img}
              alt={img}
              draggable={false}
            />
          </div>
        </div>
      ))
    }
    return (
      <div className={styles.boxEmptyPictures}>
        <div className="Grid u-textCenter">
          <div className="Grid-cell">
            <Dropzone
              accept=".jpg,.png,.jpeg,.gif"
              ref={(c) => { this.dropzone = c }}
              onDrop={this.onDrop}
              style={{ ...style.dropzonePartner }}
            >
              <div className="u-posRelative">
                <SvgIcon
                  className={styles.emptyPictures}
                  icon="icon-icon-52"
                />
                <span className={`Grid-cell ${styles.picturesText}`}>Seja o primeiro a adicionar uma foto</span>
              </div>
            </Dropzone>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className={styles.mainActionsGallery}>
        <div
          className={styles.actionsGallery}
          onScroll={_ActionsGallery.addBlur}
        >
          <div className={styles.carouselOverflow}>
            {this.renderImg()}
          </div>
        </div>
        <div id="rightBlur" className={styles.rightBlur} />
        <div id="leftBlur" className={styles.leftBlur} style={{ display: 'none' }} />
      </div>
    )
  }
}

style.dropzonePartner = {
  height: '100%',
  width: '100%',
  borderWidth: '0',
  cursor: 'pointer',
}

_ActionsGallery.propTypes = {
  gallery: PropTypes.array,
  setGalleryModalContent: PropTypes.func,
  acao: PropTypes.object,
  uploadFiles: PropTypes.func,
}

const mapActionsToProps = {
  setGalleryModalContent,
  uploadFiles,
}

const ActionsGallery = connect(null, mapActionsToProps)(_ActionsGallery)

export default ActionsGallery
