import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import Carousel from 'react-slick'
import { connect } from 'react-redux'
import styles from './GalleryModal.css'
import Arrow from './Arrow.react'
import {
   closeGalleryModal,
   setGalleryModalCurrentSlide,
 } from '../../actions/ui'

const customStyle = {
  overlay: {
    zIndex: 9,
    backgroundColor: 'rgba(25, 20, 20, 0.75)',
  },
  content: {
    boxSizing: 'border-box',
    backgroundColor: 'rgba(25, 20, 20, 0)',
    height: 'auto',
    left: '50%',
    top: '50%',
    minHeight: 'auto',
    minWidth: '320px',
    overflow: 'visible',
    padding: 0,
    border: 0,
    transform: 'translate(-50%,-50%)',
    width: '70%',
    right: 'auto',
    bottom: 'auto',
  },
}

class _GalleryModal extends React.Component {

  constructor(props) {
    super(props)
    this.changeHighlight = this.changeHighlight.bind(this)
  }

  changeHighlight(e) {
    this.props.setGalleryModalCurrentSlide(Number(e.currentTarget.dataset.index))
  }

  renderModalThumbs() {
    return this.props.content.map((img, i) => (
      <div
        key={`Thumbs-${i}`}
        className={styles.modalThumbs}
      >
        <img
          src={img}
          alt={img}
        />
      </div>
    ))
  }

  render() {
    const customPaging = i => (
      <a>
        <img src={`${this.props.content[i]}`} alt={`${this.props.content[i]}`} height={100} />
      </a>
    )
    return (
      <Modal
        isOpen={this.props.isOpen}
        style={customStyle}
        onRequestClose={this.props.closeGalleryModal}
        contentLabel=""
      >
        <Carousel
          dots
          accessibility
          useCSS
          className={styles.carousel}
          dotsClass={styles.thumbs}
          customPaging={customPaging}
          draggable={false}
          initialSlide={this.props.initialSlide}
          slidesToScroll={1}
          slidesToShow={1}
          nextArrow={<Arrow />}
          prevArrow={<Arrow direction />}
          speed={500}
          showCounter
        >
          {this.renderModalThumbs()}
        </Carousel>
      </Modal>
    )
  }
}

_GalleryModal.propTypes = {
  isOpen: PropTypes.bool,
  content: PropTypes.array,
  closeGalleryModal: PropTypes.func,
  setGalleryModalCurrentSlide: PropTypes.func,
  initialSlide: PropTypes.number,
}

const mapStateToProps = state => ({
  isOpen: state.ui.galleryModal.open,
  content: state.ui.galleryModal.galleryModalContent,
  currentSlide: state.ui.galleryModal.galleryModalCurrentSlide,
  initialSlide: state.ui.galleryModal.galleryModalInitialSlide,
})

const mapActionsToProps = {
  closeGalleryModal,
  setGalleryModalCurrentSlide,
}

const GalleryModal = connect(mapStateToProps, mapActionsToProps)(_GalleryModal)

export default GalleryModal
