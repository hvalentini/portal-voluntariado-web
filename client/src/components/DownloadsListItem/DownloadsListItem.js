import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './DownloadsListItem.css'
import { getWebStatus } from '../../actions/webTest'

const style = {}

const _DownloadsListItem = props => (
  <div>
    <li className={`${styles.lista} Grid Grid--fit`}>
      <span className={`${styles.texto} Grid-cell u-size6of12`}>
        {props.title}
      </span>
      <div className={`${styles.arquivo} Grid-cell u-size2of12 u-textRight`}>
        <span>Tamanho:</span>
      </div>
      <div className={`${styles.arquivo} Grid-cell u-size2of12 u-textLeft`}>
        <span style={style.box}>
          {props.size}
        </span>
      </div>
      <a
        className={`${styles.download} Grid-cell u-size2of12`}
        href={props.href}
        onClick={(ev) => {
          ev.preventDefault()
          props.dispatch(getWebStatus('download', props.href))
        }}
      >
        Baixar
      </a>
    </li>
  </div>
)

style.box = {
  paddingLeft: '10px',
}

_DownloadsListItem.propTypes = {
  title: PropTypes.string,
  href: PropTypes.string,
  size: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
}

const DownloadsListItem = connect(null, null)(_DownloadsListItem)

export default DownloadsListItem
