import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import CountUp from 'react-countup'
import LinearProgress from 'material-ui/LinearProgress'
import styles from './Statistic.css'
import { getVoluntariosComite } from '../../actions/voluntario'
import { getAcoesRealizadas } from '../../actions/acoes'
import LeaderDashboardLinks from '../LeaderDashboardLinks/LeaderDashboardLinks.react'

const style = {}

class _Statistic extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      volunteers: <CountUp
        start={0}
        end={this.props.leader.volunteers}
        duration={0.5}
      />,
      actionsTaken: <CountUp
        start={0}
        end={this.props.leader.actionsTaken}
        duration={0.5}
      />,
    }
  }

  componentWillMount() {
    this.props.getVoluntariosComite(this.props.idComite)
    this.props.getAcoesRealizadas(this.props.idComite)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.leader.volunteers !== nextProps.leader.volunteers) {
      this.setState({
        volunteers: <CountUp
          start={this.props.leader.volunteers}
          end={nextProps.leader.volunteers}
          duration={0.5}
        />,
      })
    }
    if (this.props.leader.actionsTaken !== nextProps.leader.actionsTaken) {
      this.setState({
        actionsTaken: <CountUp
          start={this.props.leader.actionsTaken}
          end={nextProps.leader.actionsTaken}
          duration={0.5}
        />,
      })
    }
  }

  render() {
    const loaderQtdVoluntario = this.props.loaderQtdVoluntario
                 ? <LinearProgress mode="indeterminate" style={style.loader} />
                 : null

    const loaderQtdAcaoRealizada = this.props.loaderQtdAcaoRealizada
                 ? <LinearProgress mode="indeterminate" style={style.loader} />
                 : null
    return (
      <div className="Grid">
        <div className={`Grid-cell u-size1of4 ${styles.actions}`}>
          <div className={`${styles.qtd} u-posRelative`}>
            {this.state.volunteers}
            {loaderQtdVoluntario}
          </div>
          <hr className={styles.divider} />
          <p className={styles.text}>Voluntários<br />em seu comitê</p>
        </div>
        <div className={`Grid-cell u-size1of4 ${styles.actions}`}>
          <div className={`${styles.qtd} u-posRelative`}>
            {this.state.actionsTaken}
            {loaderQtdAcaoRealizada}
          </div>
          <hr className={styles.divider} />
          <p className={styles.text}>Ações<br />realizadas</p>
        </div>
        <div className={`Grid-cell u-size2of4 ${styles.actions}`}>
          <LeaderDashboardLinks />
        </div>
      </div>
    )
  }
}

style.loader = {
  backgroundColor: 'rgba(255, 255, 255, .3)',
  top: '142px',
  left: '50%',
  marginLeft: '-35px',
  position: 'absolute',
  width: '70px',
  height: '3px',
}

_Statistic.propTypes = {
  idComite: PropTypes.string,
  leader: PropTypes.object,
  loaderQtdVoluntario: PropTypes.bool,
  loaderQtdAcaoRealizada: PropTypes.bool,
  getVoluntariosComite: PropTypes.func,
  getAcoesRealizadas: PropTypes.func,
}

const mapStateToProps = state => ({
  leader: state.statistic.leader,
  loaderQtdVoluntario: state.statistic.loaderQtdVoluntario,
  loaderQtdAcaoRealizada: state.statistic.loaderQtdAcaoRealizada,
})

const mapActionsToProps = {
  getVoluntariosComite,
  getAcoesRealizadas,
}

const Statistic = connect(mapStateToProps, mapActionsToProps)(_Statistic)
export default Statistic
