import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
const Component = props => (
  <div>
    {props.routes.map((route, i) => (
      <Route
        key={i}
        path={route.path}
        exact={route.exact}
        render={() => (
        // pass the sub-routes down to keep nesting
          <route.component {...route} routes={route.routes} />
        )}
      />
    ))}
  </div>
)

Component.displayName = 'RouteWithSubRoutes'

Component.propTypes = {
  routes: PropTypes.array.isRequired,
}

Component.defaultProps = {
  // obj: [],
}

export default Component
