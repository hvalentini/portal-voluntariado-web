import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './Header.css'
import PillButtonKeyLimePie from '../../components/PillButtonKeyLimePie/PillButtonKeyLimePie'
import PillButtonPelorous from '../../components/PillButtonPelorous/PillButtonPelorous'
import PillButtonCerulean from '../../components/PillButtonCerulean/PillButtonCerulean'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class Header extends React.Component {
  constructor(props) {
    super(props)

    this.scrollTo = this.scrollTo.bind(this)
  }

  scrollTo(to, route) {
    const scroll = to != null ? to : document.getElementsByClassName('pv-SectionVideoAboutUs-moreAboutUs')[0].offsetTop
    const callback = route ? () => { this.props.history.push(route) } : () => {}

    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
              ? document.querySelector('html')
              : document.querySelector('body')

    animatedScrollTo(
      el,
      scroll,
      1000,
      callback,
    )
  }

  render() {
    return (
      <div>
        <header className="Grid">
          <div className="Grid-cell">
            <div className={styles.headerContent}>
              <div className="Grid-cell u-size4of12">
                <div className={styles.headerBoxLogo}>
                  <SvgIcon
                    style={{ height: '100%', width: '100%' }}
                    icon="icon-icon-8"
                  />
                </div>
              </div>
              <div className="Grid-cell u-size8of12">
                <div className={styles.headerBoxPillButton}>
                  <div className={styles.headerPillButton}>
                    {/* <a href="#video"> */}
                    <PillButtonKeyLimePie
                      text="Conheça"
                      onClick={() => { this.scrollTo(null, null) }}
                      style={{
                        marginRight: '28px',
                      }}
                    />
                    {/* </a> */}
                  </div>
                  <div className={styles.headerPillButton}>
                    <PillButtonPelorous
                      text="Faça Parte"
                      onClick={() => { this.scrollTo(0, '/cadastro/passo-1') }}
                      style={{
                        marginRight: '28px',
                      }}
                    />
                  </div>
                  <div className={styles.headerPillButton}>
                    <PillButtonCerulean
                      text="Entrar"
                      onClick={() => { this.scrollTo(0, '/entrar') }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

        </header>

      </div>
    )
  }
}

Header.propTypes = {
  history: PropTypes.object,
}

export default withRouter(Header)
