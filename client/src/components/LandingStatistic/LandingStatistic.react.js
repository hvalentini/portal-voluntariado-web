import React from 'react'
import PropTypes from 'prop-types'
import CountUp from 'react-countup'
import styles from './LandingStatistic.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class LandingStatistic extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      amount: <CountUp
        start={0}
        end={this.props.amount}
        duration={0.5}
      />,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.amount !== nextProps.amount) {
      this.setState({
        amount: <CountUp
          start={this.props.amount}
          end={nextProps.amount}
          duration={0.5}
        />,
      })
    }
  }

  render() {
    const iconName = this.props.iconName
    const amount = this.props.amount
    const description = this.props.description

    return (
      <div
        className="Grid-cell"
      >
        <div className={styles.container}>
          <div className="u-textCenter">
            <SvgIcon style={this.props.iconStyles} className={styles.svgIcon} icon={iconName} />
            <p style={this.props.amountStyles} className={styles.amount} title={amount}>
              {this.state.amount}
            </p>
            <p
              style={this.props.descriptionStyles}
              className={`${styles.description} ${styles.amount}`}
              title={description}
            >
              {description}
            </p>
          </div>
        </div>
      </div>
    )
  }
}

LandingStatistic.propTypes = {
  iconName: PropTypes.string,
  description: PropTypes.string,
  amount: PropTypes.number,
  iconStyles: PropTypes.object,
  amountStyles: PropTypes.object,
  descriptionStyles: PropTypes.object,
}

export default LandingStatistic
