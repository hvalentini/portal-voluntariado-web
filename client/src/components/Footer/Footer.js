import React from 'react'
import PropTypes from 'prop-types'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './Footer.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class Footer extends React.Component {

  static scrollTopElement() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
              ? document.querySelector('html')
              : document.querySelector('body')
    animatedScrollTo(
      el,
      0,
      1000,
    )
  }

  render() {
    return (
      <div
        id="footer"
        className={styles.container}
      >
        <div className={styles.footer}>
          <div className="Grid Grid--withGutter">
            <div className="Grid-cell u-size10of12 u-textCenter">
              <img
                className={styles.logo}
                src={'/images/logo-223x152.png'}
                alt="Logo Portal do Voluntariado"
              />
            </div>
            <div className="Grid-cell u-size2of12 u-floatRight u-textCenter">
              <SvgIcon
                className={styles.upIcon}
                icon="icon-icon-26"
                onClick={Footer.scrollTopElement}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Footer.propTypes = {
  footerPosition: PropTypes.func,
  footerPositionStyles: PropTypes.object,
}

export default Footer
