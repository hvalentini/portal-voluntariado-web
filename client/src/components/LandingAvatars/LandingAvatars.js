import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Avatar from '../Avatar/Avatar'
import { searchAvatarsLeft, searchAvatarsRight } from '../../actions/voluntario'
import styles from './LandingAvatars.css'

class _LandingAvatars extends React.Component {

  componentWillMount() {
    if (this.props.position === 'left') {
      this.props.searchAvatarsLeft(17)
    } else {
      this.props.searchAvatarsRight(17)
    }
  }

  renderAvatars() {
    let photos = this.props.photosLeft
    if (this.props.position === 'right') { photos = this.props.photosRight }
    return (
      photos.map((item, i) => (
        <div
          key={i}
          style={{
            top: this.props.configPhotos[i].top,
            left: this.props.configPhotos[i].left,
            position: 'absolute',
          }}
        >
          <Avatar
            url={item.urlAvatar}
            key={i}
            disabled={this.props.configPhotos[i].disabled}
            opacity={this.props.configPhotos[i].opacity}
            size={this.props.configPhotos[i].size}
            borderLess={this.props.configPhotos[i].border}
          />
        </div>
      ))
    )
  }

  render() {
    if (!this.props.photosLeft || !this.props.photosRight) return null

    let styleBox = styles.landingBoxLeft

    if (this.props.position === 'right') { styleBox = styles.landingBoxRight }

    return (
      <div className={styleBox} >
        {this.renderAvatars()}
      </div>
    )
  }
}

_LandingAvatars.propTypes = {
  photosLeft: PropTypes.array.isRequired,
  photosRight: PropTypes.array.isRequired,
  searchAvatarsLeft: PropTypes.func.isRequired,
  searchAvatarsRight: PropTypes.func.isRequired,
  configPhotos: PropTypes.array.isRequired,
  position: PropTypes.string.isRequired,
}

_LandingAvatars.defaultProps = {
  position: 'left',
}

const mapStateToProps = state => ({
  photosLeft: state.landingAvatars.photosLeft,
  photosRight: state.landingAvatars.photosRight,
  configPhotos: state.landingAvatars.configPhotos,
})

const mapActionsToProps = {
  searchAvatarsLeft,
  searchAvatarsRight,
}

const LandingAvatars = connect(mapStateToProps, mapActionsToProps)(_LandingAvatars)
export default LandingAvatars
