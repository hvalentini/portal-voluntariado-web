import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Dialog, FlatButton } from 'material-ui'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './LandingLeaderNewInstitution.css'
import NewLocal from '../NewLocal/NewLocal'
import { closeDialog, getOneLocal } from '../../actions/newLocal'

class _LandingLeaderNewInstitution extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }
  constructor(props) {
    super(props)
    this.componentWillMount = this.componentWillMount.bind(this)
    this.handleCloseDialog = this.handleCloseDialog.bind(this)
  }

  componentWillMount() {
    if (window.location.pathname.split('/')[3]) {
      this.props.getOneLocal(window.location.pathname.split('/')[3])
    }
  }

  componentDidMount() {
    _LandingLeaderNewInstitution.scrollTo()
  }

  handleCloseDialog() {
    this.props.closeDialog()
    if (!this.props.newLocal.erro) {
      // Se não houver erro redirecione
      this.props.history.push('/lider/instituicoes-parceiras')
    }
  }

  render() {
    return (
      <div className={styles.formBg}>
        <h3 className={styles.title}>
          {
            this.props.newLocal.local._id
            ? 'Editar Instituição'
            : 'Incluir novo local'
          }
        </h3>
        <NewLocal />
        <Dialog
          actions={
            <FlatButton
              label="Ok"
              secondary
              className={styles.button}
              onTouchTap={this.handleCloseDialog}
              keyboardFocused
            />
          }
          modal={false}
          open={this.props.openDialog}
          onRequestClose={this.handleCloseDialog}
          bodyStyle={{ textAlign: 'center', color: '#484848', fontSize: '18px' }}
        >
          {this.props.serviceMessage}
        </Dialog>
      </div>
    )
  }
}

_LandingLeaderNewInstitution.propTypes = {
  openDialog: PropTypes.bool,
  closeDialog: PropTypes.func,
  newLocal: PropTypes.object,
  history: PropTypes.object,
  getOneLocal: PropTypes.func,
  serviceMessage: PropTypes.string,
}

const mapStateToProps = state => ({
  openDialog: state.newLocal.openDialog,
  serviceMessage: state.newLocal.serviceMessage,
  newLocal: state.newLocal,
})

const mapActionsToProps = {
  closeDialog,
  getOneLocal,
}

const LandingLeaderNewInstitution = connect(mapStateToProps,
  mapActionsToProps)(_LandingLeaderNewInstitution)

export default withRouter(LandingLeaderNewInstitution)
