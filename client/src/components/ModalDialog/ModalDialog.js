import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './ModalDialog.css'

const customContentStyle = {
  color: 'red',
  width: '375px',
  height: '230px',
}


class ModalDialog extends React.Component {

  render() {
    let icon = 'icon-icon-56' // Icone de success
    let classIcon = styles.iconSuccess

    if (this.props.type === 'delete') {
      icon = 'icon-icon-54'
      classIcon = styles.iconDelete
    }
    if (this.props.type === 'warning' || this.props.type === 'warningDelete') {
      icon = 'icon-icon-55'
      classIcon = styles.iconWarning
    }

    let actions = [
      <FlatButton
        label={this.props.textFirst}
        primary
        keyboardFocused
        onTouchTap={this.props.actionFirst}
      />,
    ]

    if (this.props.textSecond) {
      actions = [
        <FlatButton
          label={this.props.textFirst}
          primary
          onTouchTap={this.props.actionFirst}
        />,
        <FlatButton
          label={this.props.textSecond}
          primary
          keyboardFocused
          onTouchTap={this.props.actionSecond}
        />,
      ]
    }

    const styleBtn = this.props.textSecond ? styles.btn : styles.btnOnly
    return (
      <Dialog
        title=""
        actions={actions}
        modal
        contentStyle={customContentStyle}
        open={this.props.open}
        bodyClassName={styles.body}
        actionsContainerClassName={styleBtn}
        contentClassName={styles.container}
      >
        <SvgIcon
          className={classIcon}
          icon={icon}
        />
        <div className={styles.msg}>{this.props.msg}</div>
        <hr className={styles.separator} />
      </Dialog>
    )
  }
}

ModalDialog.propTypes = {
  type: PropTypes.string,
  open: PropTypes.bool,
  textFirst: PropTypes.string,
  msg: PropTypes.string,
  actionFirst: PropTypes.func,
  textSecond: PropTypes.string,
  actionSecond: PropTypes.func,
}



export default ModalDialog
