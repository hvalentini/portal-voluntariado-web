import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import get from 'lodash.get'
import LandingStatistic from '../../components/LandingStatistic/LandingStatistic.react'
import LandingStatement from '../../components/LandingStatement/LandingStatement'
import LandingHighlight from '../../components/LandingHighlight/LandingHighlight.react'
import styles from './LandingInfo.css'
import { getAmountBeneficiated, getAmountActions, getGallery } from '../../actions/acoes'
import { getAmountVolunteers } from '../../actions/voluntario'

class _LandingInfo extends React.Component {

  componentDidMount() {
    this.props.getAmountBeneficiated()
    this.props.getAmountActions()
    this.props.getAmountVolunteers()
    this.props.getGallery()
  }

  render() {
    const gallery = get(this.props.acoes, 'gallery', [])
    const defaultGallery = ['/images/acao1.jpg', '/images/acao2.jpg', '/images/acao3.jpg']
    const amountStyles = {
      fontWeight: '400',
    }
    const descriptionStyles = {
      fontWeight: '300',
    }
    return (
      <div
        className="Grid"
        style={{ paddingRight: '30px' }}
      >
        <div className="Grid-cell u-lg-size5of12">
          <LandingStatistic
            iconName="icon-icon-4"
            amount={this.props.voluntario.amountVolunteers}
            description="Voluntários"
            amountStyles={amountStyles}
            descriptionStyles={descriptionStyles}
          />
          <LandingStatistic
            iconName="icon-icon-3"
            amount={this.props.acoes.amountBeneficiated}
            description="Pessoas beneficiadas"
            amountStyles={amountStyles}
            descriptionStyles={descriptionStyles}
          />
          <LandingStatistic
            iconName="icon-icon-2"
            amount={this.props.acoes.amountActions}
            description="Ações sociais"
            amountStyles={amountStyles}
            descriptionStyles={descriptionStyles}
          />
        </div>
        <div
          className="Grid-cell u-lg-size7of12"
          style={{ boxShadow: '0px 0px 50px 0px rgba(0, 0, 0, 0.4)' }}
        >
          <div className={`Grid ${styles.container}`}>
            <div className={`${styles.containerHighlight} Grid-cell u-lg-size6of12`}>
              <LandingHighlight
                src={defaultGallery[0] || get(gallery, '1')}
                width="100%"
                height="100%"
                alt="imagen de informação"
              />
            </div>
            <div className={`${styles.containerHighlight} Grid-cell u-lg-size6of12`}>
              <LandingHighlight
                src={defaultGallery[1] || get(gallery, '0')}
                width="100%"
                height="100%"
                alt="imagen de informação"
              />
            </div>
            <div className={`${styles.containerHighlight} Grid-cell u-lg-size6of12`}>
              <LandingStatement
                size="100%"
              >
                A cada ação percebo claramente o quanto o trabalho voluntário é importante na vida de todos.
              </LandingStatement>
            </div>
            <div className={`${styles.containerHighlight} Grid-cell u-lg-size6of12`}>
              <LandingHighlight
                src={defaultGallery[2] || get(gallery, '2')}
                width="100%"
                height="100%"
                alt="imagen de informação"
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

_LandingInfo.propTypes = {
  acoes: PropTypes.object,
  voluntario: PropTypes.object,
  getAmountBeneficiated: PropTypes.func,
  getAmountActions: PropTypes.func,
  getAmountVolunteers: PropTypes.func,
  getGallery: PropTypes.func,
}

const mapStateToProps = state => ({
  acoes: state.acoes,
  voluntario: state.voluntario,
})

const mapActionsToProps = {
  getAmountBeneficiated,
  getAmountActions,
  getAmountVolunteers,
  getGallery,
}

const LandingInfo = connect(mapStateToProps, mapActionsToProps)(_LandingInfo)
export default LandingInfo
