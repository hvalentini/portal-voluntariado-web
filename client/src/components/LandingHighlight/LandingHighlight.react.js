import React from 'react'
import PropTypes from 'prop-types'
import styles from './LandingHighlight.css'

const style = {}

const LandingHighlight = (props) => {
  style.container = {
    width: props.width,
    height: props.height,
  }

  // width: props.width,
  style.img = {
    height: props.height,
  }

  return (
    <div
      className={`${styles.container} u-textCenter`}
      style={style.container}
      width={props.width}
    >
      <img
        className={styles.highlight}
        alt={props.alt}
        src={props.src}
        style={style.img}
      />
    </div>
  )
}

LandingHighlight.displayName = 'LandingHighlight'

LandingHighlight.defaultProps = {
  width: '300px',
  height: '340px',
  alt: 'Imagem',
}

LandingHighlight.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  alt: PropTypes.string,
  src: PropTypes.string,
}

export default LandingHighlight
