import React from 'react'
import PropTypes from 'prop-types'
import styles from './Action.css'
import InfoActionsGallery from '../InfoActionsGallery/InfoActionsGallery.react'
import ActionsGallery from '../ActionsGallery/ActionsGallery.react'

const Action = props => (
  <div className={`Grid ${styles.action}`}>
    <div className="Grid-cell u-size4of12">
      <InfoActionsGallery
        acao={props.action}
        idVoluntario={props.idVoluntario}
        userComite={props.userComite}
      />
    </div>
    <div className="Grid-cell u-size8of12">
      <ActionsGallery gallery={props.gallery} acao={props.action} />
    </div>
  </div>
)

Action.propTypes = {
  action: PropTypes.object,
  userComite: PropTypes.string,
  idVoluntario: PropTypes.string,
  gallery: PropTypes.array,
}

export default Action
