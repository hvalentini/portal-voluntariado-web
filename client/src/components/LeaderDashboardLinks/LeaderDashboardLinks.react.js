import React from 'react'
import { Link } from 'react-router-dom'
import styles from './LeaderDashboardLinks.css'

const LeaderDashboardLinks = () => (
  <div className={styles.leaderDashboardLinks}>
    <Link to="/lider/instituicoes-parceiras">
        Instituições Parceiras
    </Link>
    <Link to="/lider/acoes-voluntarias">
        Ações voluntárias
    </Link>
    <Link to="/lider/gestao-de-voluntarios">
        Gestão dos Voluntários
    </Link>
  </div>
)

export default LeaderDashboardLinks
