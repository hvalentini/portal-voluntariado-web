import React from 'react'
import PropTypes from 'prop-types'
import { SelectField, MenuItem } from 'material-ui'
import { connect } from 'react-redux'
import {
  loadCities,
  citySelectUpdate,
} from '../../actions/citySelect'

class _CitySelect extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  componentWillMount() {
    this.props.loadCities()
    if (this.props.value && this.props.value._id) {
      this.props.citySelectUpdate({ selected: this.props.value._id })
    }
  }

  handleChange(x, i, value) {
    this.props.citySelectUpdate({ selected: value })
    if (this.props.handleChange) {
      this.props.handleChange(this.props.citySelect.list[i])
    }
  }

  render() {
    const selected = this.props.citySelect.selected || this.props.value
      ? this.props.value._id
      : ''
    return (
      <SelectField
        name="selected"
        style={this.props.style}
        floatingLabelText={this.props.label}
        floatingLabelStyle={this.props.floatingLabelStyle}
        value={selected}
        onChange={this.handleChange}
        fullWidth
        disabled={this.props.disabled}
      >
        {
          this.props.citySelect.list.map((item) => {
            if (!item) return null
            return (
              <MenuItem
                className="SelectBkg"
                value={item._id}
                primaryText={`${item.nome} - ${item.uf}`}
                key={item._id}
              />
            )
          })
        }
      </SelectField>
    )
  }
}

_CitySelect.displayName = 'CitySelect'

_CitySelect.defaultProps = {
  value: {},
  disabled: false,
  floatingLabelStyle: {},
  style: { textAlign: 'left' },
}

_CitySelect.propTypes = {
  citySelect: PropTypes.object.isRequired,
  floatingLabelStyle: PropTypes.object,
  style: PropTypes.object,
  value: PropTypes.object,
  loadCities: PropTypes.func.isRequired,
  citySelectUpdate: PropTypes.func.isRequired,
  label: PropTypes.string,
  handleChange: PropTypes.func,
  disabled: PropTypes.bool,
}

const mapStateToProps = state => ({
  citySelect: state.citySelect,
})

const mapActionsToProps = {
  loadCities,
  citySelectUpdate,
}

const CitySelect = connect(mapStateToProps, mapActionsToProps)(_CitySelect)

export default CitySelect
