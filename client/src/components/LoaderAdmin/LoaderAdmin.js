import React from 'react'
import CircularProgress from 'material-ui/CircularProgress'
import styles from './LoaderAdmin.css'

const LoaderAdmin = () => (
  <div className={styles.loader}>
    <CircularProgress size={80} thickness={5} color="#00BEE7" />
  </div>
)

export default LoaderAdmin
