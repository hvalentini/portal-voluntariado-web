import React from 'react'
import PropTypes from 'prop-types'
import styles from './IconButton.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const IconButton = props => (
  <button
    className={`${styles.button} buttonNoPrint`}
    style={{
      background: `linear-gradient(to bottom right, ${props.background1}, ${props.background2})`,
      ...props.style,
    }}
    onClick={props.onClick}
  >
    <SvgIcon icon={props.icon} className={`${styles.icon} noPrint`} />
  </button>
)

IconButton.propTypes = {
  background1: PropTypes.string,
  background2: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.string,
  style: PropTypes.object,
}

export default IconButton
