import React from 'react'
import PropTypes from 'prop-types'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import styles from './LandingLeaderVoluntaryActionsHeader.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import {
  getAcoesOrder,
  handlePrintActions,
} from '../../actions/acoes'

const style = {}

const order = [
  {
    name: 'Ação com mais beneficiados',
    value: '{ "totalBeneficiados": -1 }',
  },
  {
    name: 'Ação com menos beneficiados',
    value: '{ "totalBeneficiados": 1 }',
  },
  {
    name: 'Ação com mais participantes',
    value: '{ "totalParticipantes": -1 }',
  },
  {
    name: 'Ação com menos participantes',
    value: '{ "totalParticipantes": 1 }',
  },
  {
    name: 'Ação que os voluntários mais gostaram',
    value: '{ "totalQueGostaram": -1 }',
  },
  {
    name: 'Ação que os voluntários menos gostaram',
    value: '{ "totalQueGostaram": 1 }',
  },
  {
    name: 'Data cadastro (1-31)',
    value: '{ "dataCadastro": 1 }',
  },
  {
    name: 'Data cadastro (31-1)',
    value: '{ "dataCadastro" : -1}',
  },
  {
    name: 'Nome da ação (A-Z)',
    value: '{ "nomeNormalizado" : 1}',
  },
  {
    name: 'Nome da ação (Z-A)',
    value: '{ "nomeNormalizado" : -1}',
  },
  {
    name: 'Maior valor gasto',
    value: '{ "valorGasto": -1 }',
  },
  {
    name: 'Menor valor gasto',
    value: '{ "valorGasto": 1 }',
  },
  {
    name: 'Tipo da ação (A-Z)',
    value: '{ "nomeTipoAcao": 1 }',
  },
  {
    name: 'Tipo da ação (Z-A)',
    value: '{ "nomeTipoAcao": -1 }',
  },
]

class _LandingLeaderVoluntaryActionsHeader extends React.Component {
  constructor(props) {
    super(props)
    this.onChangeActions = this.onChangeActions.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  onChangeActions(event, index, value) {
    const obj = this.props.filters
    obj.comite = `${this.props.comite._id}`
    this.props.getAcoesOrder({ filtro: JSON.stringify(obj), ordenacao: value })
  }

  onPrint() {
    const payload = this.props.filters

    payload.comite = this.props.comite._id
    this.props.handlePrintActions(payload, this.props.ordenacao, () => {
      window.print()
    })
  }

  render() {
    return (
      <div>
        <div className="Grid Grid-withGutter contentLeaderBottom">
          <div className="Grid-cell u-size6of12">
            <div>
              <h3 className="contentLeaderTitle">Ações Voluntárias</h3>
            </div>
          </div>
          <div className="Grid-cell u-size4of12">
            <p className={styles.order}> Ordenar por: </p>
            <SelectField
              value={this.props.orderSelected}
              onChange={this.onChangeActions}
              labelStyle={style.menuStyle}
              fullWidth
              style={{ overflow: 'hidden' }}
            >
              {
                order.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item.value}
                    primaryText={item.name}
                    key={i}
                  />
                ))
              }
            </SelectField>
          </div>
          <div className="Grid-cell u-size1of12 u-textCenter">
            <div className={styles.buttonAdd}>
              <Link to="/lider/nova-acao" className={styles.linkAdd}>
                <SvgIcon
                  className={styles.iconAdd}
                  icon="icon-icon-7"
                />
              </Link>
            </div>
          </div>
          <div className="Grid-cell u-size1of12 u-textCenter noPrint">
            <button
              onClick={this.onPrint}
              className={styles.printButton}
            >
              <SvgIcon
                className={styles.printIcon}
                icon="icon-icon-45"
              />
            </button>
          </div>
        </div>
      </div>
    )
  }
}

style.menuStyle = {
  color: '#484848',
  fontSize: '18px',
}

_LandingLeaderVoluntaryActionsHeader.propTypes = {
  comite: PropTypes.object,
  orderSelected: PropTypes.number,
  filters: PropTypes.object,
  getAcoesOrder: PropTypes.func,
  handlePrintActions: PropTypes.func,
  ordenacao: PropTypes.object,
}

const mapStateToProps = state => ({
  comite: state.loggedUser.comite,
  orderSelected: state.acoes.orderSelected,
  ordenacao: state.acoes.ordenacao,
  filters: state.filters.acoes,
})

const mapActionsToProps = {
  getAcoesOrder,
  handlePrintActions,
}

const LandingLeaderVoluntaryActionsHeader = connect(mapStateToProps,
mapActionsToProps)(_LandingLeaderVoluntaryActionsHeader)
export default LandingLeaderVoluntaryActionsHeader
