import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './BoxInstitutions.css'
import { deleteCompany } from '../../actions/empresas'
import { setModal } from '../../actions/modal'

class _BoxInstitutions extends React.Component {

  constructor(props) {
    super(props)
    this.handleOpenModal = this.handleOpenModal.bind(this)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina, id) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina, id)
  }

  handleInstitutions() {
    return (
      this.props.partners.list.map((item, i) => {
        const linkEditInstitution = this.props.user.adminLogin
                          ? `/admin/instituicoes-parceiras/editar/${item._id}`
                          : `/lider/editar-instituicao/${item._id}`

        return (
          <Link className={styles.link} to={linkEditInstitution} key={i}>
            <div className={styles.root} key={item._id}>
              <div className={`${styles.info}`}>
                <p className={styles.name}>{item.nome}</p>
                <p className={styles.address}>{item.endereco}</p>
              </div>

              <div className={`${styles.edit}`}>
                <p>{item.telefone}</p>
              </div>
              <hr className={`${styles.separator}`} />
            </div>
          </Link>
        )
      })
    )
  }

  render() {
    return (
      <div className={styles.containerBox}>
        {this.handleInstitutions()}
      </div>
    )
  }
}

_BoxInstitutions.propTypes = {
  partners: PropTypes.object,
  setModal: PropTypes.func,
  user: PropTypes.object,
}


const mapStateToProps = state => ({
  partners: state.partners,
})

const mapActionsToProps = {
  deleteCompany,
  setModal,
}

const BoxCompany = connect(mapStateToProps, mapActionsToProps)(_BoxInstitutions)
export default BoxCompany
