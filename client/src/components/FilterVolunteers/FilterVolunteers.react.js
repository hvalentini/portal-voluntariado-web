import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TextField, RadioButton, RadioButtonGroup } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import noAccents from 'remove-accents'
import PillButton from '../PillButton/PillButton'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { filterVolunteers, getVolunterOrder } from '../../actions/voluntario'
import styles from './FilterVolunteers.css'
import muiTheme from '../../muiTheme'

const style = {}

class _FilterVolunteers extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      btnBottom: 0,
      filtro: {
        options: '',
        nomeNormalizado: '',
      },
    }

    this.setSearchTerm = this.setSearchTerm.bind(this)
    this.changeRadio = this.changeRadio.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
    this.handleFilterVolunteers = this.handleFilterVolunteers.bind(this)
  }

  setSearchTerm(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    this.setState({ ...this.state, filtro })
  }

  handleFilterVolunteers() {
    const filtro = { ...this.state.filtro }

    filtro.nomeNormalizado = noAccents(filtro.nomeNormalizado).trim()

    Object.keys(filtro).forEach((key) => {
      if (!filtro[key] || filtro[key] === '' || filtro[key] === 'Listar todos voluntários') delete filtro[key]
    })
    filtro.comite = this.props.idComite
    this.props.filterVolunteers(filtro)
    const typeOrder = 'props.info.nomeNormalizado'
    this.props.getVolunterOrder({ orderOfVolunteers: 'nameasc', typeOrder })
    // arrumar o ordernar
  }

  changeRadio(e) {
    const filtro = { ...this.state.filtro }
    filtro[e.target.name] = e.target.value
    this.setState({ ...this.state, filtro })
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  render() {
    return (
      <div>
        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <div
            className={styles.wrapperFilter}
            style={{ maxHeight: `${window.innerHeight - 230}px` }}
            onScroll={this.adjustButtonPlacing}
          >
            <TextField
              floatingLabelText="Pesquisar voluntários"
              name="nomeNormalizado"
              underlineStyle={style.line}
              onChange={ev => this.setSearchTerm(ev)}
              maxLength={100}
              inputStyle={{ marginLeft: '27px' }}
              hintStyle={{ marginLeft: '27px' }}
              style={{ marginTop: '-15px' }}
              floatingLabelStyle={{ marginLeft: '27px' }}
              floatingLabelFocusStyle={{ marginLeft: '0' }}
              floatingLabelShrinkStyle={{ marginLeft: '0' }}
              fullWidth
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />

            <div className={styles.containerCheckbox}>
              <p className={styles.text}>Participaram de ações</p>
              <RadioButtonGroup
                name="options"
                defaultSelected="Listar todos voluntários"
                onChange={ev => this.changeRadio(ev)}
              >
                <RadioButton
                  value="Listar todos voluntários"
                  label="Listar todos voluntários"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="Participou de alguma ação"
                  label="Participou de alguma ação"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="Ja participou de ação este ano"
                  label="Ja participou de ação este ano"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="Nunca participou de ação"
                  label="Nunca participou de ação"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
              </RadioButtonGroup>
            </div>

            <div
              style={{ bottom: `${this.state.btnBottom}px` }}
              className={styles.btn}
            >
              <PillButton
                text="Filtrar"
                onClick={() => this.handleFilterVolunteers()}
                loader={this.props.loader}
              />
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterVolunteers.propTypes = {
  idComite: PropTypes.string,
  getVolunterOrder: PropTypes.func,
  filterVolunteers: PropTypes.func,
  loader: PropTypes.bool,
}

const mapStateToProps = state => ({
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  filterVolunteers,
  getVolunterOrder,
}

const FilterVolunteers = connect(mapStateToProps, mapActionsToProps)(_FilterVolunteers)
export default FilterVolunteers
