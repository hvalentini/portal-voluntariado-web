import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { AutoComplete, MenuItem, ListItem, Avatar } from 'material-ui'
import styles from './LeaderAutocomplete.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { getVoluntariosAutoComplete } from '../../actions/voluntario'
import { addSelectedUsers, filterDataSource, cleanEditUser } from '../../actions/usersAutocomplete'

const style = {}

const menuItemStyle = { borderBottom: '1px solid #b1b1b1' }

class _LeaderAutocomplete extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
    }

    this.handleDataSource = this.handleDataSource.bind(this)
    this.updateSearchText = this.updateSearchText.bind(this)
    this.chosenVolunteer = this.chosenVolunteer.bind(this)
  }

  componentDidMount() {
    //  caso onde nao existem voluntarios
    if (!this.props.comite.voluntarios) {
      return (
        this.setState({
          searchText: 'Não existem voluntários cadastrados neste comitê.',
        })
      )
    }

    const lider = this.props.comite.liderSocial ? this.props.comite.liderSocial.nome : ''
    return this.setState({
      searchText: lider,
    })
  }

  handleDataSource() {
    const data = this.props.dataSource
    if (data && data.length) {
      const autocompleteList = data.map((item, i) => {
        const obj = item
        const urlAvatar = item.urlAvatar || '/images/user-avatar.svg'
        obj.text = `${item.nome}`
        obj.value = (
          <MenuItem
            style={i !== (data.length - 1) ? menuItemStyle : {}}
            key={item._id}
          >
            <ListItem
              primaryText={(
                <span
                  style={{
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    display: 'block', 
                  }}
                >
                  {item.nome}
                </span>
              )}
              secondaryText={item.cargoNome}
              leftAvatar={<Avatar src={urlAvatar} />}
            />
          </MenuItem>
        )
        return obj
      })
      return autocompleteList
    }
    return []
  }

  updateSearchText(text) {
    let sText = text
    if (typeof text === 'object') {
      sText = text.target.value
    }
    if ((this.props.dataSource && this.props.dataSource.length > 0) && sText != '') {
      this.props.filterDataSource({
        texto: sText,
      })
    } else {
      if (!sText) { this.props.cleanEditUser() }
      this.props.getVoluntariosAutoComplete({
        texto: sText,
        comite: this.props.comite._id,
        permitirBloqueado: false,
      })
    }
    this.setState({
      searchText: sText,
    })
  }

  chosenVolunteer(chosenRequest) {
    if (typeof chosenRequest !== 'object') { return false }
    this.props.addSelectedUsers(chosenRequest, 'admin')
  }

  render() {
    if (!this.props.comite._id) { return null }
    const disabled = !this.props.comite.voluntarios ? true : false
    return (
      <div className={styles.usersAutocomplete}>
        <div className={styles.searchIcon}>
          <SvgIcon icon="icon-icon-29" />
        </div>
        <AutoComplete
          dataSource={this.handleDataSource()}
          onUpdateInput={text => this.updateSearchText(text)}
          searchText={this.state.searchText}
          onNewRequest={this.chosenVolunteer}
          floatingLabelText="Líder do comitê"
          filter={AutoComplete.noFilter}
          fullWidth
          openOnFocus
          onFocus={this.updateSearchText}
          floatingLabelStyle={style.flotLabel}
          errorText={this.props.errorTextUser}
          listStyle={{ maxHeight: '240px', overflowY: 'auto' }}
          disabled={disabled}
        />
      </div>
    )
  }
}

style.flotLabel = {
  color: '#9b9b9b',
  fontSize: '18px',
}

_LeaderAutocomplete.propTypes = {
  dataSource: PropTypes.array.isRequired,
  comite: PropTypes.object,
  getVoluntariosAutoComplete: PropTypes.func,
  addSelectedUsers: PropTypes.func,
  errorTextUser: PropTypes.string,
  filterDataSource: PropTypes.func,
}

const mapStateToProps = state => ({
  ...state.usersAutocomplete,
  comite: state.comites,
})

const mapActionsToProps = {
  getVoluntariosAutoComplete,
  addSelectedUsers,
  filterDataSource,
  cleanEditUser,
}

const LeaderAutocomplete = connect(mapStateToProps, mapActionsToProps)(_LeaderAutocomplete)
export default LeaderAutocomplete
