import React from 'react'
import PropTypes from 'prop-types'
import IconButton from '../IconButton/IconButton'

const IconButtonPlus = props => (
  <IconButton
    {...props}
    icon={props.icon}
    onClick={props.onClick}
    background1={props.background1}
    background2={props.background2}
  />
)

IconButtonPlus.propTypes = {
  background1: PropTypes.string,
  background2: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.string,
}

export default IconButtonPlus
