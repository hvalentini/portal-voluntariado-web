import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Scrollbars } from 'react-custom-scrollbars'
import styles from './Notification.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { getNotifications } from '../../actions/notificacao'

class _Notification extends React.Component {

  static getIconNotification(tipo) {
    switch (tipo) {
      case 'Novo lider social':
        return (
          <SvgIcon icon="icon-icon-24" style={{ fontSize: '28px' }} />
        )
      case 'Novo voluntário':
        return (
          <SvgIcon icon="icon-icon-25" style={{ fontSize: '28px' }} />
        )
      case 'Voluntário quer participar da ação':
        return (
          <SvgIcon icon="icon-icon-25" style={{ fontSize: '28px' }} />
        )
      case 'Nova ação cadastrada pelo seu comitê':
        return (
          <SvgIcon icon="icon-icon-20" style={{ fontSize: '28px' }} />
        )
      case 'Você participou de uma ação':
        return (
          <SvgIcon icon="icon-icon-21" style={{ fontSize: '28px' }} />
        )
      case 'Mensagem Admin':
        return (
          <SvgIcon icon="icon-icon-22" style={{ fontSize: '28px' }} />
        )
      case 'Voluntário gostou da ação':
        return (
          <SvgIcon icon="icon-icon-18" style={{ fontSize: '28px' }} />
        )
      default:
        return (
          <SvgIcon icon="icon-icon-23" style={{ fontSize: '28px' }} />
        )
    }
  }

  componentWillMount() {
    this.props.getNotifications({
      voluntarioId: this.props.loggedUser._id,
      dataUltimoAcesso: this.props.loggedUser.dataUltimoAcesso,
    })
  }

  renderNotifications() {
    const notific = this.props.notificacoes
    return (
      notific.map((item, i) => (
        <tr key={i} className={styles.line}>
          <td className={styles.icon}>{_Notification.getIconNotification(item.tipo)}</td>
          <td className={styles.text}>
            {item.texto}
          </td>
        </tr>
      ))
    )
  }

  render() {
    if (this.props.notificacoes.length === 0) {
      return (
        <div className={styles.notificacao}>
          <div className={styles.noNotification}>
            <p>Você não possui notificações</p>
          </div>
        </div>
      )
    }
    return (
      <div className={styles.notificacao}>
        <Scrollbars autoHide>
          <table className={styles.tabela}>
            <tbody>
              {this.renderNotifications()}
            </tbody>
          </table>
        </Scrollbars>
      </div>
    )
  }
}

_Notification.propTypes = {
  notificacoes: PropTypes.array,
  loggedUser: PropTypes.object,
  getNotifications: PropTypes.func,
}

const mapStateToProps = state => ({
  notificacoes: state.notificacao.notificacoes,
})

const mapActionsToProps = {
  getNotifications,
}

const Notification = connect(mapStateToProps, mapActionsToProps)(_Notification)

export default Notification
