import React from 'react'
import PropTypes from 'prop-types'
import styles from './IconBalls.css'

const IconBalls = (props) => {
  let styleName = styles.ballsYellow
  if (props.icon === 'green') { styleName = styles.ballsGreen }

  let boxImage = { height: '45px', width: '45px' }
  if (props.size === 'small') { boxImage = { height: '34px', width: '34px' } }
  if (props.size === 'large') { boxImage = { height: '77px', width: '77px' } }
  return (
    <div
      className={styleName}
      style={{ background: props.background, ...boxImage }}
    >

    </div>
  )
}

IconBalls.propTypes = {
  background: PropTypes.string,
  icon: PropTypes.string,
  size: PropTypes.string,
}

IconBalls.defaultProps = {
  icon: 'yellow',
  size: 'large',
}

export default IconBalls
