import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './HeaderInternalAdminAction.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { actionClear } from '../../actions/actionTypes'

class _HeaderInternalAdminAction extends React.Component {
  constructor(props) {
    super(props)
    this.handleNewAction = this.handleNewAction.bind(this)
  }

  handleNewAction() {
    this.props.actionClear()
  }
  render() {
    return (
      <div className={styles.container}>
        <div className="Grid Grid-withGutter">
          <div className="Grid-cell u-size7of12">
            <h3 className={styles.title}>{this.props.title}</h3>
            <Link
              to={this.props.to}
              className={styles.linkAdd}
              onClick={() => this.handleNewAction()}
            >
              <div className={styles.buttonAdd}>
                <SvgIcon
                  className={styles.iconAdd}
                  icon="icon-icon-7"
                />
              </div>
              <span>ADICIONAR</span>
            </Link>
          </div>
        </div>
        <hr className={styles.separator} />
      </div>
    )
  }
}

_HeaderInternalAdminAction.propTypes = {
  title: PropTypes.string,
  to: PropTypes.string,
  actionClear: PropTypes.func,
}


const mapStateToProps = () => ({})

const mapActionsToProps = {
  actionClear,
}

const HeaderInternalAdminAction = connect(mapStateToProps,
  mapActionsToProps)(_HeaderInternalAdminAction)

export default HeaderInternalAdminAction
