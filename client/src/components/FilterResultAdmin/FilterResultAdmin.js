import React from 'react'
import PropTypes from 'prop-types'
import styles from './FilterResultAdmin.css'

const FilterResultAdmin = props => (
  <div className={styles.root} id="FilterResultAdmin">
    {props.children}
  </div>
)

FilterResultAdmin.propTypes = {
  children: PropTypes.array,
}

export default FilterResultAdmin
