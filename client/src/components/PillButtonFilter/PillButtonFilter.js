import React from 'react'
import PropTypes from 'prop-types'
import PillButton from '../../components/PillButton/PillButton'

const PillButtonFilter = (props) => (
  <div>
    <PillButton
      {...props}
      backgroundColor="#00BEE7"
      style={{fontSize: '16px', padding:'7px 68px', textTransform: 'uppercase', color: '#fff'}}
    />

  </div>
)

PillButtonFilter.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  opacity: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
}

export default PillButtonFilter
