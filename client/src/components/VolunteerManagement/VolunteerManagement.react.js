import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import CircularProgress from 'material-ui/CircularProgress'
import { orderBy, get } from 'lodash'
import styles from './VolunteerManagement.css'
import VolunteerInfo from '../VolunteerInfo/VolunteerInfo.react'
import VolunteerManagementHeader from '../VolunteerManagementHeader/VolunteerManagementHeader.react'
import { fetchVolunteers } from '../../actions/voluntario'


const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  fontFamily: 'Chantilly-Serial',
})


class _VolunteerManagement extends React.Component {

  componentDidMount() {
    this.props.fetchVolunteers(this.props.loggedUser.comite._id)
  }

  renderVolunteers() {
    const order = this.props.voluntario.orderOfVolunteers
    const typeOrder = this.props.voluntario.typeOrder
    const volunterList = get(this.props.voluntario, 'filterVolunteers').length > 0
          ? get(this.props.voluntario, 'filterVolunteers')
          : get(this.props.voluntario, 'volunteers')

    let volunteerInfo
    if (volunterList !== 0 && this.props.voluntario.search === false) {
    // exibe o array de voluntario inicial
      volunteerInfo = volunterList.map((item, i) =>
        <VolunteerInfo
          info={item}
          key={i}
          id={`key${i}`}
          idJob={`job${i}`}
        />,
      )
    } else if (get(this.props.voluntario, 'filterVolunteers').length > 0) {
      // exibe resultado obtido pelos radioButton
      volunteerInfo = volunterList.map((item, i) =>
        <VolunteerInfo
          info={item}
          key={i}
          id={`key${i}`}
          idJob={`job${i}`}
        />,
      )
    } else {
      volunteerInfo = <p className={styles.result}>Nenhum voluntario encontrado</p>
    }

    let volunteers
    if (get(this.props.voluntario, 'searchTerm').length > 0 || this.props.voluntario.term.length > 0) {
      // exibe resultados obtidos atraves do campo textField
      if (this.props.voluntario.searchTerm.length > 0) {
        volunteers = this.props.voluntario.searchTerm.map((item, i) =>
          <VolunteerInfo
            info={item}
            key={i}
            id={`key${i}`}
            idJob={`job${i}`}
          />,
         )
      } else { volunteers = <p className={styles.result}>Nenhum voluntario encontrado</p> }
    } else {
      volunteers = volunteerInfo
    }

    volunteers = volunteers.length <= 0
               ? <p className={styles.result}>Nenhum voluntario encontrado</p>
               : volunteers

    if (order && volunteers.length >= 1) {
      if (order === 'nameasc' || order === 'participationasc') {
        volunteers = orderBy(volunteers, typeOrder, 'asc')
      }
      if (order === 'namedesc' || order === 'participationdesc') {
        volunteers = orderBy(volunteers, typeOrder, 'desc')
      }
    }

    const renderVolunteers = Array.isArray(volunteers) ? volunteers : get(volunteers, 'props.children')

    if (this.props.voluntario.loader) {
      return (
        <div className="Grid Grid--withGutter">
          <CircularProgress
            style={{ margin: '10% auto' }}
          />
        </div>
      )
    }

    return (
      <div className={`${styles.box}`}>
        {renderVolunteers}
      </div>
    )
  }

  render() {
    return (
      <div className={`contentLeader ${styles.content} contentMedia page-break`}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <VolunteerManagementHeader />
            {this.renderVolunteers()}
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

_VolunteerManagement.propTypes = {
  fetchVolunteers: PropTypes.func,
  voluntario: PropTypes.object,
  loggedUser: PropTypes.object,
}

const mapStateToProps = state => ({
  voluntario: state.voluntario,
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  fetchVolunteers,
}

const VolunteerManagement = connect(mapStateToProps, mapActionsToProps)(_VolunteerManagement)

export default VolunteerManagement
