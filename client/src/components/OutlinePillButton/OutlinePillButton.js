import React from 'react'
import PropTypes from 'prop-types'
import PillButton from '../../components/PillButton/PillButton'
import styles from './OutlinePillButton.css'

const OutlinePillButton = props => (
  <div className={styles.container}>
    <PillButton
      {...props}
      negativeColors
      className={styles.button}
    />
  </div>
)

OutlinePillButton.propTypes = {
  onClick: PropTypes.func,
}

export default OutlinePillButton
