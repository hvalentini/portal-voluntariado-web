import React from 'react'
import PropTypes from 'prop-types'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { connect } from 'react-redux'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './ActionTakenHeader.css'
import { updateStatistic } from '../../actions/statistic'

const style = {}

class _ActionTakenHeader extends React.Component {
  constructor(props) {
    super(props)
    this.onChangeActions = this.onChangeActions.bind(this)
  }

  onChangeActions(event, index, value) {
    this.props.updateStatistic({ selectedAction: value })
  }

  render() {
    return (
      <div className={styles.container}>
        <div className="Grid Grid-withGutter">
          <div className="Grid-cell u-size9of12">
            <SvgIcon
              className={styles.checkIcon}
              icon="icon-icon-19"
              style={{
                fontSize: '34px',
                marginBottom: '-4px',
              }}
            />
            <span className={styles.span}>Ações realizadas no programa de Voluntariado</span>
          </div>
          <div className="Grid-cell u-size3of12">
            <p
              style={{
                lineHeight: '1',
                marginBottom: '-10px',
              }}
            >
              Visualizando ações de:
            </p>
            <SelectField
              value={this.props.statistic.volunteer.selectedAction}
              onChange={this.onChangeActions}
              labelStyle={style.menuStyle}
              fullWidth
            >
              {
                this.props.statistic.volunteer.selectActions.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item.value}
                    primaryText={item.text}
                    key={i}
                  />
                ))
              }
            </SelectField>
          </div>

        </div>
      </div>
    )
  }
}

style.menuStyle = {
  color: '#000000',
}

_ActionTakenHeader.propTypes = {
  volunteer: PropTypes.object,
  statistic: PropTypes.object,
  updateStatistic: PropTypes.func,
}

const mapStateToProps = state => ({
  statistic: state.statistic,

})

const mapActionsToProps = {
  updateStatistic,
}

const ActionTakenHeader = connect(mapStateToProps, mapActionsToProps)(_ActionTakenHeader)

export default ActionTakenHeader
