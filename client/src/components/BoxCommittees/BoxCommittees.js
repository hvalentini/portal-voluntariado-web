import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './BoxCommittees.css'
import BoxCommitteeCard from '../../components/BoxCommitteeCard/BoxCommitteeCard'

class _BoxCommittees extends React.Component {

  handleCommittees() {
    if (this.props.comites.length < 1) {
      return (
        <div>
          <p className={styles.notFound}>Nenhum comitê encontrado.</p>
        </div>
      )
    }
    return (
      this.props.comites.map(item => (
        <Link to={`/admin/comites/editar/${item._id}`} className={styles.root} key={item._id}>
          <BoxCommitteeCard committee={item} />
          <hr className={styles.separator} />
        </Link>
      ))
    )
  }

  renderCommitteePrint() {
    return this.props.print.map(item => (
      <div className={styles.root}>
        <BoxCommitteeCard
          committee={item}
          key={item._id}
        />
        <hr className={styles.separator} />
      </div>
    ))
  }

  render() {
    return (
      <div>
        <div className={styles.committees}>
          {this.handleCommittees()}
        </div>
        <div className={`${styles.printBlock} contentMedia page-break`}>
          {this.renderCommitteePrint()}
        </div>
      </div>
    )
  }
}

_BoxCommittees.propTypes = {
  comites: PropTypes.array,
  print: PropTypes.array,
}

const mapStateToProps = state => ({
  comites: state.list.comites,
  print: state.ui.comites,
})

const mapActionsToProps = {}

const BoxCommittees = connect(mapStateToProps, mapActionsToProps)(_BoxCommittees)
export default BoxCommittees
