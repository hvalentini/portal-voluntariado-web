import React from 'react'
import PropTypes from 'prop-types'
import PillButton from '../../components/PillButton/PillButton'

const PillButtonKeyLimePie = props => (
  <PillButton
    {...props}
    style={{
      backgroundColor: '#C0D72F',
      textTransform: 'none',
      fontSize: '22px',
      lineHeight: '26px',
      ...props.style,
    }}
  />
)

PillButtonKeyLimePie.propTypes = {
  style: PropTypes.object,
}

export default PillButtonKeyLimePie
