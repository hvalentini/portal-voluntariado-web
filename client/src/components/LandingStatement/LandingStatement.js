import React from 'react'
import Styles from './LandingStatement.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const LandingStatement = (props) => (
  <div className={Styles.statement} style={{
    width: props.size,
    height: props.size,
    backgroundColor: props.backgroundColor
  }}>
    <div className={Styles.centerArea}>
      <div className={Styles.centered}>
        <SvgIcon style={{ fill: "white", width: "72px", height: "72px" }} icon="icon-icon-5" /> <br />
        <span>{props.children}</span>
      </div>
    </div>
  </div>
)

export default LandingStatement
