import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './LandingCTA.css'
import Avatar from '../Avatar/Avatar'
import LandingAvatars from '../LandingAvatars/LandingAvatars'
import IconButtonPlus from '../IconButtonPlus/IconButtonPlus'
import PillButtonCerulean from '../PillButtonCerulean/PillButtonCerulean'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class LandingCTA extends React.Component {
  constructor(props) {
    super(props)

    this.scrollTo = this.scrollTo.bind(this)
  }

  scrollTo(to, route) {
    const scroll = to != null ? to : document.getElementsByClassName('pv-LandingPage-holderContent')[0].offsetTop - 30
    const callback = route ? () => { this.props.history.push(route) } : () => {}

    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')

    animatedScrollTo(
      el,
      scroll,
      1000,
      callback,
    )
  }

  render() {
    const nome = this.props.userName === null
                ? 'você também'
                : this.props.userName

    const avatar = this.props.userAvatar

    return (
      <div className={styles.container}>
        <div className={styles.LandingCTATeste}>
          <div className={styles.LandingAvatarsLeft}>
            <div className={styles.reverser}>
              <LandingAvatars />
            </div>
          </div>
          <div className={styles.AvatarBox} >
            <div className={styles.AvatarButton}>
              <img
                className={styles.logo}
                src={'images/logo-223x152.png'}
                alt="Logo Portal do Voluntariado"
              />
              <a
                onClick={() => { this.scrollTo(0, '/cadastro/passo-1') }}
                role="button"
                tabIndex="0"
                style={{ cursor: 'pointer', outline: 'none' }}
              >
                <Avatar
                  size="large"
                  url={avatar}
                />
                <IconButtonPlus
                  background1="#00CA80"
                  background2="#009949"
                  icon="icon-icon-7"
                  style={{ boxShadow: '2px 2px 5px 0px rgb(155, 155, 155)' }}
                />
              </a>
            </div>
            <p className={styles.Text}>Faça parte,</p>
            <p className={styles.Nome}>{nome}</p>
            <PillButtonCerulean
              onClick={() => { this.scrollTo(0, '/cadastro/passo-1') }}
              text="Seja um voluntário"
              style={{
                textTransform: 'none',
                fontSize: '24px',
                lineHeight: '28px',
                margin: '0',
              }}
            />
          </div>
          <div className={styles.LandingAvatarsRight}>
            <LandingAvatars position={'right'} />
          </div>
        </div>
        <div className="Grid-cell">
          <SvgIcon
            onClick={() => this.scrollTo()}
            className={styles.seta}
            icon="icon-icon-10"
          />
        </div>
      </div>
    )
  }
}

LandingCTA.propTypes = {
  userName: PropTypes.string,
  userAvatar: PropTypes.string,
  history: PropTypes.object,
}

export default withRouter(LandingCTA)
