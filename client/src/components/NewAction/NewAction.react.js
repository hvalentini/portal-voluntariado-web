import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  TextField,
  DatePicker,
  TimePicker,
  SelectField,
  MenuItem,
  RadioButton,
  RadioButtonGroup,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { get, find, filter } from 'lodash'
import { withRouter } from 'react-router-dom'
import CircularProgress from 'material-ui/CircularProgress'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './NewAction.css'
import PillButton from '../../components/PillButton/PillButton'
import LocalsAutocomplete from '../LocalsAutocomplete/LocalsAutocomplete.react'
import UsersAutocomplete from '../UsersAutocomplete/UsersAutocomplete.react'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import BackButton from '../../components/BackButton/BackButton'
import {
  handleActionOccurred,
  handleChangeProperty,
  handleChangeRadioButton,
  handleChangeStarteDate,
  handleChangeEndDate,
  handleChangeStartHours,
  handleChangeEndHours,
  handleFetchActionsTypes,
  handleChangeTypeAction,
  handleChangeDurationHours,
  addAction,
  cleanNewAction,
  clearError,
  editAction,
  handleDialogSuccess,
  handleCleanTypeAction,
 } from '../../actions/registerFormAction'
import ActionFormGallery from '../ActionFormGallery/ActionFormGallery.react'
import { handleErrorTextLocal } from '../../actions/local'
import { handleErrorTextUser, cleanEditUser } from '../../actions/usersAutocomplete'
import { getLoggedUser } from '../../actions/loggedUser'
import { getAction, cleanEditAction, deleteAction } from '../../actions/acoes'
import { cleanEditLocal } from '../../actions/localAutocomplete'
import { setModal } from '../../actions/modal'
import Loading from '../../components/LoaderAdmin/LoaderAdmin'
import ModalCrud from '../../components/ModalCrud/ModalCrud'

const style = {}
const IntlPolyfill = require('intl')

const DateTimeFormat = IntlPolyfill.DateTimeFormat
require('intl/locale-data/jsonp/pt-BR')


const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#3f93b7',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  datePicker: {
    color: '#000',
    textColor: '#FFF',
    calendarTextColor: '#000',
    selectColor: '#01BEE8',
    selectTextColor: '#FFF',
    calendarYearBackgroundColor: '#fff',
  },
  timePicker: {
    color: '#000',
    textColor: '#000',
    clockColor: '#FFF',
    clockCircleColor: '#FFF',
    selectColor: '#01BEE8',
    selectTextColor: '#01BEE8',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  fontFamily: 'Chantilly-Serial',
  fontSize: '18px',
})

const loaderTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#00BEE7',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
})

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

class _NewAction extends React.Component {

  static convertDate(date) {
    const now = new Date(date)
    now.setTime((now.getTime() + (now.getTimezoneOffset() * 60000)))
    return now
  }

  // static blurDate() {
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  // }

  // static focusDatePicker() {
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '2px solid rgb(63, 147, 183)'
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  // }

  // static openTimePicker() {
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '2px solid rgb(63, 147, 183)'
  // }

  // static focusDatePickerEnd() {
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '2px solid rgb(63, 147, 183)'
  // }

  // static blurDatePickerEnd() {
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.getElementById('tipoAcao').children[0].children[2].focus()
  // }

  // static openTimePickerFinal() {
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '2px solid rgb(63, 147, 183)'
  // }

  // static focusTipoAcao() {
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  // }

  // static blurTimePickerFinal() {
  //   document.getElementById('tipoAcao').children[0].children[2].focus()
  // }

  constructor(props) {
    super(props)
    this.state = {
      valorGasto: '0.00',
      errorTextValorGasto: '',
      totalCartas: '',
      totalBeneficiados: '',
      nome: '',
      descricao: '',
      errorTextDataInicial: '',
      errorTextDataFinal: '',
      errorTextHoraInicial: '',
      errorTextHoraFinal: '',
      errorTextTempoDuracao: '',
      errorTextTipoAcao: '',
      errorTextLocal: '',
      errorTextUser: '',
    }
    this.handleChangeAction = this.handleChangeAction.bind(this)
    this.changeActionOccurred = this.changeActionOccurred.bind(this)
    this.changeStartDate = this.changeStartDate.bind(this)
    this.changeEndDate = this.changeEndDate.bind(this)
    this.changeStartHours = this.changeStartHours.bind(this)
    this.changeEndHours = this.changeEndHours.bind(this)
    this.onChangeTypeAction = this.onChangeTypeAction.bind(this)
    this.changeDurationHours = this.changeDurationHours.bind(this)
    this.handleAddAction = this.handleAddAction.bind(this)
    this.handleChangeAmountSpent = this.handleChangeAmountSpent.bind(this)
    this.handleChangeAmountProperty = this.handleChangeAmountProperty.bind(this)
    this.handleClearError = this.handleClearError.bind(this)
    this.scrollTo = this.scrollTo.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleDeleteAcoes = this.handleDeleteAcoes.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    // this.blurDatePicker = this.blurDatePicker.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
    this.handleBlurAutocomplete = this.handleBlurAutocomplete.bind(this)
  }

  componentWillMount() {
    this.props.getLoggedUser()
    if (this.props.match.params.id) {
      this.props.getAction(this.props.match.params.id)
    }
  }

  componentDidMount() {
    this.props.handleFetchActionsTypes()
  }

  componentWillUnmount() {
    this.props.cleanEditAction()
    this.props.cleanNewAction()
    this.props.cleanEditLocal()
    this.props.cleanEditUser()
  }

  onChangeTypeAction(ev, index, value) {
    const payload = {}
    payload.types = get(this.props.action, 'tipoAcaoTemp')
    payload.value = value
    if (payload.value && this.state.errorTextTipoAcao) {
      this.setState({ errorTextTipoAcao: '' })
    }
    this.props.handleChangeTypeAction(payload)
    // document.getElementsByName('tempoDuracao')[0].checked = true
    // document.getElementsByName('tempoDuracao')[0].focus()
    document.getElementById('teste').focus()
    // setTimeout({ document.getElementById('teste').focus() },0)
    // document.getElementById('teste').closest('div').focus()
  }

  handleClearError() {
    this.props.clearError()
  }

  handleBlur(e) {
    const newState = {}
    newState[e.target.name] = this.state[e.target.name] && e.target.value ? '' : this.state[e.target.name]
    this.setState(newState)
  }

  handleBlurAutocomplete(value) {
    if (this.state.errorTextLocal && value) {
      this.props.handleErrorTextLocal('')
    }
  }

  changeStartDate(event, date) {
    let dataInicial = this.props.acaoEdit.dataInicial ? this.props.acaoEdit.dataInicial : null
    dataInicial = typeof dataInicial === 'string' ? _NewAction.convertDate(dataInicial) : dataInicial
    this.props.handleChangeStarteDate(new Date(date.setHours(0, 0, 0, 0)))

    if (dataInicial !== date && this.props.acaoEdit.dataInicial) {
      this.props.handleChangeStartHours(null)
    }

    if (date && this.state.errorTextDataInicial) {
      this.setState({ errorTextDataInicial: '' })
    }

    //document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
    // document.getElementById('horarioInicial').focus()
  }

  changeEndDate(event, date) {
    const dataInicial = this.props.acaoEdit.dataInicial
                      ? _NewAction.convertDate(this.props.acaoEdit.dataInicial)
                      : this.props.action.dataInicial
    let dataFinal = this.props.acaoEdit.dataFinal ? this.props.acaoEdit.dataFinal : null
    dataFinal = typeof dataFinal === 'string' ? _NewAction.convertDate(dataFinal) : dataFinal

    const startDate = dataInicial ? dataInicial.getDay() : ''
    const endDate = date ? date.getDay() : ''

    if (startDate === endDate || (dataFinal !== date && this.props.acaoEdit.dataFinal)) {
      this.props.handleChangeEndHours(null)
    }

    this.props.handleChangeEndDate(new Date(date.setHours(0, 0, 0, 0)))
    if (date && this.state.errorTextDataFinal) {
      this.setState({ errorTextDataFinal: '' })
    }
    //document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
    //document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
    // this.textHoraInicial.focus()
  }

  changeActionOccurred(e) {
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    const dataInicial = typeof acao.dataInicial === 'string' ? _NewAction.convertDate(acao.dataInicial) : acao.dataInicial
    const dataFinal = typeof acao.dataFinal === 'string' ? _NewAction.convertDate(acao.dataFinal) : acao.dataFinal

    if ((e.target.value === 'true') === false) {
      if (new Date() > dataInicial || new Date() > dataFinal) {
        this.props.handleChangeStarteDate(null)
        this.props.handleChangeStartHours(null)
        this.props.handleChangeEndDate(null)
        this.props.handleChangeEndHours(null)
      }
    } else if (new Date() < dataInicial || new Date() < dataFinal) {
      this.props.handleChangeStarteDate(null)
      this.props.handleChangeStartHours(null)
      this.props.handleChangeEndDate(null)
      this.props.handleChangeEndHours(null)
    }

    this.props.handleActionOccurred({ cadastroRetroativo: e.target.value === 'true' })
    e.target.focus()
  }

  changeRadioButton(e) {
    this.props.handleChangeRadioButton({ tipoAcao_tipo: e.target.value })
    if (this.state.errorTextAcaoPontual) { this.setState({ errorTextAcaoPontual: '' }) }
    this.props.handleCleanTypeAction()
    e.target.focus()
  }

  changeStartHours(event, date) {
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    const dataInicial = this.props.acaoEdit.dataInicial
                      ? _NewAction.convertDate(this.props.acaoEdit.dataInicial) : null
    const day = dataInicial ? dataInicial.getDate() : this.props.action.dataInicial.getDate()
    const month = dataInicial ? dataInicial.getMonth() : this.props.action.dataInicial.getMonth()
    const year = dataInicial
               ? dataInicial.getFullYear() : this.props.action.dataInicial.getFullYear()
    const hours = date.getHours()
    const minutes = date.getMinutes()

    if (new Date() < new Date(year, month, day, hours, minutes)) {
      if (acao.cadastroRetroativo) {
        this.props.handleChangeStartHours(null)
        return this.setState({ errorTextHoraInicial: 'Horario não pode ser maior que o atual' })
      }
      if (acao.horaFinal) {
        this.props.handleChangeEndHours(null)
        return this.setState({ errorTextHoraFinal: 'Selecione novo horario final' })
      }
    }
    this.setState({ errorTextHoraInicial: '' })
    this.props.handleChangeStartHours(date)
    document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
    // this.textDataFinal.focus()
    return null
  }

  changeEndHours(event, date) {
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    const dataFinal = this.props.acaoEdit.dataFinal
                      ? _NewAction.convertDate(this.props.acaoEdit.dataFinal) : null
    const dataInicial = this.props.acaoEdit.dataInicial
                      ? _NewAction.convertDate(this.props.acaoEdit.dataInicial) : null
    const day = dataFinal ? dataFinal.getDate() : this.props.action.dataFinal.getDate()
    const month = dataFinal ? dataFinal.getMonth() : this.props.action.dataFinal.getMonth()
    const year = dataFinal ? dataFinal.getFullYear() : this.props.action.dataFinal.getFullYear()
    const hours = date.getHours()
    const minutes = date.getMinutes()
    let startDate = this.props.action.dataInicial ? this.props.action.dataInicial.getDay() : ''
    if (dataInicial) {
      startDate = dataInicial ? dataInicial.getDay() : ''
    }
    let endDate = this.props.action.dataFinal ? this.props.action.dataFinal.getDay() : ''
    if (dataFinal) {
      endDate = dataFinal ? dataFinal.getDay() : ''
    }

    if (new Date() < new Date(year, month, day, hours, minutes)) {
      if (acao.cadastroRetroativo) {
        this.props.handleChangeEndHours(null)
        return this.setState({ errorTextHoraFinal: 'Horario não pode ser maior que o atual' })
      }
      if (!acao.dataInicial) {
        this.setState({ errorTextHoraFinal: 'Selecione a data e horario inicial' })
        return this.props.handleChangeEndHours(null)
      }
    }
    if (startDate === endDate) {
      const payload = acao.horaInicial < date ? date : null
      if (!payload) {
        this.setState({ errorTextHoraFinal: ' Selecione horario maior que o inicial' })
      } else { this.setState({ errorTextHoraFinal: '' }) }
      return this.props.handleChangeEndHours(payload)
    }
    this.setState({ errorTextHoraFinal: '' })
    document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
    document.getElementById('tipoAcao').children[0].children[2].focus()
    return this.props.handleChangeEndHours(date)
  }

  changeDurationHours(e, index, value) {
    this.props.handleChangeDurationHours({
      tempoDuracao: !value ? parseFloat(e.target.value) : value,
    })
    if ((e.target.value || value) && this.state.errorTextTempoDuracao) {
      this.setState({ errorTextTempoDuracao: '' })
    }
    e.target.focus()
  }

  handleChangeAmountSpent(e, flag) {
    const target = e
    if (target.value.length === 1) { target.value = `0,0${target.value}` }
    if (flag) {
      const obj = {}
      obj[target.name] = parseFloat(target.value.replace(/\D/g, '').replace(/(\d{1,2})$/, '.$1')).toFixed(2)
      return this.props.handleChangeProperty(obj)
    }
    return '' || (target.value.replace(/\D/g, '')
                              .replace(/(\d{1,2})$/, ',$1')
                              .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
  }

  handleChangeAmountProperty(e) {
    const onlyNumbers = Array.isArray(e.target.value.match(/\d+/g))
                      ? e.target.value.match(/\d+/g).join('')
                      : ''
    const obj = {}
    obj[e.target.name] = onlyNumbers
    return this.props.handleChangeProperty(obj)
  }

  handleChangeAction(e) {
    const obj = {}
    obj[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    return this.props.handleChangeProperty(obj)
  }

  handleAddAction(e) {
    e.preventDefault()
    const valorGasto = Number(this.props.acaoEdit.valorGasto ||
      this.props.action.valorGasto).toFixed(2)

    const totalCartas = this.props.acaoEdit.totalCartas
                        ? this.props.acaoEdit.totalCartas
                        : this.props.action.totalCartas
    const totalBeneficiados = this.props.acaoEdit.totalBeneficiados
                        ? this.props.acaoEdit.totalBeneficiados
                        : this.props.action.totalBeneficiados
    const nome = this.props.acaoEdit.nome
                        ? this.props.acaoEdit.nome
                        : this.props.action.nome
    const descricao = this.props.acaoEdit.descricao
                        ? this.props.acaoEdit.descricao
                        : this.props.action.descricao
    const dataInicial = this.props.acaoEdit.dataInicial
                        ? this.props.acaoEdit.dataInicial
                        : this.props.action.dataInicial
    const dataFinal = this.props.acaoEdit.dataFinal
                        ? this.props.acaoEdit.dataFinal
                        : this.props.action.dataFinal
    const tempoDuracao = this.props.acaoEdit.tempoDuracao
                        ? this.props.acaoEdit.tempoDuracao
                        : this.props.action.tempoDuracao
    const tipoAcao = this.props.acaoEdit.tipoAcao
                        ? this.props.acaoEdit.tipoAcao._id
                        : this.props.action.tipoAcao._id
    const cadastroRetroativo = this.props.acaoEdit._id
                        ? this.props.acaoEdit.cadastroRetroativo
                        : this.props.action.cadastroRetroativo
    const informarQtdeCartas = this.props.acaoEdit.tipoAcao
                        ? this.props.acaoEdit.tipoAcao.informarQtdeCartas
                        : this.props.action.tipoAcao.informarQtdeCartas
    const exclusivoParaLider = this.props.acaoEdit.tipoAcao
                        ? this.props.acaoEdit.tipoAcao.exclusivoParaLider
                        : this.props.action.tipoAcao.exclusivoParaLider
    const acaoPontual = this.props.acaoEdit._id
                  ? this.props.flag
                  : this.props.action.acaoPontual
    const newState = {}
    newState.errorTextValorGasto = valorGasto === ''
                                   ? 'Favor informar valor válido'
                                   : ''
    newState.totalCartas = totalCartas === ''
                                  && informarQtdeCartas
                                   ? 'Favor informar quantidade válida'
                                   : ''
    newState.totalCartas = informarQtdeCartas
                                  && parseFloat(totalCartas) < 1
                                ? 'Favor informar quantidade cartas maior que zero.'
                                : newState.totalCartas
    newState.totalBeneficiados = totalBeneficiados === ''
                                   && !exclusivoParaLider && cadastroRetroativo
                                   ? 'Favor informar quantidade válida'
                                   : ''
    newState.totalBeneficiados = cadastroRetroativo
                                  && parseFloat(totalBeneficiados) < 1
                                   ? 'Favor informar a quantidade de beneficiados maior que zero.'
                                   : newState.totalBeneficiados
    newState.nome = nome === '' ? 'Favor informar o nome da ação' : ''
    newState.descricao = descricao === '' ? 'Favor informar a descrição da ação' : ''
    newState.errorTextDataInicial = dataInicial === null ? 'Informe a data inicial' : ''
    newState.errorTextDataFinal = dataFinal === null ? 'Informe a data final' : ''

    newState.errorTextTempoDuracao = (tempoDuracao === null) && (cadastroRetroativo)
                                    ? 'Informe o tempo de duração'
                                    : ''
    newState.errorTextTipoAcao = tipoAcao === '' ? 'Favor informar o tipo da ação' : ''

    newState.errorTextAcaoPontual = acaoPontual === null ? 'Favor informar a natureza da ação' : ''

    const volunts = (find(this.props.participantes.selected, 'nome') || []).length

    if ((volunts < 1 && acaoPontual === 1 && !exclusivoParaLider &&
      this.props.action.cadastroRetroativo)) {
      this.props.handleErrorTextUser('Favor selecionar um participante')
      newState.errorTextUser = 'Favor selecionar um participante'
    } else if (find(this.props.participantes.selected, 'nome') || acaoPontual !== 1) {
      this.props.handleErrorTextUser('')
      newState.errorTextUser = ''
    }

    this.props.handleErrorTextLocal('')
    newState.errorTextLocal = ''

    if ((get(this.props.local.selected, 'nome', '') === '') && (cadastroRetroativo)) {
      this.props.handleErrorTextLocal('Favor selecionar o local da ação')
      newState.errorTextLocal = 'Favor selecionar o local da ação'
    } else if (get(this.props.local.selected, 'nome', '') !== '') {
      this.props.handleErrorTextLocal('')
      newState.errorTextLocal = ''
    }

    this.setState({ ...newState })
    const type = this.props.loggedUser.adminLogin ? 'admin' : 'lider'

    if (cadastroRetroativo) {
      if (newState.errorTextValorGasto || newState.totalCartas ||
      newState.totalBeneficiados || newState.nome ||
      newState.descricao || newState.errorTextDataInicial ||
      newState.errorTextDataFinal || newState.errorTextTempoDuracao ||
      newState.errorTextTipoAcao || newState.errorTextLocal ||
      newState.errorTextUser || newState.errorTextAcaoPontual) { this.scrollTo(newState); return }

      if (this.props.acaoEdit._id) {
        this.props.editAction({
          action: this.props.acaoEdit,
          local: this.props.local,
          participantes: this.props.participantes,
          loggedUser: this.props.loggedUser,
          type,
        })
      } else {
        this.props.addAction({
          action: this.props.action,
          local: this.props.local,
          participantes: this.props.participantes,
          loggedUser: this.props.loggedUser,
          type,
        })
      }
    } else {
      if (newState.nome || newState.descricao || newState.errorTextDataInicial ||
      newState.errorTextDataFinal || newState.errorTextTempoDuracao || newState.errorTextTipoAcao ||
      newState.errorTextLocal || newState.errorTextAcaoPontual) {
        this.scrollTo(newState)
        return
      }

      if (this.props.acaoEdit._id) {
        this.props.editAction({
          action: this.props.acaoEdit,
          local: this.props.local,
          participantes: this.props.participantes,
          loggedUser: this.props.loggedUser,
          type,
        })
      } else {
        this.props.addAction({
          action: this.props.action,
          local: this.props.local,
          participantes: this.props.participantes,
          loggedUser: this.props.loggedUser,
          type,
        })
      }
    }
  }

  blurLocals() {
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    if (acao.cadastroRetroativo === true) {
      this.textValorGasto.focus()
    }
  }

  scrollTo(newState) {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')

    if (newState.nome || newState.errorTextAcaoPontual || newState.errorTextDataInicial
        || newState.errorTextDataFinal || newState.errorTextTipoAcao
        || newState.errorTextTempoDuracao) {
      const scroll = document.querySelector('.pv-ContentAdmin-formBg').offsetTop
      animatedScrollTo(
        el,
        scroll,
        1000,
      )
      return
    }

    if (newState.descricao) {
      this.textDescricao.focus()
      return
    }

    if (newState.errorTextLocal) {
      animatedScrollTo(
        el,
        910,
        1000,
      )
      return
    }

    if (newState.totalBeneficiados) {
      this.textTotalBeneficiados.focus()
      return
    }

    if (newState.totalCartas) {
      this.textTotalCartas.focus()
      return
    }

    if (newState.errorTextValorGasto) {
      this.textValorGasto.focus()
    }
  }

  handleClose() {
    this.props.handleDialogSuccess()
    window.location = '/lider/acoes-voluntarias'
  }

  handleDeleteAcoes() {
    this.props.deleteAction(this.props.match.params.id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina)
  }

  // blurDatePicker() {
  //   document.querySelector('.pv-NewAction-dateEnd div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioInicial div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-horarioFinal div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   document.querySelector('.pv-NewAction-date div:first-child div:last-child hr').style.borderBottom = '1px solid rgb(224, 224, 224)'
  //   this.textDataFinal.focus()
  // }

  renderAmount() {
    const totalBeneficiados = this.props.acaoEdit.totalBeneficiados
    const acaoPontual = this.props.acaoEdit._id
                      ? this.props.flag
                      : this.props.action.tipoAcao_tipo

    const exclusivoParaLider = this.props.acaoEdit._id
                              ? this.props.acaoEdit.tipoAcao.exclusivoParaLider
                              : this.props.action.tipoAcao.exclusivoParaLider
    if (!exclusivoParaLider && acaoPontual < 2) {
      return (
        <div className="Grid-cell u-size1of1 u-textLeft">
          <TextField
            floatingLabelText="Número de beneficiados"
            floatingLabelStyle={style.floating}
            inputStyle={style.inputText}
            underlineStyle={style.black}
            name="totalBeneficiados"
            value={!totalBeneficiados ? this.props.action.totalBeneficiados : totalBeneficiados}
            onChange={this.handleChangeAmountProperty}
            onBlur={this.handleBlur}
            errorText={this.state.totalBeneficiados}
            ref={(input) => { this.textTotalBeneficiados = input }}
            fullWidth
          />
        </div>
      )
    }
    return null
  }

  renderActionOccurred() {
    const valorGasto = this.handleChangeAmountSpent({
      value: Number(this.props.acaoEdit.valorGasto || this.props.action.valorGasto).toFixed(2),
      name: 'valorGasto',
    })
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    if (acao.cadastroRetroativo === true) {
      return (
        <div>
          <div className="Grid">
            {this.renderAmount()}
          </div>

          <div className="Grid">
            {this.renderNumberCards()}
          </div>

          <div className="Grid Grid--withGutter">
            <div className="Grid-cell u-size1of1 u-textLeft">
              <TextField
                floatingLabelText="Valor gasto"
                floatingLabelStyle={style.floating}
                inputStyle={style.inputText}
                underlineStyle={style.black}
                name="valorGasto"
                value={valorGasto}
                maxLength={12}
                onChange={e => this.handleChangeAmountSpent(e.target, true)}
                errorText={this.state.errorTextValorGasto}
                ref={(input) => { this.textValorGasto = input }}
                fullWidth
                id="valorGasto"
              />
            </div>
          </div>
          <div className="Grid">
            <div className="Grid-cell u-size1of1 UsersAutocomplete">
              <UsersAutocomplete
                comiteEdit={this.props.acaoEdit.comite}
                handleErrorTextUser={this.props.handleErrorTextUser}
              />
            </div>
          </div>

          <div className="Grid">
            <div className={`${styles.gallerySpacing} Grid-cell`}>
              <ActionFormGallery />
            </div>
          </div>
        </div>
      )
    }
    return null
  }

  renderBtnDelete() {
    const match = this.props.match
    if (match.url.indexOf('/lider') >= 0 && match.path.indexOf('/lider') >= 0) { return null }

    const msg = `Você tem certeza que deseja excluir a ação ${this.props.acaoEdit.nome}?`

    return (
      <div className={styles.delete}>
        <PillButton
          text="excluir"
          negativeColors
          onClick={() =>
            this.handleOpenModal(
              'delete', // TIPO DE AÇÃO - PODE SER DELETE, WARNING, OU SUCCESS
              'acoes', // ENTIDADE RELACIONADA NO BANCO
              msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM NAO VENHA DO BANCO ENVIAR
              this.handleDeleteAcoes, // AÇÃO PRINCIPAL
              this.props.loggedUser.adminLogin ? '/admin/acoes' : '/lider/acoes-voluntarias', // ROTA DA PAGINA PRINCIPAL
            )
          }
        />
      </div>
    )
  }

  renderBtnRegister() {
    const textButton = this.props.acaoEdit.nome
                     ? 'ALTERAR'
                     : 'CADASTRAR'
    const loader = this.props.action.loader
    const match = this.props.match
    let ml = ''
    if (match.url.indexOf('/admin') >= 0 && match.path.indexOf('/admin') >= 0) {
      ml = styles.ml
    }
    return (
      <div className={`${styles.register} ${ml}`}>
        <PillButton
          text={textButton}
          loader={loader}
          onClick={e => this.handleAddAction(e)}
          // className={styles.btnSave}
          class={styles.btnSave}
          id="submit"
        />
      </div>
    )
  }

  renderErrors() {
    if (this.state.errorTextAcaoPontual) {
      return (
        <div className={styles.errorText}>{this.state.errorTextAcaoPontual}</div>
      )
    }
    return null
  }

  renderNumberCards() {
    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action
    if (acao.tipoAcao && acao.tipoAcao.informarQtdeCartas) {
      return (
        <div className="Grid-cell u-size1of1 u-textLeft">
          <TextField
            floatingLabelText="Quantidade de cartas"
            floatingLabelStyle={style.floating}
            inputStyle={style.inputText}
            underlineStyle={style.black}
            name="totalCartas"
            value={acao.totalCartas}
            onChange={this.handleChangeAmountProperty}
            onBlur={this.handleBlur}
            errorText={this.state.totalCartas}
            ref={(input) => { this.textTotalCartas = input }}
            fullWidth
            id="qtdeCartas"
          />
        </div>
      )
    }
    return null
  }

  render() {
    const title = this.props.match.params.id ? 'Alterar ação' : 'Cadastro de uma nova ação'

    if (this.props.match.params.id && this.props.loading) {
      return (
        <ContentAdmin title={title} useTheme={false}>
          <Loading />
        </ContentAdmin>
      )
    }

    const hours = [
      { value: null, text: '' },
      { value: 3, text: '3Hr' },
      { value: 3.5, text: '3Hr e 30min' },
      { value: 4, text: '4Hr' },
      { value: 4.5, text: '4Hr e 30min' },
      { value: 5, text: '5Hr' },
      { value: 5.5, text: '5Hr e 30min' },
      { value: 6, text: '6Hr' },
      { value: 6.5, text: '6Hr e 30min' },
      { value: 7, text: '7Hr' },
      { value: 7.5, text: '7Hr e 30min' },
      { value: 8, text: '8Hr' },
      { value: 8.5, text: '8Hr e 30min' },
      { value: 9, text: '9Hr' },
      { value: 9.5, text: '9Hr e 30min' },
      { value: 10, text: '10Hr' },
      { value: 10.5, text: '10Hr e 30min' },
      { value: 11, text: '11Hr' },
      { value: 11.5, text: '11Hr e 30min' },
      { value: 12, text: 'Mais que 12Hr' },
    ]

    const acao = this.props.acaoEdit._id ? this.props.acaoEdit : this.props.action

    const dataInicialMax = typeof acao.dataInicial === 'string' ? _NewAction.convertDate(acao.dataInicial) : acao.dataInicial
    const dataFinalMax = typeof acao.dataFinal === 'string' ? _NewAction.convertDate(acao.dataFinal) : acao.dataFinal

    let minDate = dataInicialMax ? { minDate: dataInicialMax } : {}
    let maxDate = dataFinalMax ? { maxDate: dataFinalMax } : {}
    let minDateFinal = dataInicialMax
                     ? { minDate: dataInicialMax } : {}
    let maxDateFinal = dataFinalMax ? { maxDate: dataFinalMax } : {}

    const dataAtual = new Date()

    if (acao.cadastroRetroativo) {
      maxDate = dataFinalMax
                ? { maxDate: dataFinalMax } : { maxDate: dataAtual }
      minDate = {}
      maxDateFinal = { maxDate: dataAtual }
      minDateFinal = dataInicialMax ? { minDate: dataInicialMax } : {}
    } else {
      maxDate = dataFinalMax ? { maxDate: dataFinalMax } : {}
      minDate = { minDate: dataAtual }
      maxDateFinal = {}
      minDateFinal = dataInicialMax ? { minDate: dataInicialMax } : {}
    }

    const acaoPontual = this.props.acaoEdit._id
                      ? this.props.flag
                      : this.props.action.tipoAcao_tipo

    const cadastroRetroativo = (this.props.acaoEdit.cadastroRetroativo === true ||
                                this.props.acaoEdit.cadastroRetroativo === false)
                                ? this.props.acaoEdit.cadastroRetroativo
                                : this.props.action.cadastroRetroativo

    const typesActions = filter(this.props.action.tipoAcaoTemp, el => el.tipo === acaoPontual ||
      (acaoPontual === null || acaoPontual === undefined))

    let dataFinal = this.props.acaoEdit.dataFinal
    dataFinal = dataFinal
              ? _NewAction.convertDate(dataFinal)
              : dataFinal
    let dataInicial = this.props.acaoEdit.dataInicial
    dataInicial = dataInicial
              ? _NewAction.convertDate(dataInicial)
              : dataInicial
    const descricao = this.props.acaoEdit.descricao
    const nome = this.props.acaoEdit.nome
    const tipoAcao = this.props.acaoEdit.tipoAcao
    const tempoDuracao = this.props.acaoEdit.tempoDuracao
    let horaFinal = this.props.acaoEdit.dataFinal
    let horaInicial = this.props.acaoEdit.dataInicial

    if (this.props.acaoEdit.dataFinal) {
      horaFinal = typeof this.props.acaoEdit.horaFinal === 'number' && this.props.acaoEdit.horaFinal >= 0
                ? _NewAction.convertDate(this.props.acaoEdit.dataFinal)
                : this.props.acaoEdit.horaFinal
      horaInicial = typeof this.props.acaoEdit.horaInicial === 'number' && this.props.acaoEdit.horaInicial >= 0
                ? _NewAction.convertDate(this.props.acaoEdit.dataInicial)
                : this.props.acaoEdit.horaInicial
    }
    const tempoDuracaoNewAction = tempoDuracao || this.props.action.tempoDuracao

    let valueHoraInicial
    if (!horaInicial) {
      valueHoraInicial = this.props.action.horaInicial
    } else {
      valueHoraInicial = new Date(String(horaInicial)).getHours() === 0 &&
                         new Date(String(horaInicial)).getMinutes() === 0
                       ? null
                       : horaInicial
    }

    let valueHoraFinal
    if (!horaFinal) {
      valueHoraFinal = this.props.action.horaFinal
    } else {
      valueHoraFinal = new Date(String(horaFinal)).getHours() === 0 &&
                         new Date(String(horaFinal)).getMinutes() === 0
                       ? null
                       : horaFinal
    }
    let buttons = 'u-textRight'
    const match = this.props.match
    if (match.url.indexOf('/lider') >= 0 && match.path.indexOf('/lider') >= 0) {
      buttons = 'u-textCenter'
    }

    // const title = this.props.id && this.props.acaoEdit ? 'Editar ação' : 'Cadastrar ação'

    if (this.props.id && !this.props.acaoEdit._id) {
      return (
        <MuiThemeProvider muiTheme={loaderTheme}>
          <div className={styles.formBg}>
            <div className={styles.bottom}>
              <h3 className={styles.title}>{title}</h3>
            </div>
            <div className="Grid Grid--withGutter">
              <CircularProgress
                style={{ margin: '10% auto' }}
              />
            </div>
          </div>
        </MuiThemeProvider>
      )
    }

    return (
      <ContentAdmin title={title} useTheme={false}>
        <ModalCrud />

        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <div className="Grid-cell">
              <TextField
                floatingLabelText="Nome da ação"
                name="nome"
                value={!nome ? this.props.action.nome : nome}
                onChange={this.handleChangeAction}
                onBlur={this.handleBlur}
                hintStyle={style.black}
                underlineStyle={style.black}
                maxLength={100}
                fullWidth
                floatingLabelStyle={style.floating}
                inputStyle={style.inputText}
                errorText={this.state.nome}
                ref={(input) => { this.textNome = input }}
              />
            </div>
            <div className="Grid-cell">
              <p className={`${styles.labelText} u-textLeft`}>Ação foi finalizada?</p>
              <MuiThemeProvider muiTheme={muiTheme}>
                <RadioButtonGroup
                  name="cadastroRetroativo"
                  onChange={e => this.changeActionOccurred(e)}
                  valueSelected={cadastroRetroativo}
                  ref={(input) => { this.textCadastroRetroativo = input }}
                >
                  <RadioButton
                    value
                    label="Sim"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value={false}
                    label="Não"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                </RadioButtonGroup>
              </MuiThemeProvider>
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
                <p className={`${styles.labelText} u-textLeft`}>Essa ação é:</p>
                <RadioButtonGroup
                  name="acaoPontual"
                  onChange={e => this.changeRadioButton(e)}
                  valueSelected={acaoPontual}
                  ref={(input) => { this.textAcaoPontual = input }}
                  // key={acaoPontual}
                >
                  <RadioButton
                    value={0}
                    label="Pontual"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value={1}
                    label="Contínua"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value={2}
                    label="Gerencial"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                </RadioButtonGroup>
                {this.renderErrors()}
              </div>
            </div>
            <div className="Grid-cell">
              <TextField
                floatingLabelText="Descrição"
                name="descricao"
                value={descricao || this.props.action.descricao}
                onChange={this.handleChangeAction}
                onBlur={this.handleBlur}
                multiLine
                rows={1}
                rowsMax={6}
                fullWidth
                underlineStyle={style.black}
                hintStyle={style.black}
                floatingLabelStyle={style.floating}
                inputStyle={style.inputText}
                errorText={this.state.descricao}
                ref={(input) => { this.textDescricao = input }}
                // onFocus={_NewAction.blurDate}
              />
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock u-posRelative">
                <DatePicker
                  hintText="Início"
                  cancelLabel="Cancelar"
                  hintStyle={style.black}
                  textFieldStyle={{ width: '100%' }}
                  floatingLabelText="Data inicial"
                  DateTimeFormat={DateTimeFormat}
                  onChange={this.changeStartDate}
                  value={!dataInicial ? this.props.action.dataInicial : dataInicial}
                  locale="pt-BR"
                  floatingLabelStyle={style.floating}
                  inputStyle={style.inputText}
                  errorText={this.state.errorTextDataInicial}
                  {...maxDate}
                  {...minDate}
                  ref={(input) => { this.textDataInicial = input }}
                  firstDayOfWeek={0}
                  className={styles.date}
                  // onFocus={_NewAction.focusDatePicker}
                  // onDismiss={this.blurDatePicker}
                />
              </div>
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock u-posRelative">
                <TimePicker
                  format="24hr"
                  hintText="Horário"
                  floatingLabelText="Horário de início"
                  hintStyle={style.black}
                  textFieldStyle={{ width: '100%' }}
                  onChange={this.changeStartHours}
                  value={valueHoraInicial}
                  errorText={this.state.errorTextHoraInicial}
                  disabled={!dataInicial ? !this.props.action.dataInicial : !dataInicial}
                  floatingLabelStyle={style.floating}
                  inputStyle={style.inputText}
                  ref={(input) => { this.textHoraInicial = input }}
                  className={styles.horarioInicial}
                  // onFocus={this.openTimePicker}
                  // onDismiss={_NewAction.blurTimePicker}
                  id="horarioInicial"
                />
              </div>
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock u-posRelative">
                <DatePicker
                  hintText="Final"
                  floatingLabelText="Data Final"
                  cancelLabel="Cancelar"
                  hintStyle={style.black}
                  textFieldStyle={{ width: '100%' }}
                  DateTimeFormat={DateTimeFormat}
                  onChange={this.changeEndDate}
                  value={!dataFinal ? this.props.action.dataFinal : dataFinal}
                  errorText={this.state.errorTextDataFinal}
                  {...maxDateFinal}
                  {...minDateFinal}
                  locale="pt-BR"
                  ref={(input) => { this.textDataFinal = input }}
                  floatingLabelStyle={style.floating}
                  inputStyle={style.inputText}
                  firstDayOfWeek={0}
                  className={styles.dateEnd}
                  // onFocus={_NewAction.focusDatePickerEnd}
                  // onDismiss={_NewAction.blurDatePickerEnd}
                />
              </div>
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock u-posRelative">
                <TimePicker
                  format="24hr"
                  hintText="Horário"
                  floatingLabelText="Horário de término"
                  hintStyle={style.black}
                  textFieldStyle={{ width: '100%' }}
                  onChange={this.changeEndHours}
                  value={valueHoraFinal}
                  errorText={this.state.errorTextHoraFinal}
                  disabled={!dataFinal ? !this.props.action.dataFinal : !dataFinal}
                  floatingLabelStyle={style.floating}
                  inputStyle={style.inputText}
                  ref={(input) => { this.textHoraFinal = input }}
                  className={styles.horarioFinal}
                  // onFocus={_NewAction.openTimePickerFinal}
                  // onDismiss={_NewAction.blurTimePickerFinal}
                />
              </div>
            </div>
            <div className="Grid-cell">
              <div className="Grid-cell u-textLeft">
                <SelectField
                  value={!tipoAcao ? this.props.action.tipoAcao._id : tipoAcao._id}
                  onChange={this.onChangeTypeAction}
                  floatingLabelText="Tipo de ação"
                  floatingLabelStyle={style.floating}
                  inputStyle={style.inputText}
                  fullWidth
                  errorText={this.state.errorTextTipoAcao}
                  ref={(input) => { this.textTipoAcao = input }}
                  onFocus={_NewAction.focusTipoAcao}
                  id="tipoAcao"
                >
                  {
                    typesActions.map((item, i) => (
                      <MenuItem
                        className="SelectBkg"
                        value={item._id}
                        primaryText={item.nome}
                        style={style.menuStyle}
                        key={i}
                      />
                    ))
                  }
                </SelectField>
              </div>
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
                <p className={`${styles.labelText} u-textLeft`}>Período de duração</p>
                <RadioButtonGroup
                  name="tempoDuracao"
                  onChange={this.changeDurationHours}
                  valueSelected={tempoDuracaoNewAction}
                  ref={(input) => { this.tempoDuracao = input }}
                  autoFocus
                >
                  <RadioButton
                    value={0.5}
                    label="30m"
                    labelStyle={style.radioLabel}
                    style={{ width: '15%', display: 'inline-block' }}
                    id="teste"
                  />
                  <RadioButton
                    value={1}
                    label="1h"
                    labelStyle={style.radioLabel}
                    style={{ width: '15%', display: 'inline-block' }}
                  />
                  <RadioButton
                    value={1.5}
                    label="1h e 30m"
                    labelStyle={style.radioLabel}
                    style={{ width: '26%', display: 'inline-block' }}
                  />
                  <RadioButton
                    value={2}
                    label="2h"
                    labelStyle={style.radioLabel}
                    style={{ width: '15%', display: 'inline-block' }}
                  />
                  <RadioButton
                    value={2.5}
                    label="2h e 30m"
                    labelStyle={style.radioLabel}
                    style={{ width: '26%', display: 'inline-block' }}
                  />
                </RadioButtonGroup>
              </div>
            </div>
            <div className="Grid">
              <div className="Grid-cell u-size6of12">
                <SelectField
                  value={tempoDuracaoNewAction > 2.5 ? tempoDuracaoNewAction : null}
                  onChange={this.changeDurationHours}
                  style={{ marginTop: '10px' }}
                  hintText="Outro Período"
                  hintStyle={style.floating}
                  fullWidth
                  errorText={this.state.errorTextTempoDuracao}
                  ref={(input) => { this.textTempoDuracao = input }}
                >
                  {
                    hours.map((item, i) => (
                      <MenuItem
                        className="SelectBkg"
                        value={item.value}
                        primaryText={item.text}
                        key={i}
                      />
                      ))
                    }
                </SelectField>
              </div>
            </div>
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-textLeft u-inlineBlock u-posRelative" style={{ marginTop: '-10px' }}>
                <LocalsAutocomplete
                  required={cadastroRetroativo}
                  onBlur={this.handleBlurAutocomplete}
                />
              </div>
            </div>
            {this.renderActionOccurred()}
          </div>
        </MuiThemeProvider>
        <MuiThemeProvider muiTheme={loaderTheme}>
          <div className={`${styles.pillButton} ${buttons} Grid-cell`}>
            <div className={styles.backButton}>
              <BackButton history={this.props.history} />
            </div>
            {this.renderBtnDelete()}
            {this.renderBtnRegister()}
          </div>
        </MuiThemeProvider>
      </ContentAdmin>
    )
  }
}

_NewAction.propTypes = {
  nome: PropTypes.string,
  local: PropTypes.object,
  action: PropTypes.object,
  acaoEdit: PropTypes.object,
  participantes: PropTypes.object,
  loggedUser: PropTypes.object,
  handleActionOccurred: PropTypes.func,
  handleChangeProperty: PropTypes.func,
  handleChangeRadioButton: PropTypes.func,
  handleChangeStarteDate: PropTypes.func,
  handleChangeEndDate: PropTypes.func,
  handleChangeStartHours: PropTypes.func,
  handleChangeEndHours: PropTypes.func,
  handleFetchActionsTypes: PropTypes.func,
  handleChangeTypeAction: PropTypes.func,
  handleChangeDurationHours: PropTypes.func,
  handleErrorTextLocal: PropTypes.func,
  handleErrorTextUser: PropTypes.func,
  addAction: PropTypes.func,
  clearError: PropTypes.func,
  getLoggedUser: PropTypes.func,
  cleanEditAction: PropTypes.func,
  getAction: PropTypes.func,
  cleanEditLocal: PropTypes.func,
  cleanEditUser: PropTypes.func,
  cleanNewAction: PropTypes.func,
  editAction: PropTypes.func,
  handleDialogSuccess: PropTypes.func,
  flag: PropTypes.number,
  match: PropTypes.object,
  handleCleanTypeAction: PropTypes.func,
  deleteAction: PropTypes.func,
  setModal: PropTypes.func,
  loading: PropTypes.bool,
  history: PropTypes.object,
  id: PropTypes.string,
}

style.inputText = {
  fontSize: '16px',
}

style.floating = {
  fontSize: '17px',
  color: '#707070',
}

style.black = {
  color: '#484848',
  fontSize: '18px',
}

style.radioButton = {
  marginTop: '7px',
  width: '30%',
  fontSize: '16px',
}

style.radioButtonPeriodo = {
  marginTop: '7px',
  width: '29%',
  fontSize: '18px',
}

style.radioButtonPeq = {
  marginTop: '7px',
  width: '15%',
  fontSize: '18px',
}

style.radioButtonSize = {
  marginTop: '7px',
  width: '16%',
  fontSize: '20px',
}

style.radioLabel = {
  marginLeft: '-14px',
}

style.datePicker = {
  marginTop: '7px',
  width: '160px',
  fontSize: '20px',
}

style.menuStyle = {
  color: '#000000 !important',
}

const mapStateToProps = state => ({
  action: state.registerFormAction,
  local: state.localsAutocomplete,
  participantes: state.usersAutocomplete,
  loggedUser: state.loggedUser,
  acaoEdit: state.acoes.acaoEdit,
  flag: state.acoes.tipoAcao_tipo,
  loading: state.ui.loading,
})

const mapActionsToProps = {
  handleChangeProperty,
  handleActionOccurred,
  handleChangeRadioButton,
  handleChangeStarteDate,
  handleChangeEndDate,
  handleChangeStartHours,
  handleChangeEndHours,
  handleFetchActionsTypes,
  handleChangeTypeAction,
  handleChangeDurationHours,
  handleErrorTextLocal,
  handleErrorTextUser,
  addAction,
  clearError,
  getLoggedUser,
  getAction,
  cleanEditAction,
  cleanEditLocal,
  cleanEditUser,
  cleanNewAction,
  editAction,
  handleDialogSuccess,
  handleCleanTypeAction,
  deleteAction,
  setModal,
}

const NewAction = connect(mapStateToProps, mapActionsToProps)(_NewAction)
export default withRouter(NewAction)
