import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { apiWarningUpdate } from '../../actions/apiWarning'
import ModalDialog from '../../components/ModalDialog/ModalDialog'

class _RenderRoutes extends React.Component {

  constructor(props) {
    super(props)
    this.handleCloseModal = this.handleCloseModal.bind(this)
  }

  handleCloseModal() {
    this.props.apiWarningUpdate(false)
  }

  render() {
    return (
      <ModalDialog
        type="warning"
        open={this.props.apiWarning}
        textFirst="Ok"
        actionFirst={this.handleCloseModal}
        msg="Estamos com um pequeno problema neste momento. Tente novamente mais tarde."
      />
    )
  }
}

_RenderRoutes.propTypes = {
  apiWarningUpdate: PropTypes.func.isRequired,
  apiWarning: PropTypes.bool.isRequired,
}

const mapStateToProps = state => ({
  apiWarning: state.apiWarning,
})

const mapActionToProps = dispatch => ({
  apiWarningUpdate(payload) {
    dispatch(apiWarningUpdate(payload))
  },
})

const RenderRoutes = connect(mapStateToProps, mapActionToProps)(_RenderRoutes)

export default RenderRoutes
