import React from 'react'
import PropTypes from 'prop-types'
import PillButton from '../../components/PillButton/PillButton'
import styles from './PillButtonWhite.css'

const PillButtonWhite = props => (
  <div>
    <PillButton
      {...props}
      className={styles.btn}
      style={props.style}
    />
  </div>
)

PillButtonWhite.propTypes = {
  style: PropTypes.object,
}

export default PillButtonWhite
