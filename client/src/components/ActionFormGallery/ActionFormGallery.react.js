import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import get from 'lodash.get'
import Dropzone from 'react-dropzone'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import Checkbox from 'material-ui/Checkbox'
import findIndex from 'lodash.findindex'
import styles from './ActionFormGallery.css'
import { uploadFormGallery, handleChangeCheckbox, handleRemoveGalleryItem } from '../../actions/registerFormAction'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#484848',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  fontFamily: 'Chantilly-Serial',
})

const style = {}

class _ActionFormGallery extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      image: '',
      status: false,
    }
    this.onDrop = this.onDrop.bind(this)
    this.onChangeCheckbox = this.onChangeCheckbox.bind(this)
    this.removeGalleryItem = this.removeGalleryItem.bind(this)
  }

  onDrop(files) {
    if (!files.length) return

    const filesTemp = this.props.galeriaEdit
                    ? get(this.props.galeriaEdit, 'galeria', [])
                    : get(this.props.gallery, 'galeria', [])
    this.props.uploadFormGallery({ files, filesTemp, idVoluntario: this.props.idVoluntario })
  }

  onChangeCheckbox(id) {
    this.props.handleChangeCheckbox(id)
  }

  removeGalleryItem(id) {
    const index = this.props.galeriaEdit
          ? findIndex(this.props.galeriaEdit, id)
          : findIndex(this.props.gallery.galleryTemp.galeria, id)
    const payload = {
      images: this.props.galeriaEdit ? this.props.galeriaEdit : this.props.gallery.galleryTemp,
      index,
    }
    this.props.handleRemoveGalleryItem(payload)
  }

  renderGallery() {
    let galeria = get(this.props.gallery.galleryTemp, 'galeria', [])
    galeria = this.props.galeriaEdit ? this.props.galeriaEdit : galeria

    return (
      galeria.map((item, i) => {
        let id = null
        let chekId = null
        if (this.props.galeriaEdit) {
          id = item._id ? { _id: item._id } : { preview: item.preview }
          chekId = item._id ? { _id: item._id } : { idTemp: item.idTemp }
        } else {
          id = { preview: item.preview }
          chekId = { idTemp: item.idTemp }
        }

        return (
          <MuiThemeProvider muiTheme={muiTheme} key={`teste-${i}`}>
            <div className={`Grid-cell u-size1of3 ${styles.pictures}`}>
              <div className={`${styles.content} u-posRelative`}>
                <button
                  className={styles.btn}
                  style={style.deleteBtn}
                  onClick={() => this.removeGalleryItem(id)}
                >
                  <SvgIcon
                    className={styles.iconDelete}
                    icon="icon-icon-11"
                  />
                </button>
                <img src={item.urlFoto ? item.urlFoto : item.preview} alt="text" />
                <Checkbox
                  label="Destaque"
                  style={style.checkbox}
                  labelStyle={style.checkboxLabel}
                  checked={item.fotoDestaque}
                  onClick={() => this.onChangeCheckbox(chekId)}
                />
              </div>
            </div>
          </MuiThemeProvider>
        )
      })
    )
  }

  render() {
    return (
      <div
        className="Grid-cell"
      >
        <div className="Grid">
          <div className="Grid-cell u-textLeft">
            <h2 className={styles.title}> Galeria de fotos </h2>
          </div>
          <div className="Grid-cell u-textLeft">
            <div className="Grid">
              {/* Adicionar */}
              {/* <FlatButton primary={true} label="Choose an Image">
                <input type="file" id="imageButton" style={{display: 'none'}}></input>
              </FlatButton> */}

              <div className={`Grid-cell u-size1of3 ${styles.boxDashed}`}>
                <Dropzone
                  accept=".jpg,.png,.jpeg,.gif"
                  ref={(c) => { this.dropzone = c }}
                  onDrop={this.onDrop}
                  className={styles.dropzone}
                >
                  <div className="u-posRelative">
                    <SvgIcon
                      className={styles.icons}
                      icon="icon-icon-72"
                    />
                    <a style={{ fontSize: '15px' }}>Arraste ou clique para selecionar as fotos da ação</a>
                  </div>
                </Dropzone>
              </div>
              {this.renderGallery()}
            </div>

          </div>
          <div className="Grid-cell u-textLeft">
            <p className={styles.note}>
              *As fotos devem estar em uma resolução mínima de 800x600 e ter um tamanho máximo de 5Mb cada.
            </p>
            <p className={styles.note}>
              **Marque as fotos que deseja destacar, elas aparecem em ações realizadas.
            </p>
          </div>
        </div>
      </div>
    )
  }
}

style.checkbox = {
  width: '101px',
  margin: '0 auto',
}

style.checkboxLabel = {
  marginLeft: '-8px',
}

_ActionFormGallery.propTypes = {
  gallery: PropTypes.object,
  galeriaEdit: PropTypes.array,
  idVoluntario: PropTypes.string,
  uploadFormGallery: PropTypes.func,
  handleChangeCheckbox: PropTypes.func,
  handleRemoveGalleryItem: PropTypes.func,
}

const mapStateToProps = state => ({
  gallery: state.registerFormAction,
  idVoluntario: state.loggedUser._id,
  galeriaEdit: state.acoes.acaoEdit.galeria,
})

const mapActionsToProps = {
  uploadFormGallery,
  handleChangeCheckbox,
  handleRemoveGalleryItem,
}

const ActionFormGallery = connect(mapStateToProps, mapActionsToProps)(_ActionFormGallery)
export default ActionFormGallery
