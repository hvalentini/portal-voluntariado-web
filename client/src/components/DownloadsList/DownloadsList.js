import React from 'react'
import { Scrollbars } from 'react-custom-scrollbars'
import styles from './DownloadsList.css'
import DownloadsListItem from '../../components/DownloadsListItem/DownloadsListItem'

const DownloadsList = () => {
  return (
    <div className={styles.container}>
      <Scrollbars autoHide>
        <DownloadsListItem size="57.8 MB" title="Clube da Cidadania - Guia dos voluntários" href="downloads/Clube da Cidadania - Guia dos voluntários.pdf" />
        <DownloadsListItem size="1.8 MB" title="Clube da Correspondência" href="downloads/Clube da Correspondência.pdf" />
        <DownloadsListItem size="1.1 MB" title="Clube da Leitura" href="downloads/Clube da Leitura.pdf" />
        <DownloadsListItem size="1.7 MB" title="Clube das Mídias" href="downloads/Clube das Mídias.pdf" />
        <DownloadsListItem size="16.1 MB" title="Clube do Meio Ambiente" href="downloads/Clube do Meio Ambiente.pdf" />
        <DownloadsListItem size="594 KB" title="Descrição do Programa de Voluntariado 2017" href="downloads/Descrição do Programa de Voluntariado 2017.pdf" />
        <DownloadsListItem size="162 KB" title="Política de Voluntariado Corporativo Instituto Algar_ 2017" href="downloads/Política de Voluntariado Corporativo Instituto Algar_ 2017.pdf" />
      </Scrollbars>
    </div>
  )
}

export default DownloadsList
