import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './LandingAdminInfo.css'
import { getAmountBeneficiated, getAmountActions } from '../../actions/acoes'
import { getAmountVolunteers } from '../../actions/voluntario'
import LandingStatistic from '../LandingStatistic/LandingStatistic.react'

class _LandingAdminInfo extends React.Component {

  componentDidMount() {
    this.props.getAmountBeneficiated()
    this.props.getAmountActions()
    this.props.getAmountVolunteers()
  }

  render() {
    const iconStyles = {
      color: '#44C6E2',
      fontSize: 56,
    }
    const amountStyles = {
      fontSize: '48px',
      fontWeight: '300',
      color: '#484848',
      lineHeight: 1,
    }
    const descriptionStyles = {
      fontSize: '20px',
      fontWeight: '300',
      color: '#707070',
      lineHeight: 1,
    }

    return (
      <div className={styles.container}>
        <div className={styles.boxLandingStatistics}>
          <LandingStatistic
            iconName="icon-icon-64"
            amount={this.props.voluntario.amountVolunteers}
            description="Voluntários"
            iconStyles={iconStyles}
            amountStyles={amountStyles}
            descriptionStyles={descriptionStyles}
          />
        </div>
        <div className={styles.boxLandingStatistics}>
          <LandingStatistic
            iconName="icon-icon-3"
            amount={this.props.acoes.amountBeneficiated}
            description="Pessoas beneficiadas"
            iconStyles={iconStyles}
            amountStyles={amountStyles}
            descriptionStyles={descriptionStyles}
          />
        </div>
        <div className={styles.boxLandingStatistics}>
          <LandingStatistic
            iconName="icon-icon-63"
            amount={this.props.acoes.amountActions}
            description="Ações sociais"
            iconStyles={iconStyles}
            amountStyles={amountStyles}
            descriptionStyles={descriptionStyles}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
  acoes: state.acoes,
  voluntario: state.voluntario,
})

const mapActionsToProps = {
  getAmountBeneficiated,
  getAmountActions,
  getAmountVolunteers,
}

_LandingAdminInfo.propTypes = {
  acoes: PropTypes.object,
  voluntario: PropTypes.object,
  getAmountBeneficiated: PropTypes.func,
  getAmountActions: PropTypes.func,
  getAmountVolunteers: PropTypes.func,
}

const LandingAdminInfo = connect(mapStateToProps, mapActionsToProps)(_LandingAdminInfo)
export default LandingAdminInfo
