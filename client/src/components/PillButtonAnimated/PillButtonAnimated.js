import React from 'react'
import PropTypes from 'prop-types'
import styles from './PillButtonAnimated.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'

class PillButtonAnimated extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      animate: this.props.animate || false,
    }

    this.makeAnimation = this.makeAnimation.bind(this)
  }

  makeAnimation() {
    this.setState({
      animate: !this.state.animate,
    })

    this.props.onClick()
  }

  render() {
    const animationBg = this.state.animate
                      ? [styles.bg, styles.bgShrunken]
                      : [styles.bg, styles.bgFull]

    const animationText = this.state.animate
                      ? [styles.text, styles.textRight]
                      : [styles.text, styles.textCenter]
    return (
      <button
        className={`${styles.PillButtonAnimated} ${this.props.className}`}
        onClick={() => this.makeAnimation()}
      >
        <span className={`${animationText.join(' ')}`}>
          {this.props.text}
        </span>
        <span className={`${animationBg.join(' ')}`}>
          <div className={styles.holderSvg}>
            <SvgIcon
              icon="icon-icon-48"
            />
          </div>
        </span>
      </button>
    )
  }
}

PillButtonAnimated.propTypes = {
  animate: PropTypes.bool,
  className: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
}

export default PillButtonAnimated
