import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './ActionsMade.css'
import { getAcoesFiltros } from '../../actions/acoes'
import Action from '../Action/Action.react'
import ActionTakenHeader from '../ActionTakenHeader/ActionTakenHeader'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'

class _ActionsMade extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      verificationAction: '0',
    }

    this.getAcoes = this.getAcoes.bind(this)
  }

  componentWillMount() {
    this.getAcoes()
  }

  componentDidUpdate() {
    this.getAcoes()
  }

  getAcoes() {
    if (this.state.verificationAction === this.props.selectedAction) return null

    let filtro = `{ "dataFinal": "${new Date()}", "desativada": false`

    switch (this.props.selectedAction) {
      case '1':
        filtro = `${filtro}, "comite.empresa": "${this.props.comite.empresa._id}"`
        break
      case '2':
        filtro = `${filtro}, "comite": "${this.props.comite._id}"`
        break
      case '3':
        filtro = `${filtro}, "comite.cidade": "${this.props.comite.cidade}"`
        break
      case '4':
        filtro = `${filtro}, "comite.cidadeUF": "${this.props.comite.cidadeUF}"`
        break
      case '6':
        filtro = `${filtro}, "participantes.voluntarioId": "${this.props.idVoluntario}"`
        break
      default:
        break
    }

    filtro = `${filtro} }`
    this.props.getAcoesFiltros({ type: 'ACTIONS-MADE', filtro })
    return this.setState({ verificationAction: this.props.selectedAction })
  }

  render() {
    if (this.props.loader) {
      return (
        <div className={styles.containerLoader}>
          <LoaderAdmin />
        </div>
      )
    }

    const actions = this.props.acoes
    const componentes = this.props.acoes.length !== 0
                      ? actions.map((item, i) =>
                        (
                          <Action
                            key={i}
                            action={item}
                            idVoluntario={this.props.idVoluntario}
                            userComite={this.props.comite._id}
                            gallery={item.galeria.map(foto => (foto.urlFoto))}
                          />
                        ))
                      : (
                        <div className={styles.emptyState}>
                          <div className={styles.img}>
                            <img src="/images/empty_action.svg" alt="" />
                          </div>
                          <div className={styles.text}>
                            <h1>Aaaah...</h1>
                            <h2>Nenhuma ação foi encontrada.</h2>
                          </div>
                        </div>
                        )
    return (
      <div className={styles.container}>
        <div>
          <ActionTakenHeader />
        </div>
        {componentes}
      </div>
    )
  }
}

_ActionsMade.propTypes = {
  acoes: PropTypes.array,
  selectedAction: PropTypes.string,
  idVoluntario: PropTypes.string,
  comite: PropTypes.object,
  getAcoesFiltros: PropTypes.func,
  loader: PropTypes.bool,
}

const mapStateToProps = state => ({
  acoes: state.acoes.acoes,
  selectedAction: state.statistic.volunteer.selectedAction,
  idVoluntario: state.loggedUser._id,
  comite: state.loggedUser.comite,
  loader: state.acoes.loading,
})

const mapActionsToProps = {
  getAcoesFiltros,
}

const ActionsMade = connect(mapStateToProps, mapActionsToProps)(_ActionsMade)
export default ActionsMade
