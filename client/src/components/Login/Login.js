import React from 'react'
import PropTypes from 'prop-types'
import { TextField } from 'material-ui'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PillButtonWhite from '../PillButtonWhite/PillButtonWhite'
import styles from './Login.css'
import ErrorMessage from '../ErrorMessage/ErrorMessage.react'
import {
  updateLogin,
  updateLoginUser,
  loginAuth,
} from '../../actions/login'

class _Login extends React.Component {

  constructor(props) {
    super(props)

    this.clearError = this.clearError.bind(this)
    this.submitAuth = this.submitAuth.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeUser = this.handleChangeUser.bind(this)
  }

  handleChange(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateLogin(obj)
  }

  handleChangeUser(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateLoginUser(obj)
  }

  submitAuth(ev) {
    ev.preventDefault()
    this.props.loginAuth()
  }

  clearError() {
    this.props.updateLogin({ error: null })
  }

  renderAction(disableSubmit) {
    if (this.props.login.error) {
      return (
        <ErrorMessage close={this.clearError} style={{ marginTop: '20px', width: '100%' }}>
          {this.props.login.error}
        </ErrorMessage>
      )
    }
    return (
      <div className={styles.buttons}>
        <PillButtonWhite
          text="Entrar"
          color="#01BEE8"
          disabled={disableSubmit}
          className={styles.actionBtn}
          onClick={ev => this.submitAuth(ev)}
          loader={this.props.login.loader}
        />
        <p className={styles.action}>
          Ainda não é um voluntário? <Link to="/cadastro/passo-1">Cadastre-se</Link>
        </p>
      </div>
    )
  }

  render() {
    if (this.props.login.success) {
      const user = this.props.login.volunteer
      window.location.href = `/redirect?nome=${user.nome}&email=${user.email}`
    }
    const user = this.props.login.user
    const disableSubmit = !(user.username && user.password)


    return (
      <div>
        <h1>Acesse o seu perfil para fazer a diferença</h1>
        <form>
          <div>
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-posRelative u-textLeft">
                <TextField
                  floatingLabelText="Seu E-mail/CPF"
                  floatingLabelStyle={{ color: '#fff' }}
                  name="username"
                  defaultValue={user.username}
                  onChange={e => this.handleChangeUser(e.target.name, e.target.value)}
                  maxLength={100}
                  fullWidth
                />
              </div>
            </div>
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-posRelative">
                <TextField
                  floatingLabelText="Senha"
                  floatingLabelStyle={{ color: '#fff' }}
                  name="password"
                  defaultValue={user.password}
                  onChange={e => this.handleChangeUser(e.target.name, e.target.value)}
                  maxLength={32}
                  type="password"
                  fullWidth
                />
                <Link
                  className={styles.remember}
                  onClick={this.clearError}
                  to="/recuperar-senha"
                >
                    Esqueci
                </Link>
              </div>
            </div>
          </div>
          {this.renderAction(disableSubmit)}
        </form>
      </div>
    )
  }
}

_Login.displayName = 'Login'

_Login.propTypes = {
  login: PropTypes.object.isRequired,
  loginAuth: PropTypes.func.isRequired,
  updateLogin: PropTypes.func.isRequired,
  updateLoginUser: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  login: state.login,
})

const mapActionsToProps = {
  updateLogin,
  updateLoginUser,
  loginAuth,
}

const Login = connect(mapStateToProps, mapActionsToProps)(_Login)

export default Login
