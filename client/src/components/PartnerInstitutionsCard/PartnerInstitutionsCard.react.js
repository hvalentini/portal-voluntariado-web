import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import styles from './PartnerInstitutionsCard.css'
import { openModalEdit } from '../../actions/partners'
import { getOneLocal } from '../../actions/newLocal'

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  fontFamily: 'Chantilly-Serial',
})

const PartnerInstitutionsCard = (props) => {
  const name = props.partnerInfo.nome
  const address = props.partnerInfo.endereco
  const city = props.partnerInfo.city
  const telephone = props.partnerInfo.telefone
  const id = props.partnerInfo._id

  const localEdit = (idValue) => {
    props.getOneLocal(idValue)
    props.history.push('/lider/nova-instituicao')
  }

  return (
    <div className={styles.card}>
      <MuiThemeProvider muiTheme={muiTheme}>
        <div>
          <div className={styles.info}>
            <p id={styles.name}>{name}</p>
            <p id={styles.address}>{address}</p>
            <p id="city">{city}</p>
          </div>
          <div className={styles.edit}>
            <p id="telephone">Telefone: {telephone}</p>
            <a
              className={`${styles.link} noPrint`}
              onClick={() => localEdit(id)}
              role="button"
              tabIndex="0"
            >
              Ver/Editar
            </a>
          </div>
        </div>
      </MuiThemeProvider>
      <hr className="separator" />
    </div>
  )
}

PartnerInstitutionsCard.propTypes = {
  partnerInfo: PropTypes.object,
  getOneLocal: PropTypes.func,
  history: PropTypes.object,
}

const mapActionsToProps = {
  openModalEdit,
  getOneLocal,
}

export default withRouter(connect(null, mapActionsToProps)(PartnerInstitutionsCard))
