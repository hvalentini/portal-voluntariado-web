import React from 'react'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './LandingLeaderPartnerInstitutions.css'
import PartnerInstitutions from '../PartnerInstitutions/PartnerInstitutions.react'
import PartnerInstitutionsFilters from '../PartnerInstitutionsFilters/PartnerInstitutionsFilters.react'

class LandingLeaderPartnerInstitutions extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  componentDidMount() {
    LandingLeaderPartnerInstitutions.scrollTo()
  }

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.blocks}>
          <div className={styles.filter}>
            <div className="filterLeader noPrint">
              <PartnerInstitutionsFilters />
            </div>
          </div>
          <PartnerInstitutions />
        </div>
      </div>
    )
  }
}

export default LandingLeaderPartnerInstitutions
