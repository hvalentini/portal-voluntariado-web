import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  TextField,
  SelectField,
  MenuItem,
  RadioButton,
  RadioButtonGroup,
  DatePicker,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import PillButtonFilter from '../PillButtonFilter/PillButtonFilter'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import {
  getFilterActions,
  editStartDate,
  editEndDate,
  searchActionType,
  editType,
  editFilterVoluntary,
  editDate,
  setFilters,
  editFilterVoluntaryName } from '../../actions/filters'
import { handleFetchActionsTypes } from '../../actions/registerFormAction'
import styles from './FilterActions.css'

const IntlPolyfill = require('intl')

const DateTimeFormat = IntlPolyfill.DateTimeFormat
require('intl/locale-data/jsonp/pt-BR')

const style = {}

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  checkbox: {
    checkedColor: '#01BEE8',
    boxColor: '#636363',
  },
  fontFamily: 'Chantilly-Serial',
})

class _FilterActions extends React.Component {
  static formatValue(e) {
    let value = e.toString()

    if (value.length === 1) { value = value.replace(/(\d)/, '0.0$1') }

    if (value.indexOf('.') === -1 && value.length < 4) {
      value = value.replace(/\D/g, '')
      value = value.replace(/(\d{1,2})$/, ',$1')
      value = value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    } else {
      if (value.indexOf('0.') !== -1 && value.length === 3) value = `${value}0`
      value = value.replace(/\D/g, '')
      value = value.replace(/(\d{1,2})$/, ',$1')
      value = value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    const partValue = value.substring(0, 2)

    if ((partValue === '00' && value.length === 5) || (value[0] === '0' && value.length >= 5)) {
      value = value.substring(1)
    }

    if ((partValue === '00' && value.length > 5) || (partValue === '0.')) {
      value = value.substring(2)
    }

    value = value !== '' ? `R$ ${value}` : ''
    return value
  }

  constructor(props) {
    super(props)
    this.state = {
      btnBottom: 0,
    }
    this.getFilterActions = this.getFilterActions.bind(this)
    this.handleChangeStatus = this.handleChangeStatus.bind(this)
    this.onChangeTypeActions = this.onChangeTypeActions.bind(this)
    this.handleChangeNumber = this.handleChangeNumber.bind(this)
    this.changeStartDate = this.changeStartDate.bind(this)
    this.changeEndDate = this.changeEndDate.bind(this)
    this.format = this.format.bind(this)
    this.adjustButtonPlacing = this.adjustButtonPlacing.bind(this)
  }

  componentWillMount() {
    this.props.handleFetchActionsTypes()
  }

  onChangeTypeActions(ev, index, value) {
    const filter = { ...this.props.filters, tipoAcao: value }
    this.props.editType(filter)
  }

  getFilterActions() {
    this.props.getFilterActions({ ...this.props.filters, comite: this.props.comite._id })
  }

  format() {
    if (this.props.filters.valorGasto && this.props.filters.valorGasto.length <= 2) {
      const newValue = parseFloat(`0.${this.props.filters.valorGasto}0`)
      this.handleChangeValue(newValue)
    }
    if (this.props.filters.valorGasto && this.props.filters.valorGasto.length === 3) {
      const newValue = parseFloat(`0${this.props.filters.valorGasto}`)
      this.handleChangeValue(newValue)
    }
  }

  handleChangeName(prop, value) {
    const filter = { ...this.props.filters }
    filter[prop] = value
    this.props.editFilterVoluntaryName(value)
  }


  handleChangeStatus(e) {
    const status = e.target.value
    this.props.editDate(status)
  }

  changeStartDate(event, date) {
    this.props.editDate('concluidas', date, this.props.filters.dataFinal)
  }

  changeEndDate(event, date) {
    this.props.editDate('concluidas', this.props.filters.dataInicial, date)
  }

  handleChangeNumber(prop, value) {
    const onlyNumbers = Array.isArray(value.match(/\d+/g))
                      ? value.match(/\d+/g).join('')
                      : ''
    const filter = { ...this.props.filters }
    filter[prop] = onlyNumbers
    this.props.editFilterVoluntary(filter)
  }

  handleChangeValue(value) {
    let valor = value
    if ((valor.length > 2) && (valor.indexOf('R$') !== -1)) {
      valor = valor.substring(3, (valor.length))
      valor = valor.replace(/\D/g, '')
      valor = valor.replace(/(\d{1,2})$/, '.$1')
    }

    const obj = { ...this.props.filters, valorGasto: valor }

    return this.props.editFilterVoluntary(obj)
  }

  adjustButtonPlacing(e) {
    return this.setState({
      btnBottom: -1 * e.target.scrollTop,
    })
  }

  render() {
    const showDate = this.props.status !== 'concluidas' ? 'u-hidden' : ''
    const totalBeneficiados = this.props.filters.totalBeneficiados >= 0
                            ? this.props.filters.totalBeneficiados
                            : null
    const totalVoluntarios = this.props.filters.totalParticipantes >= 0
                            ? this.props.filters.totalParticipantes
                            : null
    const gastos = !this.props.filters.valorGasto ? '' : _FilterActions.formatValue(this.props.filters.valorGasto)
    const minDate = this.props.filters.dataInicial
                  ? { minDate: this.props.filters.dataInicial }
                  : {}
    const maxDateInicial = this.props.filters.dataFinal
                          ? { maxDate: this.props.filters.dataFinal }
                          : { maxDate: new Date() }
    const maxDateFinal = { maxDate: new Date() }
    return (
      <div>
        <h2 className={styles.title}>Filtros</h2>
        <hr className={styles.separator} />
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <TextField
              hintText="Procure uma ação"
              name="nome"
              underlineStyle={style.line}
              value={this.props.nome}
              onChange={e => this.handleChangeName('nomeNormalizado', e.target.value)}
              maxLength={100}
              inputStyle={{ marginLeft: '38px' }}
              hintStyle={{ marginLeft: '38px' }}
              fullWidth
              autoComplete="off"
            />
            <SvgIcon
              icon="icon-icon-29"
              className={styles.iconSearch}
            />
            <div className={styles.containerCheckbox}>
              <p className="filterLeaderText">Status das ações</p>
              <RadioButtonGroup
                name="filters"
                onChange={this.handleChangeStatus}
                valueSelected={this.props.status}
              >
                <RadioButton
                  value="todas"
                  label="Todas as ações"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="programadas"
                  label="Agendadas"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="andamento"
                  label="Em andamento"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
                <RadioButton
                  value="concluidas"
                  label="Já ocorreu"
                  style={style.radioButton}
                  className="u-inlineBlock"
                />
              </RadioButtonGroup>
              <div className={showDate}>
                <div className={styles.date}>
                  <DatePicker
                    floatingLabelText="Data inicial"
                    cancelLabel="Cancelar"
                    hintStyle={style.black}
                    textFieldStyle={{ width: '100%', zIndex: 2, cursor: 'pointer' }}
                    DateTimeFormat={DateTimeFormat}
                    onChange={this.changeStartDate}
                    locale="pt-BR"
                    {...maxDateInicial}
                    firstDayOfWeek={0}
                  />
                  <SvgIcon
                    icon="icon-icon-32"
                    className={styles.iconDate}
                  />
                </div>
                <div className={styles.date}>
                  <DatePicker
                    floatingLabelText="Data final"
                    cancelLabel="Cancelar"
                    hintStyle={style.black}
                    textFieldStyle={{ width: '100%', zIndex: 2, cursor: 'pointer' }}
                    DateTimeFormat={DateTimeFormat}
                    onChange={this.changeEndDate}
                    locale="pt-BR"
                    {...minDate}
                    {...maxDateFinal}
                    firstDayOfWeek={0}
                  />
                  <SvgIcon
                    icon="icon-icon-32"
                    className={styles.iconDateLast}
                  />
                </div>
              </div>
            </div>
            <p className="filterLeaderText">Tipo de Ação: </p>
            <SelectField
              value={this.props.tipoAcaoSelected}
              onChange={this.onChangeTypeActions}
              labelStyle={style.menuStyle}
              underlineStyle={style.line}
              fullWidth
            >
              <MenuItem
                className="SelectBkg"
                value=""
                primaryText="Todas"
              />
              {
                this.props.tipoAcao.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={item._id}
                    primaryText={item.nome}
                    key={i}
                  />

               ))
              }
            </SelectField>
            <div className={styles.boxText}>
              <TextField
                floatingLabelText="Participantes"
                floatingLabelShrinkStyle={{ fontSize: '18px' }}
                name="voluntarios"
                underlineStyle={style.line}
                value={totalVoluntarios}
                onChange={e => this.handleChangeNumber('totalParticipantes', e.target.value)}
                maxLength={5}
                fullWidth
                disabled={this.props.status === 'andamento'}
                autoComplete="off"
                // type="number"
              />
            </div>

            <div className={styles.boxTextRight}>
              <TextField
                floatingLabelText="Beneficiados"
                floatingLabelShrinkStyle={{ fontSize: '18px' }}
                name="beneficiados"
                underlineStyle={style.line}
                value={totalBeneficiados}
                onChange={e => this.handleChangeNumber('totalBeneficiados', e.target.value)}
                maxLength={6}
                fullWidth
                disabled={this.props.status === 'andamento'}
                autoComplete="off"
                // type="number"
              />
            </div>

            <div className={styles.boxText}>
              <TextField
                floatingLabelText="Gastos"
                floatingLabelShrinkStyle={{ fontSize: '18px' }}
                name="gastos"
                underlineStyle={style.line}
                value={gastos}
                onChange={e => this.handleChangeValue(e.target.value)}
                onBlur={this.format}
                fullWidth
                disabled={this.props.status === 'andamento'}
                autoComplete="off"
              />
            </div>

            <div
              className={styles.btn}
            >
              <PillButtonFilter
                text="Filtrar"
                color="white"
                opacity="1"
                onClick={e => this.getFilterActions(e)}
                loader={this.props.loader}
              />
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

style.date = {
  width: '50%',
}

style.black = {
  color: '#484848',
  fontSize: '18px',
}

style.radioButton = {
  margin: '4px 0',
}

style.menuStyle = {
  marginBottom: '-12px',
}

style.line = {
  borderColor: 'rgb(170, 170, 170)',
}

_FilterActions.propTypes = {
  filters: PropTypes.object,
  status: PropTypes.string,
  nome: PropTypes.string,
  loader: PropTypes.bool,
  tipoAcao: PropTypes.object,
  comite: PropTypes.object,
  tipoAcaoSelected: PropTypes.number,
  editFilterVoluntary: PropTypes.func,
  editType: PropTypes.func,
  editDate: PropTypes.func,
  getFilterActions: PropTypes.func,
  editFilterVoluntaryName: PropTypes.func,
  handleFetchActionsTypes: PropTypes.func,
}

const mapStateToProps = state => ({
  filters: state.filters.acoes,
  actions: state.acoes.acoesLeader,
  status: state.filters.statusSelected,
  tipoAcao: state.registerFormAction.tipoAcaoTemp,
  tipoAcaoSelected: state.filters.tipoAcaoSelected,
  comite: state.loggedUser.comite,
  loader: state.filters.loading,
  nome: state.filters.nomeAcao,
})

const mapActionsToProps = {
  getFilterActions,
  editStartDate,
  editEndDate,
  searchActionType,
  editType,
  editFilterVoluntary,
  handleFetchActionsTypes,
  editDate,
  setFilters,
  editFilterVoluntaryName,
}

const FilterActions = connect(mapStateToProps, mapActionsToProps)(_FilterActions)
export default FilterActions
