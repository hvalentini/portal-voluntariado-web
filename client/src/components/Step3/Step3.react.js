import React from 'react'
import PropTypes from 'prop-types'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import getSalaryRange from '../../actions/faixaSalarial'
import { Step3ChangeForm, updateStep } from '../../actions/registerForm'
import styles from './Step3.css'
import UserAvatar from '../UserAvatar/UserAvatar'
import PillButtonWhite from '../PillButtonWhite/PillButtonWhite'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const style = {}

class _Step3 extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      cssAnimation: styles.leftAnimation,
    }
    this.onChangeSalaryRange = this.onChangeSalaryRange.bind(this)
    this.onChangeSchooling = this.onChangeSchooling.bind(this)
    this.onChangeShirtSize = this.onChangeShirtSize.bind(this)
    this.onChangeGender = this.onChangeGender.bind(this)
  }

  componentWillMount() {
    this.setState({
      cssAnimation: this.props.step > 3 ? styles.rightAnimation : styles.leftAnimation,
    })

    this.props.updateStep(3)
  }

  componentDidMount() {
    this.props.getSalaryRange()
  }

  onChangeSalaryRange(event, index) {
    this.props.Step3ChangeForm({ faixaSalarial: this.props.salaryRange[index] })
  }

  onChangeSchooling(event, index, value) {
    this.props.Step3ChangeForm({ escolaridade: value })
  }

  onChangeShirtSize(e) {
    this.props.Step3ChangeForm({ tamanhoCamiseta: e.target.value })
  }

  onChangeGender(e) {
    this.props.Step3ChangeForm({ genero: e.target.value })
  }

  renderLink() {
    const salary = this.props.step3.faixaSalarial
    const scholarity = this.props.step3.escolaridade
    const shirt = this.props.step3.tamanhoCamiseta
    const gender = this.props.step3.genero

    const opacity = salary && scholarity && shirt && gender ? '1' : '0.5'
    const disabled = salary && scholarity && shirt && gender ? false : true

    if (!disabled) {
      return (
        <Link to="passo-4">
          <PillButtonWhite
            text="Próximo"
            color="#01BEE8"
            disabled={false}
            opacity={opacity}
            style={{ margin: 0 }}
          />
        </Link>
      )
    }
    return (
      <div className={styles.linkNext}>
        <PillButtonWhite
          text="Próximo"
          color="#01BEE8"
          disabled
          opacity={opacity}
          style={{ margin: 0 }}
        />
      </div>
    )
  }

  render() {
    const salary = this.props.step3.faixaSalarial ? this.props.step3.faixaSalarial : {}
    const shirtSizes = [
      { value: 'P', label: 'P' },
      { value: 'M', label: 'M' },
      { value: 'G', label: 'G' },
      { value: 'GG', label: 'GG' },
      { value: 'EXG', label: 'EXG' },
    ]

    const salaryRange = this.props.salaryRange

    const schooling = [
      { value: 'Nível fundamental', text: 'Nível fundamental' },
      { value: 'Nível médio', text: 'Nível médio' },
      { value: 'Nível superior', text: 'Nível superior' },
      { value: 'Pós graduação', text: 'Pós graduação' },
      { value: 'Mestrado', text: 'Mestrado' },
      { value: 'Doutorado', text: 'Doutorado' },
      { value: 'Pós doutorado', text: 'Pós doutorado' },
    ]

    const note = ' *Essa informação será utilizada apenas para o calcúlo de investimento indireto em ações voluntárias, e não será divulgada.'
    const size = window.innerHeight > 800
                ? 'large'
                : 'medium'
    return (
      <div className={styles.formBg}>
        <div className={styles.avatarStep}>
          <UserAvatar
            url={this.props.step1.urlAvatar}
            size={size}
          />
        </div>
        <h1>{this.props.step1.nome}</h1>
        <div className={styles.containerRadios}>
          <div className={this.state.cssAnimation}>
            <div className="Grid-cell">
              <div className={`${styles.radioButton} Grid-cell u-lg-size5of12 u-textLeft u-inlineBlock`}>
                <p className={`${styles.labelText} u-textLeft`}>Gênero</p>
                <RadioButtonGroup
                  name="gender"
                  defaultSelected={this.props.step3.genero}
                  onChange={this.onChangeGender}
                >
                  <RadioButton
                    value="masculino"
                    label="Masculino"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value="feminino"
                    label="Feminino"
                    style={style.radioButton}
                    labelStyle={style.radioLabel}
                    className="u-inlineBlock"
                  />
                </RadioButtonGroup>
              </div>
              <div className="Grid-cell u-lg-size7of12 u-textLeft u-inlineBlock">
                <p className={`${styles.labelText} u-textLeft`}>Tamanho da camiseta</p>
                <RadioButtonGroup
                  name="shirtSize"
                  defaultSelected={this.props.step3.tamanhoCamiseta ? this.props.step3.tamanhoCamiseta : ''}
                  onChange={this.onChangeShirtSize}
                >
                  {
                    shirtSizes.map((item, i) => (
                      <RadioButton
                        value={item.value}
                        label={item.label}
                        style={style.radioButtonSize}
                        labelStyle={style.radioLabel}
                        className="u-inlineBlock"
                        key={i}
                      />
                    ))
                  }
                </RadioButtonGroup>
              </div>
            </div>
            <div className="Grid-cell">
              <div className={`${styles.selectField} Grid-cell u-lg-size5of12 u-textLeft u-posRelative`}>
                <p className={`${styles.labelSelect} u-textLeft`}>Faixa salarial</p>
                <SelectField
                  value={salary._id}
                  onChange={this.onChangeSalaryRange}
                  hintText="Selecionar"
                  hintStyle={{ color: '#FFF' }}
                  style={style.itemMenu}
                  fullWidth
                >
                  {
                    salaryRange.map((item, i) => (
                      <MenuItem
                        className="SelectBkg"
                        value={item._id}
                        primaryText={item.nome}
                        style={style.menuStyle}
                        key={i}
                      />
                    ))
                  }
                </SelectField>
              </div>
            </div>
            <div className="Grid-cell u-lg-size11of12">
              <p className={`${styles.labelNote} u-textLeft`}>{note}</p>
            </div>
            <div className="Grid-cell">
              <div className={`${styles.selectFieldSchooling} Grid-cell u-lg-size5of12 u-textLeft u-posRelative`}>
                <p className={`${styles.labelSelect} u-textLeft`}>Escolaridade</p>
                <SelectField
                  value={this.props.step3.escolaridade}
                  onChange={this.onChangeSchooling}
                  hintText="Selecionar"
                  hintStyle={{ color: '#FFF' }}
                  style={style.itemMenu}
                  fullWidth
                >
                  {
                    schooling.map((item, i) => (
                      <MenuItem
                        className="SelectBkg"
                        value={item.value}
                        primaryText={item.text}
                        style={style.menuStyle}
                        key={i}
                      />
                    ))
                  }
                </SelectField>
              </div>
            </div>
          </div>
          <div className={styles.buttons}>
            <Link
              to="passo-2"
              className={styles.btnBack}
            >
              <SvgIcon
                icon="icon-icon-10"
                className={styles.icon}
              />
              Voltar
            </Link>
            {this.renderLink()}
          </div>
        </div>
      </div>
    )
  }
}

_Step3.propTypes = {
  salaryRange: PropTypes.array,
  step3: PropTypes.object,
  step1: PropTypes.object,
  step: PropTypes.number,
  getSalaryRange: PropTypes.func,
  Step3ChangeForm: PropTypes.func,
  updateStep: PropTypes.func,
}

style.radioButton = {
  marginTop: '7px',
  width: '50%',
  fontSize: '20px',
}

style.radioButtonSize = {
  marginTop: '7px',
  width: '16%',
  fontSize: '20px',
}

style.radioLabel = {
  marginLeft: '-14px',
}

style.menuStyle = {
  color: '#171515',
}

style.itemMenu = {
  position: 'absolute',
}

const mapStateToProps = state => ({
  salaryRange: state.faixaSalarial.salaryRange,
  step3: state.registerForm.step3,
  step1: state.registerForm.step1,
  step: state.registerForm.step,
})

const mapActionsToProps = {
  getSalaryRange,
  Step3ChangeForm,
  updateStep,
}

const Step3 = connect(mapStateToProps, mapActionsToProps)(_Step3)

export default Step3
