import React from 'react'
import PropTypes from 'prop-types'
import { CircularProgress, TextField } from 'material-ui'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import styles from './Step1.css'
import UserAvatar from '../UserAvatar/UserAvatar'
import PillButtonWhite from '../PillButtonWhite/PillButtonWhite'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import SpinnerSuccess from '../SpinnerSuccess/SpinnerSuccess.react'
import SpinnerFail from '../SpinnerFail/SpinnerFail.react'
import ErrorMessage from '../ErrorMessage/ErrorMessage.react'
import { updateStep, updateStep1, validateCPF, updateErrorCPF, clearName } from '../../actions/registerForm'

class _Step1 extends React.Component {
  static isCpfValid(cpf) {
    let add = 0
    let rev

    // Elimina CPFs invalidos conhecidos
    if (cpf.length !== 11 || cpf.match(new RegExp(cpf.charAt(0), 'g')).length === 11) {
      return false
    }
    // Valida 1o digito

    for (let i = 0; i < 9; i += 1) {
      add += parseInt(cpf.charAt(i), 10) * (10 - i)
    }
    rev = 11 - (add % 11)
    if (rev === 10 || rev === 11) {
      rev = 0
    }
    if (rev !== parseInt(cpf.charAt(9), 10)) {
      return false
    }

    // Valida 2o digito
    add = 0
    for (let i = 0; i < 10; i += 1) {
      add += parseInt(cpf.charAt(i), 10) * (11 - i)
    }
    rev = 11 - (add % 11)
    if (rev === 10 || rev === 11) {
      rev = 0
    }
    if (rev !== parseInt(cpf.charAt(10), 10)) {
      return false
    }
    return true
  }

  constructor(props) {
    super(props)

    this.state = {
      cpfMasked: '',
      cpf: '',
      cpfInvalid: true,
      finishValidate: false,
      optsButton: {},
      optsTextField: {},
      cssAnimation: styles.divAnimation,
    }
    this.maskField = this.maskField.bind(this)
    this.updateStep1 = this.updateStep1.bind(this)
  }

  componentWillMount() {
    this.setState({
      cssAnimation: this.props.registerForm.step <= 1 ? styles.divAnimation : styles.divAvatar,
    })

    if (this.props.registerForm.step1.cpf &&
        this.props.registerForm.step1.cpf !== this.state.cpf &&
        this.props.registerForm.step === 1) {
      this.props.updateErrorCPF('No seu registro consta um outro CPF. Favor tentar novamente!')
      this.state.cpfInvalid = true
    }
    if (this.props.registerForm.step !== 1 && this.props.registerForm.step1.cpf) {
      this.setState({
        cpf: this.props.registerForm.step1.cpf,
        cpfMasked: this.props.registerForm.step1.cpfMasked,
        finishValidate: true,
        cpfInvalid: false,
      })
    }

    this.props.updateStep(1)

    if (this.state.cpf !== this.props.registerForm.step1.cpf
        && this.state.finishValidate
        && this.props.registerForm.voluntarioUI.nomeCompleto) {
      this.updateStep1(this.state.cpf, this.state.cpfMasked, this.props.registerForm.voluntarioUI)
    }
  }

  maskField(e) {
    if (!e.target.value) {
      return this.setState({
        cpfMasked: '',
        cpf: '',
      })
    }
    const onlyNumbers = Array.isArray(e.target.value.match(/\d+/g))
                      ? e.target.value.match(/\d+/g).join('')
                      : ''
    const inputVal = onlyNumbers
                    .replace(/(\d{3})(\d)/, '$1.$2')
                    .replace(/(\d{3})(\d)/, '$1.$2')
                    .replace(/(\d{3})(\d)/, '$1-$2')

    let finish = false

    this.props.registerForm.voluntarioUI = {}
    this.props.registerForm.errorCpf = null

    if (onlyNumbers.length === 11) {
      if (_Step1.isCpfValid(onlyNumbers)) {
        this.state.cpfInvalid = false
        const voluntarioIncompleto = this.props.registerForm.step1.voluntarioIncompleto
        this.props.clearName()
        this.props.validateCPF(onlyNumbers, voluntarioIncompleto, inputVal)
      } else {
        this.props.registerForm.voluntarioUI = {}
        this.state.cpfInvalid = true
      }

      finish = true
    }

    return this.setState({
      cpfMasked: inputVal,
      cpf: onlyNumbers,
      finishValidate: finish,
    })
  }

  updateStep1(CPF, CPFMASKED, VOLUNTARIO) {
    this.props.updateStep1({
      cpf: CPF,
      cpfMasked: CPFMASKED,
      voluntarioUI: VOLUNTARIO,
    })
  }

  render() {
    let nextStep
    let displayButton
    let card
    let nameOutPoint = this.props.registerForm.voluntarioUI.nomeCompleto
                        ? this.props.registerForm.voluntarioUI.nomeCompleto.split(' ', 1)
                        : ''

    nameOutPoint = this.props.registerForm.step1.nome
                  ? this.props.registerForm.step1.nome
                  : nameOutPoint
    const size = window.innerHeight > 800
                ? 'another'
                : 'medium'

    if (this.state.finishValidate) {
      const footer = 'Por favor, entre em contato com o Líder Social ' +
        'da sua empresa ou envie um email para ' +
        'contato@institutoalgar.org.br'
      if (this.props.registerForm.errorCpf) {
        card = (
          <ErrorMessage footer={footer} style={{ width: '520px' }}>
            {this.props.registerForm.errorCpf}
          </ErrorMessage>
        )
        nextStep = (<div className={styles.loading}>
          <SpinnerFail />
        </div>)
      } else if (this.props.registerForm.voluntarioUI.dataDemissao) {
        card = (<ErrorMessage footer={footer} style={{ width: '520px' }}>Data de demissão preenchida.</ErrorMessage>)
        nextStep = (<div className={styles.loading}>
          <SpinnerFail />
        </div>)
      } else if (this.state.cpfInvalid) {
        card = (<ErrorMessage footer={footer} style={{ width: '520px' }}>CPF inválido.</ErrorMessage>)
        nextStep = (<div className={styles.loading}>
          <SpinnerFail />
        </div>)
      } else if (!this.state.cpfInvalid && this.props.registerForm.step1.nome) {
        displayButton = (<Link
          to="passo-2"
        >
          <PillButtonWhite
            text="Sim, sou eu"
            color="#01BEE8"
            onClick={this.enableButton}
            style={{ display: displayButton, margin: 0 }}
          />
        </Link>)

        nextStep = (
          <div className={styles.loading}>
            <SpinnerSuccess />
          </div>
          )
        card = (<div>
          <span>Nome: {this.props.registerForm.voluntarioUI.nomeCompleto
                      || this.props.registerForm.step1.nomeCompleto}</span>
          <br />
          <span>Cargo: {this.props.registerForm.voluntarioUI.cargoNome
            || this.props.registerForm.voluntarioCompleto.cargoNome}</span>
          <br />
          <span>CR: {this.props.registerForm.voluntarioUI.centroResultadoNome
            || this.props.registerForm.voluntarioCompleto.centroResultadoNome}</span>
          <br />
        </div>)
      } else if (this.props.registerForm.loader) {
        nextStep = (<CircularProgress className={styles.circular} thickness={2} />)
      }
    }

    return (
      <div className={styles.containerFormBg}>
        <div className={styles.formBg}>
          <div className={this.state.cssAnimation}>
            <UserAvatar
              url={this.props.registerForm.step1.urlAvatar || '/images/user-avatar.svg'}
              size={size}
            />
          </div>
          <div className={styles.texto}>
            <h1>{nameOutPoint}</h1>
            <h2>Vamos Começar?</h2>
          </div>
          <div className={styles.inputSpinner}>
            <TextField
              floatingLabelText="Digite o seu CPF"
              floatingLabelStyle={{
                color: '#fff',
              }}
              style={{ width: '440px', marginBottom: '40px' }}
              value={this.state.cpfMasked}
              onInput={this.maskField}
              maxLength={14}
            />
            {nextStep}
          </div>
          {card}
        </div>
        <div className={styles.buttons}>
          <Link
            to="/entrar"
            className={styles.btnBack}
          >
            <SvgIcon
              icon="icon-icon-10"
              className={styles.icon}
            />
            Voltar
          </Link>
          {displayButton}
        </div>
      </div>
    )
  }
}

_Step1.propTypes = {
  validateCPF: PropTypes.func,
  updateStep: PropTypes.func,
  updateStep1: PropTypes.func,
  updateErrorCPF: PropTypes.func,
  clearName: PropTypes.func,
  registerForm: PropTypes.object,
}

const mapStateToProps = state => ({
  registerForm: state.registerForm,
})

const mapActionsToProps = {
  validateCPF,
  updateStep1,
  updateStep,
  updateErrorCPF,
  clearName,
}

const Step1 = connect(mapStateToProps, mapActionsToProps)(_Step1)

export default Step1
