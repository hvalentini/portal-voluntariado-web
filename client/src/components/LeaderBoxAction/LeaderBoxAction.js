import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import currencyFormatter from 'currency-formatter'
import { Link } from 'react-router-dom'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './LeaderBoxAction.css'
import AvatarGroup from '../AvatarGroup/AvatarGroup'
import ModalListParticipants from '../ModalListParticipants/ModalListParticipants'

class LeaderBoxAction extends React.Component {
  static renderNatureAction(type) {
    if (type === 1) {
      return (
        <span>
          <SvgIcon icon="icon-icon-66" className={styles.actionTypeIcon} />
          Contínua
        </span>
      )
    } else if (type === 2) {
      return (
        <span>
          <SvgIcon icon="icon-icon-65" className={styles.actionTypeIcon} />
          Gerencial
        </span>
      )
    }
    return (
      <span>
        <SvgIcon icon="icon-icon-67" className={styles.actionTypeIcon} />
        Pontual
      </span>
    )
  }

  static formatarData(value) {
    const data = new Date(value)
    data.setTime(data.getTime() + (data.getTimezoneOffset() * 60000))

    const dia = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate()
    const mes = data.getMonth() + 1 < 10 ? `0${data.getMonth() + 1}` : data.getMonth() + 1
    const ano = data.getFullYear()
    const dataFormatada = `${dia}/${mes}/${ano}`

    const min = data.getMinutes()
    const minFinal = min.toString().length === 1 ? `0${data.getMinutes()}` : data.getMinutes()

    const horaF = data.getHours().toString().length === 1 ? `0${data.getHours()}` : data.getHours()
    const horaFinal = `${horaF}:${minFinal}`
    const dataFinal = horaFinal !== '00:00' ? `${dataFormatada} às ${horaFinal}` : dataFormatada
    return dataFinal
  }

  constructor(props) {
    super(props)
    this.state = {
      modalOpen: false,
      users: [],
      title: '',
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.renderComittee = this.renderComittee.bind(this)
  }

  openModal(ev, users, title) {
    ev.preventDefault()
    const open = users.length > 0
    this.setState({
      modalOpen: open,
      users,
      title,
    })
  }

  closeModal() {
    this.setState({
      modalOpen: false,
      users: [],
    })
  }

  renderComittee() {
    if (this.props.isAdmin) {
      return (
        <div className={`Grid ${styles.boxLine}`}>
          <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
            <div className={`Grid ${styles.boxLine}`}>
              <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                <span className={styles.itemTitle}>Empresa</span>
                <span className={styles.itemText}>{this.props.acao.comite.empresa.nome || '' }</span>
              </div>
              <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                <span className={styles.itemTitle}>Unidade</span>
                <span className={styles.itemText}>{this.props.acao.comite.unidade || '' }</span>
              </div>
            </div>
          </div>
          <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
            <span className={styles.itemTitle}>Cidade/UF</span>
            <span className={styles.itemText}>{this.props.acao.comite.cidadeNome || ''}/{this.props.acao.comite.cidadeUF || '' }</span>
          </div>
        </div>
      )
    }

    return ''
  }


  render() {
    const acao = this.props.acao

    const beneficiados = acao.totalBeneficiados === 0 ? '-' : acao.totalBeneficiados

    let gasto = '-'

    if (acao.valorGasto > 0) {
      gasto = currencyFormatter.format(acao.valorGasto, { code: 'BRL' })
      gasto = `R$${gasto.substring(2)}`
    }


    const dataFinal = LeaderBoxAction.formatarData(acao.dataFinal)
    const dataInicial = LeaderBoxAction.formatarData(acao.dataInicial)

    let status = '/em andamento'
    if (new Date(acao.dataFinal) < new Date()) {
      status = acao.cadastroRetroativo
              ? '/finalizada'
              : <span style={{ color: '#FF8A00' }}>/não finalizada</span>
    }
    if ((new Date(acao.dataInicial) > new Date()) && (new Date(acao.dataFinal) > new Date())) status = '/agendada'

    const customStyle = {
      overlay: {
        zIndex: 9,
        backgroundColor: 'rgba(0, 0, 0, 0.54)',
      },
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderRadius: '10px',
        padding: '15px',
      },
    }
    const local = acao.local ? acao.local.nome : ''
    const showLocal = local ? '' : 'u-hidden'

    const linkEditAction = this.props.user.adminLogin
                          ? `/admin/acoes/editar/${acao._id}`
                          : `/lider/editar-acao/${acao._id}`

    return (
      <Link
        to={linkEditAction}
        className={styles.rootLink}
        key={acao._id}
      >
        <div className={styles.root}>
          <div className={styles.boxRow}>
            <Modal
              isOpen={this.state.modalOpen}
              onRequestClose={() => this.closeModal()}
              style={customStyle}
              contentLabel={''}
            >
              <div className={styles.modalHeader} id="VolunteerInfoIcon">
                <SvgIcon
                  className={styles.iconClose}
                  icon="icon-icon-11"
                  onClick={() => this.closeModal()}
                />
              </div>
              <ModalListParticipants
                users={this.state.users}
                title={this.state.title}
              />
            </Modal>
            <div className={`Grid ${styles.boxLine}`}>
              <div className={`Grid-cell u-size1of2  ${styles.title} ${styles.boxColumn}`}>{acao.nome}</div>
              <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                <div className={`Grid ${styles.boxLine}`}>
                  <div className={`Grid-cell u-size1of3 ${styles.boxColumn}`}>
                    <span className={styles.itemTitle}>{status}</span>
                    <span className={styles.itemText}>
                      {LeaderBoxAction.renderNatureAction(this.props.acao.tipoAcao.tipo) }
                    </span>
                  </div>
                  <div className={`Grid-cell u-size1of3 ${styles.boxColumn}`}>
                    <span className={styles.itemTitle}>Data de início</span>
                    <span className={styles.itemText}>{dataInicial}</span>
                  </div>
                  <div className={`Grid-cell u-size1of3 ${styles.boxColumn}`}>
                    <span className={styles.itemTitle}>Data de término</span>
                    <span className={styles.itemText}>{dataFinal}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className={`Grid ${styles.boxLine}`}>
              <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                <span className={styles.itemTitle}>Tipo de ação</span>
                <span className={styles.itemText}>{acao.tipoAcao.nome}</span>
              </div>
              <div className={`Grid-cell u-size1of2 ${showLocal} ${styles.boxColumn}`}>
                <span className={styles.itemLocal}>
                  <SvgIcon
                    className={styles.actionTypeIcon}
                    icon="icon-icon-68"
                  />
                  {local}
                </span>
              </div>
            </div>
            {this.renderComittee()}
            <div className={`Grid ${styles.boxLine}`}>
              <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                <div className={`Grid ${styles.boxLine}`}>
                  <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                    <span className={styles.itemTitle}>Benefíciados</span>
                    <span className={styles.itemText}>{beneficiados}</span>
                  </div>
                  <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                    <span className={styles.itemTitle}>Valor Gasto</span>
                    <span className={styles.itemText}>{gasto}</span>
                  </div>
                </div>
              </div>
              <div className={`Grid-cell u-size1of2 ${styles.boxColumn}`}>
                <div className={`Grid ${styles.boxLine}`}>
                  <div className={`Grid-cell u-size1of3 ${styles.boxColumn}`}>
                    <button
                      className={styles.avatar}
                      onClick={value => this.openModal(value, acao.queremParticipar, 'Pessoas interessadas em participar')}
                    >
                      <AvatarGroup
                        title="Interessados"
                        titleClass={styles.itemTitle}
                        avatars={acao.queremParticipar}
                        qtdMinima={3}
                        sizeAvatar="smaller"
                      />
                    </button>
                  </div>
                  <div className={`Grid-cell u-size1of3 ${styles.boxColumn}`}>
                    <button
                      className={styles.avatar}
                      onClick={value => this.openModal(value, acao.gostaram, 'Pessoas que gostaram da ação')}
                    >
                      <AvatarGroup
                        title="Gostaram"
                        titleClass={styles.itemTitle}
                        avatars={acao.gostaram}
                        qtdMinima={3}
                        sizeAvatar="smaller"
                      />
                    </button>
                  </div>
                  <div className={`Grid-cell u-size1of3 ${styles.boxColumn}`}>
                    <button
                      className={styles.avatar}
                      onClick={value => this.openModal(value, acao.participantes, 'Pessoas que estão participando')}
                    >
                      <AvatarGroup
                        title="Participantes"
                        titleClass={styles.itemTitle}
                        avatars={acao.participantes}
                        qtdMinima={3}
                        sizeAvatar="smaller"
                      />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr className={styles.separator} />
        </div>
      </Link>
    )
  }
}

LeaderBoxAction.propTypes = {
  acao: PropTypes.object,
  isAdmin: PropTypes.string,
  user: PropTypes.object,
}

export default LeaderBoxAction
