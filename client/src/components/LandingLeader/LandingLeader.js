import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import LeaderDashboard from '../../components/LeaderDashboard/LeaderDashboard'
import ActionsMade from '../../components/ActionsMade/ActionsMade.react'
import GalleryModal from '../../components/ActionsGallery/GalleryModal.react'
import LandingLeaderNewAction from '../../components/LandingLeaderNewAction/LandingLeaderNewAction'
import EditInstitutionsPage from '../../pages/EditInstitutionsPage/EditInstitutionsPage'
import Footer from '../../components/Footer/Footer'
import styles from './LandingLeader.css'
import { getLoggedUser } from '../../actions/loggedUser'
import ActionsPage from '../../pages/ActionsPage/ActionsPage'
import InstitutionsPage from '../../pages/InstitutionsPage/InstitutionsPage'
import LandingVolunteerPage from '../../pages/LandingVolunteerPage/LandingVolunteerPage'
import CarouselCommiteeActions from '../../components/CarouselCommiteeActions/CarouselCommiteeActions.react'
import CommiteeCarouselFilter from '../../components/CommiteeCarouselFilter/CommiteeCarouselFilter.react'

class _LandingLeader extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      filterCommiteeCarousel: null,
    }

    this.selectTab = this.selectTab.bind(this)
  }

  componentWillMount() {
    this.props.getLoggedUser()
  }

  selectTab(filter) {
    this.setState({
      filterCommiteeCarousel: filter,
    })
  }

  render() {
    if (!this.props.loggedUser._id) {
      return null
    }
    return (
      <Router>
        <div className="Grid">
          <div className={`${styles.blocks} noPrint`}>
            <LeaderDashboard />
          </div>
          <Route
            exact path="/lider" render={() => (
              <div className={styles.blocks}>
                <div className={styles.blocks}>
                  <CommiteeCarouselFilter
                    selectTab={this.selectTab}
                  />
                  <CarouselCommiteeActions
                    filter={this.state.filterCommiteeCarousel}
                  />
                </div>
                <GalleryModal />
                <div className={styles.actionsMade}>
                  <ActionsMade />
                </div>
              </div>
            )}
          />
          <Route path="/lider/nova-acao" component={LandingLeaderNewAction} />
          <Route path="/lider/acoes-voluntarias" component={ActionsPage} />
          <Route path="/lider/gestao-de-voluntarios" component={LandingVolunteerPage} />
          <Route
            path="/lider/instituicoes-parceiras"
            component={InstitutionsPage}
          />
          <Route path="/lider/editar-acao/:id" component={LandingLeaderNewAction} />
          <Route path="/lider/nova-instituicao" component={EditInstitutionsPage} />
          <Route path="/lider/editar-instituicao/:id" component={EditInstitutionsPage} />
          <div style={{ paddingTop: '160px' }} className={`noPrint ${styles.blocks}`}>
            <Footer />
          </div>
        </div>
      </Router>
    )
  }
}

_LandingLeader.propTypes = {
  loggedUser: PropTypes.object,
  getLoggedUser: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  getLoggedUser,
}

const LandingLeader = connect(mapStateToProps, mapActionsToProps)(_LandingLeader)

export default LandingLeader
