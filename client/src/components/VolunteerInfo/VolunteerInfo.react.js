import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import get from 'lodash.get'
import Shave from '../Shave'
import styles from './VolunteerInfo.css'
import Avatar from '../Avatar/Avatar'
import ModalVolunteerInfo from '../ModalVolunteerInfo/ModalVolunteerInfo.react'
import IconButtonPlus from '../IconButtonPlus/IconButtonPlus'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const customStyle = {
  overlay: {
    zIndex: 9,
    backgroundColor: 'rgba(0, 0, 0, 0.54)',
  },
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    borderRadius: '10px',
  },
}

class _VolunteerInfo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpen: false,
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  openModal() {
    this.setState({
      modalOpen: true,
    })
  }

  closeModal() {
    this.setState({
      modalOpen: false,
    })
  }

  render() {
    const name = get(this.props, 'info.nome')
    const cargo = get(this.props, 'info.cargoNome')
    const participation = get(this.props, 'info.quantidadeDeParticipacoes')

    const avatar = get(this.props, 'info.urlAvatar')
    return (
      <div className={`${styles.boxInfo} heightInfo`}>
        <a
          className={styles.gridInfo}
          onClick={() => this.openModal()}
          tabIndex="0"
          role="button"
        >
          <Modal
            isOpen={this.state.modalOpen}
            onRequestClose={() => this.closeModal()}
            style={customStyle}
            contentLabel={''}
          >
            <div className={styles.modalHeader} id="VolunteerInfoIcon">
              <SvgIcon
                className={styles.iconClose}
                icon="icon-icon-11"
                onClick={() => this.closeModal()}
              />
            </div>
            <ModalVolunteerInfo
              info={this.props.info}
            />
          </Modal>
          <div className={styles.AvatarBox}>
            <div className={styles.AvatarButton}>
              <Avatar
                size="exMedium"
                url={avatar}
              />
              <IconButtonPlus
                background1="#00BEE7"
                background2="#00bee7"
                icon="icon-icon-47"
                key={this.props.newKey}
              />
            </div>
            <div className={`${styles.name} textName`}>
              <Shave
                key={name}
                maxHeight={50}
              >
                {name}
              </Shave>
            </div>
            <div className={`${styles.cargo} textRole`}>
              <Shave
                key={cargo}
                maxHeight={60}
              >
                {cargo}
              </Shave>
            </div>
            <p className={`${styles.participation}`}>{`Participação em ações: ${participation}`}</p>
          </div>
        </a>
      </div>
    )
  }
}

_VolunteerInfo.propTypes = {
  info: PropTypes.object,
  newKey: PropTypes.string,
}

const mapStateToProps = state => ({
  voluntario: state.voluntario,
})

const VolunteerInfo = connect(mapStateToProps, null)(_VolunteerInfo)

export default VolunteerInfo
