import React from 'react'
import PropTypes from 'prop-types'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import styles from './ContentAdmin.css'

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#3f93b7',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  datePicker: {
    color: '#000',
    textColor: '#FFF',
    calendarTextColor: '#000',
    selectColor: '#01BEE8',
    selectTextColor: '#FFF',
    calendarYearBackgroundColor: '#fff',
  },
  timePicker: {
    color: '#000',
    textColor: '#000',
    clockColor: '#FFF',
    clockCircleColor: '#FFF',
    selectColor: '#01BEE8',
    selectTextColor: '#01BEE8',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  fontFamily: 'Chantilly-Serial',
  fontSize: '18px',
})

class ContentAdmin extends React.Component {

  handleContent() {
    if (this.props.useTheme) {
      return (
        <MuiThemeProvider muiTheme={muiTheme}>
          {this.props.children}
        </MuiThemeProvider>
      )
    }
    return (
      this.props.children
    )
  }

  render() {
    const fullWidth = this.props.fullWidth ? null : styles.container
    return (
      <div className={styles.formBg}>
        <div className={[styles.bottom, this.props.titleClass].join(' ')}>
          <h3 className={styles.title}>{this.props.title}</h3>
        </div>
        <div className={fullWidth}>
          {this.handleContent()}
        </div>
      </div>
    )
  }
}

ContentAdmin.propTypes = {
  title: PropTypes.string,
  useTheme: PropTypes.bool,
  children: PropTypes.node,
  fullWidth: PropTypes.bool,
  titleClass: PropTypes.string,
}

ContentAdmin.defaultProps = {
  useTheme: true,
}
export default ContentAdmin
