import React from 'react'
import PropTypes from 'prop-types'
import styles from './SectionVideoAboutUs.css'
import MoreAboutUs from '../../components/MoreAboutUs/MoreAboutUs'
import LandingVideo from '../../components/LandingVideo/LandingVideo.react'

const SectionVideoAboutUs = () => (
  <section>
    <div className={styles.moreAboutUs} id="video">
      <MoreAboutUs />
    </div>
    <div className={styles.landingVideo}>
      <LandingVideo
        url="/videos/VIDEO_VOLUNTARIADO"
        cover="/images/IMG_VIDEO.png"
      />
    </div>
  </section>
  )

SectionVideoAboutUs.displayName = 'SectionVideoAboutUs'

SectionVideoAboutUs.propTypes = {
  url: PropTypes.string.isRequired,
  cover: PropTypes.string.isRequired,
}

export default SectionVideoAboutUs
