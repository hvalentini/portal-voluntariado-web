import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import get from 'lodash.get'
import styles from './LeaderDashboard.css'
import LeaderAvatar from '../LeaderAvatar/LeaderAvatar.react'
import Statistic from '../Statistic/Statistic'
import LeaderTabs from '../LeaderTabs/LeaderTabs'
import OutlinePillButton from '../OutlinePillButton/OutlinePillButton'
import AvailableBalance from '../AvailableBalance/AvailableBalance'
import { readNotifications } from '../../actions/notificacao'
import { handleGetSaldoAtual } from '../../actions/comites'

class _LeaderDashboard extends React.Component {
  componentDidMount() {
    this.props.handleGetSaldoAtual(get(this.props.loggedUser.comite, '_id'))
  }

  render() {
    const loggedUser = this.props.loggedUser
    return (
      <div className="Grid">
        <div className={styles.leftCorner}>
          <div className={styles.leftCornerGrid}>
            <div className={styles.leftCornerGridCell}>
              <AvailableBalance
                saldoAtual={get(this.props.comite, 'saldoAtual', 0)}
              />
            </div>
            <div className={styles.balanceAndActionGridCell}>
              <Link to="/lider" className={styles.buttonHome}>Home</Link>
              <Link to="/lider/nova-acao" style={{ display: 'inline-block', verticalAlign: 'top' }}>
                <OutlinePillButton
                  text="Nova Ação"
                />
              </Link>
            </div>
          </div>
          <div className="Grid">
            <div className="Grid-cell">
              <Statistic
                idComite={loggedUser.comite._id}
              />
            </div>
          </div>
        </div>
        <div className={styles.rightCorner}>
          <div>
            <LeaderTabs
              numberNotifications={this.props.notificacao.amountNotifications}
              loggedUser={loggedUser}
              readNotifications={this.props.notificacao.readNotifications}
              readNotificationsFunction={this.props.readNotifications}
            >
              <LeaderAvatar
                url={loggedUser.urlAvatar || '/images/user-avatar.svg'}
                name={loggedUser.nome}
                companyName={loggedUser.comite.empresa.nome}
                companyUnit={loggedUser.comite.unidade}
                category={'Líder Social'}
              />
            </LeaderTabs>
          </div>
        </div>
      </div>
    )
  }
}

_LeaderDashboard.propTypes = {
  loggedUser: PropTypes.object,
  notificacao: PropTypes.object,
  comite: PropTypes.object,
  readNotifications: PropTypes.func,
  handleGetSaldoAtual: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
  notificacao: state.notificacao,
  comite: state.comites,
})

const mapActionsToProps = { readNotifications, handleGetSaldoAtual }


const LeaderDashboard = connect(mapStateToProps, mapActionsToProps)(_LeaderDashboard)

export default LeaderDashboard
