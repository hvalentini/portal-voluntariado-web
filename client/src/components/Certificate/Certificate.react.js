import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { DatePicker, Dialog, FlatButton } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import styles from './Certificate.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import PillButtonWhite from '../../components/PillButtonWhite/PillButtonWhite'
import URL from '../../../../server/constant/urls'
import { getVolunteerCreditHour } from '../../actions/voluntario'

const IntlPolyfill = require('intl')

const DateTimeFormat = IntlPolyfill.DateTimeFormat

require('intl/locale-data/jsonp/pt-BR')

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    secondaryTextColor: '#00BEE7',
    primary1Color: '#484848',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  textField: {
    errorColor: '#FFF',
  },
  fontFamily: 'Chantilly-Serial',
})

class _Certificate extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      startDate: null,
      endDate: null,
      startError: '',
      endError: '',
      dialogError: '',
      isOpen: false,
      loader: false,
    }

    this.changeStartDate = this.changeStartDate.bind(this)
    this.changeEndDate = this.changeEndDate.bind(this)
    this.downloadCertificate = this.downloadCertificate.bind(this)
    this.checkForm = this.checkForm.bind(this)
    this.validadeHours = this.validadeHours.bind(this)
    this.closeDialog = this.closeDialog.bind(this)
  }

  closeDialog() {
    return this.setState({
      isOpen: false,
      dialogError: '',
    })
  }

  changeStartDate(event, date) {
    this.setState({
      startDate: date,
      startError: '',
    })
  }

  changeEndDate(event, date) {
    this.setState({
      endDate: date,
      endError: '',
    })
  }

  checkForm(e) {
    e.preventDefault()
    if (!this.state.startDate || !this.state.endDate) {
      const obj = {}
      if (!this.state.startDate) {
        obj.startError = '*Selecione uma data inicial'
      }
      if (!this.state.endDate) {
        obj.endError = '*Selecione uma data final'
      }
      return this.setState({ ...obj })
    }
    this.setState({ loader: true })
    return this.validadeHours()
  }

  async validadeHours() {
    const getHours = await this.props.getVolunteerCreditHour({
      voluntarioId: this.props.voluntarioId,
      dataInicialFiltro: this.state.startDate,
      dataFinalFiltro: this.state.endDate,
    })
    if (getHours === false) {
      return this.setState({
        dialogError: 'Quantidade de horas insuficientes para gerar o seu certificado. Por favor, verifique o período em que você participou de ações.',
        isOpen: true,
        loader: false,
      })
    }
    this.setState({ loader: false })
    return this.downloadCertificate()
  }

  downloadCertificate() {
    const path = `${URL.baseDNS}/voluntario/emitir-certificado?voluntarioId=${this.props.voluntarioId}&dataInicialFiltro=${this.state.startDate.toISOString().substring(0, 10)}&dataFinalFiltro=${this.state.endDate.toISOString().substring(0, 10)}`

    window.open(path, '_blank')
  }

  render() {
    const minDate = this.state.startDate
                  ? { minDate: this.state.startDate, maxDate: new Date() }
                  : {}
    const maxDate = this.state.endDate ? { maxDate: this.state.endDate } : { maxDate: new Date() }
    return (
      <div className={styles.conteiner}>
        <Dialog
          actions={<FlatButton
            label="Ok"
            secondary
            onTouchTap={this.closeDialog}
            className={styles.button}
          />}
          open={this.state.isOpen}
          onRequestClose={this.closeDialog}
          bodyStyle={{ textAlign: 'center', color: '#484848', fontSize: '18px' }}
        >
          {this.state.dialogError}
        </Dialog>
        <div className={styles.texto}>
          <span>Você pode emitir um certificado com as horas dedicadas ao voluntariado.
            Informe o período em que as ações foram realizadas.
          </span>
        </div>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div className={`${styles.datas} Grid Grid--withGutter`}>
            <div className="Grid-cell u-size2of4 u-textLeft u-inlineBlock u-posRelative">
              <SvgIcon
                icon="icon-icon-32"
                className={styles.icon}
              />
              <DatePicker
                className={styles.dateInput}
                style={{ width: '100%', padding: '0px 10px 0 20px' }}
                textFieldStyle={{ color: '#FFF', width: '100%', zIndex: 2, cursor: 'pointer' }}
                DateTimeFormat={DateTimeFormat}
                {...maxDate}
                okLabel="OK"
                cancelLabel="Cancelar"
                locale="pt-br"
                onChange={this.changeStartDate}
                floatingLabelText="Data Inicial"
                errorText={this.state.startError}
                firstDayOfWeek={0}
              />
            </div>
            <div className="Grid-cell u-size2of4 u-textLeft u-inlineBlock u-posRelative">
              <SvgIcon
                icon="icon-icon-32"
                className={styles.icon}
              />
              <DatePicker
                className={styles.dateInput}
                style={{ width: '100%', padding: '0px 10px 0 20px' }}
                textFieldStyle={{ color: '#FFF', width: '100%', zIndex: 2, cursor: 'pointer' }}
                DateTimeFormat={DateTimeFormat}
                locale="pt-BR"
                value={this.state.endDate}
                {...minDate}
                onChange={this.changeEndDate}
                floatingLabelText="Data Final"
                errorText={this.state.endError}
                firstDayOfWeek={0}
              />
            </div>
          </div>
        </MuiThemeProvider>
        <div className={styles.buttonBaixar}>
          <PillButtonWhite
            text="Baixar certificado"
            onClick={this.checkForm}
            loader={this.state.loader}
            styleLoader={{ color: '#FFF' }}
            disabled={(!this.state.startDate || !this.state.endDate)}
          />
        </div>
      </div>
    )
  }
}

_Certificate.propTypes = {
  voluntarioId: PropTypes.string,
}

const mapStateToProps = state => ({
  voluntarioId: state.loggedUser._id,
})

const mapActionToProps = {
  getVolunteerCreditHour,
}

const Certificate = connect(mapStateToProps, mapActionToProps)(_Certificate)

export default Certificate
