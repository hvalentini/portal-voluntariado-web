import React from 'react'
import PropTypes from 'prop-types'
import PillButton from '../../components/PillButton/PillButton'

const PillButtonPelorous = props => (
  <div>
    <PillButton
      {...props}
      style={{
        backgroundColor: '#3EAFB7',
        textTransform: 'none',
        fontSize: '22px',
        lineHeight: '26px',
        ...props.style,
      }}
    />

  </div>
)

PillButtonPelorous.propTypes = {
  style: PropTypes.object,
}

export default PillButtonPelorous
