import React from 'react'
import PropTypes from 'prop-types'
import styles from './BackButton.css'

const BackButton = props => (
  <a
    className={[styles.action, props.className].join(' ')}
    style={{
      color: props.color,
    }}
    onClick={() => props.history.goBack()}
    // disabled={props.disabled}
    tabIndex="0"
    role="button"
  >
    {props.text}
  </a>
)

BackButton.propTypes = {
  color: PropTypes.string,
  text: PropTypes.string,
  className: PropTypes.string,
  history: PropTypes.object,
  // disabled: PropTypes.boll,
}

BackButton.defaultProps = {
  className: null,
  text: 'Voltar',
  color: '#00BEE7',
}

export default BackButton
