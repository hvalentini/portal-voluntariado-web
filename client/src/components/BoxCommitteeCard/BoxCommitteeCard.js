import React from 'react'
import PropTypes from 'prop-types'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './BoxCommitteeCard.css'
import Avatar from '../Avatar/Avatar'
import currencyFormatter from 'currency-formatter'

class BoxCommitteeCard extends React.Component {

  constructor(props) {
    super(props)
    this.handleFormat = this.handleFormat.bind(this)
  }

  handleFormat(value) {
    let newValue = currencyFormatter.format(value, { code: 'BRL' })
    newValue = (value < 0) ? newValue.substring(4) : newValue.substring(3)
    const symbol = (value < 0) ? '-' : ''
    const style = (value < 0) ? { worn: { color: '#FFB137' } } : {}
    return `${symbol}R$ ${newValue}`
  }

  render() {
    const comite = this.props.committee

    const saldoAtual = this.handleFormat(comite.saldoAtual)
    const verbaExtra = this.handleFormat(comite.verbaExtra)
    const verbaInicial = this.handleFormat(comite.verbaInicial)

    const url = comite.liderSocial ? comite.liderSocial.urlAvatar : ''
    const liderNome = comite.liderSocial ? comite.liderSocial.nome : ''
    const liderEmail = comite.liderSocial ? comite.liderSocial.email : ''
    const showLider = comite.liderSocial && comite.liderSocial.email ? '' : 'u-hidden'
    const showUnd = comite.unidade ? '' : 'u-hidden'
    const showVerbaExtra = comite.verbaExtra ? '' : 'u-hidden'
    const beneficiados = comite.beneficiadas === 0 ? 0 : comite.beneficiadas.sum

    return (
      <div className={`${styles.root}`}>
        <div className={`Grid ${styles.link}`}>
          <div className={styles.container}>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Empresa</p>
              <p className={styles.textBlue} title={comite.empresas.nome}>
                {comite.empresas.nome}
              </p>
            </div>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <div className={showUnd}> 
                <p className={styles.textSmall}>Unidade</p>
                <p className={styles.textBlue} title={comite.unidade}>
                  {comite.unidade}
                </p>
              </div>
            </div>
            <div className={`Grid-cell u-size2of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Cidade</p>
              <p className={styles.textBlue} title={`${comite.cidadeNome}/${comite.cidadeUF}`}>
                {comite.cidadeNome}/{comite.cidadeUF}
              </p>
            </div>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Saldo Atual</p>
              <p className={styles.textBlue}>{saldoAtual}</p>
            </div>
          </div>

          <div className={styles.container}>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Voluntários</p>
              <p className={styles.numbers}>{comite.voluntarios}</p>
            </div>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Ações</p>
              <p className={styles.numbers}>{comite.acoes}</p>
            </div>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Beneficiados</p>
              <p className={styles.numbers}>{beneficiados}</p>
            </div>
            <div className={`Grid-cell u-size1of5 ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Verba Inicial</p>
              <p className={styles.numbers}>{verbaInicial}</p>
            </div>
            <div className={`Grid-cell u-size1of5 ${showVerbaExtra} ${styles.paddingCell}`}>
              <p className={styles.textSmall}>Verba Extra</p>
              <p className={styles.numbers}>{verbaExtra}</p>
            </div>
          </div>

          <div className={`${styles.container} ${showLider}`}>
            <div className={styles.containerGridCell1}>
              <Avatar
                url={url}
                size="smaller"
              />
            </div>
            <div className={styles.containerGridCell2}>
              <p className={styles.lider}>{liderNome}</p>
              <p className={styles.lider}>{liderEmail}</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

BoxCommitteeCard.propTypes = {
  committee: PropTypes.object,
}

export default BoxCommitteeCard
