import React from 'react'
import styles from './SpinnerFail.css'

class SpinnerFail extends React.Component {
  render() {

    if (((navigator.appName === 'Netscape') ||
    (navigator.appVersion.indexOf('MSIE') !== -1) ||
    (navigator.appName === 'Microsoft Internet Explorer')) && (navigator.userAgent.indexOf('Chrome') === -1)) {
      return (
        <div className={styles.loaderIE}>
          <svg className={styles.iconIE} width="20px" height="20px" viewBox="0 0 25 25">
            <path d="M17,13 L7,13 L7,11 L17,11 L17,13.9" />
          </svg>
        </div>
      )
    }

    return (
      <div className={styles.loader}>
        <svg className={styles.circular}>
          <circle
            className={styles.path}
            cx="50"
            cy="50"
            r="20"
            fill="none"
            strokeWidth="2"
            strokeMiterlimit="10"
          />
        </svg>
        <svg className={styles.suc} width="20px" height="20px" viewBox="0 0 25 25">
          <path className={styles.checkmarkCheck} fill="none" d="M17,13 L7,13 L7,11 L17,11 L17,13.9" />
        </svg>
      </div>
    )
  }
}
export default SpinnerFail
