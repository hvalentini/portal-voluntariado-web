import React from 'react'
import PropTypes from 'prop-types'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import CircularProgress from 'material-ui/CircularProgress'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import styles from './LandingLeaderVoluntaryActions.css'
import LandingHeader from '../LandingLeaderVoluntaryActionsHeader/LandingLeaderVoluntaryActionsHeader.react'
import BoxAction from '../LeaderBoxAction/LeaderBoxAction'
import FilterActions from '../FilterActions/FilterActions'
import { getAcoesOrder } from '../../actions/acoes'
import Pagination from '../Pagination/Pagination'

class _LandingLeaderVoluntaryActions extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
          ? document.querySelector('html')
          : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  componentWillMount() {
    const filtro = `{"comite":"${this.props.comite._id}"}`

    this.props.getAcoesOrder({ filtro })
  }

  componentDidMount() {
    _LandingLeaderVoluntaryActions.scrollTo()
  }

  handleActions() {
    if (this.props.loading) {
      return (
        <div className={styles.circular}><CircularProgress size={60} thickness={4} color="#00BEE7" /></div>
      )
    }
    if (!this.props.acoes.length && !this.props.loading) {
      return (
        <div>
          <p className={styles.notFound}>Nenhuma ação encontrada</p>
        </div>
      )
    }
    return this.props.acoes.map((item, i) => {
      if (i > 5) { return null }
      return (
        <Link to={`/lider/editar-acao/${item._id}`} className={styles.link} key={i}>
          <BoxAction acao={item} />
        </Link>
      )
    })
  }

  handlePrint() {
    if (!this.props.print.length) {
      return null
    }
    return this.props.print.map((item, i) => (
      <Link to={`/lider/editar-acao/${item._id}`} className={`${styles.link}`} key={i}>
        <BoxAction acao={item} />
      </Link>
    ))
  }

  render() {
    return (
      <div className={`${styles.root} page-break`}>
        <div className={styles.filter}>
          <div className="filterLeader noPrint">
            <FilterActions />
          </div>
        </div>
        <div className="contentLeader contentMedia page-break">
          <LandingHeader />
          <div className="noPrint">
            {this.handleActions()}
            <Pagination />
          </div>
          <div className={styles.printBlock}>
            {this.handlePrint()}
          </div>
        </div>
      </div>
    )
  }
}

_LandingLeaderVoluntaryActions.propTypes = {
  acoes: PropTypes.array,
  print: PropTypes.array,
  comite: PropTypes.object,
  getAcoesOrder: PropTypes.func,
  loading: PropTypes.bool,
}

const mapStateToProps = state => ({
  acoes: state.acoes.acoesLeader,
  print: state.acoes.acoesPrint,
  comite: state.loggedUser.comite,
  loading: state.acoes.loading,
})

const mapActionsToProps = {
  getAcoesOrder,
}

const LandingLeaderVoluntaryActions = connect(mapStateToProps, mapActionsToProps)(_LandingLeaderVoluntaryActions)
export default LandingLeaderVoluntaryActions
