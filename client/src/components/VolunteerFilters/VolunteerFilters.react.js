import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import noAccents from 'remove-accents'
import { RadioButton, RadioButtonGroup, TextField } from 'material-ui'
import styles from './VolunteerFilters.css'
import { handleSearchVolunteers, handleSearchTermVolunteers, handleFilterSelected } from '../../actions/voluntario'
import PillButtonCerulean from '../PillButtonCerulean/PillButtonCerulean'

import SvgIcon from '../SvgIcon/SvgIcon.react'


const muiTheme = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: 'red',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  checkbox: {
    checkedColor: '#01BEE8',
    boxColor: '#01BEE8',
  },
  fontFamily: 'Chantilly-Serial',
})

const style = {}

class _VolunteerFilters extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      radio: '',
      lastSelected: '',
    }
    this.setSearchTerm = this.setSearchTerm.bind(this)
    this.boxSearchTerm = this.boxSearchTerm.bind(this)
    this.changeRadio = this.changeRadio.bind(this)
  }

  componentWillUnmount() {
    const filterVolunteers = this.props.voluntario.volunteers
    const searchTerm = this.props.voluntario.volunteers
    const term = ''
    this.props.handleSearchVolunteers({ filterVolunteers })
    this.props.handleSearchTermVolunteers({ searchTerm, term })
  }

  setSearchTerm(ev) {
    const value = noAccents(ev.target.value)
    const reg = new RegExp(value, 'gi')
    const volunteers = this.props.voluntario.filterVolunteers.length > 0
                     ? this.props.voluntario.filterVolunteers
                     : this.props.voluntario.volunteers
    const searchTerm = volunteers.filter(item => (
      item.nomeNormalizado.match(reg)
    ))

    const term = value.replace(/\W/g, '')

    const newValue = ev.target.value

    this.props.handleSearchTermVolunteers({ searchTerm, newValue })
  }

  boxSearchTerm(term) {
    const selectedFilter = term
    this.props.handleFilterSelected({ selectedFilter })
    if (term === 'Listar todos voluntários') {
      const filterVolunteers = this.props.voluntario.volunteers
      if (this.state.lastSelected !== term) {
        this.props.handleSearchVolunteers({ filterVolunteers })
      }
    } else if (term === 'Participou de alguma ação') {
      const filterVolunteers = this.props.voluntario.volunteers.filter(item => (
        item.jaParticipouAcao
      ))
      if (this.state.lastSelected !== term) {
        this.props.handleSearchVolunteers({ filterVolunteers })
      }
    } else if (term === 'Ja participou de ação este ano') {
      const filterVolunteers = this.props.voluntario.volunteers.filter(item => (
        item.jaParticipouAcaoEsseAno
      ))
      if (this.state.lastSelected !== term) {
        this.props.handleSearchVolunteers({ filterVolunteers })
      }
    } else if (term === 'Nunca participou de ação') {
      const filterVolunteers = this.props.voluntario.volunteers.filter(item => (
        item.quantidadeDeParticipacoes === 0
      ))
      if (this.state.lastSelected !== term) {
        this.props.handleSearchVolunteers({ filterVolunteers })
      }
    }
    return this.setState({ lastSelected: term })
  }

  changeRadio(ev) {
    this.setState({
      radio: ev.target.value,
    })
  }

  render() {
    return (
      <div className="noPrint filterLeader">
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <div className={styles.bottom}>
              <h3 className={styles.title}>Filtros</h3>
            </div>
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-posRelative">
                <SvgIcon
                  className={styles.icon}
                  icon="icon-icon-29"
                  onClick={() => this.closeModal()}
                />
                <TextField
                  hintText="Pesquisar voluntários"
                  hintStyle={style.hint}
                  inputStyle={style.input}
                  name="nome"
                  value={this.props.voluntario.term}
                  onChange={ev => this.setSearchTerm(ev)}
                  maxLength={100}
                  fullWidth
                />
              </div>
              <div className="Grid-cell u-textLeft u-inlineBlock">
                <p className="filterLeaderText u-textLeft">Participaram de ações</p>
                <RadioButtonGroup
                  name="filters"
                  defaultSelected="Listar todos voluntários"
                  onChange={ev => this.changeRadio(ev)}
                >
                  <RadioButton
                    value="Listar todos voluntários"
                    label="Listar todos voluntários"
                    style={style.radioButton}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value="Participou de alguma ação"
                    label="Participou de alguma ação"
                    style={style.radioButton}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value="Ja participou de ação este ano"
                    label="Ja participou de ação este ano"
                    style={style.radioButton}
                    className="u-inlineBlock"
                  />
                  <RadioButton
                    value="Nunca participou de ação"
                    label="Nunca participou de ação"
                    style={style.radioButton}
                    className="u-inlineBlock"
                  />
                </RadioButtonGroup>
              </div>
              <div className="Grid-cell u-textCenter">
                <PillButtonCerulean
                  text="Filtrar voluntário"
                  color="#FFFFFF"
                  opacity="1"
                  onClick={() => this.boxSearchTerm(this.state.radio)}
                  className={styles.button}
                />
              </div>
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

_VolunteerFilters.propTypes = {
  voluntario: PropTypes.object,
  handleSearchVolunteers: PropTypes.func,
  handleSearchTermVolunteers: PropTypes.func,
  handleFilterSelected: PropTypes.func,
}

style.radioButton = {
  margin: '4px 0',
}

style.hint = {
  paddingLeft: '38px',
}

style.input = {
  paddingLeft: '38px',
}

const mapStateToProps = state => ({
  voluntario: state.voluntario,
})

const mapActionsToProps = {
  handleSearchVolunteers,
  handleSearchTermVolunteers,
  handleFilterSelected,
}

const VolunteerFilters = connect(mapStateToProps, mapActionsToProps)(_VolunteerFilters)

export default VolunteerFilters
