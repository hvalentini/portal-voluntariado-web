import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  IconMenu,
  MenuItem,
  IconButton,
  Divider } from 'material-ui'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './HeaderAdminUser.css'
import { getLoggedUser } from '../../actions/loggedUser'
import { getWebStatus } from '../../actions/webTest'

const _Component = props => (
  <div>
    <div className={styles.divAvatar}>
      <img
        src={props.url}
        className={styles.Avatar}
        alt="avatar"
      />
    </div>

    <IconMenu
      className={styles.avatarMenuIcon}
      style={{ display: 'inline-flex' }}
      iconButtonElement={
        <IconButton
          className={styles.IconButton}
        >
          <p className={styles.p}>Admin</p>
          <SvgIcon
            className={styles.icon}
            icon="icon-icon-28"
          />
        </IconButton>
      }
      anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
      targetOrigin={{ horizontal: 'left', vertical: 'top' }}
      listStyle={{ padding: '10px !important' }}
    >
      <MenuItem
        primaryText="NOTIFICAÇÕES"
        style={{ color: 'black', fontSize: '14px', padding: '0' }}
        innerDivStyle={{ padding: '0 16px 0 60px' }}
        leftIcon={
          <SvgIcon
            className={styles.iconList}
            icon="icon-icon-59"
          />
        }
        onTouchTap={() => {
          props.history.push('/admin/notificacoes')
        }}
      />
      <Divider />
      <MenuItem
        primaryText="TROCAR A SENHA"
        style={{ color: 'black', fontSize: '14px', padding: '0' }}
        innerDivStyle={{ padding: '0 16px 0 60px' }}
        leftIcon={
          <SvgIcon
            className={styles.iconList}
            icon="icon-icon-60"
          />
        }
        onTouchTap={() => {
          props.history.push('/admin/trocar-senha')
        }}
      />
      <Divider />
      <MenuItem
        primaryText="SAIR"
        style={{ color: 'black', fontSize: '14px', padding: '0' }}
        innerDivStyle={{ padding: '0 16px 0 60px' }}
        leftIcon={
          <SvgIcon
            className={styles.iconList}
            icon="icon-icon-61"
          />
        }
        onTouchTap={() => {
          props.getWebStatus('logout')
        }}
      />
    </IconMenu>
  </div>
)

_Component.componentWillMount = (props) => {
  props.getLoggedUser()
}

_Component.displayName = 'HeaderAdminUser'

_Component.propTypes = {
  url: PropTypes.string,
  history: PropTypes.object.isRequired,
  getWebStatus: PropTypes.func.isRequired,
}

_Component.defaultProps = {
  url: '/images/user-avatar.svg',
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  getLoggedUser,
  getWebStatus,
}

const Component = withRouter(connect(mapStateToProps, mapActionsToProps)(_Component))

export default Component
