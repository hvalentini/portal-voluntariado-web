import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { find } from 'lodash'
import styles from './Breadcrumb.css'

const Component = (props) => {
  const pathname = window.location.pathname
  const bc = find(props.routes, (item) => {
    if (!item.breadcrumb) return false
    const path = item.path.split(':')
    if (path[1]) {
      return pathname.indexOf(item.path.split(':')[0]) >= 0
    }
    return pathname === item.path
  }) || { breadcrumb: [] }

  return (
    <ul className={styles.breadcrumb}>
      {bc.breadcrumb.map((route, i, arr) => {
        if (i === arr.length - 1) {
          return (<li key={i}>{route.name}</li>)
        }
        return (
          <li key={i}>
            <Link to={route.path}>{route.name}</Link>
            <span> {props.separetor} </span>
          </li>)
      })}
    </ul>
  )
}

Component.displayName = 'Breadcrumb'

Component.propTypes = {
  separetor: PropTypes.string,
  routes: PropTypes.array.isRequired,
}

Component.defaultProps = {
  separetor: ' > ',
  // routes: [],
}

export default Component
