import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './VolunteerDashboard.css'
import LeaderTabs from '../LeaderTabs/LeaderTabs'
import LeaderAvatar from '../LeaderAvatar/LeaderAvatar.react'
import VolunteerStatistic from '../VolunteerStatistic/VolunteerStatistic'
import Avatar from '../Avatar/Avatar'
import { readNotifications } from '../../actions/notificacao'

class _VolunteerDashboard extends React.Component {

  liderDados() {
    const lider = this.props.loggedUser.comite.liderSocial || {}
    const styleName = lider.nome ? {} : { maxWidth: '320px' }
    const styleEmail = lider.email ? {} : { display: 'none' }
    return ((<div className={`${styles.boxLeader} Grid`}>
      <div className="Grid u-textLeft">
        <Avatar
          size={'small'}
          url={lider.urlAvatar || '/images/profile_icon.svg'}
          style={lider.urlAvatar ? { boxShadow: 'none' } : { border: 'none', boxShadow: 'none', background: 'none' }}
        />
      </div>
      <div className="Grid-cell u-size4of5 u-textLeft Grid--alingMidlle">
        <div className={styles.textLeader}>
          <p className={styles.leader} style={styleName}>{lider.nome || 'Por enquanto o seu comitê ainda não possui um líder social'}</p>
          <p className={styles.leader} style={styleEmail}>{lider.email || null }</p>
        </div>
      </div>
    </div>))
  }

  render() {
    return (
      <div className="Grid">
        <div className={`Grid-cell u-size3of5 ${styles.leftCorner}`}>
          <div className={`Grid ${styles.title}`}>
            <h3>Você já participou de:</h3>
          </div>
          <div className="Grid-cell u-textCenter">
            <VolunteerStatistic
              idComite={this.props.loggedUser.comite._id}
              idVoluntario={this.props.loggedUser._id}
            />
          </div>
          <div className="Grid u-textLeft">
            <p className={styles.subtitle}>Líder Social do seu comitê</p>
          </div>
          { this.liderDados() }
        </div>
        <div className={`Grid-cell u-size2of5 ${styles.rightCorner}`}>
          <div>
            <LeaderTabs
              numberNotifications={this.props.notificacao.amountNotifications}
              loggedUser={this.props.loggedUser}
              readNotifications={this.props.notificacao.readNotifications}
              readNotificationsFunction={this.props.readNotifications}
            >
              <LeaderAvatar
                url={this.props.loggedUser.urlAvatar || '/images/user-avatar.svg'}
                name={this.props.loggedUser.nome}
                companyName={this.props.loggedUser.comite.empresa.nome}
                category={'Voluntário'}
              />
            </LeaderTabs>
          </div>
        </div>
      </div>
    )
  }
}

_VolunteerDashboard.propTypes = {
  notificacao: PropTypes.object,
  loggedUser: PropTypes.object,
  readNotifications: PropTypes.func,
}

const mapStateToProps = state => ({
  notificacao: state.notificacao,
  loggedUser: state.loggedUser,
})

const mapActionsToProps = { readNotifications }


const VolunteerDashboard = connect(mapStateToProps, mapActionsToProps)(_VolunteerDashboard)

export default VolunteerDashboard
