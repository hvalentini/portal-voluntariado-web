import React from 'react'
import PropTypes from 'prop-types'
import { TextField } from 'material-ui'
import { connect } from 'react-redux'
import PillButton from '../PillButton/PillButton'
import styles from './LoginForm.css'
import ErrorMessage from '../ErrorMessage/ErrorMessage.react'
import {
  updateLogin,
  updateLoginUser,
  loginAuthAdmin,
} from '../../actions/login'

class _Component extends React.Component {

  constructor(props) {
    super(props)
    this.clearError = this.clearError.bind(this)
    this.submitAuth = this.submitAuth.bind(this)
  }

  handleChangeUser(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateLoginUser(obj)
  }

  submitAuth(ev) {
    ev.preventDefault()
    this.props.loginAuthAdmin()
  }

  clearError() {
    this.props.updateLogin({ error: null })
  }

  renderAction(disableSubmit) {
    if (this.props.login.error) {
      return (
        <ErrorMessage
          close={this.clearError}
          style={{ marginTop: '20px', backgroundColor: '#ff8a00', width: '100%' }}
        >
          {this.props.login.error}
        </ErrorMessage>
      )
    }
    return (
      <div className={styles.buttons}>
        <PillButton
          text="Entrar"
          disabled={disableSubmit}
          onClick={this.submitAuth}
          loader={this.props.login.loader}
        />
      </div>
    )
  }

  render() {
    if (this.props.login.success) {
      window.location.href = '/admin'
    }

    const user = this.props.login.user
    const disableSubmit = !(user.username && user.password)

    return (
      <form>
        <div>
          <div className="Grid Grid--withGutter">
            <div className="Grid-cell u-posRelative u-textLeft">
              <TextField
                floatingLabelText="Usuário"
                name="username"
                defaultValue={user.username}
                onChange={e => this.handleChangeUser(e.target.name, e.target.value)}
                maxLength={100}
                fullWidth
              />
            </div>
          </div>
          <div className="Grid Grid--withGutter">
            <div className="Grid-cell u-posRelative">
              <TextField
                floatingLabelText="Senha"
                name="password"
                defaultValue={user.password}
                onChange={e => this.handleChangeUser(e.target.name, e.target.value)}
                maxLength={32}
                type="password"
                fullWidth
              />
            </div>
          </div>
        </div>
        {this.renderAction(disableSubmit)}
      </form>
    )
  }
}

_Component.displayName = 'LoginForm'

_Component.propTypes = {
  login: PropTypes.object.isRequired,
  loginAuthAdmin: PropTypes.func.isRequired,
  updateLogin: PropTypes.func.isRequired,
  updateLoginUser: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  login: state.login,
})

const mapActionsToProps = {
  updateLogin,
  updateLoginUser,
  loginAuthAdmin,
}

const Componet = connect(mapStateToProps, mapActionsToProps)(_Component)

export default Componet
