import React from 'react'
import PropTypes from 'prop-types'
import styles from './ModalListParticipants.css'
import Avatar from '../Avatar/Avatar'
import { orderBy } from 'lodash'

class ModalListParticipants extends React.Component {

  handleUsers() {
    const newArray =  orderBy(this.props.users, 'nome', 'asc')
    return newArray.map((item, i) => {
      return(
        <div className={styles.box} key={i}>
          <div className={styles.avatar}>
            <Avatar
              url={item.urlAvatar}
              size='smaller' />
          </div>
          <div className={styles.dados}>
            <p className={styles.nome}>{item.nome}</p>
            <p className={styles.cargo}>{item.cargo}</p>
          </div>
        </div>
      )
    })
  }

  render() {
    return (
      <div className={styles.boxInfo}>
        <p className={styles.title}>{this.props.title}</p>
        <hr className={styles.separator} />
        <div className={styles.usersRoot}>
          {this.handleUsers()}
        </div>
      </div>
    )
  }
}

ModalListParticipants.propTypes = {
  title: PropTypes.string,
  users: PropTypes.array,
}


export default ModalListParticipants
