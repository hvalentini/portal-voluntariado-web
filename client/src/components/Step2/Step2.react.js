import React from 'react'
import PropTypes from 'prop-types'
import {
  TextField,
  SelectField,
  MenuItem,
  CircularProgress,
} from 'material-ui'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import PillButtonWhite from '../PillButtonWhite/PillButtonWhite'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import styles from './Step2.css'
import UserAvatar from '../UserAvatar/UserAvatar'
import CitySelect from '../CitySelect/CitySelect.react'
import dddList from '../../utils/ddd.json'
import {
  loadCommittee,
  selectCompany,
  selectCity,
  selectUnit,
 } from '../../actions/committeeSelects'
import {
   updateStep,
   updateStep2,
   updateStep2Phone,
 } from '../../actions/registerForm'

const errorStyle = {
  color: '#FF0',
  fontWeight: 'bold',
}
class _SubscribeStep2 extends React.Component {

  static checkDDD(tel) {
    const ddd = tel.substring(0, 2)
    let found = true
    for (let i = 0, j = dddList.length - 1; i !== j; i += 1, j -= 1) {
      if (ddd === dddList[i] || ddd === dddList[j]) {
        found = false
      }
    }

    return found
  }

  static checkEmail(email) {
    if (email.indexOf('@') === -1) {
      return true
    }
    if (!email.split('@')[1].match(/((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      return true
    }

    return false
  }

  constructor(props) {
    super(props)

    this.state = {
      cssAnimation: styles.leftAnimation,
      msgErro: {
        telefonePessoal: '',
        telefoneProfissional: '',
        email: '',
        emailSuperior: '',
      },
    }
    this.handleChangeCity = this.handleChangeCity.bind(this)
    this.handleChangeUnit = this.handleChangeUnit.bind(this)
    this.handleChangeCompany = this.handleChangeCompany.bind(this)
    this.handleChangeUserCity = this.handleChangeUserCity.bind(this)
    this.validateForm = this.validateForm.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
    this.handleChangeMail = this.handleChangeMail.bind(this)
  }

  componentWillMount() {
    this.props.loadCommittee()

    this.setState({
      cssAnimation: this.props.step > 2 ? styles.rightAnimation : styles.leftAnimation,
      loaderComite: this.props.comites.list.length === 0,
    })

    this.props.updateStep(2)
  }

  componentWillReceiveProps(newProps) {
    if (this.state.loaderComite) {
      if (this.props.comites.list !== newProps.comites.list
        && this.props.registerForm.step2.comite && !this.props.registerForm.step2.comite._id) {
        this.setState({ loaderComite: false })
      }

      if (this.props.comites.list !== newProps.comites.list
          && this.props.registerForm.step2.comite && this.props.registerForm.step2.comite._id) {
        this.props.selectCompany(this.props.registerForm.step2.comite.empresa)
      }

      if (this.props.comites.byCity !== newProps.comites.byCity) {
        this.props.selectCity(this.props.registerForm.step2.comite)
      }

      if (this.props.comites.byUnit !== newProps.comites.byUnit) {
        this.props.selectUnit(this.props.registerForm.step2.comite)
      }

      if (this.props.comites.selectedUnit !== newProps.comites.selectedUnit) {
        this.setState({ loaderComite: false })
      }
    }
  }

  handleChange(key, value) {
    const obj = {}
    obj[key] = value.replace(/^\s+/gm, '')
    this.props.updateStep2(obj)
  }

  handleChangeMail(key, value) {
    const obj = {}
    obj[key] = value.trim()
    this.props.updateStep2(obj)
  }

  handleChangePhone(prop, value) {
    this.props.updateStep2Phone(prop, value)
  }

  handleChangeCompany(x, i) {
    this.props.selectCompany(this.props.comites.byCompany[i])
  }

  handleChangeCity(x, i) {
    this.props.selectCity(this.props.comites.byCity[i])
  }

  handleChangeUnit(x, i) {
    this.props.selectUnit(this.props.comites.byUnit[i])
  }

  handleChangeUserCity(value) {
    this.props.updateStep2({ cidade: value })
  }

  handleBlur(e) {
    const newStateMsgErro = this.state.msgErro
    newStateMsgErro[e.target.name] = this.state.msgErro[e.target.name] && e.target.value ? '' : this.state.msgErro[e.target.name]
    this.setState({ ...this.state.msgErro, ...newStateMsgErro })
  }

  validateForm(e) {
    e.preventDefault()
    const step2 = this.props.registerForm.step2
    const msgErro = {}

    if (_SubscribeStep2.checkEmail(step2.email)) {
      msgErro.email = 'Por favor, preencha um email válido.'
    }
    if (_SubscribeStep2.checkEmail(step2.emailSuperior)) {
      msgErro.emailSuperior = 'Por favor, preencha um email válido.'
    }
    if ((step2.telefonePessoal.match(/\d/g) || []).length < 10) {
      msgErro.telefonePessoal = 'Por favor, preencha um telefone com ddd e no minimo 8 números.'
    } else if (_SubscribeStep2.checkDDD(step2.telefonePessoal.match(/\d/g).join(''))) {
      msgErro.telefonePessoal = 'Por favor, digite um ddd válido.'
    }
    if ((step2.telefoneProfissional.match(/\d/g) || []).length < 10) {
      msgErro.telefoneProfissional = 'Por favor, preencha um telefone com ddd e no minimo 8 números.'
    } else if (_SubscribeStep2.checkDDD(step2.telefoneProfissional.match(/\d/g).join(''))) {
      msgErro.telefoneProfissional = 'Por favor, digite um ddd válido.'
    }

    if (step2.bairro && step2.bairro.trim().length < 2) {
      msgErro.bairro = 'Por favor, preencha um bairro válido.'
    }

    if (step2.endereco && step2.endereco.trim().length < 2) {
      msgErro.endereco = 'Por favor, preencha um endereço válido.'
    }

    if (Object.keys(msgErro).length > 0) {
      return this.setState({ msgErro })
    }

    return this.props.history.push('passo-3')
  }

  renderUnits() {
    if (this.props.comites.byUnit.length >= 1
      && !(this.props.comites.byUnit.length === 1 && !this.props.comites.byUnit[0].unidade)) {
      return (
        <SelectField
          floatingLabelText="Unidade"
          value={this.props.comites.selectedUnit._id || ''}
          onChange={this.handleChangeUnit}
          fullWidth
        >
          {
            this.props.comites.byUnit.map((item, i) => (
              <MenuItem
                className="SelectBkg"
                value={item._id}
                primaryText={item.unidade}
                key={i}
              />
            ))
          }
        </SelectField>
      )
    }
    return null
  }

  renderComite() {
    if (this.state.loaderComite) {
      return (
        <div className={styles.loader}>
          <CircularProgress size={35} />
        </div>
      )
    }
    const city = this.props.comites.byCity || []
    const company = this.props.comites.byCompany || []

    return (
      <div className="Grid Grid--withGutter">
        <div className="Grid-cell u-size4of12 u-posRelative u-textLeft">
          <SelectField
            name="empresas"
            floatingLabelText="Empresas"
            floatingLabelStyle={{ color: '#fff' }}
            value={this.props.comites.selectedCompany._id}
            onChange={this.handleChangeCompany}
            fullWidth
          >
            {
              company.map((item, i) => {
                if (!item) return null
                return (
                  <MenuItem
                    className="SelectBkg"
                    value={item._id}
                    primaryText={item.nome}
                    key={i}
                  />
                )
              })
            }
          </SelectField>
        </div>
        <div className="Grid-cell u-size4of12 u-posRelative u-textLeft">
          <SelectField
            name="cidade"
            floatingLabelText="Cidade"
            floatingLabelStyle={{ color: '#fff' }}
            value={this.props.comites.selectedCity.cidade}
            onChange={this.handleChangeCity}
            fullWidth
            disabled={(this.props.comites.byCity.length === 0)}
          >
            {
              city.map((item, i) => (
                <MenuItem
                  className="SelectBkg"
                  value={item.cidade}
                  primaryText={`${item.cidadeNome} / ${item.cidadeUF}`}
                  key={i}
                />
              ))
            }
          </SelectField>
        </div>
        <div className="Grid-cell u-size4of12 u-posRelative u-textLeft">
          { this.renderUnits() }
        </div>
      </div>
    )
  }

  render() {
    const step2 = this.props.registerForm.step2
    const disableNextBtn = !((step2.comite && step2.comite._id) && step2.email
      && step2.emailSuperior && step2.telefonePessoal && step2.telefoneProfissional
      && step2.endereco && step2.bairro && (step2.cidade && step2.cidade._id))

    const size = window.innerHeight > 800
                ? 'large'
                : 'medium'

    return (
      <div className={styles.formBg}>
        <div className={styles.center}>
          <UserAvatar
            url={this.props.registerForm.step1.urlAvatar}
            size={size}
          />
        </div>
        <h1>{this.props.registerForm.step1.nome}</h1>
        <form className={styles.form}>
          <div className={this.state.cssAnimation}>
            {this.renderComite()}
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-size8of12 u-posRelative">
                <TextField
                  floatingLabelText="Seu E-mail"
                  floatingLabelStyle={{ color: '#fff' }}
                  name="email"
                  value={this.props.registerForm.step2.email}
                  onChange={e => this.handleChangeMail(e.target.name, e.target.value)}
                  onBlur={this.handleBlur}
                  maxLength={100}
                  errorText={this.state.msgErro.email}
                  errorStyle={errorStyle}
                  fullWidth
                  autoComplete="off"
                />
              </div>
              <div className="Grid-cell u-size4of12 u-posRelative">
                <TextField
                  floatingLabelText="Telefone profissional com DDD"
                  floatingLabelStyle={{ color: '#fff' }}
                  name="telefoneProfissional"
                  value={this.props.registerForm.step2.telefoneProfissional}
                  onChange={e => this.handleChangePhone(e.target.name, e.target.value)}
                  onBlur={this.handleBlur}
                  errorText={this.state.msgErro.telefoneProfissional}
                  errorStyle={errorStyle}
                  maxLength={15}
                  fullWidth
                />
              </div>
            </div>
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-lg-size8of12">
                <TextField
                  name="emailSuperior"
                  floatingLabelStyle={{ color: '#fff' }}
                  floatingLabelText="E-mail do superior / líder imediato"
                  value={this.props.registerForm.step2.emailSuperior}
                  onChange={e => this.handleChangeMail(e.target.name, e.target.value)}
                  onBlur={this.handleBlur}
                  autoComplete="off"
                  errorText={this.state.msgErro.emailSuperior}
                  errorStyle={errorStyle}
                  maxLength={100}
                  fullWidth
                />
              </div>
              <div className="Grid-cell u-lg-size4of12">
                <TextField
                  name="telefonePessoal"
                  floatingLabelText="Seu celular com DDD"
                  floatingLabelStyle={{ color: '#fff' }}
                  value={this.props.registerForm.step2.telefonePessoal}
                  onChange={e => this.handleChangePhone(e.target.name, e.target.value)}
                  onBlur={this.handleBlur}
                  errorText={this.state.msgErro.telefonePessoal}
                  errorStyle={errorStyle}
                  maxLength={15}
                  fullWidth
                />
              </div>
            </div>
            <div className="Grid Grid--withGutter">
              <div className="Grid-cell u-lg-size8of12">
                <CitySelect
                  label="Sua Cidade"
                  value={step2.cidade}
                  handleChange={this.handleChangeUserCity}
                  floatingLabelStyle={{ color: '#fff' }}
                />
              </div>
              <div className="Grid-cell u-lg-size4of12">
                <TextField
                  name="bairro"
                  floatingLabelText="Seu Bairro"
                  floatingLabelStyle={{ color: '#fff' }}
                  value={this.props.registerForm.step2.bairro}
                  onChange={e => this.handleChange(e.target.name, e.target.value)}
                  onBlur={this.handleBlur}
                  errorText={this.state.msgErro.bairro}
                  errorStyle={errorStyle}
                  maxLength={15}
                  fullWidth
                  autoComplete="off"
                />
              </div>
            </div>
            <div className="Grid-cell">
              <TextField
                name="endereco"
                floatingLabelStyle={{ color: '#fff' }}
                floatingLabelText="Seu Endereço (av., rua, praça, …,  número e complemento)"
                value={this.props.registerForm.step2.endereco}
                onChange={e => this.handleChange(e.target.name, e.target.value)}
                onBlur={this.handleBlur}
                autoComplete="off"
                errorText={this.state.msgErro.endereco}
                errorStyle={errorStyle}
                maxLength={100}
                fullWidth
              />
            </div>
          </div>
          <div className={styles.boxBtns}>
            <Link
              to="passo-1"
              className={styles.btnBack}
            >
              <SvgIcon
                icon="icon-icon-10"
                className={styles.icon}
              />
              Voltar
            </Link>
            <PillButtonWhite
              onClick={this.validateForm}
              text="Próximo"
              color="#01BEE8"
              disabled={disableNextBtn}
              style={{ margin: 0 }}
              loader={this.state.loaderComite}
            />
          </div>
        </form>
      </div>
    )
  }
}

_SubscribeStep2.displayName = 'Step2'

_SubscribeStep2.propTypes = {
  registerForm: PropTypes.object.isRequired,
  comites: PropTypes.object.isRequired,
  history: PropTypes.object,
  loadCommittee: PropTypes.func.isRequired,
  selectCompany: PropTypes.func.isRequired,
  selectCity: PropTypes.func.isRequired,
  selectUnit: PropTypes.func.isRequired,
  step: PropTypes.number.isRequired,
  updateStep: PropTypes.func.isRequired,
  updateStep2: PropTypes.func.isRequired,
  updateStep2Phone: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  comites: state.committeeSelects,
  registerForm: state.registerForm,
  step: state.registerForm.step,
})

const mapActionsToProps = {
  loadCommittee,
  selectCompany,
  selectCity,
  selectUnit,
  updateStep,
  updateStep2,
  updateStep2Phone,
}

const Step2 = connect(mapStateToProps, mapActionsToProps)(_SubscribeStep2)

export default withRouter(Step2)
