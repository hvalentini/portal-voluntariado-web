import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import VolunteerDashboard from '../../components/VolunteerDashboard/VolunteerDashboard'
import ActionSuggestion from '../../components/ActionSuggestion/ActionSuggestion'
import ActionsMade from '../../components/ActionsMade/ActionsMade.react'
import CarouselCommiteeActions from '../../components/CarouselCommiteeActions/CarouselCommiteeActions.react'
import CommiteeCarouselFilter from '../../components/CommiteeCarouselFilter/CommiteeCarouselFilter.react'
import GalleryModal from '../../components/ActionsGallery/GalleryModal.react'
import Footer from '../../components/Footer/Footer'
import styles from './LandingVolunteer.css'

import { getLoggedUser } from '../../actions/loggedUser'

class _LandingVolunteer extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      filterCommiteeCarousel: null,
    }

    this.selectTab = this.selectTab.bind(this)
  }

  componentWillMount() {
    this.props.getLoggedUser()
  }

  selectTab(filter) {
    this.setState({
      filterCommiteeCarousel: filter,
    })
  }

  render() {
    if (!this.props.loggedUser._id) {
      return null
    }
    const email = this.props.loggedUser.comite.liderSocial
      ? this.props.loggedUser.comite.liderSocial.email : ''
    return (
      <div className="Grid">
        <div className={styles.blocks}>
          <VolunteerDashboard />
        </div>
        <div className={styles.blocks}>
          <CommiteeCarouselFilter
            selectTab={this.selectTab}
          />
          <CarouselCommiteeActions
            filter={this.state.filterCommiteeCarousel}
          />
        </div>
        <div className={styles.blocks}>
          <ActionSuggestion email={email} />
        </div>
        <div className={styles.blocks}>
          <GalleryModal />
          <ActionsMade />
        </div>
        <div style={{ paddingTop: '160px' }} className={styles.blocks}>
          <Footer />
        </div>
      </div>
    )
  }
}

_LandingVolunteer.propTypes = {
  loggedUser: PropTypes.object,
  getLoggedUser: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  getLoggedUser,
}

const LandingVolunteer = connect(mapStateToProps, mapActionsToProps)(_LandingVolunteer)

export default LandingVolunteer
