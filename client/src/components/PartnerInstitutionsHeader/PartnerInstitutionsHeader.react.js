import React from 'react'
import PropTypes from 'prop-types'
import SelectField from 'material-ui/SelectField'
import { Link } from 'react-router-dom'
import MenuItem from 'material-ui/MenuItem'
import { connect } from 'react-redux'
import styles from './PartnerInstitutionsHeader.css'
import { updatePartnersFilterOrderBy, filterPartners } from '../../actions/filters'
import SvgIcon from '../SvgIcon/SvgIcon.react'

const style = {}

class _PartnerInstitutionsHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 0,
    }
    this.onChangeOrderBy = this.onChangeOrderBy.bind(this)
  }

  onChangeOrderBy(event, index, value) {
    this.setState({ value }) // State para alternar lista do Material UI
    const obj = {}
    const orderBy = (value === 1) ? { nomeNormalizado: -1 } : { nomeNormalizado: 1 }
    //  1 Ascendente  (A-Z)
    // -1 Descendente (Z-A)
    // Altera de 1 para -1 para compatibilizar com serviço de ordenar
    obj.orderBy = orderBy
    this.props.updatePartnersFilterOrderBy(obj)
    this.props.filterPartners()
  }

  render() {
    return (
      <div className={styles.container}>
        <div className="Grid Grid-withGutter contentLeaderBottom">
          <div className="Grid-cell u-size6of12">
            <div>
              <h3 className="contentLeaderTitle">Instituições Parceiras</h3>
            </div>
          </div>
          <div className="Grid-cell u-size4of12">
            <p className={styles.order}> Ordenar por: </p>
            <SelectField
              hintText="Selecione uma ordem"
              value={this.state.value}
              onChange={this.onChangeOrderBy}
              labelStyle={style.menuStyle}
              fullWidth
            >
              {
                this.props.partners.institution.selectActions.map((item, i) => (
                  <MenuItem
                    className="SelectBkg"
                    value={i}
                    primaryText={item}
                    key={i}
                  />
                ))
              }
            </SelectField>
          </div>
          <div className="Grid-cell u-size1of12 u-textCenter">
            <div className={styles.buttonAdd}>
              <Link to="/lider/nova-instituicao" className={styles.linkAdd}>
                <SvgIcon
                  className={styles.iconAdd}
                  icon="icon-icon-7"
                />
              </Link>
            </div>
          </div>
          <div className="Grid-cell u-size1of12 u-textCenter noPrint">
            <button
              onClick={() => window.print()}
              className={styles.printButton}
            >
              <SvgIcon
                className={styles.printIcon}
                icon="icon-icon-45"
              />
            </button>
          </div>
        </div>
      </div>
    )
  }
}

style.menuStyle = {
  color: '#484848',
  fontSize: '18px',
}

_PartnerInstitutionsHeader.propTypes = {
  partners: PropTypes.object,
  updatePartnersFilterOrderBy: PropTypes.func,
  filterPartners: PropTypes.func,
}

const mapStateToProps = state => ({
  partners: state.partners,
})

const mapActionsToProps = {
  updatePartnersFilterOrderBy,
  filterPartners,
}

const PartnerInstitutionsHeader =
connect(mapStateToProps, mapActionsToProps)(_PartnerInstitutionsHeader)

export default PartnerInstitutionsHeader
