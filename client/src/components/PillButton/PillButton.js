import React from 'react'
import PropTypes from 'prop-types'
import CircularProgress from 'material-ui/CircularProgress'
import styles from './PillButton.css'


const PillButton = (props) => {
  const className = props.negativeColors ? styles.buttonStroke : styles.buttonFill

  if (props.loader && props.hasLoader) {
    return (
      <CircularProgress
        className={styles.circular}
        style={{ ...props.styleLoader }}
      />
    )
  }
  return (
    <button
      type={props.type}
      className={[props.className || className, props.class].join(' ')}
      style={props.style}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.text}
    </button>
  )
}

PillButton.propTypes = {
  hasLoader: PropTypes.bool,
  loader: PropTypes.bool,
  negativeColors: PropTypes.bool,
  text: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
  type: PropTypes.string,
  style: PropTypes.object,
  styleLoader: PropTypes.object,
  class: PropTypes.string,
}

PillButton.defaultProps = {
  hasLoader: true,
  disabled: false,
  loader: false,
  text: '',
  className: null,
  size: null,
  style: {},
}

export default PillButton
