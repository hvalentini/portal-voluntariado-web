import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { AutoComplete, MenuItem, ListItem, Avatar, Chip } from 'material-ui'
import noAccents from 'remove-accents'
import styles from './UsersAutocomplete.css'
import SvgIcon from '../SvgIcon/SvgIcon.react'
import { getVoluntariosAutoComplete } from '../../actions/voluntario'
import {
  addSelectedUsers,
  removeSelectedUsers,
  editSelectedUsers,
  changeUsers,
  filterDataSource,
} from '../../actions/usersAutocomplete'

const style = {}
const chipStyle = {
  display: 'inline-flex',
  borderRadius: '20px',
  marginRight: '10px',
}
const menuItemStyle = { borderBottom: '1px solid #b1b1b1' }
class _UsersAutocomplete extends React.Component {

 

  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
    }
    this.handleDataSource = this.handleDataSource.bind(this)
    this.updateSearchText = this.updateSearchText.bind(this)
    this.chosenVolunteer = this.chosenVolunteer.bind(this)
    this.removeVolunteerFromList = this.removeVolunteerFromList.bind(this)
    this.renderChosenVolunteers = this.renderChosenVolunteers.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleDataSource() {
    const data = this.props.dataSource
    if (data && data.length) {
      const autocompleteList = data.map((item, i) => {
        const obj = item
        const urlAvatar = item.urlAvatar || '/images/user-avatar.svg'
        obj.text = `${item.nome}`
        obj.value = (
          <MenuItem
            style={i !== (data.length - 1) ? menuItemStyle : {}}
            key={item._id}
          >
            <ListItem
              primaryText={(
                <span
                  style={{
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    display: 'block',
                  }}
                >
                  {item.nome}
                </span>
              )}
              secondaryText={item.cargoNome}
              leftAvatar={<Avatar src={urlAvatar} />}
              rightIcon={<SvgIcon
                icon="icon-icon-30"
                style={{ color: '#04B8E5', fontSize: '5px' }}
              />}
            />
          </MenuItem>
        )
        return obj
      })
      return autocompleteList
    }
    return []
  }

  updateSearchText(text) {
    let sText = text
    if (typeof text === 'object') {
      sText = text.target.value
    }
    const textNormalized = noAccents(sText).toLowerCase().trim()
    if (this.props.dataSource.length > 0) {
      this.props.filterDataSource({
        texto: textNormalized,
      })
    } else {
      this.props.getVoluntariosAutoComplete({
        texto: textNormalized,
        comite: this.props.comiteEdit ? this.props.comiteEdit._id : this.props.comite._id,
        permitirBloqueado: false,
      })
    }

    this.setState({
      searchText: sText,
    })

    if (sText && this.props.errorTextUser) {
      this.props.handleErrorTextUser('')
    }
  }

  chosenVolunteer(chosenRequest) {
    if (typeof chosenRequest !== 'object') { return false }
    this.props.addSelectedUsers(chosenRequest)
    return this.setState({
      searchText: '',
    })
  }

  removeVolunteerFromList(element) {
    this.props.removeSelectedUsers(element.currentTarget.dataset.index)
  }

  handleClose() {
    const data = this.props.selected
    if (data && data.length) {
      document.getElementById('participantes').children[1].focus()
    } else {
      document.getElementsByClassName('pv-NewAction-btnSave')[0].focus()
    }
  }

  renderChosenVolunteers() {
    const data = this.props.selected
    if (data && data.length) {
      const chosenList = data.map((item, i) => (
        <Chip
          key={`volunteerChip-${i}`}
          onTouchTap={this.removeVolunteerFromList}
          backgroundColor="#00bee7"
          labelColor="#FFF"
          data-index={i}
          style={chipStyle}
          icon="icon-icon-30"
          className={styles.chipStyle}
        >
          <Avatar
            src={item.urlAvatar || '/images/user-avatar.svg'}
            style={{ width: '40px', height: '40px' }}
          />
          <div className={styles.innerChipHolder}>
            <div
              style={{ display: 'inline-block', margin: '4px 0', paddingRight: '5px' }}
            >
              <p
                style={{ fontSize: '14px', margin: 0, lineHeight: '15px' }}
              >
                {item.nome}
              </p>
              <p
                style={{ fontSize: '12px', margin: 0, lineHeight: '14px' }}
              >
                {item.cargoNome}
              </p>
            </div>
            <div
              style={{ display: 'inline-block', padding: '0 5px', verticalAlign: 'top', margin: '4px 0' }}
            >
              <SvgIcon
                icon="icon-icon-31"
                style={{ verticalAlign: 'middle' }}
              />
            </div>
          </div>
        </Chip>
        ))
      return chosenList
    }
    return []
  }


  render() {
    return (
      <div className={styles.usersAutocomplete}>
        <div className={styles.searchIcon}>
          <SvgIcon icon="icon-icon-29" />
        </div>
        <AutoComplete
          dataSource={this.handleDataSource()}
          onUpdateInput={text => this.updateSearchText(text)}
          searchText={this.state.searchText}
          onNewRequest={this.chosenVolunteer}
          floatingLabelText="Selecione os participantes"
          filter={AutoComplete.noFilter}
          fullWidth
          openOnFocus
          onFocus={this.updateSearchText}
          floatingLabelStyle={style.flotLabel}
          errorText={this.props.errorTextUser}
          listStyle={{ maxHeight: '240px', overflowY: 'auto' }}
          onClose={this.handleClose}
        />
        <div id="participantes">
          <h2
            style={{ fontSize: '16px', lineHeight: '24px', color: '#707070', padding: '10px 0', margin: 0, fontWeight: '300' }}
          >
            Listagem de participantes
          </h2>
          {this.renderChosenVolunteers()}
        </div>
      </div>
    )
  }
}

style.flotLabel = {
  color: '#707070',
  fontSize: '18px',
}

_UsersAutocomplete.propTypes = {
  dataSource: PropTypes.array.isRequired,
  selected: PropTypes.array.isRequired,
  comite: PropTypes.object,
  comiteEdit: PropTypes.object,
  getVoluntariosAutoComplete: PropTypes.func,
  addSelectedUsers: PropTypes.func,
  removeSelectedUsers: PropTypes.func,
  errorTextUser: PropTypes.string,
  filterDataSource: PropTypes.func,
  handleErrorTextUser: PropTypes.func,
}

const mapStateToProps = state => ({
  ...state.usersAutocomplete,
  comite: state.loggedUser.comite,
  loggedUser: state.loggedUser,
  acaoEdit: state.acoes.acaoEdit,
})

const mapActionsToProps = {
  getVoluntariosAutoComplete,
  addSelectedUsers,
  removeSelectedUsers,
  editSelectedUsers,
  changeUsers,
  filterDataSource,
}

const UsersAutocomplete = connect(mapStateToProps, mapActionsToProps)(_UsersAutocomplete)
export default UsersAutocomplete
