import React from 'react'
import PropTypes from 'prop-types'
import styles from './ErrorMessage.css'
import SvgIcon from '../../components/SvgIcon/SvgIcon.react'

class ErrorMessage extends React.Component {
  close() {
    if (this.props.close) {
      return (
        <SvgIcon
          className={styles.close}
          onClick={() => { this.props.close() }}
          icon="icon-icon-11"
        />
      )
    }
    return null
  }

  footer() {
    if (this.props.footer) {
      return (
        <div className={styles.footer}>
          <span>{this.props.footer}</span>
        </div>
      )
    }
    return null
  }

  render() {
    return (
      <div className={styles.card} style={this.props.style}>
        {this.close()}
        <div className={styles.header}>
          <span>
            Ooops...&nbsp;&nbsp;
            <span style={{ fontSize: '36px' }}>:(</span>
          </span>
        </div>
        <div className={styles.body}>
          <span>{this.props.children}</span>
        </div>
        {this.footer()}
      </div>
    )
  }
}

ErrorMessage.displayName = 'ErrorMessage'

ErrorMessage.propTypes = {
  children: PropTypes.string.isRequired,
  footer: PropTypes.string,
  style: PropTypes.object,
  close: PropTypes.func,
}

export default ErrorMessage
