import getMuiTheme from 'material-ui/styles/getMuiTheme'

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#FFF',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#FFF',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#00BEE7',
    accent3Color: '#00BEE7',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#7fdef2',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  fontFamily: 'Chantilly-Serial',
})

const formFild = {
  disabledTextColor: '#c3c3c3',
  floatingLabelColor: '#9b9b9b',
  textColor: '#484848',
  errorColor: '#FF8A00',
}
const muiThemeForm = getMuiTheme({
  palette: {
    textColor: '#484848',
    primary1Color: '#01BEE8',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent2Color: '#484848',
    accent3Color: '#484848',
    disabledColor: '#484848',
  },
  textField: formFild,
  selectField: formFild,
  fontFamily: 'Chantilly-Serial',
})

const muiThemeFormAction = getMuiTheme({
  palette: {
    textColor: '#484848',
    alternateTextColor: '#EFEFFF',
    primary1Color: '#3f93b7',
    primary2Color: '#00BEE7',
    primary3Color: '#00BEE7',
    accent1Color: '#FFF',
    accent2Color: '#636363',
    accent3Color: '#636363',
    pickerHeaderColor: '#FFF',
    canvasColor: '#fff',
    disabledColor: '#636363',
  },
  menuItem: {
    selectedTextColor: '#00BEE7',
    hoverColor: '#E8E8E8',
    alternateTextColor: '#171515',
  },
  datePicker: {
    color: '#000',
    textColor: '#FFF',
    calendarTextColor: '#000',
    selectColor: '#01BEE8',
    selectTextColor: '#FFF',
    calendarYearBackgroundColor: '#fff',
  },
  timePicker: {
    color: '#000',
    textColor: '#000',
    clockColor: '#FFF',
    clockCircleColor: '#FFF',
    selectColor: '#01BEE8',
    selectTextColor: '#01BEE8',
  },
  textField: {
    errorColor: '#FF8A00',
  },
  fontFamily: 'Chantilly-Serial',
  fontSize: '18px',
})

export default {
  muiTheme,
  muiThemeForm,
  muiThemeFormAction,
}
