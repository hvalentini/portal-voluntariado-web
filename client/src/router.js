import React from 'react'
import LandingPage from './pages/LandingPage/LandingPage'
import GuidelineAdmin from './pages/GuidelineAdmin/GuidelineAdmin'
import Welcome from './pages/Welcome/Welcome'
import AccessDeniedPage from './pages/AccessDeniedPage/AccessDeniedPage'
import Step1 from './components/Step1/Step1.react'
import Step2 from './components/Step2/Step2.react'
import Step3 from './components/Step3/Step3.react'
import Step4 from './components/Step4/Step4'
import Login from './components/Login/Login'
import WrapperLoginPass from './components/WrapperLoginPass/WrapperLoginPass'
import LoginPassRecovery from './components/LoginPassRecovery/LoginPassRecovery'
import LoginAdminPage from './pages/LoginAdminPage/LoginAdminPage'

const routes = [{
  path: '/',
  exact: true,
  main: () => <LandingPage />,
}, {
  path: '/cadastro/passo-1',
  main: () => <LandingPage><Step1 /></LandingPage>,
  exact: true,
}, {
  path: '/cadastro/passo-2',
  main: () => <LandingPage><Step2 /></LandingPage>,
  state: { name: 'Passo 2' },
  exact: true,
}, {
  path: '/cadastro/passo-3',
  main: () => <LandingPage><Step3 /></LandingPage>,
  exact: true,
}, {
  path: '/cadastro/passo-4',
  main: () => <LandingPage><Step4 /></LandingPage>,
  exact: true,
}, {
  path: '/entrar',
  main: () => <LandingPage><WrapperLoginPass><Login /></WrapperLoginPass></LandingPage>,
  exact: true,
}, {
  path: '/recuperar-senha',
  main: () => <LandingPage><WrapperLoginPass><LoginPassRecovery /></WrapperLoginPass></LandingPage>,
  exact: true,
}, {
  path: '/welcome',
  main: () => <Welcome />,
}, {
  path: '/guidelineAdmin',
  main: () => <GuidelineAdmin />,
}, {
  path: '/acesso-negado',
  main: () => <AccessDeniedPage />,
}, {
  path: '/admin/login',
  main: () => <LoginAdminPage />,
}]

export default routes
