import React from 'react'
import LandingLeaderVolunteerManagement from './components/LandingLeaderVolunteerManagement/LandingLeaderVolunteerManagement'
import InstitutionsPage from './pages/InstitutionsPage/InstitutionsPage'
import EditInstitutionsPage from './pages/EditInstitutionsPage/EditInstitutionsPage'
import ActionsPage from './pages/ActionsPage/ActionsPage'
import CitiesPage from './pages/CitiesPage/CitiesPage'
import EditCityPage from './pages/EditCityPage/EditCityPage'
import CompaniesPage from './pages/CompaniesPage/CompaniesPage'
import NewActionTypePage from './pages/NewActionTypePage/NewActionTypePage'
import ActionTypesPage from './pages/ActionTypesPage/ActionTypesPage'
import EditCompaniesPage from './pages/EditCompanyPage/EditCompanyPage'
import LoginAdminPage from './pages/LoginAdminPage/LoginAdminPage'
import CommitteePage from './pages/CommitteesPage/CommitteesPage'
import EditCommitteePage from './pages/EditCommitteePage/EditCommitteePage'
import LandingAdminInfo from './components/LandingAdminInfo/LandingAdminInfo.react'
import ChangePassAdminPage from './pages/ChangePassAdminPage/ChangePassAdminPage'
import NewAction from './components/NewAction/NewAction.react'
import NotificationsAdminPage from './pages/NotificationsAdminPage/NotificationsAdminPage'

// breadcrumb config paths and names
const bc = {
  inicio: {
    path: '/admin',
    name: 'Inicio',
  },
  cidades: {
    path: '/admin/cidades',
    name: 'Cidades',
  },
  novaCidade: {
    path: '/admin/cidades/adicionar',
    name: 'Cadastro de Cidades',
  },
  editarCidade: {
    path: '/admin/cidades/editar/',
    name: 'Editar Cidade',
  },
  empresas: {
    path: '/admin/empresas',
    name: 'Empresas',
  },
  novaEmpresa: {
    path: '/admin/empresas/adicionar',
    name: 'Cadastro de Empresas',
  },
  editarEmpresa: {
    path: '/admin/empresas/editar/',
    name: 'Editar Empresa',
  },
  tiposDeAcoes: {
    path: '/admin/tipos-de-acoes',
    name: 'Tipos de Ações',
  },
  novoTipoDeAcao: {
    path: '/admin/tipos-de-acoes/adicionar',
    name: 'Novo Tipo de Ação',
  },
  editarTipoDeAcao: {
    path: '/admin/tipos-de-acoes/editar/',
    name: 'Editar Tipo de Ação',
  },
  gestaoDeVoluntarios: {
    path: '/admin/gestao-de-voluntarios',
    name: 'Gestão de Voluntários',
  },
  instituicoesParceiras: {
    path: '/admin/instituicoes-parceiras',
    name: 'Instituições parceiras',
  },
  novaInstituicao: {
    path: '/admin/instituicoes-parceiras/adicionar',
    name: 'Cadastro de Instituições',
  },
  editarInstituicao: {
    path: '/admin/instituicoes-parceiras/editar/',
    name: 'Editar Instituição',
  },
  comites: {
    path: '/admin/comites',
    name: 'Comitês',
  },
  editarComite: {
    path: '/admin/comites/editar/',
    name: 'Editar Comitê',
  },
  novoComite: {
    path: '/admin/comites/adicionar',
    name: 'Cadastro de Comitê',
  },
  acoes: {
    path: '/admin/acoes',
    name: 'Ações',
  },
  novaAcao: {
    path: '/admin/acoes/adicionar',
    name: 'Cadastro de Ação',
  },
  editarAcao: {
    path: '/admin/acoes/editar/',
    name: 'Editar Ação',
  },
  trocarSenha: {
    path: '/admin/trocar-senha',
    name: 'Alterar senha',
  },
  notificacoes: {
    path: '/admin/notificacoes',
    name: 'Notificações',
  },
}

const routes = [
  {
    path: bc.inicio.path,
    component: () => <LandingAdminInfo />,
    exact: true,
    breadcrumb: [bc.inicio],
  },
  {
    path: bc.cidades.path,
    component: () => <CitiesPage />,
    breadcrumb: [bc.inicio, bc.cidades],
    exact: true,
  },
  {
    path: bc.novaCidade.path,
    component: () => <EditCityPage />,
    breadcrumb: [bc.inicio, bc.cidades, bc.novaCidade],
    exact: true,
  },
  {
    path: `${bc.editarCidade.path}:id`,
    component: () => <EditCityPage />,
    breadcrumb: [bc.inicio, bc.cidades, bc.editarCidade],
  },
  {
    path: bc.empresas.path,
    component: () => <CompaniesPage />,
    breadcrumb: [bc.inicio, bc.empresas],
    exact: true,
  },
  {
    path: bc.novaEmpresa.path,
    component: () => <EditCompaniesPage />,
    breadcrumb: [bc.inicio, bc.empresas, bc.novaEmpresa],
    exact: true,
  },
  {
    path: `${bc.editarEmpresa.path}:id`,
    component: () => <EditCompaniesPage />,
    breadcrumb: [bc.inicio, bc.empresas, bc.editarEmpresa],
  },
  {
    path: bc.tiposDeAcoes.path,
    component: () => <ActionTypesPage />,
    breadcrumb: [bc.inicio, bc.tiposDeAcoes],
    exact: true,
  },
  {
    path: bc.novoTipoDeAcao.path,
    component: () => <NewActionTypePage />,
    breadcrumb: [bc.inicio, bc.tiposDeAcoes, bc.novoTipoDeAcao],
    exact: true,
  },
  {
    path: `${bc.editarTipoDeAcao.path}:id`,
    component: () => <NewActionTypePage />,
    breadcrumb: [bc.inicio, bc.tiposDeAcoes, bc.editarTipoDeAcao],
  },
  {
    path: bc.gestaoDeVoluntarios.path,
    component: () => <LandingLeaderVolunteerManagement />,
    breadcrumb: [bc.inicio, bc.gestaoDeVoluntarios],
  },
  {
    path: bc.instituicoesParceiras.path,
    component: () => <InstitutionsPage />,
    breadcrumb: [bc.inicio, bc.instituicoesParceiras],
    exact: true,
  },
  {
    path: bc.novaInstituicao.path,
    component: () => <EditInstitutionsPage />,
    breadcrumb: [bc.inicio, bc.instituicoesParceiras, bc.novaInstituicao],
    exact: true,
  },
  {
    path: `${bc.editarInstituicao.path}:id`,
    component: () => <EditInstitutionsPage />,
    breadcrumb: [bc.inicio, bc.instituicoesParceiras, bc.editarInstituicao],
  },
  {
    path: bc.comites.path,
    component: () => <CommitteePage />,
    breadcrumb: [bc.inicio, bc.comites],
    exact: true,
  },
  {
    path: bc.novoComite.path,
    component: () => <EditCommitteePage />,
    breadcrumb: [bc.inicio, bc.comites, bc.novoComite],
    exact: true,
  },
  {
    path: `${bc.editarComite.path}:id`,
    component: () => <EditCommitteePage />,
    breadcrumb: [bc.inicio, bc.comites, bc.editarComite],
  },
  {
    path: bc.acoes.path,
    component: () => <ActionsPage />,
    breadcrumb: [bc.inicio, bc.acoes],
    exact: true,
  },
  {
    path: bc.novaAcao.path,
    component: () => <NewAction />,
    breadcrumb: [bc.inicio, bc.acoes, bc.novaAcao],
    exact: true,
  },
  {
    path: `${bc.editarAcao.path}:id`,
    component: () => <NewAction />,
    breadcrumb: [bc.inicio, bc.acoes, bc.editarAcao],
  },
  {
    path: '/admin/login',
    main: () => <LoginAdminPage />,
  },
  {
    path: bc.trocarSenha.path,
    component: () => <ChangePassAdminPage />,
    breadcrumb: [bc.inicio, bc.trocarSenha],
  },
  {
    path: bc.notificacoes.path,
    component: () => <NotificationsAdminPage />,
    breadcrumb: [bc.inicio, bc.notificacoes],
  },
]

export default routes
