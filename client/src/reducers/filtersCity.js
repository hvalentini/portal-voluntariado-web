import noAccents from 'remove-accents'
import * as types from '../actions/types'

const initialState = {
  nome: '',
  nomeNormalizado: null,
  uf: null,
  orderSelected: '{ "nomeNormalizado" : 1 }',
}

const filtersCity = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.FILTERS_CITY_EDIT: {
      const nomeNormalizado = payload.nome
                            ? noAccents(payload.nome).toLowerCase()
                            : ''
      return { ...state, ...payload, nomeNormalizado }
    }
    case types.FILTERS_CITY_ORDER: {
      return { ...state, orderSelected: payload }
    }
    case types.FILTERS_CITY_CLEAR: {
      return { ...state, nome: '', nomeNormalizado: '', uf: '', orderSelected: '{ "nomeNormalizado" : 1 }' }
    }
    default:
      return state
  }
}

export default filtersCity
