import { findIndex } from 'lodash'
import * as types from '../actions/types'

const initialState = {
  list: [],
  orderBy: {},
  institution: {
    selectActions: [
      'Nome da Instituição (A-Z)',
      'Nome da Instituição (Z-A)',
    ],
  },
  loader: true,
  isModalOpen: false,
  isDialogOpen: false,
  orderSelected: '{ "nomeNormalizado" : 1 }',
}

const partners = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.PARTNERS_UPDATE: {
      let orderSelected
      if (payload.orderBy && payload.orderBy.nomeNormalizado) {
        orderSelected = payload.orderBy && payload.orderBy.nomeNormalizado === 1
        ? '{ "nomeNormalizado" : 1 }'
        : '{ "nomeNormalizado": -1 }'
      } else { orderSelected = state.orderSelected }
      return { ...state, ...payload, loader: payload.loader, orderSelected }
    }
    case types.PARTNERS_OPEN_MODAL: {
      return { ...state, isModalOpen: payload }
    }
    case types.PARTNERS_CLOSE_MODAL: {
      return { ...state, isModalOpen: payload }
    }
    case types.PARTNER_UPDATE_DIALOG: {
      return { ...state, isDialogOpen: payload }
    }
    case types.PARTNERS_UPDATE_CLEAR: {
      return { ...state, orderSelected: '{ "nomeNormalizado" : 1 }' }
    }
    case types.PARTNERS_UPDATE_EDIT: {
      const list = [...state.list]
      const index = findIndex(list, { _id: payload._id })
      list[index] = payload
      return { ...state, list }
    }
    default:
      return state
  }
}

export default partners
