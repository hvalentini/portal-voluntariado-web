import * as types from '../actions/types'

const initialState = {
  local: {
    nome: '',
    endereco: '',
    telefone: '',
    responsavelNome: '',
    responsavelEmail: '',
    responsavelNome2: '',
    responsavelEmail2: '',
    observacao: '',
    qtdeSalas: '',
    qtdeAlunos: '',
    escola: false,
    serie: '',
    criadoPor: '',
  },
  new: {
    _id: '',
    nome: '',
    endereco: '',
    telefone: '',
    responsavelNome: '',
    responsavelEmail: '',
    responsavelNome2: '',
    responsavelEmail2: '',
    observacao: '',
    qtdeSalas: '',
    qtdeAlunos: '',
    escola: false,
    serie: '',
    criadoPor: '',
  },
  todosLocais: [],
  loader: false,
  openDialog: false,
  erro: false,
  serviceMessage: '',
}

const newLocal = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.NEW_LOCAL_CHANGE: {
      const obj = { ...state.local, ...payload }
      return { ...state, local: { ...obj } }
    }
    case types.NEW_LOCAL_LOADER: {
      return { ...state, loader: payload }
    }
    case types.NEW_LOCAL_EDIT_PHONE: {
      const obj = { ...state.local, ...payload }
      return { ...state, local: { ...obj } }
    }
    case types.NEW_LOCAL_ADD_LOCAL: {
      const obj = { ...initialState.local }
      return { ...state, local: { ...obj }, new: payload }
    }
    case types.NEW_LOCAL_OPEN_DIALOG: {
      return { ...state,
        openDialog: payload.isOpen,
        serviceMessage: payload.message,
        erro: payload.erro,
      }
    }
    case types.NEW_LOCAL_CLOSE_DIALOG: {
      return { ...state, openDialog: payload }
    }
    case types.NEW_LOCAL_GET: {
      return { ...state, local: payload }
    }
    case types.NEW_LOCAL_CLEAR: {
      return { ...initialState }
    }
    default:
      return state
  }
}

export default newLocal
