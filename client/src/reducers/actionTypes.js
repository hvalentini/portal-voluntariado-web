import { findIndex } from 'lodash'
import * as types from '../actions/types'

const initialState = {
  actions: [],
  action: {
    _id: '',
    nome: '',
    exclusivoParaLider: false,
    informarQtdeCartas: false,
    tipo: 0,
  },
  deleteRequestFinished: false,
  deleteRequestFinishedOk: false,
  message: '',
  clear: true,
}

const actionTypes = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.ACTION_TYPES_UPDATE: {
      return { ...state, ...payload }
    }
    case types.ACTION_TYPES_GET_ONE: {
      const obj = { ...state.action, ...payload }
      return { ...state, action: { ...obj } }
    }
    case types.ACTION_TYPES_UPDATE_ACTION: {
      return { ...state.action, action: { ...payload } }
    }
    case types.ACTION_TYPES_CLEAR: {
      return { ...state.action, ...initialState.action }
    }
    case types.ACTION_TYPES_DELETE: {
      const newActions = [...state.actions]
      const index = findIndex(newActions, { _id: payload._id })
      newActions.splice(index, 1)
      return { ...state, actions: [...newActions] }
    }
    default:
      return state
  }
}

export default actionTypes
