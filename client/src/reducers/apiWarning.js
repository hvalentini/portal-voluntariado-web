import * as types from '../actions/types'

const initialState = false

const apiWarning = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.API_WARNING: {
      return payload
    }
    default:
      return state
  }
}

export default apiWarning
