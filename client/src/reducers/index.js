import { combineReducers } from 'redux'
import ui from './ui'
import apiWarning from './apiWarning'
import webTest from './webTest'
import comites from './comites'
import acoes from './acoes'
import voluntario from './voluntario'
import landingAvatars from './landingAvatars'
import notificacao from './notificacoes'
import faixaSalarial from './faixaSalarial'
import statistic from './statistic'
import registerForm from './registerForm'
import newAction from './newAction'
import newLocal from './newLocal'
import registerFormAction from './registerFormAction'
import usersAutocomplete from './usersAutocomplete'
import localsAutocomplete from './localsAutocomplete'
import commiteVolunteerAction from './commiteVolunteerAction'
import login from './login'
import loggedUser from './loggedUser'
import filters from './filters'
import partners from './partners'
import citySelect from './citySelect'
import newPass from './newPass'
  // import heartButton from './heartButton'
import cidades from './cidades'
import empresas from './empresas'
import actionTypes from './actionTypes'
import filtersCity from './filtersCity'
import filtersActionTypes from './filtersActionTypes'
import modal from './modal'
import list from './list'
import filtersCompany from './filtersCompany'
import committeeSelects from './committeeSelects'

// TODO: importar os reducers à medida que forem sendo criados

export default combineReducers({
  ui,
  apiWarning,
  webTest,
  comites,
  acoes,
  voluntario,
  landingAvatars,
  notificacao,
  statistic,
  registerForm,
  registerFormAction,
  faixaSalarial,
  newAction,
  newLocal,
  usersAutocomplete,
  localsAutocomplete,
  loggedUser,
  login,
  filters,
  partners,
  commiteVolunteerAction,
  citySelect,
  newPass,
  // heartButton,
  cidades,
  empresas,
  actionTypes,
  filtersCity,
  filtersActionTypes,
  modal,
  list,
  filtersCompany,
  committeeSelects,
})
