import * as types from '../actions/types'

const initialState = {
  cidades: [],
  empresas: [],
  estados: [],
  comites: [],
  acoes: [],
  total: 0,
  loading: false,
}

const cidades = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.LIST_CITY_FETCH: {
      return { ...state, cidades: payload }
    }
    case types.LIST_COMPANY_FETCH: {
      return { ...state, empresas: payload }
    }
    case types.LIST_STATE_FETCH: {
      return { ...state, estados: payload }
    }
    case types.LIST_COMMITTEES_FETCH: {
      return { ...state, comites: payload.slice(0, 6), total: payload.length, loading: false }
    }
    case types.LIST_COMMITTEES_PAGINATE: {
      return { ...state, comites: payload.slice(0, 6), loading: false }
    }
    case types.LIST_ACTIONS_FETCH: {
      return { ...state, acoes: payload.slice(0, 6), total: payload.length, loading: false }
    }
    case types.LIST_ACTIONS_PAGINATE: {
      return { ...state, acoes: payload, loading: false }
    }
    case types.LIST_ACTIONS_LOADING: {
      return { ...state, loading: payload }
    }
    default:
      return state
  }
}

export default cidades
