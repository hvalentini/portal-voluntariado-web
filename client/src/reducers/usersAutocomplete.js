import { reject, reduce } from 'lodash'
import * as types from '../actions/types'

const initialState = {
  dataSource: [],
  searchText: '',
  selected: [],
  errorTextUser: '',
  editUsers: false,
  bkSource: [],
}

const usersAutocomplete = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.USERSAUTOCOMPLETE_UPDATE_DATASOURCE: {
      const temp = reject(payload, element =>
        reduce(state.selected, (result, value) => {
          if (element._id === value.voluntarioId || element._id === value._id) { return true }
          return result
        }, false)
      )

      return {
        ...state,
        dataSource: temp,
        bkSource: temp,
      }
    }
    case types.USERSAUTOCOMPLETE_UPDATE_SELECTED: {
      const temp = reject(state.dataSource, element => element._id === payload._id || element._id === payload.voluntarioId)
      return { ...state, selected: [...state.selected, payload], dataSource: temp }
    }
    case types.USERSAUTOCOMPLETE_REMOVE_SELECTED: {
      return {
        ...state,
        selected: [
          ...state.selected.slice(0, Number(payload)),
          ...state.selected.slice(Number(payload) + 1),
        ],
        dataSource: reject(state.dataSource, element => (
          element._id === payload._id || element._id === payload.voluntarioId
        )),
      }
    }
    case types.USERSAUTOCOMPLETE_FILTER_DATASOURCE: {
      const reg = new RegExp(payload.texto, 'i')
      const temp = state.bkSource.filter(el => el.nomeNormalizado.match(reg))
      return { ...state, dataSource: temp }
    }
    case types.USERSAUTOCOMPLETE_ERROR_TEXT: {
      return { ...state, errorTextUser: payload }
    }
    case types.USERSAUTOCOMPLETE_EDIT_SELECTED: {
      return { ...state, selected: [...state.selected, payload] }
    }
    case types.USERSAUTOCOMPLETE_SET_SELECTED: {
      return { ...state, selected: [...state.selected, ...payload] }
    }
    case types.USERSAUTOCOMPLETE_CLEAN_SELECTED: {
      return { ...state, selected: [] }
    }
    case types.USERSAUTOCOMPLETE_CHANGE_USERS: {
      return { ...state, editUsers: true }
    }
    default:
      return state
  }
}

export default usersAutocomplete
