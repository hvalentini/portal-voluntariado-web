import * as types from '../actions/types'
import noAccents from 'remove-accents'

const initialState = {
  voluntario: {
    comite: {
      saldoAtual: 15000,
      empresa: {
        nome: 'Algar Telecom',
      },
      unidade: 'Industrial',
    },
    nome: 'Marcelo Parreira Pacheco',
    urlAvatar: 'https://goo.gl/0NRuks',
  },
  amountVolunteers: 0,
  cpf: '',
  volunteers: [],
  loader: true,
  filterVolunteers: [],
  search: false,
  searchTerm: [],
  term: '',
  normalizedTerm: '',
  orderOfVolunteers: 'nameasc',
  typeOrder: '',
  selectedFilter: '',
  filtro: {},
}

const voluntario = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.VOLUNTEERS_FETCH_AMOUNT: {
      return { ...state, amountVolunteers: payload.quantidade }
    }
    case types.VOLUNTEERS_ENT_UPDATE: {
      return { ...state, voluntario: { ...state.voluntario, ...payload } }
    }
    case types.VOLUNTEERS_EDIT_CPF: {
      return { ...state, cpf: payload }
    }
    case types.VOLUNTEERS_FETCH_VOLUNTEERS: {
      return { ...state, volunteers: payload.data, loader: payload.loader }
    }
    case types.VOLUNTEERS_FETCH_VOLUNTEERS_FAIL: {
      return { ...state, loader: false }
    }
    case types.VOLUNTEERS_LAST_FILTER:
    case types.VOLUNTEERS_SORTBY: {
      return { ...state, ...payload }
    }
    case types.VOLUNTEERS_SEARCH_TERM_VOLUNTEERS: {
      const normalizedTerm = payload.newValue ? noAccents(payload.newValue) : ''
      return { ...state, searchTerm: payload.searchTerm, term: payload.newValue, search: false, normalizedTerm: normalizedTerm }
    }
    case types.VOLUNTEERS_SEARCH_VOLUNTEERS: {
      const filterVolunteers = payload
      return { ...state, ...filterVolunteers, search: true, searchTerm: [], term: '' }
    }
    case types.VOLUNTEERS_SELECTED_FILTER: {
      const selectedFilter = payload
      return { ...state, ...selectedFilter }
    }
    default:
      return state
  }
}

export default voluntario
