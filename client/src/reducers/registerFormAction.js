import find from 'lodash.find'
import findIndex from 'lodash.findindex'
import * as types from '../actions/types'

const initialState = {
  galeria: [],
  galleryTemp: {},
  nome: '',
  descricao: '',
  dataInicial: null,
  dataFinal: null,
  horaInicial: null,
  horaFinal: null,
  totalBeneficiados: '',
  totalCartas: '',
  valorGasto: '0.00',
  acaoPontual: null,
  tempoDuracao: null,
  cadastroRetroativo: true,
  tipoAcao: {
    _id: '',
    nome: '',
    exclusivoParaLider: false,
    informarQtdeCartas: false,
  },
  tipoAcaoTemp: [],
  success: false,
  loader: false,
  error: '',
}

const cleanState = {
  galeria: [],
  galleryTemp: {},
  nome: '',
  descricao: '',
  dataInicial: null,
  dataFinal: null,
  horaInicial: null,
  horaFinal: null,
  totalBeneficiados: '',
  totalCartas: '',
  valorGasto: '0.00',
  acaoPontual: null,
  tempoDuracao: null,
  cadastroRetroativo: true,
  tipoAcao: {
    _id: '',
    nome: '',
    exclusivoParaLider: false,
    informarQtdeCartas: false,
  },
  tipoAcaoTemp: [],
  success: false,
  loader: false,
  error: '',
  tipoAcao_tipo: null,
}

const registerFormAction = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.REGISTER_FORM_ACTION_PREVIEW_GALLERY: {
      const galleryTemp = state.galleryTemp
      galleryTemp.galeria = galleryTemp.galeria || []
      const newGaleria = payload.map(file => ({
        originalName: file.id,
        preview: file.preview,
        urlFoto: null,
        id: file.idTemp,
        fotoDestaque: false,
        idTemp: file.key,
      }))

      galleryTemp.galeria = [
        ...galleryTemp.galeria,
        ...newGaleria,
      ]

      return { ...state, ...galleryTemp }
    }
    case types.REGISTER_FORM_ACTION_UPDATE_GALLERY_IMAGE: {
      const galleryTemp = { ...state.galleryTemp }
      payload.forEach((file) => {
        const index = findIndex(galleryTemp.galeria, 'originalName', file.originalName)
        const updateImage = galleryTemp.galeria[index]
        updateImage.urlFoto = `/images/${updateImage.id}/${file.mediaPath}`
        // delete updateImage.preview
        delete updateImage.originalName
        // delete updateImage.id
        // delete updateImage.idTemp
      })

      return { ...state, ...galleryTemp }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_CHECKBOX: {
      const result = { ...state.galleryTemp }
      const galeria = result.galeria
      find(galeria, payload).fotoDestaque =
      !(find(galeria, payload).fotoDestaque)
      // verifica.fotoDestaque = !verifica.fotoDestaque

      return { ...state, galleryTemp: result }
    }
    case types.REGISTER_FORM_ACTION_REMOVE_GALLERY_ITEM: {
      const galleryTemp = { ...state.galleryTemp }
      galleryTemp.galeria = payload
      return { ...state, galleryTemp: { galeria: payload }, galeria: payload }
    }
    case types.REGISTER_FORM_ACTION_OCCURRED:
    case types.REGISTER_FORM_ACTION_CHANGE_DURATION_HOURS:
    case types.REGISTER_FORM_ACTION_CHANGE_PROPERTY: {
      return { ...state, ...payload }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_RADIO_BUTTON: {
      const acaoPontual = Number(payload.tipoAcao_tipo) === 0
      return { ...state, tipoAcao_tipo: Number(payload.tipoAcao_tipo), acaoPontual }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_START_DATE: {
      return { ...state, dataInicial: payload }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_END_DATE: {
      return { ...state, dataFinal: payload }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_START_HOURS: {
      return { ...state, horaInicial: payload }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_END_HOURS: {
      return { ...state, horaFinal: payload }
    }
    case types.REGISTER_FORM_ACTION_FETCH_ACTION_TYPES: {
      return { ...state, tipoAcaoTemp: payload }
    }
    case types.REGISTER_FORM_ACTION_SUCCESS: {
      return { ...state, success: payload }
    }
    case types.REGISTER_FORM_CLEAN_NEW_ACTION: {
      return { ...state, ...cleanState, galleryTemp: {} }
    }
    case types.REGISTER_FORM_ACTION_LOADER: {
      return { ...state, loader: true }
    }
    case types.REGISTER_FORM_ACTION_ERROR: {
      return { ...state, loader: payload.loader, error: payload.error }
    }
    case types.REGISTER_FORM_ACTION_CHANGE_TYPE_ACTION: {
      const tipoAcao = {}
      tipoAcao._id = payload.tipoAcao._id
      tipoAcao.nome = payload.tipoAcao.nome
      tipoAcao.informarQtdeCartas = payload.tipoAcao.informarQtdeCartas
      tipoAcao.exclusivoParaLider = payload.tipoAcao.exclusivoParaLider

      return { ...state, tipoAcao }
    }
    case types.REGISTER_FORM_ACTION_CLEAN_TYPE_ACTION: {
      const tipoAcao = {
        _id: '',
        nome: '',
        exclusivoParaLider: false,
        informarQtdeCartas: false,
      }
      return { ...state, tipoAcao }
    }
    default:
      return state
  }
}

export default registerFormAction
