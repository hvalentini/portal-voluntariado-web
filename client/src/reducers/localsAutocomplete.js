import * as types from '../actions/types'

const initialState = {
  base: [],
  dataSource: [],
  searchText: undefined,
  selected: {},
  errorTextLocal: '',
  modalOpen: false,
}

const usersAutocomplete = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.LOCALSAUTOCOMPLETE_UPDATE_DATASOURCE: {
      const searchBase = { ...state }
      const reg = new RegExp(payload, 'gi')
      const result = searchBase.base.filter(element => (
        element.nomeNormalizado.match(reg)
      ))
      return { ...state, dataSource: result }
    }
    case types.LOCALSAUTOCOMPLETE_UPDATE_SELECTED: {
      return { ...state, selected: payload }
    }
    case types.LOCALSAUTOCOMPLETE_UPDATE: {
      return { ...state, ...payload }
    }
    case types.LOCALSAUTOCOMPLETE_UPDATE_ERROR_TEXT: {
      return { ...state, errorTextLocal: payload }
    }
    case types.LOCALSAUTOCOMPLETE_SET_BASE: {
      return { ...state, base: payload }
    }
    case types.LOCALSAUTOCOMPLETE_OPEN_MODAL: {
      return { ...state, modalOpen: payload }
    }
    case types.LOCALSAUTOCOMPLETE_CLOSE_MODAL: {
      return { ...state, modalOpen: payload }
    }
    default:
      return state
  }
}

export default usersAutocomplete
