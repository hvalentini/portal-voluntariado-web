import * as types from '../actions/types'

const initialState = {
  errorValidation: null,
  loading: false,
  loadingForm: false,
  galleryModal: {
    galleryModalContent: [],
    open: false,
    galleryModalCurrentSlide: null,
  },
  loaderBtn: false,
  comites: [],
}

const ui = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.UI_LOADING: {
      return { ...state, loading: payload, errorValidation: null }
    }
    case types.UI_LOADING_LIST: {
      return { ...state, loading: payload }
    }
    case types.UI_GALLERY_MODAL_OPEN: {
      const galleryModal = { ...state.galleryModal, open: true }
      return { ...state, galleryModal }
    }
    case types.UI_GALLERY_MODAL_CLOSE: {
      const galleryModal = { ...state.galleryModal, open: false }
      return { ...state, galleryModal }
    }
    case types.UI_SET_GALLERY_MODAL_CONTENT: {
      return { ...state,
        galleryModal: {
          galleryModalContent: payload.gallery,
          open: true,
          galleryModalCurrentSlide: payload.pos,
          galleryModalInitialSlide: payload.pos,
        },
      }
    }
    case types.UI_SET_GALLERY_MODAL_CURRENTSLIDE: {
      const galleryModal = { ...state.galleryModal, galleryModalCurrentSlide: payload }
      if (payload >= galleryModal.galleryModalContent.length) {
        galleryModal.galleryModalCurrentSlide = 0
      }
      if (payload < 0) {
        galleryModal.galleryModalCurrentSlide = galleryModal.galleryModalContent.length - 1
      }

      return { ...state, galleryModal }
    }
    case types.UI_SET_LOADER_BTN: {
      return { ...state, loaderBtn: payload }
    }
    case types.UI_LOADING_FORM: {
      return { ...state, loadingForm: payload }
    }
    case types.COMMITTEE_SET_PRINT: {
      return { ...state, comites: payload }  
    }
    default:
      return state
  }
}

export default ui
