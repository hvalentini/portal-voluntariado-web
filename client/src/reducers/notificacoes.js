import * as types from '../actions/types'

const initialState = {
  notificacoes: [],
  amountNotifications: 0,
  readNotifications: false,
  entidade: {
    publicoAlvo: [],
    texto: '',
    tipo: '',
  },
}

const notificacao = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.NOTIFICATIONS_GET_ALL: {
      return {
        ...state,
        notificacoes: payload.notifications,
        amountNotifications: payload.amountNotifications,
      }
    }
    case types.NOTIFICATIONS_UPDATE_ENTIDADE: {
      return { ...state, entidade: { ...state.entidade, ...payload } }
    }
    case types.NOTIFICATIONS_UPDATE: {
      return { ...state, ...payload }
    }
    case types.NOTIFICATIONS_READ: {
      return { ...state, ...payload }
    }
    default:
      return state
  }
}

export default notificacao
