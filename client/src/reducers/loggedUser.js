import * as types from '../actions/types'

const initialState = {
  // nome: 'Lucas Sales',
  // urlAvatar: 'http://cdn.collider.com/wp-content/uploads/2016/10/logan-patrick-stewart.jpg',
  // email: 'lucas.sales@algartelecom.com.br',
  // comite: {
  //   saldoAtual: 15000,
  //   empresa: {
  //     nome: 'Algar Telecom',
  //   },
  //   unidade: 'Industrial',
  // },
}

const loggedUser = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.LOGGED_USER_UPDATE: {
      return { ...state, ...payload }
    }
    default:
      return state
  }
}

export default loggedUser
