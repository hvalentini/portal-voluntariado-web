import * as types from '../actions/types'

const initialState = {
  nome: '',
  uf: '',
}

const cidades = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.CITY_EDIT: {
      return { ...state, ...payload }
    }
    case types.CITY_SEARCH: {
      return { ...state, ...payload }
    }
    case types.CITY_CLEAR: {
      return { ...initialState }
    }
    default:
      return state
  }
}

export default cidades
