import * as types from '../actions/types'

const initialState = {
  leader: {
    volunteers: null,
    actionsTaken: null,
  },
  volunteer: {
    selectActions: [
      { value: '5', text: 'Todos comitês' },
      { value: '6', text: 'Ações que participei' },
      { value: '2', text: 'Meu comitê' },
      { value: '1', text: 'Comitês da minha empresa' },
      { value: '3', text: 'Comitês da minha cidade' },
      { value: '4', text: 'Comitês do meu estado' },
    ],
    selectedAction: '5',
    actionsAllProgram: 0,
    actionMyComite: 0,
  },
  participated: [],
  loader: false,
  loaderMyComite: false,
  loaderQtdVoluntario: false,
  loaderQtdAcaoRealizada: false,
}

const statistic = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.STATISTIC_VOLUNTEERS_COMMITTEES: {
      const leader = { ...state.leader }
      leader.volunteers = payload
      return { ...state, leader, loaderQtdVoluntario: false }
    }
    case types.STATISTIC_ACTIONS_COMMITTEES: {
      const leader = { ...state.leader }
      leader.actionsTaken = payload
      return { ...state, leader, loaderQtdAcaoRealizada: false }
    }
    case types.STATISTIC_UPDATE: {
      const volunteer = { ...state.volunteer, ...payload }
      return { ...state, volunteer }
    }
    case types.STATISTIC_ALL_PROGRAM: {
      const volunteer = { ...state.volunteer, actionsAllProgram: payload }
      return { ...state, volunteer, loader: false }
    }
    case types.STATISTIC_ACTIONS_MY_COMITE: {
      const volunteer = { ...state.volunteer, actionMyComite: payload }
      return { ...state, volunteer, loaderMyComite: false }
    }
    case types.STATISTIC_SET_LOADER_ALL_PROGRAM: {
      return { ...state, loader: payload }
    }
    case types.STATISTIC_SET_LOADER_MY_COMITE: {
      return { ...state, loaderMyComite: payload }
    }
    case types.STATISTIC_SET_LOADER_QTD_VOLUNTEER: {
      return { ...state, loaderQtdVoluntario: payload }
    }
    case types.STATISTIC_SET_LOADER_QTD_ACTIONS: {
      return { ...state, loaderQtdAcaoRealizada: payload }
    }
    default:
      return state
  }
}

export default statistic
