import * as types from '../actions/types'

const initialState = {
  nome: '',
  descricao: '',
  dataInicial: 0,
  dataFinal: 0,
  horaInicial: 0,
  horaFinal: 0,
  totalBeneficiados: '',
  totalCartas: '',
  valorGasto: '',
  cadastroRetroativo: false,
  acaoPontual: true,
  tempoDuracao: '',
}

const newAction = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.NEW_ACTION_CHANGE_NAME: {
      return { ...state, ...payload }
    }
    default:
      return state
  }
}

export default newAction
