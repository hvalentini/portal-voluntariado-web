import find from 'lodash.find'
import * as types from '../actions/types'
import noAccents from 'remove-accents'

const initialState = {
  acoes: {},
  partners: {
    nome: null,
    escola: 'null',
    comite: null,
    dataFinal: '',
    dataInicial: '',
    statusAcao: 'todas',
  },
  acoesTemp: {},
  statusSelected: 'todas',
  tipoAcaoSelected: '',
  nomeAcao: '',
  loader: false,
  query: '',
}

const filters = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.FILTERS_UPDATE: {
      return { ...state, ...payload }
    }
    case types.FILTERS_EDIT_START_DATE: {
      // return { ...state, acoes: { ...state.acoes, dataInicial: payload }, statusSelected: 'programadas' }
      const acoesRes = { ...state.acoes }
      acoesRes.dataInicial = payload
      return { ...state, acoes: acoesRes, statusSelected: 'programadas' }
    }
    case types.FILTERS_EDIT_END_DATE: {
      // return { ...state, acoes: { ...state.acoes, dataFinal: payload }, statusSelected: 'concluidas' }
      const acoesRes = { ...state.acoes }
      acoesRes.dataFinal = payload
      return { ...state, acoes: acoesRes, statusSelected: 'concluidas' }
    }
    case types.FILTERS_EDIT_TYPE: {
      const acoesRes = payload
      if (acoesRes.tipoAcao === '') {
        delete acoesRes.tipoAcao
      }
      const tipoSelected = payload.tipoAcao ? payload.tipoAcao : ''
      return { ...state, acoes: acoesRes, tipoAcaoSelected: tipoSelected }
    }
    case types.FILTERS_EDIT_VOLUNTARY: {
      return { ...state, acoes: payload }
    }
    case types.FILTERS_EDIT_DATE: {
      // return { ...state, acoes: { ...payload }, statusSelected: payload.status }
      const acoesRes = { ...state.acoes }
      acoesRes.dataInicial = payload.dataInicial
      acoesRes.dataFinal = payload.dataFinal
      acoesRes.statusAcao = payload.status
      return { ...state, acoes: acoesRes, statusSelected: payload.status }
    }
    case types.FILTERS_EDIT_PAYLOAD: {
      return { ...state, acoesTemp: payload }
    }
    case types.FILTERS_SET_FILTERS: {
      const acoesRes = {
        comite: null,
        dataFinal: '',
        dataInicial: '',
        statusAcao: 'todas',
      }
      return { ...state, acoes: acoesRes, statusSelected: 'todas', tipoAcaoSelected: '', nomeAcao: '' }
    }
    case types.FILTERS_UPDATE_PARTNERS: {
      return { ...state, partners: { ...state.partners, ...payload } }
    }
    case types.FILTERS_EDIT_VOLUNTARY_NAME: {
      const acoes = { ...state.acoes }
      acoes.nomeNormalizado = noAccents(payload).toLowerCase()
      return { ...state, nomeAcao: payload, acoes }
    }
    case types.FILTER_SET_LOADING: {
      return { ...state, loading: payload }
    }
    case types.FILTER_SET_LOADER: {
      return { ...state, loader: payload }
    }
    case types.FILTERS_CLEAN_PARTNERS: {
      const partners = {
        nome: null,
        escola: 'null',
        comite: null,
        dataFinal: '',
        dataInicial: '',
        statusAcao: 'todas',
      }
      return { ...state, partners }
    }
    default:
      return state
  }
}

export default filters
