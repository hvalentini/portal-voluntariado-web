import * as types from '../actions/types'

const initialState = {
  photosLeft: [],
  photosRight: [],
  configPhotos: [
    { top: 65, left: 40, disabled: true },
    { top: 20, left: 190, disabled: true },
    { top: 75, left: 375, disabled: true },
    { top: 120, left: 100, opacity: 1, border: true, size: 'medium' },
    { top: 105, left: 260, opacity: 1, border: true, size: 'medium' },
    { top: 220, left: 25, disabled: true },
    { top: 315, left: 350, disabled: true },
    { top: 230, left: 210, disabled: true },
    { top: 390, left: 180, disabled: true },
    { top: 430, left: 375, disabled: true },
    { top: 470, left: 105, disabled: true },
    { top: 430, left: 10, opacity: 1, border: true, size: 'medium' },
    { top: 500, left: 166, opacity: 1, border: true, size: 'medium' },
    { top: 285, left: 70, opacity: 1, border: true, size: 'medium' },
    { top: 220, left: 360, opacity: 1, border: true, size: 'medium' },
    { top: 320, left: 245, opacity: 1, border: true, size: 'medium' },
    { top: 490, left: 315, opacity: 1, border: true, size: 'medium' },
  ],
}

const ui = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.LANDING_AVATARS_RANDOM_LEFT: {
      return { ...state, photosLeft: payload }
    }
    case types.LANDING_AVATARS_RANDOM_RIGHT: {
      return { ...state, photosRight: payload }
    }
    default:
      return state
  }
}

export default ui
