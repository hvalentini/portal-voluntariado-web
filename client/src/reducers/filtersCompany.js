import noAccents from 'remove-accents'
import * as types from '../actions/types'

const initialState = {
  nome: '',
  nomeNormalizado: '',
  orderSelected: '{ "nomeNormalizado" : 1 }',
}

const filtersCompany = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.FILTERS_COMPANY_EDIT: {
      const nomeNormalizado = payload
                            ? noAccents(payload).toLowerCase()
                            : ''
      return { ...state, nome: payload, nomeNormalizado }
    }
    case types.FILTERS_COMPANY_ORDER: {
      return { ...state, orderSelected: payload }
    }
    case types.FILTERS_COMPANY_CLEAR: {
      return { ...state, nome: '', nomeNormalizado: '', orderSelected: '{ "nomeNormalizado" : 1 }' }
    }
    default:
      return state
  }
}

export default filtersCompany
