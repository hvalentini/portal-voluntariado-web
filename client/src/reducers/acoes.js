import find from 'lodash.find'
import findIndex from 'lodash.findindex'
import * as types from '../actions/types'

const initialState = {
  amountBeneficiated: 0,
  amountActions: 0,
  gallery: [],
  acoes: [],
  galeria: [{
    url: '',
    fotoDestaque: null,
    _id: '',
  }],
  acoesPrint: [],
  acoesLeader: [],
  acaoEdit: {},
  orderSelected: '{ "dataCadastro" : -1}',
  qtdActions: '',
  success: false,
  loader: false,
  ordenacao: '{ "dataCadastro": -1 }',
  tipoAcao_tipo: null,
  loaderBeneficiated: false,
  loadingCarousel: false,
}

const acoes = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.ACTION_FETCH_AMOUNT_BENEFICIATED: {
      return { ...state, amountBeneficiated: payload.quantidade, loaderBeneficiated: false }
    }
    case types.ACTION_FETCH_AMOUNT_ACTIONS: {
      return { ...state, amountActions: payload.quantidade }
    }
    case types.ACTION_FETCH_GALLERY: {
      return { ...state, gallery: payload }
    }
    case types.ACTIONS_FILTERS: {
      return { ...state, acoes: payload }
    }
    case types.ACTION_PREVIEW_GALLERY: {
      let acoesGallery = [...state.acoes]
      acoesGallery = acoesGallery || []

      acoesGallery.forEach((acao) => {
        if (acao._id === payload._id) {
          const galleryFull = acao.galeria.concat(payload.galeria)
          acao.galeria = galleryFull
        }
      })

      return { ...state, acoes: acoesGallery }
    }
    case types.ACTIONS_BY_LEADER: {
      const qtdActions = payload['qtdActions'] ? payload['qtdActions'] : state.qtdActions
      return { ...state, acoesLeader: payload['data'], orderSelected: payload['order'], loading: false, qtdActions }
    }
    case types.ACTIONS_SET_LOADING: {
      return { ...state, loading: payload }
    }
    case types.ACTIONS_SET_LOADING_CAROUSEL: {
      return { ...state, loadingCarousel: payload }
    }
    case types.ACTIONS_EDITION: {
      if (Object.keys(payload).length < 1) { return { ...state, acaoEdit: payload, tipoAcao_tipo: null } }
      return { ...state, acaoEdit: payload, tipoAcao_tipo: Number(payload.tipoAcao.tipo) }
    }
    case types.ACTIONS_EDIT_PROPERTY:
    case types.ACTIONS_EDIT_START_DATE:
    case types.ACTIONS_EDIT_END_DATE:
    case types.ACTIONS_CHANGE_START_HOURS:
    case types.ACTIONS_CHANGE_END_HOURS:
    case types.ACTIONS_EDIT_TYPE_ACTION:
    case types.ACTIONS_CHANGE_DURATION_HOURS:
    case types.ACTIONS_EDIT_REMOVE_GALLERY_ITEM:
    case types.ACTIONS_EDIT_ACTION_OCCURRED: {
      return { ...state, acaoEdit: { ...state.acaoEdit, ...payload } }
    }
    case types.ACTIONS_CHANGE_RADIO_BUTTON: {
      const acaoPontual = Number(payload.tipoAcao_tipo) === 0
      return {
        ...state,
        acaoEdit: {
          ...state.acaoEdit,
          tipoAcao_tipo: Number(payload.tipoAcao_tipo),
          acaoPontual,
        },
      }
    }
    case types.ACTIONS_EDIT_ACTION_CHANGE_CHECKBOX: {
      const galeriaContainer = [...state.acaoEdit.galeria]
      find(galeriaContainer, payload).fotoDestaque =
      !(find(galeriaContainer, payload).fotoDestaque)
      const result = { galeria: galeriaContainer }
      return { ...state, acaoEdit: { ...state.acaoEdit, ...result } }
    }
    case types.ACTIONS_EDIT_ACTION_PREVIEW_GALLERY: {
      const acaoEdit = { ...state.acaoEdit }
      acaoEdit.galeria = acaoEdit.galeria || []
      const newGaleria = payload.map(file => ({
        originalName: file.id,
        preview: file.preview,
        urlFoto: null,
        id: file.idTemp,
        fotoDestaque: false,
        idTemp: file.key,
      }))

      acaoEdit.galeria = [
        ...acaoEdit.galeria,
        ...newGaleria,
      ]

      return { ...state.acaoEdit, acaoEdit }
    }
    case types.ACTIONS_EDIT_ACTION_UPDATE_GALLERY_IMAGE: {
      const acaoEdit = { ...state.acaoEdit }
      const result = acaoEdit.galeria.filter(item => item.originalName)

      payload.forEach((file) => {
        const index = findIndex(result, { originalName: file.originalName })
        const updateImage = result[index]
        updateImage.urlFoto = `/images/${updateImage.id}/${file.mediaPath}`
        delete updateImage.originalName
      })

      acaoEdit.galeria = [
        ...acaoEdit.galeria,
        ...result,
      ]

      return { ...state, ...acaoEdit }
    }
    case types.ACTIONS_EDIT_FORM_ACTION_SUCCESS: {
      return { ...state, success: true, loader: true }
    }
    case types.ACTIONS_CLEAN_TYPE_ACTION: {
      const acaoEdit = { ...state.acaoEdit }
      acaoEdit.tipoAcao = {
        _id: '',
        nome: '',
        exclusivoParaLider: false,
        informarQtdeCartas: false,
      }
      return { ...state.acaoEdit, acaoEdit }
    }
    case types.ACTIONS_SET_PRINT: {
      return { ...state, acoesPrint: payload }
    }
    case types.ACTIONS_SET_ORDER: {
      return { ...state, ordenacao: payload }
    }
    case types.ACTIONS_SET_LOADER: {
      return { ...state, loaderBeneficiated: payload }
    }
    default:
      return state
  }
}

export default acoes
