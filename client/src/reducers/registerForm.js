import get from 'lodash.get'
import * as types from '../actions/types'
import phoneMask from '../utils/phoneMask'

const initialState = {
  step: 0,
  step1: {
    nome: '',
    cargoNome: '',
    centroResultadoNome: '',
    urlAvatar: '',
    cpf: null,
    cpfMasked: null,
    voluntarioUI: {},
    voluntarioIncompleto: false,
    usuarioSharePoint: false,
  },
  step2: {
    comite: {},
    email: '',
    telefoneProfissional: '',
    emailSuperior: '',
    telefonePessoal: '',
    endereco: '',
    bairro: '',
    cidade: {},
  },
  step3: {
    faixaSalarial: {},
    escolaridade: null,
  },
  success: false,
  errorCpf: null,
  errorMsg: null,
  loader: false,
  voluntarioUI: {},
  voluntarioCompleto: {},
}

const registerForm = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.REGISTER_FORM_SESSION_VOLUNTEER: {
      const STEP1 = {
        _id: payload._id,
        nome: state.step1.nome ||
          (payload.nome ? payload.nome.split(' ', 1)[0] : payload.nome),
        nomeCompleto: state.step1.nomeCompleto ? state.step1.nomeCompleto : payload.nome,
        urlAvatar: state.step1.urlAvatar ? state.step1.urlAvatar : payload.urlAvatar,
        cpf: state.step1.cpf ? state.step1.cpf : payload.cpf,
        cpfMasked: state.step1.cpfMasked ||
          (payload.cpf ? payload.cpf
          .replace(/(\d{3})(\d)/, '$1.$2')
          .replace(/(\d{3})(\d)/, '$1.$2')
          .replace(/(\d{3})(\d)/, '$1-$2')
          : null),
        usuarioSharePoint: payload.usuarioSharePoint,
        cargoNome: state.step1.cargoNome ? state.step1.cargoNome : payload.cargoNome,
        cargoId: state.step1.cargoId ? state.step1.cargoId : payload.cargoId,
        centroResultadoNome: state.step1.centroResultadoNome
                            ? state.step1.centroResultadoNome
                            : payload.centroResultadoNome,
        centroResultadoId: state.step1.centroResultadoId
                            ? state.step1.centroResultadoId
                            : payload.centroResultadoId,
        associadoExecutivo: typeof state.step1.associadoExecutivo === 'boolean'
                            ? state.step1.associadoExecutivo
                            : payload.associadoExecutivo,
        voluntarioIncompleto: true,
      }
      const STEP2 = {
        comite: (state.step2.comite && state.step2.comite._id)
          ? state.step2.comite
          : payload.comite,
        email: state.step2.email ? state.step2.email : payload.email,
        emailSuperior: state.step2.emailSuperior
                      ? state.step2.emailSuperior
                      : payload.emailSuperior,
        telefonePessoal: state.step2.telefonePessoal
                        ? state.step2.telefonePessoal
                        : phoneMask(payload.telefonePessoal),
        telefoneProfissional: state.step2.telefoneProfissional
                              ? state.step2.telefoneProfissional
                              : phoneMask(payload.telefoneProfissional),
        endereco: state.step2.endereco || payload.endereco,
        bairro: state.step2.bairro || payload.bairro,
        cidade: (state.step2.cidade && state.step2.cidade._id)
          ? state.step2.cidade
          : payload.cidade,
      }
      const STEP3 = {
        faixaSalarial: (state.step3.faixaSalarial && state.step3.faixaSalarial._id)
                    ? state.step3.faixaSalarial
                    : payload.faixaSalarial,
        tamanhoCamiseta: state.step3.tamanhoCamiseta
                  ? state.step3.tamanhoCamiseta
                  : payload.tamanhoCamiseta,
        genero: state.step3.genero
                ? state.step3.genero
                : payload.genero,
        escolaridade: state.step3.escolaridade
                ? state.step3.escolaridade
                : payload.escolaridade,
      }
      return { ...state, step1: STEP1, step2: STEP2, step3: STEP3, voluntarioCompleto: payload }
    }
    case types.REGISTER_FORM_UPDATE: {
      return { ...state, ...payload }
    }
    case types.REGISTER_FORM_UPDATE_STEP1: {
      const STEP1 = { ...state.step1 }
      STEP1.cpf = get(payload, 'cpf') ? get(payload, 'cpf') : state.step1.cpf
      STEP1.cpfMasked = get(payload, 'cpfMasked') ? get(payload, 'cpfMasked') : state.step1.cpfMasked
      STEP1.voluntarioUI = get(payload, 'voluntarioUI') ? get(payload, 'voluntarioUI') : state.step1.voluntarioUI
      STEP1.nome = state.step1.nome ||
            (STEP1.voluntarioUI.nomeCompleto ? STEP1.voluntarioUI.nomeCompleto.split(' ', 1)[0] : '')
      return { ...state, step1: STEP1 }
    }
    case types.REGISTER_FORM_UPDATE_STEP2: {
      return { ...state, step2: { ...state.step2, ...payload } }
    }
    case types.REGISTER_FORM_CHANGE_STEP3: {
      return { ...state, step3: { ...state.step3, ...payload } }
    }
    case types.REGISTER_FORM_CLEAR_NAME: {
      return { ...state,
        step1: { ...state.step1,
          ...{ nome: '',
            cargoNome: '',
            centroResultadoNome: '',
            urlAvatar: '',
            cpf: null,
            voluntarioIncompleto: false,
            usuarioSharePoint: false,
          },
        },
      }
    }
    case types.VOLUNTEERS_VALIDATE_CPF: {
      const STEP1 = state.step1
      STEP1.nomeCompleto = state.step1.nomeCompleto || payload.nomeCompleto
      STEP1.cargoId = payload.cargoId
      STEP1.cargoNome = payload.cargoNome
      STEP1.centroResultadoNome = payload.centroResultadoNome
      STEP1.centroResultadoId = payload.centroResultadoId
      STEP1.associadoExecutivo = payload.associadoExecutivo
      STEP1.nome = state.step1.nome || payload.nomeCompleto.split(' ')[0]
      STEP1.cpf = payload.cpf
      STEP1.cpfMasked = payload.cpfMasked
      return { ...state, voluntarioUI: payload, step1: STEP1 }
    }
    case types.VOLUNTEERS_ERROR_CPF: {
      return { ...state, errorCpf: payload }
    }
    case types.REGISTER_FORM_UPDATE_STEP: {
      if ((state.step + 1) < payload) {
        return { ...state, errorMsg: `É preciso passar pelo passo ${(state.step + 1)}, para completar o cadastro.` }
      }
      return { ...state, step: payload }
    }
    default:
      return state
  }
}

export default registerForm
