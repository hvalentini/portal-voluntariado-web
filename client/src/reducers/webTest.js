import * as types from '../actions/types'

const initialState = true

const webTest = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.WEB_TEST: {
      return payload
    }
    default:
      return state
  }
}

export default webTest
