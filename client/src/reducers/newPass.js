import * as types from '../actions/types'

const initialState = {
  data: {
    email: '',
    senhaAtual: '',
    senhaNova: '',
    senhaNovaConfirma: '',
  },
  alterouSenha: false,
  isDialogOpen: false,
  loader: false,
  message: '',
  error: '',
}

const newPass = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.NEW_PASS_UPDATE: {
      return { ...state, ...payload }
    }
    case types.NEW_PASS_UPDATE_DATA: {
      const obj = { ...state.data, ...payload }
      return { ...state, data: { ...obj } }
    }
    default:
      return state
  }
}

export default newPass
