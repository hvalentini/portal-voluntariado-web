import noAccents from 'remove-accents'
import * as types from '../actions/types'

const initialState = {
  nome: '',
  loader: false,
  modalidade: 'null',
  informarQtdeCartas: 'null',
  nomeNormalizado: '',
  orderSelected: '{ "nomeNormalizado" : 1 }',
}

const filtersActionTypes = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.FILTERS_ACTION_TYPES_EDIT: {
      return { ...state, ...payload }
    }
    case types.FILTERS_ACTION_TYPES_EDIT_NAME: {
      const nomeNormalizado = payload
                            ? noAccents(payload).toLowerCase()
                            : ''
      return { ...state, nome: payload, nomeNormalizado }
    }
    case types.FILTERS_ACTION_TYPES_ORDER: {
      return { ...state, orderSelected: payload }
    }
    case types.FILTERS_ACTION_TYPES_CLEAR: {
      return initialState
    }
    default:
      return state
  }
}

export default filtersActionTypes
