// COMPANY_FETCH

import * as types from '../actions/types'

const initialState = {
  nome: '',
}

const empresas = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.COMPANY_EDIT: {
      return { ...state, ...payload }
    }
    case types.COMPANY_SEARCH: {
      return { ...state, ...payload }
    }
    case types.COMPANY_CLEAR: {
      return { ...state, nome: '' }
    }
    default:
      return state
  }
}

export default empresas
