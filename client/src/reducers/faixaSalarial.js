import * as types from '../actions/types'

const initialState = {
  salaryRange: [],
}

const faixaSalarial = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.SALARY_FETCH_SALARY_RANGE: {
      return { ...state, salaryRange: payload }
    }
    default:
      return state
  }
}

export default faixaSalarial
