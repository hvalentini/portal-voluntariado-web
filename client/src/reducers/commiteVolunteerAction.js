import * as types from '../actions/types'

const initialState = {
  actions: [],
}

const commiteVolunteerAction = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.COMMITEVOLUNTEERACTION_UPDATE: {
      return { ...state, actions: [...payload] }
    }
    default:
      return state
  }
}

export default commiteVolunteerAction
