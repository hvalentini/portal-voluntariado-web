import * as types from '../actions/types'

const initialState = {
  list: [],
  selected: null,
}

const citySelect = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.CITY_SELECT_UPDATE: {
      return { ...state, ...payload }
    }
    default:
      return state
  }
}

export default citySelect
