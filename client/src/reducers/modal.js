import * as types from '../actions/types'

const initialState = {
  tipo: null,
  entidade: null,
  msg: null,
  openDelete: false,
  openWarningDelete: false,
  openWarning: false,
  openSuccess: false,
  openSuccessOne: false,
  action: null,
  rotaPagina: null,
  deleteSuccess: false,
  typeAction: null,
  id: null,
}

const modal = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.MODAL_SET: {
      const obj = payload
      obj.openDelete = payload.tipo === 'delete'
      obj.openWarning = payload.tipo === 'warning'
      obj.openSuccess = payload.tipo === 'success'
      obj.openSuccessOne = payload.tipo === 'successOne'
      obj.openWarningDelete = payload.tipo === 'warningDelete'
      return { ...state, ...obj }
    }
    case types.MODAL_CLOSE: {
      const deleteSuccess = payload ? payload.deleteSuccess : false
      return {
        ...state,
        openSuccess: false,
        openWarning: false,
        openDelete: false,
        openWarningDelete: false,
        openSuccessOne: false,
        deleteSuccess }
    }

    default:
      return state
  }
}

export default modal
