import * as types from '../actions/types'

const initialState = {}

const comites = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.COMMITTEE_UPDATE:
    case types.COMMITTEE_UPDATE_SALDO_ATUAL: {
      return { ...state, ...payload }
    }
    case types.COMMITTEE_CLEAR: {
      return { unidade: '', verbaInicial: '', verbaExtra: '', beneficiadas: 0, liderSocial: null }
    }
    case types.COMMITTEE_SEARCH: {
      let committee = payload
      let value = committee.verbaInicial.toString()
      if(value.length === 1) {
        committee.verbaInicial = value.replace(/(\d)/,"$1.00");
      }

      if(committee.verbaExtra) {
        let valueExtra = committee.verbaExtra.toString()
        if(valueExtra.length === 1) {
          committee.verbaExtra = valueExtra.replace(/(\d)/,"$1.00");
        }
      }
      return { ...state, ...committee }
    }
    /*
     * Caso especifico de uma action realizar um dispatch
     * para realizar a mesma açao em reducer diferente
     */
    case types.USERSAUTOCOMPLETE_UPDATE_SELECTED: {
      const liderSocial = {
        cargo: payload.cargoNome,
        email: payload.email,
        nome: payload.nome,
        urlAvatar: payload.urlAvatar,
        voluntarioId: payload._id,
      }
      return { ...state, liderSocial}
    }
    case types.COMMITTEE_CITY: {
      return { ...state, cidade: payload._id, cidadeNome: payload.nome, cidadeUF: payload.uf }
    }
    default:
      return state
  }
}

export default comites
