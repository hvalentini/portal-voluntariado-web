import * as types from '../actions/types'

const initialState = {
  user: {},
  volunteer: {},
  error: null,
  success: null,
  loader: false,
  isDialogOpen: false,
  message: '',
}

const login = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.LOGIN_UPDATE: {
      return { ...state, ...payload }
    }
    case types.LOGIN_UPDATE_USER: {
      return { ...state, user: { ...state.user, ...payload } }
    }
    default:
      return state
  }
}

export default login
