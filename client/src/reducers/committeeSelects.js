import * as types from '../actions/types'

const initialState = {
  list: [],
  byCompany: [],
  byCity: [],
  byUnit: [],
  selectedCompany: {},
  selectedCity: {},
  selectedUnit: {},
}

const committeeSelects = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case types.COMMITTEE_SELECTS_UPDATE: {
      return { ...state, ...payload }
    }
    case types.COMMITTEE_SELECTS_CLEAR: {
      return { ...state, selectedCompany: {}, selectedCity: {}, byCity: [] }
    }
    default:
      return state
  }
}

export default committeeSelects
