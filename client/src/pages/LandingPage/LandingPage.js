import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Modal from 'react-modal'
import styles from './LandingPage.css'
import Header from '../../components/Header/Header'
import LandingCTA from '../../components/LandingCTA/LandingCTA'
import LandingInfo from '../../components/LandingInfo/LandingInfo.react'
import SectionVideoAboutUs from '../../components/SectionVideoAboutUs/SectionVideoAboutUs.react'
import LandingFooter from '../../components/LandingFooter/LandingFooter.react'
import SvgIcon from '../../components/SvgIcon/SvgIcon.react'
import { getSessionVolunteer } from '../../actions/registerForm'

const modalStyle = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
    zIndex: 99,
  },
  content: {
    position: 'absolute',
    top: 0,
    left: '50%',
    transform: 'translateX(-50%)',
    bottom: 0,
    height: '100%',
    background: '#01BEE8',
    overflow: 'auto',
    boxSizing: 'border-box',
    outline: 'none',
    border: 0,
    borderRadius: 0,
    boxShadow: '0px 0px 15px 5px #C3C3C3',
    padding: '30px 30px 0',
    display: 'flex',
    flexDirection: 'column',
  },
}


class _LandingPage extends React.Component {

  componentWillMount() {
    this.props.getSessionVolunteer()
  }

  render() {
    modalStyle.content.width = (this.props.children && this.props.children.type.displayName === 'Connect(WrapperLoginPass)')
      ? '500px'
      : '820px'
    return (
      <div>
        <Modal
          isOpen={(this.props.children != null)}
          style={modalStyle}
          contentLabel=""
        >
          <Link
            to="/"
            className={styles.buttonClosed}
          >
            <SvgIcon
              icon="icon-icon-11"
            />
          </Link>
          {this.props.children}
        </Modal>
        <div className={styles.landing} style={{ position: this.props.children ? 'fixed' : 'absolute' }}>
          <section className={styles.holderTop}>
            <Header />
            <LandingCTA
              userName={this.props.step1.nome}
              userAvatar={this.props.step1.urlAvatar}
            />
          </section>
          <section className={styles.holderContent}>
            <LandingInfo />
            <SectionVideoAboutUs
              url="/videos/VIDEO_VOLUNTARIADO.mp4"
              cover="/images/IMG_VIDEO.png"
            />
            <LandingFooter />
          </section>
        </div>
      </div>
    )
  }
}

_LandingPage.propTypes = {
  children: PropTypes.node,
  step1: PropTypes.object,
  getSessionVolunteer: PropTypes.func,
}

const mapStateToProps = state => ({
  step1: state.registerForm.step1,
})

const mapActionsToProps = {
  getSessionVolunteer,
}

const LandingPage = connect(mapStateToProps, mapActionsToProps)(_LandingPage)

export default LandingPage
