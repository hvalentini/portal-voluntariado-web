import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import { loadPartnerInstitutions } from '../../actions/partners'
import { updatePartnersFilterOrderBy, filterPartners, clearFilter } from '../../actions/filters'
import BoxInstitutions from '../../components/BoxInstitutions/BoxInstitutions'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import FilterInstitutions from '../../components/FilterInstitutions/FilterInstitutions'
import styles from './InstitutionsPage.css'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'

const order = [
  {
    name: 'Instituições (A-Z)',
    value: '{ "nomeNormalizado" : 1 }',
  },
  {
    name: 'Instituições (Z-A)',
    value: '{ "nomeNormalizado": -1 }',
  },
]

class _InstitutionsPage extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  static onPrint() {
    window.print()
  }

  constructor(props) {
    super(props)
    this.state = {
      value: 0,
    }
    this.onChangeOrderBy = this.onChangeOrderBy.bind(this)
  }

  componentWillMount() {
    this.props.clearFilter()
    this.props.loadPartnerInstitutions({}, { nomeNormalizado: 1 })
  }

  componentDidMount() {
    if (this.props.match.path === '/lider/instituicoes-parceiras') {
      _InstitutionsPage.scrollTo()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.deleteSuccess) {
      this.props.loadPartnerInstitutions({}, { nomeNormalizado: 1 })
    }
  }

  onChangeOrderBy(event, index, value) {
    this.setState({ value }) // State para alternar lista do Material UI
    const obj = {}
    const orderBy = (value.indexOf(-1) > 0) ? { nomeNormalizado: -1 } : { nomeNormalizado: 1 }
    obj.orderBy = orderBy
    this.props.updatePartnersFilterOrderBy(obj)
    this.props.filterPartners()
  }


  handleInstitutions() {
    if (this.props.loading) {
      return (
        <LoaderAdmin />
      )
    }

    if (!this.props.partners.list.length) {
      return (
        <div>
          <p className={styles.notFound}>Nenhuma instituição encontrada</p>
        </div>
      )
    }
    return <BoxInstitutions user={this.props.user} />
  }

  render() {
    const linkAddInstitution = this.props.user.adminLogin
                  ? '/admin/instituicoes-parceiras/adicionar'
                  : '/lider/nova-instituicao'

    return (
      <div className={styles.root}>
        <FilterAdmin>
          <form style={{ height: '100%' }}>
            <FilterInstitutions />
          </form>
        </FilterAdmin>
        <FilterResultAdmin>
          <HeaderInternalAdmin
            title="Instituições" // Titulo da pagina
            // ROTA PARA o botao de add.Caso nao tenha deixar null
            to={linkAddInstitution}
            order={order} // Array com opçoes de ordenação
            orderSelected={this.props.partners.orderSelected} // Valor selecionado
            onChangeOrder={this.onChangeOrderBy} // Funcão ao selecionar um novo valor
            print={this.props.partners.list.length > 0}
            onPrint={_InstitutionsPage.onPrint}
          />
          {this.handleInstitutions()}
        </FilterResultAdmin>
      </div>
    )
  }
}

_InstitutionsPage.propTypes = {
  partners: PropTypes.object,
  filters: PropTypes.object,
  loadPartnerInstitutions: PropTypes.func,
  updatePartnersFilterOrderBy: PropTypes.func,
  filterPartners: PropTypes.func,
  clearFilter: PropTypes.func,
  loading: PropTypes.bool,
  match: PropTypes.object,
  user: PropTypes.object,
}

const mapStateToProps = state => ({
  partners: state.partners,
  filters: state.filters,
  loading: state.ui.loading,
  user: state.loggedUser,
})

const mapActionsToProps = {
  loadPartnerInstitutions,
  updatePartnersFilterOrderBy,
  filterPartners,
  clearFilter,
}

const InstitutionsPage = connect(mapStateToProps, mapActionsToProps)(_InstitutionsPage)
export default withRouter(InstitutionsPage)
