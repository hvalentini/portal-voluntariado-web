import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './CitiesPage.css'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import BoxCity from '../../components/BoxCity/BoxCity'
import FilterCity from '../../components/FilterCity/FilterCity'
import { getCities } from '../../actions/cidades'
import { getFilterCity, clearFilter } from '../../actions/filtersCity'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'

const order = [
  {
    name: 'Cidades (A-Z)',
    value: '{ "nomeNormalizado" : 1 }',
  },
  {
    name: 'Cidades (Z-A)',
    value: '{ "nomeNormalizado": -1 }',
  },
  {
    name: 'UF (A-Z)',
    value: '{ "uf" : 1, "nomeNormalizado" : 1 }',
  },
  {
    name: 'UF (Z-A)',
    value: '{ "uf": -1, "nomeNormalizado" : 1 }',
  },
]

class _CitiesPage extends React.Component {

  constructor(props) {
    super(props)
    this.changeOrder = this.changeOrder.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  componentWillMount() {
    this.props.clearFilter()
    this.props.getCities()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.deleteSuccess) {
      this.props.getCities()
    }
  }

  onPrint() {
    window.print()
  }

  handleCities() {
    if (this.props.loading) {
      return (
        <LoaderAdmin />
      )
    }
    if (!this.props.cidades.length && !this.props.loading) {
      return (
        <div>
          <p className={styles.notFound}>Nenhuma cidade encontrada</p>
        </div>
      )
    }
    return <BoxCity />
  }

  changeOrder(event, index, value) {
    const filtro = this.props.filter.uf || this.props.filter.nomeNormalizado
    ? { nomeNormalizado: this.props.filter.nomeNormalizado, uf: this.props.filter.uf }
    : { uf: this.props.filter.uf }

    let payload = { ...value }

    if (value) {
      if (value === '{ "nomeNormalizado" : 1 }') {
        payload = { nomeNormalizado: 1 }
      }
      if (value === '{ "nomeNormalizado": -1 }') {
        payload = { nomeNormalizado: -1 }
      }
      if (value === '{ "uf" : 1, "nomeNormalizado" : 1 }') {
        payload = { uf: 1, nomeNormalizado: 1 }
      }
      if (value === '{ "uf": -1, "nomeNormalizado" : 1 }') {
        payload = { uf: -1, nomeNormalizado: 1 }
      }
    }

    this.props.getFilterCity({ filtro, ordenacao: payload })
  }

  render() {
    return (
      <div className={`${styles.root} page-break`}>
        <ModalCrud />
        <FilterAdmin>
          <FilterCity />
        </FilterAdmin>
        <FilterResultAdmin>
          <HeaderInternalAdmin
            title="Cidades" // Titulo da pagina
            to="/admin/cidades/adicionar" // ROTA PARA o botao de add.Caso nao tenha deixar null
            order={order} // Array com opçoes de ordenação
            orderSelected={this.props.filter.orderSelected} // Valor selecionado
            onChangeOrder={this.changeOrder} // Funcão ao selecionar um novo valor
            print={this.props.cidades.length > 0}
            onPrint={this.onPrint}
          />
          {this.handleCities()}
        </FilterResultAdmin>
      </div>
    )
  }
}

_CitiesPage.propTypes = {
  cidades: PropTypes.array,
  clearFilter: PropTypes.func,
  filter: PropTypes.object,
  getFilterCity: PropTypes.func,
  loading: PropTypes.bool,
  getCities: PropTypes.func,
}

const mapStateToProps = state => ({
  cidades: state.list.cidades,
  loading: state.ui.loading,
  deleteSuccess: state.modal.deleteSuccess,
  filter: state.filtersCity,
})

const mapActionsToProps = {
  getCities,
  getFilterCity,
  clearFilter,
}

const CitiesPage = connect(mapStateToProps, mapActionsToProps)(_CitiesPage)
export default CitiesPage
