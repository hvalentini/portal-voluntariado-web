import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import styles from './EditCommitteePage.css'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import TabsCommittee from '../../components/TabsCommittee/TabsCommittee'
import { clearCommittee, searchCommittee } from '../../actions/comites'
import { clearCommitteeSelected } from '../../actions/committeeSelects'
import FormCommittee from '../../components/FormCommittee/FormCommittee'
import ModalCrud from '../../components/ModalCrud/ModalCrud'

class _EditCommitteePage extends React.Component {

  componentWillUnmount() {
    this.props.clearCommitteeSelected()
    this.props.clearCommittee()
  }

  handleTabs() {
    if (this.props.match.params.id) {
      return (
        <div className={styles.containerTabsCommittee}>
          <TabsCommittee {...this.props} />
        </div>
      )
    }
    return (
      <div className={styles.divEdit}>
        <FormCommittee {...this.props} />
      </div>
    )
  }

  render() {
    const title = this.props.match.params.id ? 'Alterar Comitê' : 'Cadastro de comitês'
    return (
      <div>
        <ContentAdmin fullWidth title={title} useTheme={false}>
          <ModalCrud />
          {this.handleTabs()}
        </ContentAdmin>
      </div>
    )
  }
}

_EditCommitteePage.propTypes = {
  match: PropTypes.object,
  clearCommittee: PropTypes.func,
  clearCommitteeSelected: PropTypes.func,
}

const mapStateToProps = state => ({
  comite: state.comites,
  comiteSelected: state.committeeSelects,
  loading: state.ui.loading,
  loadingForm: state.ui.loadingForm,
})

const mapActionsToProps = {
  searchCommittee,
  clearCommitteeSelected,
  clearCommittee,
}

const EditCommitteePage = connect(mapStateToProps, mapActionsToProps)(_EditCommitteePage)
export default withRouter(EditCommitteePage)
