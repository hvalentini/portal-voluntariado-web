import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  BrowserRouter as Router,
} from 'react-router-dom'
import Footer from '../../components/Footer/Footer'
import styles from './LandingAdmin.css'
import HeaderAdmin from '../../components/HeaderAdmin/HeaderAdmin'
import { getLoggedUser } from '../../actions/loggedUser'
import Breadcrumb from '../../components/Breadcrumb/Breadcrumb'
import RouteWithSubRoutes from '../../components/RouteWithSubRoutes/RouteWithSubRoutes'
import routes from '../../adminRoutes'

class _LandingLeader extends React.Component {

  componentDidMount() {
    this.props.getLoggedUser()
  }

  render() {
    if (!this.props.loggedUser._id) {
      return null
    }

    return (
      <Router>
        <div>
          <div className={`${styles.blocks} noPrint`}>
            <HeaderAdmin />
            {/* VERIFICAR SE É A PAGINA INICIAL PARA RETIRAR  */}
            <Breadcrumb routes={routes} />
          </div>
          <RouteWithSubRoutes routes={routes} />
          <div style={{ paddingTop: '160px' }} className={`noPrint ${styles.blocks}`}>
            <Footer />
          </div>
        </div>
      </Router>
    )
  }
}

_LandingLeader.propTypes = {
  loggedUser: PropTypes.object,
  getLoggedUser: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  getLoggedUser,
}

const LandingLeader = connect(mapStateToProps, mapActionsToProps)(_LandingLeader)

export default LandingLeader
