import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { TextField } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styles from './ChangePassAdminPage.css'
import muiTheme from '../../muiTheme'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import BackButton from '../../components/BackButton/BackButton'
import PillButton from '../../components/PillButton/PillButton'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import { updateNewPass, updateNewPassData, sendChangePassAdmin } from '../../actions/newPass'

class _Component extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      msgErro: {
        senhaAtual: '',
        senhaNova: '',
        senhaNovaConfirma: '',
      },
    }
    this.handleChangePass = this.handleChangePass.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillMount() {
    this.props.updateNewPass({ data: { email: this.props.loggedUser.adminLogin || 'admin' } })
  }

  handleChangePass(key, value) {
    const obj = {}
    obj[key] = value
    this.props.updateNewPassData(obj)
  }

  handleBlur(e) {
    const newStateMsgErro = this.state.msgErro
    newStateMsgErro[e.target.name] = this.state.msgErro[e.target.name] && e.target.value ? '' : this.state.msgErro[e.target.name]
    this.setState({ ...this.state.msgErro, ...newStateMsgErro })
  }

  checkPassMatch(e) {
    e.preventDefault()
    const msgErro = {}
    if (!this.props.newPass.data.senhaAtual) {
      msgErro.senhaAtual = 'Insira a sua senha atual.'
    }

    if (!this.props.newPass.data.senhaNova) {
      msgErro.senhaNova = 'Insira a sua nova senha.'
    }
    if (this.props.newPass.data.senhaNova && this.props.newPass.data.senhaNova
      && this.props.newPass.data.senhaNova.length < 6) {
      msgErro.senhaNova = 'Insira pelo menos 6 dígitos'
    }

    if (!this.props.newPass.data.senhaNovaConfirma) {
      msgErro.senhaNovaConfirma = 'Insira a confirmação da nova senha.'
    }
    if (this.props.newPass.data.senhaNova && this.props.newPass.data.senhaNovaConfirma
      && this.props.newPass.data.senhaNovaConfirma.length < 6) {
      msgErro.senhaNovaConfirma = 'Insira pelo menos 6 dígitos'
    }

    if (this.props.newPass.data.senhaNovaConfirma !== this.props.newPass.data.senhaNova) {
      msgErro.senhaNova = 'As senhas não são iguais'
      msgErro.senhaNovaConfirma = 'As senhas não são iguais'
    }

    if (Object.keys(msgErro).length > 0) {
      return this.setState({ msgErro })
    }

    msgErro.senhaAtual = ''
    msgErro.senhaNova = ''
    msgErro.senhaNovaConfirma = ''

    this.setState({ msgErro })

    return this.props
      .sendChangePassAdmin(this.props.newPass.data)
  }

  render() {
    const title = 'Alterar senha'
    const textBtn = 'ALTERAR'
    return (
      <ContentAdmin title={title} useTheme={false}>
        <ModalCrud />

        <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
          <form className={styles.box}>
            <div className={['Grid-cell', 'u-textLeft', 'u-inlineBlock', styles.fields].join(' ')}>
              <TextField
                floatingLabelText="Senha atual"
                name="senhaAtual"
                errorText={this.state.msgErro.senhaAtual}
                onChange={e => this.handleChangePass(e.target.name, e.target.value)}
                onBlur={this.handleBlur}
                maxLength={100}
                type="password"
                fullWidth
              />
              <TextField
                floatingLabelText="Nova senha"
                name="senhaNova"
                errorText={this.state.msgErro.senhaNova}
                onChange={e => this.handleChangePass(e.target.name, e.target.value)}
                onBlur={this.handleBlur}
                maxLength={100}
                type="password"
                fullWidth
              />
              <TextField
                floatingLabelText="Confirmar nova senha"
                name="senhaNovaConfirma"
                onChange={e => this.handleChangePass(e.target.name, e.target.value)}
                onBlur={this.handleBlur}
                errorText={this.state.msgErro.senhaNovaConfirma}
                maxLength={100}
                type="password"
                fullWidth
              />
            </div>
            <div className={styles.divBtn}>
              <div className="Grid-cell u-size3of12 u-inlineBlock">
                <BackButton history={this.props.history} />
              </div>
              <div className="Grid-cell u-size6of12 u-textCenter u-inlineBlock">
                <PillButton
                  text={textBtn}
                  type="submit"
                  loader={this.props.newPass.loader}
                  style={{ float: 'none', margin: 'auto' }}
                  onClick={e => this.checkPassMatch(e)}
                />
              </div>
              <div className="Grid-cell u-size3of12 u-inlineBlock" />
            </div>
          </form>
        </MuiThemeProvider>
      </ContentAdmin>
    )
  }
}

_Component.displayName = 'ChangePassAdminPage'

_Component.propTypes = {
  loggedUser: PropTypes.object,
  newPass: PropTypes.object,
  updateNewPassData: PropTypes.func,
  updateNewPass: PropTypes.func,
  sendChangePassAdmin: PropTypes.func,
  history: PropTypes.object,
}

const mapStateToProps = state => ({
  cidade: state.cidades,
  deleteSuccess: state.ui.deleteSuccess,
  newPass: state.newPass,
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  updateNewPass,
  updateNewPassData,
  sendChangePassAdmin,
}

const Component = connect(mapStateToProps, mapActionsToProps)(_Component)
export default withRouter(Component)
