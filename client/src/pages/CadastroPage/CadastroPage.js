import React from 'react'
import styles from './CadastroPage.css'

const CadastroPage = () => (
  <div>
    <h1 className={styles.title}>Cadastro Page :D</h1>
    <p>Formulário de cadastro</p>
  </div>
)

export default CadastroPage
