import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { RadioButton, RadioButtonGroup, MuiThemeProvider } from 'material-ui'
import { filter } from 'lodash'
import styles from './NotificationsAdminPage.css'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import NotificationsAdmin from '../../components/NotificationsAdmin/NotificationsAdmin'
import { updateNotificationEntity } from '../../actions/notificacao'
import muiTheme from '../../muiTheme'

const style = {}

class _Component extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      notificacoes: [],
      tipo: '',
    }
    this.onChangeTipo = this.onChangeTipo.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.notificacao.notificacoes.length !== this.props.notificacao.notificacoes.length) {
      const notificacoes = filter(nextProps.notificacao.notificacoes, { tipo: this.state.tipo })
      this.setState({ notificacoes })
    }
  }

  onChangeTipo(e) {
    if (e.target.value === 'Mensagem Admin') {
      const notificacoes = filter(this.props.notificacao.notificacoes, { tipo: 'Mensagem Admin' })
      this.setState({ notificacoes, tipo: 'Mensagem Admin' })
    } else {
      const notificacoes = filter(this.props.notificacao.notificacoes, { tipo: 'Mensagem Admin Somente Líderes' })
      this.setState({ notificacoes, tipo: 'Mensagem Admin Somente Líderes' })
    }

    this.props.updateNotificationEntity({ tipo: e.target.value.trim() })
  }

  render() {
    const contacts = [
      { value: 'Mensagem Admin', label: 'Todos' },
      { value: 'Mensagem Admin Somente Líderes', label: 'Líderes' }]

    return (
      <div className={styles.root}>
        <div className={styles.leftBox}>
          <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
            <RadioButtonGroup
              name="shirtSize"
              defaultSelected={this.props.notificacao.entidade.tipo || ''}
              onChange={this.onChangeTipo}
            >
              {
                  contacts.map((item, i) => (
                    <RadioButton
                      value={item.value}
                      label={item.label}
                      style={style.radioButton}
                      labelStyle={style.radioLabel}
                      key={i}
                    />
                  ))
                }
            </RadioButtonGroup>
          </MuiThemeProvider>
        </div>
        <div className={styles.rightBox}>
          <NotificationsAdmin notificacoes={this.state.notificacoes} />
        </div>
        <ModalCrud />
      </div>
    )
  }
}

style.radioButton = {
  marginBottom: '10px',
}

style.radioLabel = {
  color: '#707070',
}

_Component.displayName = 'NotificationsAdminPage'

_Component.propTypes = {
  notificacao: PropTypes.object,
  updateNotificationEntity: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
  notificacao: state.notificacao,
})

const mapActionsToProps = {
  updateNotificationEntity,
}

const Component = connect(mapStateToProps, mapActionsToProps)(_Component)
export default withRouter(Component)
