import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import VolunteerDashboard from '../../components/VolunteerDashboard/VolunteerDashboard'
import LeaderDashboard from '../../components/LeaderDashboard/LeaderDashboard'
import Footer from '../../components/Footer/Footer'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import NewPassword from '../../components/NewPassword/NewPassword.react'
import styles from './PageNewPassword.css'

import { getLoggedUser } from '../../actions/loggedUser'

class _PageNewPassword extends React.Component {
  componentWillMount() {
    if (!this.props.loggedUser._id) {
      this.props.getLoggedUser()
    }
  }

  renderDashboard() {
    if (this.props.loggedUser && this.props.loggedUser.comite.liderSocial && this.props.loggedUser._id === this.props.loggedUser.comite.liderSocial.voluntarioId) {
      return (<LeaderDashboard />)
    }
    return (<VolunteerDashboard />)
  }

  render() {
    if (!this.props.loggedUser._id) {
      return null
    }
    return (
      <div className="Grid">
        <div className={styles.blocks}>
          {this.renderDashboard()}
        </div>
        <div className={styles.root}>
          <ModalCrud />
          <NewPassword />
        </div>
        <div style={{ paddingTop: '160px' }} className={styles.blocks}>
          <Footer />
        </div>
      </div>
    )
  }
}

_PageNewPassword.propTypes = {
  loggedUser: PropTypes.object,
  getLoggedUser: PropTypes.func,
}

const mapStateToProps = state => ({
  loggedUser: state.loggedUser,
})

const mapActionsToProps = {
  getLoggedUser,
}

const PageNewPassword = connect(mapStateToProps, mapActionsToProps)(_PageNewPassword)

export default PageNewPassword
