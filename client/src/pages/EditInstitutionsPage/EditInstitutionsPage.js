import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TextField, Checkbox } from 'material-ui'
import { withRouter } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styles from './EditInstitutionsPage.css'
// import muiTheme from '../../components/MuiThemeUtils/MuiThemeEditPage'
import muiTheme from '../../muiTheme'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import BackButton from '../../components/BackButton/BackButton'
import PillButton from '../../components/PillButton/PillButton'
import {
  getOneLocal,
  handleChange,
  editPhone,
  addLocal,
  deleteInstitution,
  clearState,
} from '../../actions/newLocal'
import { setModal } from '../../actions/modal'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import dddList from '../../utils/ddd.json'

const style = {}

class _EditInstitutionsPage extends React.Component {

  static checkDDD(tel) {
    const ddd = tel.substring(0, 2)
    let found = true
    for (let i = 0, j = dddList.length - 1; i < j; i += 1, j -= 1) {
      if (ddd === dddList[i] || ddd === dddList[j]) {
        found = false
      }
    }

    return found
  }

  static checkEmail(email) {
    if (email.indexOf('@') === -1) {
      return true
    }
    if (!email.split('@')[1].match(/((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      return true
    }

    return false
  }

  constructor(props) {
    super(props)
    this.state = {
      msgErro: {
        nome: '',
        telefone: '',
        responsavelEmail: '',
        endereco: '',
        responsavelNome: '',
        errorTextEmail2: '',
        qtdeSalas: '',
        qtdeAlunos: '',
        serie: '',
      },
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeCheckbox = this.handleChangeCheckbox.bind(this)
    this.handleDeleteInstitution = this.handleDeleteInstitution.bind(this)
    this.handleChangeNumber = this.handleChangeNumber.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillMount() {
    this.props.clearState()
    if (this.props.match.params.id && !this.props.inModal) {
      this.props.getOneLocal(this.props.match.params.id)
    }
  }

  handleChange(e) {
    const obj = {}
    obj[e.target.name] = e.target.value.replace(/^\s+/gm, '')

    this.props.handleChange(obj)
  }

  handleBlur(e) {
    const newStateMsgErro = this.state.msgErro
    newStateMsgErro[e.target.name] = this.state.msgErro[e.target.name] && e.target.value ? '' : this.state.msgErro[e.target.name]
    this.setState({ ...this.state.msgErro, ...newStateMsgErro })
  }

  handleChangeNumber(e) {
    const onlyNumbers = Array.isArray(e.target.value.match(/\d+/g))
                      ? e.target.value.match(/\d+/g).join('')
                      : ''
    const obj = {}
    obj[e.target.name] = onlyNumbers
    this.props.handleChange(obj)
  }

  handleChangeCheckbox(e) {
    const obj = {}
    obj[e.target.name] = !this.props.newLocal.local.escola
    if (!obj[e.target.name]) {
      obj.responsavelNome2 = ''
      obj.responsavelEmail2 = ''
      obj.qtdeSalas = ''
      obj.qtdeAlunos = ''
      obj.serie = ''
    }
    this.props.handleChange(obj)
  }

  handleChangePhone(e) {
    let phone = e.target.value.replace(/\D/g, '').substr(0, 11)

    if (phone.length <= 10) {
      phone = phone.replace(/(\d{0})(\d)/, '$1($2')
                    .replace(/(\d{2})(\d)/, '$1) $2')
                    .replace(/(\d{4})(\d)/, '$1-$2')
    } else {
      phone = phone.replace(/(\d{0})(\d)/, '$1($2')
                    .replace(/(\d{2})(\d)/, '$1) $2')
                    .replace(/(\d{5})(\d)/, '$1-$2')
    }

    this.props.editPhone({ telefone: phone })
  }

  addLocal(e) {
    e.preventDefault()
    const msgErro = {}
    const local = this.props.newLocal.local
    if (local.nome.replace(/(^\s+|\s+$)/g, '') === '') {
      msgErro.nome = 'Nome do local/Instituição é obrigatório'
    }
    if (local.endereco.replace(/(^\s+|\s+$)/g, '') === '') {
      msgErro.endereco = 'Informe o endereço do local'
    }
    if (_EditInstitutionsPage.checkEmail(local.responsavelEmail)) {
      msgErro.responsavelEmail = 'Por favor, preencha um email válido.'
    }
    if ((local.telefone.match(/\d/g) || []).length < 10) {
      msgErro.telefone = 'Por favor, preencha um telefone com ddd e no minimo 8 números.'
    } else if (_EditInstitutionsPage.checkDDD(local.telefone.match(/\d/g).join(''))) {
      msgErro.telefone = 'Por favor, digite um ddd válido.'
    }
    if (local.responsavelNome.replace(/(^\s+|\s+$)/g, '') === '') {
      msgErro.responsavelNome = 'Informe o nome do responsável pelo local'
    }
    if (local.escola) {
      if (local.responsavelEmail2 !== '' && _EditInstitutionsPage.checkEmail(local.responsavelEmail2)) {
        msgErro.errorTextEmail2 = 'Por favor, preencha um email válido.'
      }
      if (!local.qtdeSalas) {
        msgErro.qtdeSalas = 'Quantidade de salas é obrigatorio para locais do tipo Escola'
      }
      if (!local.qtdeAlunos) {
        msgErro.qtdeAlunos = 'Quantidade de alunos é obrigatorio para locais do tipo Escola'
      }
      if (!local.serie) {
        msgErro.serie = 'Série/Ano é obrigatorio para locais do tipo Escola'
      }
    }

    if (Object.keys(msgErro).length > 0) {
      return this.setState({ msgErro })
    }

    local.editadoPor = !this.props.loggedUser.adminLogin
                     ? this.props.loggedUser.nome : this.props.loggedUser.adminLogin
    local.criadoPor = !this.props.loggedUser.adminLogin
                    ? this.props.loggedUser.nome : this.props.loggedUser.adminLogin
    return this.props.addLocal(local, this.props.inModal)
  }

  handleDeleteInstitution() {
    this.props.deleteInstitution(this.props.match.params.id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina)
  }

  renderPartnerSchool() {
    if (!this.props.newLocal.local.escola) {
      return null
    }
    return (
      <div className="Grid-cell">
        <div className="Grid-cell">
          <TextField
            floatingLabelText="Nome do responsável 2"
            name="responsavelNome2"
            value={this.props.newLocal.local.responsavelNome2}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            maxLength={100}
            fullWidth
          />
        </div>
        <div className="Grid-cell">
          <TextField
            floatingLabelText="E-mail do responsável 2"
            name="responsavelEmail2"
            value={this.props.newLocal.local.responsavelEmail2}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            errorText={this.state.msgErro.errorTextEmail2}
            maxLength={100}
            fullWidth
          />
        </div>
        <div className="Grid-cell">
          <TextField
            floatingLabelText="Quantidade de salas"
            name="qtdeSalas"
            value={this.props.newLocal.local.qtdeSalas}
            onChange={this.handleChangeNumber}
            onBlur={this.handleBlur}
            errorText={this.state.msgErro.qtdeSalas}
            maxLength={100}
            fullWidth
          />
        </div>
        <div className="Grid-cell">
          <TextField
            floatingLabelText="Quantidade de alunos"
            name="qtdeAlunos"
            value={this.props.newLocal.local.qtdeAlunos}
            onChange={this.handleChangeNumber}
            onBlur={this.handleBlur}
            errorText={this.state.msgErro.qtdeAlunos}
            maxLength={100}
            fullWidth
          />
        </div>
        <div className="Grid-cell">
          <TextField
            floatingLabelText="Série/Ano"
            name="serie"
            value={this.props.newLocal.local.serie}
            errorText={this.state.msgErro.serie}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            maxLength={100}
            fullWidth
          />
        </div>
      </div>
    )
  }

  renderBtnDelete(nome) {
    const match = this.props.match
    if ((match.url.indexOf('/lider') >= 0 && match.path.indexOf('/lider') >= 0)
        || !match.params.id || this.props.inModal) {
      return null
    }

    const msg = `Você tem certeza que deseja excluir esta instituição ${nome}?`

    return (
      <div className={styles.delete}>
        <PillButton
          text="excluir"
          negativeColors
          onClick={() =>
            this.handleOpenModal(
              'delete', // TIPO DE AÇÃO - PODE SER DELETE, WARNING, OU SUCCESS
              'locais', // ENTIDADE RELACIONADA NO BANCO
              msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM NAO VENHA DO BANCO ENVIAR
              this.handleDeleteInstitution, // AÇÃO PRINCIPAL
              '/admin/instituicoes-parceiras', //ROTA DA PAGINA PRINCIPAL
            )
          }
        />
      </div>
    )
  }

  renderBtnRegister() {
    const match = this.props.match
    const textBtn = (match.params.id && !this.props.inModal) ? 'ALTERAR' : 'CADASTRAR'
    let ml = ''
    if ((match.url.indexOf('/admin') >= 0 && match.path.indexOf('/admin') >= 0)
        && match.params.id) {
      ml = styles.ml
    }

    return (
      <div className={`${styles.register} ${ml}`}>
        <PillButton
          text={textBtn}
          loader={this.props.loaderBtn}
          onClick={e => this.addLocal(e, this.props.inModal)}
        />
      </div>
    )
  }

  renderBtnBack() {
    if (this.props.inModal) { return null }

    return (
      <div className={styles.backButton}>
        <BackButton history={this.props.history} />
      </div>
    )
  }

  render() {
    let title = this.props.match.params.id ? 'Alterar instituição' : 'Cadastro de nova instituição'
    title = this.props.inModal ? '' : title
    const match = this.props.match
    let buttons = 'u-textRight'

    if (this.props.newLocal.loader && this.props.match.params.id) {
      return (
        <ContentAdmin title={title} useTheme={false}>
          <LoaderAdmin />
        </ContentAdmin>
      )
    }
    if ((match.url.indexOf('/lider') >= 0 && match.path.indexOf('/lider') >= 0)
        || !match.params.id) {
      buttons = 'u-textCenter'
    }

    const nome = this.props.newLocal.local.nome !== '' ? this.props.newLocal.local.nome : ''

    return (
      <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
        <ContentAdmin
          title={title}
          useTheme={false}
          titleClass={this.props.inModal ? styles.hideHeader : ''}
        >
          <ModalCrud />
          <div className="Grid">
            <div className="Grid-cell">
              <TextField
                floatingLabelText="*Nome do local/instituição"
                name="nome"
                className={styles.inputRequired}
                value={this.props.newLocal.local.nome}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                errorText={this.state.msgErro.nome}
                maxLength={100}
                fullWidth
              />
            </div>
            <div className="Grid-cell">
              <TextField
                floatingLabelText="*Endereço"
                name="endereco"
                className={styles.inputRequired}
                value={this.props.newLocal.local.endereco}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                errorText={this.state.msgErro.endereco}
                maxLength={100}
                fullWidth
              />
            </div>
            <div className="Grid-cell">
              <TextField
                floatingLabelText="*Telefone"
                name="telefone"
                className={styles.inputRequired}
                value={this.props.newLocal.local.telefone}
                errorText={this.state.msgErro.telefone}
                onChange={e => this.handleChangePhone(e)}
                onBlur={this.handleBlur}
                maxLength={15}
                fullWidth
              />
            </div>
            <div className="Grid-cell" style={style.checkbox}>
              <Checkbox
                label="É uma escola parceira?"
                checked={this.props.newLocal.local.escola}
                name="escola"
                onCheck={e => this.handleChangeCheckbox(e)}
                value={this.props.newLocal.local.escola}
              />
            </div>
            <div className="Grid-cell">
              <TextField
                floatingLabelText="*Nome do responsável"
                name="responsavelNome"
                className={styles.inputRequired}
                value={this.props.newLocal.local.responsavelNome}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                errorText={this.state.msgErro.responsavelNome}
                maxLength={100}
                fullWidth
              />
            </div>
            <div className="Grid-cell">
              <TextField
                floatingLabelText="*E-mail do responsável"
                name="responsavelEmail"
                value={this.props.newLocal.local.responsavelEmail}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                errorText={this.state.msgErro.responsavelEmail}
                maxLength={100}
                fullWidth
              />
            </div>
            {this.renderPartnerSchool()}
            <div className={`${styles.observacao} Grid-cell`}>
              <TextField
                floatingLabelText="Observações"
                name="observacao"
                value={this.props.newLocal.local.observacao}
                onChange={this.handleChange}
                multiLine
                rows={1}
                rowsMax={6}
                fullWidth
              />
            </div>
          </div>
          <div className={`${styles.divBtn} ${buttons}`}>
            {this.renderBtnBack()}
            {this.renderBtnDelete(nome)}
            {this.renderBtnRegister()}
          </div>
        </ContentAdmin>
      </MuiThemeProvider>
    )
  }
}

style.checkbox = {
  marginTop: '25px',
  marginBottom: '-10px',
}

_EditInstitutionsPage.propTypes = {
  getOneLocal: PropTypes.func,
  addLocal: PropTypes.func,
  handleChange: PropTypes.func,
  deleteInstitution: PropTypes.func,
  clearState: PropTypes.func,
  editPhone: PropTypes.func,
  newLocal: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object,
  loggedUser: PropTypes.object,
  setModal: PropTypes.func,
  loaderBtn: PropTypes.bool,
  inModal: PropTypes.bool,
}

const mapStateToProps = state => ({
  empresa: state.empresas,
  newLocal: state.newLocal,
  loggedUser: state.loggedUser,
  loaderBtn: state.ui.loaderBtn,
})

const mapActionsToProps = {
  getOneLocal,
  setModal,
  deleteInstitution,
  handleChange,
  editPhone,
  addLocal,
  clearState,
}

const EditInstitutionsPage = connect(mapStateToProps, mapActionsToProps)(_EditInstitutionsPage)
export default withRouter(EditInstitutionsPage)
