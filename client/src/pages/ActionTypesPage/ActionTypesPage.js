import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import BoxActionTypes from '../../components/BoxActionTypes/BoxActionTypes'
import FilterActionTypes from '../../components/FilterActionTypes/FilterActionTypes'
import { clearFilter, getFilterActionTypes } from '../../actions/filtersActionTypes'
import { getActions } from '../../actions/actionTypes'
import styles from './ActionTypesPage.css'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
let key = ''

const order = [
  {
    name: 'Tipos de Ação (A-Z)',
    value: '{ "nomeNormalizado" : 1 }',
  },
  {
    name: 'Tipos de Ação (Z-A)',
    value: '{ "nomeNormalizado": -1 }',
  },
]

class _ActionTypesPage extends React.Component {

  static onPrint() {
    window.print()
  }

  static renderHeader() {
    return (
      <div className={styles.itemHeader}>
        <div className={'Grid-cell u-size6of12'}>
          Tipos de Ação
        </div>
        <div className={'Grid-cell u-size2of12'}>
          Modalidade
        </div>
        <div className={`Grid-cell u-size2of12 ${styles.center}`}>
          Existe Carta?
        </div>
        <div className={'Grid-cell u-size2of12'} />
      </div>
    )
  }

  constructor(props) {
    super(props)

    this.state = {
      orderSelected: '{ "nomeNormalizado" : 1 }',
    }

    this.changeOrder = this.changeOrder.bind(this)
    this.renderActionTypes = this.renderActionTypes.bind(this)
  }

  componentWillMount() {
    this.props.clearFilter()
    this.props.getActions({ naoOrdernadarPersonalizado: true })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.deleteSuccess) {
      this.props.getActions()
    }
  }

  changeOrder(event, index, value) {
    const filtro = {
      nomeNormalizado: this.props.filter.nomeNormalizado,
    }

    if (this.props.filter.modalidade !== 'null') {
      filtro.tipo = parseInt(this.props.filter.modalidade, 10)
    }

    if (this.props.filter.informarQtdeCartas !== 'null') {
      filtro.informarQtdeCartas = this.props.filter.informarQtdeCartas === 'true'
    }

    this.props.getFilterActionTypes({ filtro, ordenacao: value, naoOrdernadarPersonalizado: true })
    this.setState({
      orderSelected: value,
    })
  }

  renderActionTypes() {
    let BoxActionTypesCard
    if (this.props.actionTypes.loader || this.props.ui.loading) {
      return (
        <LoaderAdmin />
      )
    }
    if (this.props.actionTypes.actions.length === 0) {
      BoxActionTypesCard = (
        <div className={styles.notFound}>
          Nenhum tipo de ação foi encontrada
        </div>
      )
    } else {
      BoxActionTypesCard = this.props.actionTypes.actions.map((item, i) => (
        <BoxActionTypes
          key={i}
          keyProp = {`${key}${item._id}${i}`}
          actionTypes={item}
        />
      ))
    }
    return (
      <div>
        <div className={styles.header}>
          {_ActionTypesPage.renderHeader()}
          <hr className={`Grid-cell u-size12of12 ${styles.separator}`} />
        </div>
        {BoxActionTypesCard}
      </div>
    )
  }

  render() {
    return (
      <div className={styles.root}>
        <ModalCrud />
        <FilterAdmin>
          <form style={{ height: '100%' }}>
            <FilterActionTypes />
          </form>
        </FilterAdmin>

        <FilterResultAdmin>
          <HeaderInternalAdmin
            to="/admin/tipos-de-acoes/adicionar"
            title="Tipos de Ações"
            order={order} // Array com opçoes de ordenação
            orderSelected={this.props.filter.orderSelected}
            onChangeOrder={this.changeOrder} // Funcão ao selecionar um novo valor
            print={this.props.actionTypes.actions.length > 0}
            onPrint={_ActionTypesPage.onPrint}
          />
          {this.renderActionTypes()}
        </FilterResultAdmin>
      </div>
    )
  }
}

_ActionTypesPage.propTypes = {
  getActions: PropTypes.func,
  getFilterActionTypes: PropTypes.func,
  clearFilter: PropTypes.func,
  actionTypes: PropTypes.object,
  filter: PropTypes.object,
  ui: PropTypes.object,
}

const mapStateToProps = state => ({
  actionTypes: state.actionTypes,
  deleteSuccess: state.modal.deleteSuccess,
  filter: state.filtersActionTypes,
  ui: state.ui,
})

const mapActionsToProps = {
  getFilterActionTypes,
  getActions,
  clearFilter,
}

const ActionTypesPage = connect(mapStateToProps, mapActionsToProps)(_ActionTypesPage)
export default ActionTypesPage
