import React from 'react'
import styles from './GuidelineAdmin.css'
import { TextField } from 'material-ui'

import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'

import HeaderAdminButton from '../../components/HeaderAdminButton/HeaderAdminButton.js'
import HeaderAdmin from '../../components/HeaderAdmin/HeaderAdmin.js'

const GuidelineAdmin = () => (

  <div style={{ padding: '30px'}}>

    {/* UTILIZAR PARA PAGINAS QUE USEM FILTRO E BOX. Caso tenha outro titulo, usar a propriedade title */}
    <FilterAdmin>
      <TextField
        hintText="Procure uma ação"
        name="nome"
        value="nome"
        maxLength={100}
        inputStyle={{ marginLeft: '38px' }}
        hintStyle={{ marginLeft: '38px' }}
        fullWidth
      />
      {/* Inserir Conteudo */}
    </FilterAdmin>

    <FilterResultAdmin>
      Inserir Conteudo
    </FilterResultAdmin>
    {/* FIM */}

    <br /><br />

    {/* Utilizar para paginas que vao fazer edição/adição.
      A propriedade UseTheme, caso nao vá utilizar itens de formulario. Default já definido no componente. */}
    <ContentAdmin title="Editar Ação" useTheme={false}>
      Inserir o conteudo
    </ContentAdmin>


    <br /><br />
    Header<br />
    <div style={{ margin:'0 -30px'}}>
      <HeaderAdmin />
    </div>

  </div>
)

export default GuidelineAdmin
