import React from 'react'
import PropTypes from 'prop-types'
// import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styles from './Welcome.css'
import muiTheme from '../../muiTheme'
import UserAvatar from '../../components/UserAvatar/UserAvatar'
import LandingAvatars from '../../components/LandingAvatars/LandingAvatars'
import PillButtonCerulean from '../../components/PillButtonCerulean/PillButtonCerulean'
import SvgIcon from '../../components/SvgIcon/SvgIcon.react'
import { getStoreVolunteer } from '../../actions/registerForm'
import confetti from './confetti'

class _Welcome extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      loader: false,
      cssCanvas: styles.confetti,
    }
    this.stopConfetti = this.stopConfetti.bind(this)
    this.login = this.login.bind(this)
  }

  componentWillMount() {
    this.props.getStoreVolunteer()
  }

  componentDidMount() {
    confetti()
    setTimeout(this.stopConfetti, 4000)
  }

  stopConfetti() {
    this.setState({
      cssCanvas: `${styles.confetti} ${styles.confettiAnim}`,
    })
  }

  login() {
    const href = `/redirect?nome=${this.props.voluntario.nome.split(' ', 1)[0]}&email=${this.props.voluntario.email}&temp=true`
    window.location.assign(href)
    this.setState({
      loader: true,
    })
  }

  render() {
    let nome = this.props.voluntario && this.props.voluntario.nome ? this.props.voluntario.nome.split(' ', 1)[0] : null
    nome = nome ? nome.toLowerCase() : ''

    return (
      <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
        <div className={styles.container}>
          <div className={this.state.cssCanvas}>
            <canvas id="confeti" width="300" height="300" />
          </div>
          <div className={styles.headerBoxLogo}>
            <SvgIcon
              style={{ height: '100%', width: '100%' }}
              icon="icon-icon-8"
            />
          </div>
          <div className={styles.LandingCTATeste}>
            <div className={styles.LandingAvatarsLeft}>
              <div className={styles.reverser}>
                <LandingAvatars />
              </div>
            </div>
            <div className={styles.AvatarBox} >
              <div className={styles.avatar}>
                <UserAvatar
                  url={this.props.voluntario && this.props.voluntario.urlAvatar}
                />
              </div>
              <p className={styles.Nome}>
                Parabéns, <span>{nome}</span>
              </p>
              <div className={styles.texto}>
                <p className={styles.Text}>Você já pode participar</p>
                <p className={styles.Text}>das ações voluntárias.</p>
              </div>
              <PillButtonCerulean
                text="Conhecer o Portal"
                color="white"
                loader={this.state.loader}
                onClick={() => this.login()}
              />
            </div>
            <div className={styles.LandingAvatarsRight}>
              <LandingAvatars position={'right'} />
            </div>
            <div className={styles.animationImg}>
              <img
                className={styles.logo}
                src={'images/logo-223x152.png'}
                alt="Logo Portal do Voluntariado"
              />
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

_Welcome.propTypes = {
  voluntario: PropTypes.object,
  getStoreVolunteer: PropTypes.func,
}

const mapStateToProps = state => ({
  voluntario: state.registerForm.voluntarioCompleto,
})

const mapActionsToProps = {
  getStoreVolunteer,
}

const Welcome = connect(mapStateToProps, mapActionsToProps)(_Welcome)

export default Welcome
