import React from 'react'
import { Link } from 'react-router-dom'
import styles from './AccessDeniedPage.css'
import PillButtonPelorous from '../../components/PillButtonPelorous/PillButtonPelorous'
import SvgIcon from '../../components/SvgIcon/SvgIcon.react'

const AccessDeniedPage = () => (
  <div className={styles.container}>
    <div className={styles.errorIcon}>
      <SvgIcon icon="icon-icon-46" />
    </div>
    <h1 className={styles.title}>Você não possui permissão para acessar o portal</h1>
    <Link to="/">
      <div className={styles.link}>
        <PillButtonPelorous
          text="Voltar para home"
          onClick={() => {}}
        />
      </div>
    </Link>
    <div className={styles.animationImg}>
      <img
        className={styles.logo}
        src={'images/logo-223x152.png'}
        alt="Logo Portal do Voluntariado"
      />
    </div>
  </div>
)

AccessDeniedPage.displayName = 'AccessDeniedPage'

export default AccessDeniedPage
