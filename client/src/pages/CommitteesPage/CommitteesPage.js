import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import styles from './CommitteesPage.css'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import { getCommittees, paginateCommmite, handlePrintCommittees } from '../../actions/comites'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import BoxCommittee from '../../components/BoxCommittees/BoxCommittees'
import FilterCommittee from '../../components/FilterCommittee/FilterCommittee'
import Pagination from '../../components/PaginationComittee/PaginationComittee'

const order = [
  {
    name: 'Ação com mais beneficiados',
    value: '{ "beneficiadas" : -1 }',
  },
  {
    name: 'Ação com menos beneficiados',
    value: '{ "beneficiadas": 1 }',
  },
  {
    name: 'Cidade/UF (A - Z)',
    value: '{ "cidadeNome": 1 }',
  },
  {
    name: 'Cidade/UF (Z - A)',
    value: '{ "cidadeNome": -1 }',
  },
  {
    name: 'Empresa (A - Z)',
    value: '{ "empresas.nomeNormalizado": 1 }',
  },
  {
    name: 'Empresa (Z - A)',
    value: '{ "empresas.nomeNormalizado": -1 }',
  },
  {
    name: 'Maior quantidade de ações',
    value: '{ "acoes": -1 }',
  },
  {
    name: 'Menor quantidade de ações',
    value: '{ "acoes": 1 }',
  },
  {
    name: 'Unidade (A - Z)',
    value: '{ "unidadeNormalizada": 1 }',
  },
  {
    name: 'Unidade (Z - A)',
    value: '{ "unidadeNormalizada": -1 }',
  },
  {
    name: 'Maior quantidade de verba extra',
    value: '{ "verbaExtra": -1 }',
  },
  {
    name: 'Menor quantidade de verba extra',
    value: '{ "verbaExtra": 1 }',
  },
  {
    name: 'Maior quantidade de verba inicial',
    value: '{ "verbaInicial": -1 }',
  },
  {
    name: 'Menor quantidade de verba inicial',
    value: '{ "verbaInicial": 1 }',
  },
  {
    name: 'Maior quantidade de voluntários',
    value: '{ "voluntarios": -1 }',
  },
  {
    name: 'Menor quantidade de voluntários',
    value: '{ "voluntarios": 1 }',
  },
]

class _CommitteesPage extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    animatedScrollTo(
      el,
      0,
      1000,
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      orderSelected: '{ "empresas.nomeNormalizado": 1 }',
      filtro: {},
    }

    this.paginate = this.paginate.bind(this)
    this.changeOrder = this.changeOrder.bind(this)
    this.getFilter = this.getFilter.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  componentWillMount() {
    this.props.getCommittees()
  }

  onPrint() {
    const payload = { ...this.state.filtro }
    this.props.handlePrintCommittees(payload, () => {
      window.print()
    })
  }

  getFilter(filtro) {
    this.setState({ filtro })
  }

  handleCommittees() {
    if (this.props.loading) {
      return (
        <LoaderAdmin />
      )
    }

    return <BoxCommittee />
  }

  changeOrder(event, index, value) {
    if (this.state.orderSelected !== value) {
      const id = this.props.match ? this.props.match.params.id : null
      if (id) {
        this.props.getCommittees({ filtro: `{"comite":"${id}"}`, ordenacao: value })
      } else {
        this.props.getCommittees({ ordenacao: value, filtro: this.state.filtro })
      }
      this.setState({
        orderSelected: value,
      })
    }
  }

  paginate(page) {
    const newPage = parseInt(page, 10) + 1

    this.props.paginateCommmite({
      skip: (6 * newPage) - 6,
      filtro: this.state.filtro,
      ordenacao: this.state.orderSelected,
    })
    setTimeout(() => {
      _CommitteesPage.scrollTo()
    }, 200)
  }

  render() {
    return (
      <div className={styles.root}>
        <FilterAdmin>
          <form style={{ height: '100%' }}>
            <FilterCommittee
              handleFilter={this.getFilter}
            />
          </form>
        </FilterAdmin>
        <FilterResultAdmin>
          <HeaderInternalAdmin
            title="Comitês" // Titulo da pagina
            to="/admin/comites/adicionar" // ROTA PARA o botao de add.Caso nao tenha deixar null
            order={order} // Array com opçoes de ordenação
            orderSelected={this.state.orderSelected} // Valor selecionado
            onChangeOrder={this.changeOrder} // Funcão ao selecionar um novo valor.
            print={this.props.total > 0}
            onPrint={this.onPrint}
          />
          {this.handleCommittees()}
          <Pagination
            totalItens={this.props.total}
            onClick={this.paginate}
            className={this.props.loading ? 'u-hidden' : ''}
            order={this.state.orderSelected}
          />

        </FilterResultAdmin>
      </div>
    )
  }
}

_CommitteesPage.propTypes = {
  getCommittees: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  paginateCommmite: PropTypes.func,
  total: PropTypes.number,
  match: PropTypes.object,
  handlePrintCommittees: PropTypes.func,
}

_CommitteesPage.defaultProps = {
  loading: false,
}

const mapStateToProps = state => ({
  loading: state.list.loading,
  deleteSuccess: state.modal.deleteSuccess,
  total: state.list.total,
})

const mapActionsToProps = {
  getCommittees,
  paginateCommmite,
  handlePrintCommittees,
}

const CommitteesPage = connect(mapStateToProps, mapActionsToProps)(_CommitteesPage)
export default CommitteesPage
