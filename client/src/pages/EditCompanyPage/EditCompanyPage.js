import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TextField } from 'material-ui'
import { withRouter } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styles from './EditCompanyPage.css'
import muiTheme from '../../muiTheme'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import BackButton from '../../components/BackButton/BackButton'
import PillButton from '../../components/PillButton/PillButton'
import {
  handleChangeCompany,
  handleAddCompany,
  searchCompany,
  handleSaveEditCompany,
  clearCompany, deleteCompany } from '../../actions/empresas'
import { setModal } from '../../actions/modal'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'


class _EditCompanyPage extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      nome: '',
    }
    this.handleAddCompany = this.handleAddCompany.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleDeleteCompany = this.handleDeleteCompany.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillMount() {
    this.props.clearCompany()
    if (this.props.match.params.id) {
      this.props.searchCompany(this.props.match.params.id)
    }
  }

  handleChange(e) {
    const obj = { ...this.props.empresa }
    obj[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    this.props.handleChangeCompany(obj)
  }

  handleBlur(e) {
    const newState = {}
    newState[e.target.name] = this.state[e.target.name] && e.target.value ? '' : this.state[e.target.name]
    this.setState(newState)
  }

  handleAddCompany(e) {
    e.preventDefault()
    let nome = ''

    this.setState({ nome })

    if (!this.props.empresa || !this.props.empresa.nome) {
      nome = 'Nome da cidade é obrigatório'
    }

    if (nome !== '') {
      return this.setState({ nome })
    }

    if (this.props.match.params.id) {
      return this.props.handleSaveEditCompany(this.props.empresa)
    }

    return this.props.handleAddCompany(this.props.empresa.nome)
  }

  handleDeleteCompany() {
    this.props.deleteCompany(this.props.match.params.id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina)
  }

  renderBtnDelete(nome) {
    if (!this.props.match.params.id) { return null }

    const msg = `Você tem certeza que deseja excluir a empresa ${nome}?`

    return (
      <div className={styles.delete}>
        <PillButton
          text="excluir"
          negativeColors
          onClick={() =>
            this.handleOpenModal(
              'delete', // TIPO DE AÇÃO - PODE SER DELETE, WARNING, OU SUCCESS
              'empresas', // ENTIDADE RELACIONADA NO BANCO
              msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM NAO VENHA DO BANCO ENVIAR
              this.handleDeleteCompany, // AÇÃO PRINCIPAL
              '/admin/empresas', // ROTA DA PAGINA PRINCIPAL
            )
          }
        />
      </div>
    )
  }

  renderBtnRegister() {
    const match = this.props.match
    const textBtn = match.params.id ? 'ALTERAR' : 'CADASTRAR'
    let ml = ''
    if (this.props.match.params.id) {
      ml = styles.ml
    }

    return (
      <div className={`${styles.register} ${ml}`}>
        <PillButton
          text={textBtn}
          loader={this.props.loader}
          onClick={e => this.handleAddCompany(e)}
        />
      </div>
    )
  }

  render() {
    const match = this.props.match
    const title = match.params.id ? 'Alterar empresa' : 'Cadastro de empresas'
    let buttons = 'u-textRight'
    if (!match.params.id) { buttons = 'u-textCenter' }

    if ((this.props.loading) && this.props.match.params.id) {
      return (
        <ContentAdmin title={title} useTheme={false}>
          <LoaderAdmin />
        </ContentAdmin>
      )
    }

    const nome = this.props.empresa ? this.props.empresa.nome : ''

    return (
      <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
        <ContentAdmin title={title} useTheme={false}>
          <ModalCrud />
          <TextField
            floatingLabelText="Nome da Empresa"
            name="nome"
            value={nome}
            errorText={this.state.nome}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            maxLength={120}
            type="text"
            fullWidth
          />
          <div className={`${styles.divBtn} ${buttons}`}>
            <div className={styles.backButton}>
              <BackButton history={this.props.history} />
            </div>
            {this.renderBtnDelete(nome)}
            {this.renderBtnRegister()}
          </div>
        </ContentAdmin>
      </MuiThemeProvider>
    )
  }
}

_EditCompanyPage.propTypes = {
  clearCompany: PropTypes.func,
  match: PropTypes.object,
  searchCompany: PropTypes.func,
  empresa: PropTypes.object,
  handleChangeCompany: PropTypes.func,
  handleSaveEditCompany: PropTypes.func,
  handleAddCompany: PropTypes.func,
  deleteCompany: PropTypes.func,
  setModal: PropTypes.func,
  loading: PropTypes.bool,
  history: PropTypes.object,
  loader: PropTypes.bool,
}

const mapStateToProps = state => ({
  empresa: state.empresas,
  loading: state.ui.loading,
  loader: state.ui.loaderBtn,
})

const mapActionsToProps = {
  handleChangeCompany,
  handleAddCompany,
  searchCompany,
  handleSaveEditCompany,
  clearCompany,
  setModal,
  deleteCompany,
}

const EditCompanyPage = connect(mapStateToProps, mapActionsToProps)(_EditCompanyPage)
export default withRouter(EditCompanyPage)
