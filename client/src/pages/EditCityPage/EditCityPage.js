import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { TextField } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styles from './EditCityPage.css'
import muiTheme from '../../muiTheme'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import BackButton from '../../components/BackButton/BackButton'
import PillButton from '../../components/PillButton/PillButton'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import {
  handleChangeCity,
  handleAddCity,
  searchCity,
  handleSaveEditCity,
  deleteCity,
  clearCity } from '../../actions/cidades'
import { setModal } from '../../actions/modal'

class _EditCityPage extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      nome: '',
      uf: '',
    }
    this.handleAddCity = this.handleAddCity.bind(this)
    this.handleDeleteCity = this.handleDeleteCity.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillMount() {
    this.props.clearCity()
    if (this.props.match.params.id) {
      this.props.searchCity(this.props.match.params.id)
    }
  }

  handleChange(e) {
    let value = e.target.value.replace(/^\s+/gm, '')
    if (e.target.name === 'uf') {
      value = value.replace(/_|-|[^\w]|\d/g, '').toUpperCase()
    }
    const obj = { ...this.props.cidade }
    obj[e.target.name] = value
    this.props.handleChangeCity(obj)
  }

  handleBlur(e) {
    const newState = {}
    newState[e.target.name] = this.state[e.target.name] && e.target.value ? '' : this.state[e.target.name]
    this.setState(newState)
  }

  handleAddCity(e) {
    e.preventDefault()
    let nome = ''
    let uf = ''

    this.setState({ nome, uf })

    if (!this.props.cidade || !this.props.cidade.nome) {
      nome = 'Nome da cidade é obrigatório'
    }

    if (!this.props.cidade || !this.props.cidade.uf) {
      uf = 'Estado é obrigatório'
    }

    if ((this.props.cidade && this.props.cidade.uf) && (this.props.cidade.uf.length < 2)) {
      uf = 'Quantidade mínima 2 caracteres'
    }

    if (nome !== '' || uf !== '') {
      return this.setState({ nome, uf })
    }

    if (this.props.match.params.id) {
      const newCity = {
        _id: this.props.cidade._id,
        nome: this.props.cidade.nome,
        uf: this.props.cidade.uf,
      }
      return this.props.handleSaveEditCity(newCity)
    }

    return this.props.handleAddCity(this.props.cidade)
  }

  // AÇÕES DA MODAL
  handleDeleteCity() {
    this.props.deleteCity(this.props.match.params.id)
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina)
  }

  renderBtnDelete(nome, uf) {
    if (!this.props.match.params.id) { return null }

    const msg = `Você tem certeza que deseja excluir a cidade ${nome}/${uf}?`

    return (
      <div className={styles.delete}>
        <PillButton
          text="excluir"
          negativeColors
          onClick={() =>
            this.handleOpenModal(
              'delete', // TIPO DE AÇÃO - PODE SER DELETE, WARNING, OU SUCCESS
              'cidades', // ENTIDADE RELACIONADA NO BANCO
              msg, // PARAMETRO OPCIONAL - CASO A MENSAGEM NAO VENHA DO BANCO ENVIAR
              this.handleDeleteCity, // AÇÃO PRINCIPAL
              '/admin/cidades', //ROTA DA PAGINA PRINCIPAL
            )
          }
        />
      </div>
    )
  }

  renderBtnRegister() {
    const textBtn = this.props.match.params.id ? 'ALTERAR' : 'CADASTRAR'
    let ml = ''
    if (this.props.match.params.id) {
      ml = styles.ml
    }
    return (
      <div className={`${styles.register} ${ml}`}>
        <PillButton
          text={textBtn}
          loader={this.props.loaderBtn}
          onClick={e => this.handleAddCity(e)}
        />
      </div>
    )
  }

  render() {
    const match = this.props.match
    const title = match.params.id ? 'Alterar cidade' : 'Cadastro de Cidades'
    let buttons = 'u-textRight'

    if (this.props.loading && this.props.match.params.id) {
      return (
        <ContentAdmin title={title} useTheme={false}>
          <LoaderAdmin />
        </ContentAdmin>
      )
    }
    if (!match.params.id) { buttons = 'u-textCenter' }

    const nome = this.props.cidade ? this.props.cidade.nome : ''
    const uf = this.props.cidade ? this.props.cidade.uf : ''

    return (
      <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
        <ContentAdmin title={title} useTheme={false}>
          <ModalCrud />

          <div className="Grid Grid--withGutter">
            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <TextField
                floatingLabelText="Nome da cidade"
                name="nome"
                value={nome}
                errorText={this.state.nome}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                maxLength={80}
                type="text"
                fullWidth
              />
            </div>
            <div className="Grid-cell u-size1of1 u-textLeft u-inlineBlock">
              <TextField
                floatingLabelText="UF"
                name="uf"
                value={uf}
                errorText={this.state.uf}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                type="text"
                maxLength={2}
                fullWidth
              />
            </div>
          </div>
          <div className={`${styles.divBtn} ${buttons}`}>
            <div className={styles.backButton}>
              <BackButton history={this.props.history} />
            </div>
            {this.renderBtnDelete(nome, uf)}
            {this.renderBtnRegister()}
          </div>
        </ContentAdmin>
      </MuiThemeProvider>
    )
  }
}

_EditCityPage.propTypes = {
  clearCity: PropTypes.func,
  match: PropTypes.object,
  searchCity: PropTypes.func,
  cidade: PropTypes.object,
  handleChangeCity: PropTypes.func,
  handleSaveEditCity: PropTypes.func,
  handleAddCity: PropTypes.func,
  deleteCity: PropTypes.func,
  setModal: PropTypes.func,
  loading: PropTypes.bool,
  loaderBtn: PropTypes.bool,
  history: PropTypes.object,
}

const mapStateToProps = state => ({
  cidade: state.cidades,
  deleteSuccess: state.ui.deleteSuccess,
  loading: state.ui.loading,
  loaderBtn: state.ui.loaderBtn,
})

const mapActionsToProps = {
  handleChangeCity,
  handleAddCity,
  searchCity,
  handleSaveEditCity,
  deleteCity,
  clearCity,
  setModal,
}

const EditCityPage = connect(mapStateToProps, mapActionsToProps)(_EditCityPage)
export default withRouter(EditCityPage)
