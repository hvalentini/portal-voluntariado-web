import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import { fetchVolunteers, getVolunterOrder } from '../../actions/voluntario'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import FilterVolunteers from '../../components/FilterVolunteers/FilterVolunteers.react'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import styles from './LandingVolunteerPage.css'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import BoxVolunteers from '../../components/BoxVolunteers/BoxVolunteers.react'

const order = [
  {
    name: 'Nome de voluntário (A-Z)',
    value: 'nameasc',
  },
  {
    name: 'Nome de voluntário (Z-A)',
    value: 'namedesc',
  },
  {
    name: 'Participou em ação (0-9)',
    value: 'participationasc',
  },
  {
    name: 'Participou em ação (9-0)',
    value: 'participationdesc',
  },
]

class _LandingVolunteerPage extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  constructor(props) {
    super(props)
    this.state = {
      value: 'nameasc',
    }
    this.onChangeOrderBy = this.onChangeOrderBy.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  componentWillMount() {
    if (this.props.user.adminLogin || this.props.match.path === '/admin/comites/editar/:id') {
      const id = this.props.match ? this.props.match.params.id : null
      this.props.fetchVolunteers(id)
    } else {
      const id = this.props.user.comite._id
      this.props.fetchVolunteers(id)
    }
  }

  componentDidMount() {
    if (this.props.match.path === '/lider/gestao-de-voluntarios') {
      _LandingVolunteerPage.scrollTo()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.deleteSuccess) {
      this.props.fetchVolunteers(this.props.idComite)
    }
  }

  onChangeOrderBy(event, index, value) {
    this.setState({ value }) // State para alternar lista do Material UI
    let typeOrder = ''
    if (value === 'nameasc' || value === 'namedesc') { typeOrder = 'props.info.nomeNormalizado' }
    if (value === 'participationasc' || value === 'participationdesc') { typeOrder = 'props.info.quantidadeDeParticipacoes' }
    this.props.getVolunterOrder({ orderOfVolunteers: value, typeOrder })
  }

  onPrint() {
    if (this.props.volunteer.volunteers.length > 0
      || this.props.volunteer.filterVolunteers.length > 0
      || this.props.volunteer.searchTerm.length > 0) {
      window.print()
    }
  }

  handleInstitutions() {
    if (this.props.loading) {
      return (
        <LoaderAdmin />
      )
    }
    if (!this.props.volunteerStore.volunteers.length && !this.props.loading) {
      return (
        <div>
          <p className={styles.notFound}>Nenhum voluntario encontrado</p>
        </div>
      )
    }
    return <BoxVolunteers />
  }

  renderModal() {
    if (!this.props.user.adminLogin) {
      return (
        <ModalCrud />
      )
    }

    return null
  }

  render() {
    const idComite = this.props.user.adminLogin || this.props.match.path === '/admin/comites/editar/:id'
                   ? this.props.match.params.id
                   : this.props.user.comite._id

    const recalcFilter = this.props.volunteerStore.volunteers.length > 0

    return (
      <div className={styles.root}>
        {/* <ModalCrud /> */}
        {this.renderModal()}
        <FilterAdmin
          recalc={recalcFilter}
        >
          <form style={{ height: '100%' }}>
            <FilterVolunteers idComite={idComite} />
          </form>
        </FilterAdmin>
        <FilterResultAdmin>
          <HeaderInternalAdmin
            title="Voluntários" // Titulo da pagina
            to={null} // ROTA PARA o botao de add.Caso nao tenha deixar null
            order={order} // Array com opçoes de ordenação
            orderSelected={this.props.volunteer.orderOfVolunteers} // Valor selecionado
            onChangeOrder={this.onChangeOrderBy} // Funcão ao selecionar um novo valor
            print={this.props.volunteerStore.volunteers.length > 0}
            onPrint={this.onPrint}
          />
          {this.handleInstitutions()}
        </FilterResultAdmin>
      </div>
    )
  }
}

_LandingVolunteerPage.propTypes = {
  fetchVolunteers: PropTypes.func,
  getVolunterOrder: PropTypes.func,
  volunteerStore: PropTypes.object,
  loading: PropTypes.bool,
  idComite: PropTypes.string,
  volunteer: PropTypes.object,
  match: PropTypes.object,
  user: PropTypes.object,
}

const mapStateToProps = state => ({
  volunteerStore: state.voluntario,
  loading: state.ui.loading,
  volunteer: state.voluntario,
  user: state.loggedUser,
})

const mapActionsToProps = {
  fetchVolunteers,
  getVolunterOrder,
}

const LandingVolunteerPage = connect(mapStateToProps, mapActionsToProps)(_LandingVolunteerPage)
export default withRouter(LandingVolunteerPage)
