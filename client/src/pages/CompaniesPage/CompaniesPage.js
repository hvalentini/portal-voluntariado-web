import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './CompaniesPage.css'
import BoxCompany from '../../components/BoxCompany/BoxCompany'
import { handleCompanies } from '../../actions/empresas'
import { clearFilter, getFilterCompany } from '../../actions/filtersCompany'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import FilterCompany from '../../components/FilterCompany/FilterCompany'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'

const order = [
  {
    name: 'Empresas (A-Z)',
    value: '{ "nomeNormalizado" : 1 }',
  },
  {
    name: 'Empresas (Z-A)',
    value: '{ "nomeNormalizado": -1 }',
  },
]

class _CompaniesPage extends React.Component {

  constructor(props) {
    super(props)
    this.changeOrder = this.changeOrder.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  componentWillMount() {
    this.props.clearFilter()
    this.props.handleCompanies()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.deleteSuccess) {
      this.props.handleCompanies()
    }
  }

  handleCompanies() {
    if (this.props.loading) {
      return (
        <LoaderAdmin />
      )
    }
    if (!this.props.empresas.length && !this.props.loading) {
      return (
        <div>
          <p className={styles.notFound}>Nenhuma empresa encontrada</p>
        </div>
      )
    }
    return <BoxCompany />
  }

  changeOrder(event, index, value) {
    const filtro = this.props.filter.nomeNormalizado
                    ? { nomeNormalizado: this.props.filter.nomeNormalizado }
                    : {}

    this.props.getFilterCompany({ filtro, ordenacao: value })
  }

  onPrint() {
    window.print()
  }

  render() {
    return (
      <div className={styles.root}>
        <ModalCrud />
        <FilterAdmin>
          <FilterCompany />
        </FilterAdmin>
        <FilterResultAdmin>
          <HeaderInternalAdmin
            title="Empresas" // Titulo da pagina
            to="/admin/empresas/adicionar" // ROTA PARA o botao de add.Caso nao tenha deixar null
            order={order} // Array com opçoes de ordenação
            orderSelected={this.props.filter.orderSelected} // Valor selecionado
            onChangeOrder={this.changeOrder} // Funcão ao selecionar um novo valor
            print={this.props.empresas.length > 0}
            onPrint={this.onPrint}
          />
          {this.handleCompanies()}
        </FilterResultAdmin>
      </div>
    )
  }
}

_CompaniesPage.propTypes = {
  empresas: PropTypes.array,
  clearFilter: PropTypes.func,
  handleCompanies: PropTypes.func,
  loading: PropTypes.bool,
  filter: PropTypes.object,
  getFilterCompany: PropTypes.func,
}

const mapStateToProps = state => ({
  empresas: state.list.empresas,
  loading: state.ui.loading,
  deleteSuccess: state.modal.deleteSuccess,
  filter: state.filtersCompany,
})

const mapActionsToProps = {
  handleCompanies,
  getFilterCompany,
  clearFilter,
}

const CompaniesPage = connect(mapStateToProps, mapActionsToProps)(_CompaniesPage)
export default CompaniesPage
