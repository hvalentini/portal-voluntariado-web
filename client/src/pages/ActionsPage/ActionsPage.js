import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import animatedScrollTo from 'animated-scrollto'
import Browser from 'detect-browser'
import FilterAdmin from '../../components/FilterAdmin/FilterAdmin'
import FilterResultAdmin from '../../components/FilterResultAdmin/FilterResultAdmin'
import LoaderAdmin from '../../components/LoaderAdmin/LoaderAdmin'
import HeaderInternalAdmin from '../../components/HeaderInternalAdmin/HeaderInternalAdmin'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import FilterActionsAdmin from '../../components/FilterActionsAdmin/FilterActionsAdmin'
import { getAcoes, paginateAction, handlePrintActions } from '../../actions/acoes'
import styles from './ActionsPage.css'
import BoxAction from '../../components/LeaderBoxAction/LeaderBoxAction'
import Pagination from '../../components/PaginationComittee/PaginationComittee'
import order from './orderBy'

class _ActionsPage extends React.Component {

  static scrollTo() {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    animatedScrollTo(
      el,
      510,
      1000,
    )
  }

  static scrollToPagination(adminLogin) {
    const el = (Browser && (Browser.name === 'firefox' || Browser.name === 'ie'))
      ? document.querySelector('html')
      : document.querySelector('body')
    if (adminLogin) {
      animatedScrollTo(
        el,
        0,
        1000,
      )
    } else {
      animatedScrollTo(
        el,
        510,
        500,
      )
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      orderSelected: '{ "dataCadastro" : -1}',
      filtro: { desativada: false },
      calcView: false,
    }

    this.changeOrder = this.changeOrder.bind(this)
    this.paginate = this.paginate.bind(this)
    this.getFiltro = this.getFiltro.bind(this)
    this.renderActionTypes = this.renderActionTypes.bind(this)
    this.onPrint = this.onPrint.bind(this)
  }

  componentWillMount() {
    if (this.props.user.adminLogin) {
      const id = this.props.match ? this.props.match.params.id : null
      if (id) {
        this.props.getAcoes({ filtro: `{"comite":"${id}"}`, order: this.state.orderSelected })
      } else {
        this.props.getAcoes({ filtro: { desativada: false }, order: this.state.orderSelected })
      }
    } else {
      const id = this.props.user.comite._id
      if (id) {
        this.props.getAcoes({ filtro: `{"comite":"${id}"}`, order: this.state.orderSelected })
      }
    }
  }

  componentDidMount() {
    if (this.props.match && this.props.match.path === '/lider/acoes-voluntarias') {
      _ActionsPage.scrollTo()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.deleteSuccess) {
      const id = this.props.match ? this.props.match.params.id : null
      if (id) {
        this.props.getAcoes({ filtro: `{"comite":"${id}"}`, order: this.state.orderSelected })
      } else {
        this.props.getAcoes({ filtro: { desativada: false }, order: this.state.orderSelected })
      }
    }
  }

  onPrint() {
    let payload = { ...this.state.filtro }
    if (!this.props.user.adminLogin) {
      payload = { ...this.state.filtro, comite: this.props.comite._id }
    }
    this.props.handlePrintActions(payload, this.state.ordenacao, () => {
      window.print()
    })
  }

  getFiltro(filtro) {
    this.setState({ filtro: { ...filtro } })
  }

  paginate(page) {
    let filtro = this.state.filtro
    if (!this.props.user.adminLogin) {
      filtro = { ...filtro, comite: this.props.user.comite._id }
    }

    this.props.paginateAction({
      limit: 6,
      page,
      filters: JSON.stringify(filtro) || '',
      order: this.state.orderSelected || '',
    })
    setTimeout(() => _ActionsPage.scrollToPagination(this.props.user.adminLogin), 200)
  }

  changeOrder(event, index, value) {
    const id = this.props.match ? this.props.match.params.id : null
    if (id) {
      this.props.getAcoes({ filtro: `{"comite":"${id}"}`, order: value })
    } else {
      this.props.getAcoes({ filtro: this.state.filtro, order: value })
    }
    this.setState({
      orderSelected: value,
    })
  }

  renderActionTypes() {
    if (this.props.loading) {
      return (
        <LoaderAdmin />
      )
    } else if (this.props.acoes.length < 1 && !this.props.loading) {
      return (
        <div>
          <p className={styles.notFound}>Nenhuma ação encontrada</p>
        </div>
      )
    }

    return this.props.acoes.map((item) => {
      if (item.desativada) { return null }

      return (
        <BoxAction
          isAdmin={this.props.user.adminLogin}
          key={item._id}
          acao={item}
          user={this.props.user}
        />
      )
    })
  }


  renderActionsPrint() {
    if (!this.props.print || !this.props.print.length) { return null }
    return this.props.print.map(item => (
      <BoxAction
        isAdmin={this.props.user.adminLogin}
        key={item._id}
        acao={item}
        user={this.props.user}
      />
    ))
  }

  renderModal() {
    if (!this.props.user.adminLogin) {
      return (
        <ModalCrud />
      )
    }

    return null
  }

  render() {
    const btnAdd = this.props.user.adminLogin ? '' : '/lider/nova-acao'

    return (
      <div className={`${styles.root} page-break`}>
        {this.renderModal()}
        <FilterAdmin>
          <form style={{ height: '100%' }}>
            <FilterActionsAdmin
              handleFilter={this.getFiltro}
              styleBnt={this.props.styleBnt}
            />
          </form>
        </FilterAdmin>

        <FilterResultAdmin>
          <HeaderInternalAdmin
            to={btnAdd}
            title="Ações"
            order={this.props.user.adminLogin ? order.pvAdmin : order.pvWeb}
            orderSelected={this.state.orderSelected}
            onChangeOrder={this.changeOrder} // Funcão ao selecionar um novo valor
            className="noPrint"
            print={this.props.acoes.length > 0}
            onPrint={this.onPrint}
          />
          <div className="noPrint">
            {this.renderActionTypes()}
          </div>
          <Pagination
            totalItens={this.props.total}
            onClick={this.paginate}
            className={this.props.loading ? 'u-hidden' : ''}
            order={this.state.orderSelected}
          />
          <div className={styles.printBlock}>
            {this.renderActionsPrint()}
          </div>
        </FilterResultAdmin>
      </div>
    )
  }
}

_ActionsPage.propTypes = {
  match: PropTypes.object,
  user: PropTypes.object,
  getAcoes: PropTypes.func,
  loading: PropTypes.bool,
  acoes: PropTypes.array,
  print: PropTypes.array,
  total: PropTypes.number,
  paginateAction: PropTypes.func,
  comite: PropTypes.object,
  handlePrintActions: PropTypes.func,
  styleBnt: PropTypes.object,
}

_ActionsPage.defaultProps = {
  loading: false,
  styleBnt: {},
}

const mapStateToProps = state => ({
  acoes: state.list.acoes,
  print: state.acoes.acoesPrint,
  comite: state.loggedUser.comite,
  loading: state.list.loading,
  user: state.loggedUser,
  total: state.list.total,
})

const mapActionsToProps = {
  getAcoes,
  paginateAction,
  handlePrintActions,
}

const ActionsPage = connect(mapStateToProps, mapActionsToProps)(_ActionsPage)
export default ActionsPage
