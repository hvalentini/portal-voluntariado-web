const order = {
// pvWeb orderby para Portal voluntariado WEB
  pvWeb: [
    {
      name: 'Ação com mais beneficiados',
      value: '{ "totalBeneficiados": -1 }',
    },
    {
      name: 'Ação com menos beneficiados',
      value: '{ "totalBeneficiados": 1 }',
    },
    {
      name: 'Ação com mais participantes',
      value: '{ "totalParticipantes": -1 }',
    },
    {
      name: 'Ação com menos participantes',
      value: '{ "totalParticipantes": 1 }',
    },
    {
      name: 'Ação que os voluntários mais gostaram',
      value: '{ "totalQueGostaram": -1 }',
    },
    {
      name: 'Ação que os voluntários menos gostaram',
      value: '{ "totalQueGostaram": 1 }',
    },
    {
      name: 'Data cadastro (1-31)',
      value: '{ "dataCadastro": 1 }',
    },
    {
      name: 'Data cadastro (31-1)',
      value: '{ "dataCadastro" : -1}',
    },
    {
      name: 'Nome da ação (A-Z)',
      value: '{ "nomeNormalizado" : 1}',
    },
    {
      name: 'Nome da ação (Z-A)',
      value: '{ "nomeNormalizado" : -1}',
    },
    {
      name: 'Tipo da ação (A-Z)',
      value: '{ "nomeTipoAcao": 1 }',
    },
    {
      name: 'Tipo da ação (Z-A)',
      value: '{ "nomeTipoAcao": -1 }',
    },
    {
      name: 'Maior valor gasto',
      value: '{ "valorGasto": -1 }',
    },
    {
      name: 'Menor valor gasto',
      value: '{ "valorGasto": 1 }',
    },
  ],
// pvAdmin orderby  para portal admin
  pvAdmin: [
    {
      name: 'Ação com mais beneficiados',
      value: '{ "totalBeneficiados": -1 }',
    },
    {
      name: 'Ação com menos beneficiados',
      value: '{ "totalBeneficiados": 1 }',
    },
    {
      name: 'Maior quantidade de cartas',
      value: '{ "totalCartas": -1 }',
    },
    {
      name: 'Menor quantidade de cartas',
      value: '{ "totalCartas": 1 }',
    },
    {
      name: 'Cidade/UF (A-Z)',
      value: '{ "comite.cidade.nomeNormalizado": 1, "comite.cidadeUF": 1 }',
    },
    {
      name: 'Cidade/UF (Z-A)',
      value: '{ "comite.cidade.nomeNormalizado": -1, "comite.cidadeUF": -1 }',
    },
    {
      name: 'Empresa (A-Z)',
      value: '{ "comite.empresa.nomeNormalizado": 1 }',
    },
    {
      name: 'Empresa (Z-A)',
      value: '{ "comite.empresa.nomeNormalizado": -1 }',
    },
    {
      name: 'Ação com mais participantes',
      value: '{ "totalParticipantes": -1 }',
    },
    {
      name: 'Ação com menos participantes',
      value: '{ "totalParticipantes": 1 }',
    },
    {
      name: 'Ação que os voluntários mais gostaram',
      value: '{ "totalQueGostaram": -1 }',
    },
    {
      name: 'Ação que os voluntários menos gostaram',
      value: '{ "totalQueGostaram": 1 }',
    },
    {
      name: 'Data cadastro (1-31)',
      value: '{ "dataCadastro": 1 }',
    },
    {
      name: 'Data cadastro (31-1)',
      value: '{ "dataCadastro" : -1}',
    },
    {
      name: 'Nome da ação (A-Z)',
      value: '{ "nomeNormalizado" : 1}',
    },
    {
      name: 'Nome da ação (Z-A)',
      value: '{ "nomeNormalizado" : -1}',
    },
    {
      name: 'Tipo da ação (A-Z)',
      value: '{ "tipoAcao.nomeNormalizado": 1 }',
    },
    {
      name: 'Tipo da ação (Z-A)',
      value: '{ "tipoAcao.nomeNormalizado": -1 }',
    },
    {
      name: 'Unidade (A-Z)',
      value: '{ "comite.unidadeNormalizada": 1 }',
    },
    {
      name: 'Unidade (Z-A)',
      value: '{ "comite.unidadeNormalizada": -1 }',
    },
    {
      name: 'Maior valor gasto',
      value: '{ "valorGasto": -1 }',
    },
    {
      name: 'Menor valor gasto',
      value: '{ "valorGasto": 1 }',
    },
  ],
}

export default order
