import React from 'react'
import { Link } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import muiTheme from '../../muiTheme'
import styles from './LoginAdminPage.css'
import LoginForm from '../../components/LoginForm/LoginForm'

const LoginAdminPage = () => (
  <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
    <div className="Grid">
      <div className="Grid-cell">
        <div className={styles.LoginBox} >
          <div className={styles.LogoButton}>
            <Link to="/">
              <img
                className={styles.logo}
                src={'/images/logo-223x152.png'}
                alt="Logo Portal do Voluntariado"
              />
            </Link>
          </div>
          <h1>Gestão do Portal de Voluntariado</h1>
          <p className={styles.desc}>
            Bem vindo de volta! Preciso que você faça o seu login para continuar.
          </p>
          <LoginForm />
        </div>
      </div>
    </div>
  </MuiThemeProvider>
)

export default LoginAdminPage
