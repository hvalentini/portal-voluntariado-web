import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import findindex from 'lodash.findindex'
import {
  RadioButton,
  RadioButtonGroup,
  TextField,
  Toggle,
} from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import muiTheme from '../../muiTheme'
import ContentAdmin from '../../components/ContentAdmin/ContentAdmin'
import PillButton from '../../components/PillButton/PillButton'
import BackButton from '../../components/BackButton/BackButton'
import ModalCrud from '../../components/ModalCrud/ModalCrud'
import { putUpdateActions, updateReducerAction, sendDeleteActionType } from '../../actions/actionTypes'
import { setModal } from '../../actions/modal'
import { getFilterActionTypes } from '../../actions/filtersActionTypes'
import trim from '../../utils/trim'
import styles from './NewActionTypePage.css'

class _NewActionTypePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      errorNome: '',
      action: {
        informarQtdeCartas: false,
        tipo: 0,
      },
    }
    this.handleAddActionType = this.handleAddActionType.bind(this)
    this.handleDeleteAction = this.handleDeleteAction.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.changeRadio = this.changeRadio.bind(this)
    this.handleToggleCartas = this.handleToggleCartas.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillMount() {
    if (!this.props.match.params.id) return

    if (this.props.actionTypes && this.props.actionTypes.actions.length > 0) {
      const index = findindex(this.props.actionTypes.actions, { _id: this.props.match.params.id })
      const action = this.props.actionTypes.actions[index]

      this.setState({
        action,
      })
    } else {
      const filtro = { _id: this.props.match.params.id }
      this.props.getFilterActionTypes({ filtro })
    }
  }

  componentWillReceiveProps() {
    if (!this.props.match.params.id) {
      return this.setState({
        errorNome: '',
        action: {
          nome: '',
          informarQtdeCartas: false,
          tipo: 0,
        },
      })
    }

    return null
  }

  handleAddActionType() {
    const action = this.state.action
    if (!action.nome || trim(action.nome).length === 0) {
      const errorNome = 'Nome da ação é obrigatório'
      return this.setState({ errorNome })
    }

    action.exclusivoParaLider = action.tipo === 2

    return this.props.putUpdateActions(action)
  }

  handleDeleteAction(_id) {
    this.props.sendDeleteActionType(_id)
  }

  handleChange(e) {
    const action = this.state.action
    action[e.target.name] = e.target.value.replace(/^\s+/gm, '')
    this.setState({
      action,
    })
  }

  handleBlur(e) {
    if (this.state.errorNome && e.target.value) {
      this.setState({ errorNome: '' })
    }
  }

  handleToggleCartas(e, isInputChecked) {
    const action = this.state.action
    action[e.target.name] = isInputChecked

    this.setState({
      action,
    })
  }

  changeRadio(e) {
    const action = this.state.action
    action[e.target.name] = e.target.value
    this.setState({
      action,
    })
  }

  handleToggleTipo(e, isInputChecked, tipo) {
    const obj = {}
    if (isInputChecked) {
      obj[e.target.name] = tipo
      this.props.updateReducerAction(obj)
    }
  }

  handleOpenModal(tipo, entidade, msg, action, rotaPagina, id) {
    this.props.setModal(tipo, entidade, msg, action, rotaPagina, id)
  }

  renderBtnDelete() {
    if (!this.props.match.params.id) { return null }

    const msg = `Você tem certeza que deseja excluir o Tipo de Ação ${this.state.action.nome}?`

    return (
      <div className={styles.delete}>
        <PillButton
          negativeColors
          text="excluir"
          onClick={() =>
            this.handleOpenModal(
              'delete', // TIPO DE AÇÃO
              'tipoDeAcoes', // ENTIDADE RELACIONADA NO BANCO
              msg,
              this.handleDeleteAction, // AÇÃO PRINCIPAL
              '/admin/tipos-de-acoes', // ROTA DA PAGINA PRINCIPAL
              this.props.match.params.id,
            )
          }
        />
      </div>
    )
  }

  renderBtnRegister() {
    const textBtn = this.props.match.params.id ? 'ALTERAR' : 'CADASTRAR'
    let ml = ''
    if (this.props.match.params.id) {
      ml = styles.ml
    }
    return (
      <div className={`${styles.register} ${ml}`}>
        <PillButton
          onClick={e => this.handleAddActionType(e)}
          loader={this.props.loaderBtn}
          text={textBtn}
        />
      </div>
    )
  }

  render() {
    const title = this.props.match.params.id ? 'Alterar Tipo de Ação' : 'Cadastro de Tipo de Ação'
    let buttons = 'u-textRight'
    if (!this.props.match.params.id) { buttons = 'u-textCenter' }

    return (
      <ContentAdmin title={title} useTheme={false}>
        <ModalCrud />
        <div className={styles.rootNewAction}>
          <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
            <div className="Grid-cell-center">
              <div className="Grid-cell u-textLeft">
                <TextField
                  floatingLabelText="Nome do tipo da ação"
                  errorText={this.state.errorNome}
                  className={styles.textFieldNome}
                  name="nome"
                  value={this.state.action.nome}
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  maxLength={120}
                  type="text"
                  fullWidth
                />
              </div>
              <div className="Grid-cell u-textLeft">
                <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                  <div>
                    <div
                      style={{
                        display: 'inline-grid',
                      }}
                    >
                      <span className={`${styles.labelModalidade} u-textLeft`}>Modalidade</span>
                      <RadioButtonGroup
                        name="tipo"
                        className={styles.toggleMaterial}
                        defaultSelected={this.state.action.tipo}
                        onChange={e => this.changeRadio(e)}
                        style={{
                          display: 'inline-flex',
                          width: '100%',
                        }}
                      >
                        <RadioButton
                          value={0}
                          label="Pontual"
                          style={{
                            marginTop: '7px',
                            width: '30%',
                            fontSize: '16px',
                          }}
                          labelStyle={{ marginLeft: '-14px' }}
                          className="u-inlineBlock"
                        />
                        <RadioButton
                          value={1}
                          label="Contínua"
                          style={{
                            marginTop: '7px',
                            width: '30%',
                            fontSize: '16px',
                            marginLeft: '15px',
                          }}
                          labelStyle={{ marginLeft: '-14px' }}
                          className="u-inlineBlock"
                        />
                        <RadioButton
                          value={2}
                          label="Gerencial"
                          style={{
                            marginTop: '7px',
                            width: '30%',
                            fontSize: '16px',
                            marginLeft: '20px',
                          }}
                          labelStyle={{ marginLeft: '-14px' }}
                          className="u-inlineBlock"
                        />
                      </RadioButtonGroup>
                    </div>
                    <Toggle
                      name="informarQtdeCartas"
                      label="Existe carta?"
                      labelPosition="right"
                      defaultToggled={this.state.action.informarQtdeCartas}
                      // className={styles.toggleMaterial}
                      onToggle={
                        (event, isInputChecked) => this.handleToggleCartas(event, isInputChecked)
                      }
                    />
                  </div>
                </MuiThemeProvider>
                <MuiThemeProvider muiTheme={muiTheme.muiThemeForm}>
                  <div className={`${styles.divPill} ${buttons}`}>
                    <div className={styles.backButton}>
                      <BackButton history={this.props.history} />
                    </div>
                    {this.renderBtnDelete()}
                    {this.renderBtnRegister()}
                  </div>
                </MuiThemeProvider>
              </div>
            </div>
          </MuiThemeProvider>
        </div>
      </ContentAdmin>
    )
  }
}

_NewActionTypePage.propTypes = {
  actionTypes: PropTypes.object,
  history: PropTypes.object,
  match: PropTypes.object.isRequired,
  updateReducerAction: PropTypes.func,
  putUpdateActions: PropTypes.func,
  getFilterActionTypes: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  sendDeleteActionType: PropTypes.func.isRequired,
  loaderBtn: PropTypes.bool,
}

const mapStateToProps = state => ({
  actionTypes: state.actionTypes,
  loaderBtn: state.ui.loaderBtn,
})

const mapActionsToProps = {
  putUpdateActions,
  updateReducerAction,
  getFilterActionTypes,
  setModal,
  sendDeleteActionType,
}

const NewActionTypePage = connect(mapStateToProps, mapActionsToProps)(_NewActionTypePage)
export default withRouter(NewActionTypePage)
