import web from './web'

const WebTestService = {
  test() {
    const res = web.get(`/images/WebTest.png#${+ new Date()}-${Math.random()}`)
    return res
  },
}

export default WebTestService
