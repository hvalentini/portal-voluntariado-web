import api from './api'
import trimObject from '../utils/trimObject'

const LocalService = {
  addLocal(payload) {
    return api.post('/local', trimObject(payload))
  },
  editLocal(payload) {
    return api.put('/local', trimObject(payload))
  },
  getLocals(filter, orderBy) {
    const thisFilter = encodeURIComponent(JSON.stringify(trimObject(filter) || {}))
    const thisOrderBy = orderBy && Object.keys(orderBy).length !== 0 ? JSON.stringify(orderBy) : '{ "nomeNormalizado" : 1}'

    return api.get(`/local?filtro=${thisFilter}&ordenacao=${thisOrderBy}`)
  },
  getOneLocal(id) {
    return api.get(`/local/${id}`)
  },
  handleDeleteInstitution(id) {
    return api.delete(`/local/${id}`)
  },
}

export default LocalService
