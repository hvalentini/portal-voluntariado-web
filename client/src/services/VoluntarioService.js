import api from './api'
import apiLocal from './api-local'

const VoluntarioService = {
  getAvatars(qtd) {
    return api.get('/voluntario/avatar', { quantidade: qtd })
  },
  handleGetAmountVolunteers() {
    return api.get('/voluntario/total-ativo')
  },
  validateCPF(CPF, incompleto) {
    return api.get('/valida-cpf', { cpf: CPF, usuarioIncompleto: incompleto })
  },

  getVoluntariosComite(IdComite) {
    return api.get('/voluntario/quantidade', { idComite: IdComite })
  },
  getVoluntariosAutoComplete(opts) {
    return api.get('/voluntario/autocomplete', {
      comite: opts.comite,
      texto: opts.texto,
      permitirBloqueado: opts.permitirBloqueado,
    })
  },

  login(payload) {
    return api.get('/voluntario/emitir-certificado', { ...payload })
  },

  handleGetSessionVolunteers() {
    return apiLocal.get('/logged-user')
  },

  handleUpdateSessionVolunteers(payload) {
    const link = `/redirect?nome=${payload.nome}&email=${payload.email}`
    return apiLocal.get(link)
  },

  handleUpdateLastAcess(_id) {
    const link = '/voluntario/atualiza-data-acesso'
    return api.put(link, { _id })
  },

  save(payload) {
    if (payload._id) {
      return api.put('/voluntario', { ...payload })
    }
    return api.post('/voluntario', { ...payload })
  },
  getFetchVolunteers(IdComite) {
    return api.get('/voluntario', { filtro: `{"comite":"${IdComite}", "bloqueado": false }` })
  },
  getFilterVolunteers(filter, orderBy) {
    const thisFilter = encodeURIComponent(JSON.stringify(filter) || {})
    const thisOrderBy = !orderBy ? '{ "nomeNormalizado" : 1}' : orderBy
    return api.get(`/voluntario?filtro=${thisFilter}&ordenacao=${thisOrderBy}`)
  },
}

export default VoluntarioService
