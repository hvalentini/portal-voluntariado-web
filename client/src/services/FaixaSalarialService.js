import api from './api'

const FaixaSalarialService = {
  handleGetSalaryRange() {
    return api.get('/faixa-salarial', { ordenacao: { valor: 1 } })
  },
}

export default FaixaSalarialService
