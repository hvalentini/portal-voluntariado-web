import apiLocal from './api-local'

const LoggedUserServices = {
  getLoggedUser() {
    return apiLocal.get('/logged-user')
  },
}

export default LoggedUserServices
