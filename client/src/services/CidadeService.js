import api from './api'
import trimObject from '../utils/trimObject'

const CidadeService = {
  getList() {
    return api.get('/cidade')
  },
  handleCities(payloadFilter, orderBy) {
    const filter = { ...payloadFilter }
    if (filter.uf === '') {
      delete filter.uf
    }
    if (filter.nomeNormalizado === '') {
      delete filter.nomeNormalizado
    }
    const thisFilter = JSON.stringify(trimObject(filter) || {})
    const thisOrderBy = orderBy && Object.keys(orderBy).length !== 0 ? JSON.stringify(orderBy) : '{ "nomeNormalizado" : 1}'
    return api.get(`/cidade?filtro=${encodeURIComponent(thisFilter)}&ordenacao=${thisOrderBy}`)
  },

  addCity(payload) {
    return api.post('/cidade', trimObject(payload))
  },

  getCity(id) {
    return api.get(`/cidade/${id}`)
  },

  saveEditCity(payload) {
    return api.put('/cidade', trimObject(payload))
  },

  deleteCity(_id) {
    return api.delete(`/cidade?_id=${_id}`)
  },

  getState() {
    return api.get('/cidade/uf')
  },
}

export default CidadeService
