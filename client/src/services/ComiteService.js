import api from './api'
import trimObject from '../utils/trimObject'

const ComiteService = {

  getList(payload = {}) {
    const thisFilter = JSON.stringify(trimObject(payload.filter) || {})
    const thisOrderBy = JSON.stringify(payload.order || { unidade: 1 })
    return api.get(`/comite?filtro=${thisFilter}&ordenacao=${thisOrderBy}`)
  },

  getSaldoAtual(id) {
    return api.get(`/comite/${id}`)
  },

  getListCommmittee(payload = {}) {
    const options = payload
    options.filtro = JSON.stringify(trimObject(options.filtro) || {})
    options.ordenacao = options.ordenacao || '{ "empresas.nomeNormalizado": 1}'
    return api.get('/comite/listar', options)
  },

  addCommittee(payload) {
    return api.post('/comite', trimObject(payload))
  },

  saveEditCommittee(payload) {
    return api.put('/comite', trimObject(payload))
  },

  getCommittee(id) {
    return api.get(`/comite/${id}`)
  },

  deleteCommittee(_id) {
    return api.delete(`/comite?id=${_id}`)
  },
}

export default ComiteService
