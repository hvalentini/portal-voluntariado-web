import api from './api'
import trimObject from '../utils/trimObject'

const EmpresaService = {
  handleCompanies() {
    return api.get('/empresa')
  },

  addCompany(payload) {
    return api.post('/empresa', trimObject(payload))
  },

  saveEditCompany(payload) {
    return api.put('/empresa', trimObject(payload))
  },

  getCompany(id) {
    return api.get(`/empresa/${id}`)
  },

  deleteCompany(_id) {
    return api.delete(`/empresa?_id=${_id}`)
  },

  filterCompany(filtro = {}, order = '{ "nomeNormalizado" : 1}') {
    const filter = JSON.stringify(trimObject(filtro))
    return api.get(`/empresa?filtro=${encodeURIComponent(filter)}&ordenacao=${order}`)
  },
}

export default EmpresaService
