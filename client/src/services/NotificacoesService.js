import api from './api'

const NotificacoesService = {
  handleGetNotifications(payload) {
    return api.get('/notificacao', payload)
  },
  getNotifications(filter, orderBy) {
    const thisFilter = encodeURIComponent(JSON.stringify(filter || {}))
    const thisOrderBy = orderBy && Object.keys(orderBy).length !== 0
      ? JSON.stringify(orderBy) : null

    return api.get(`/notificacoes?filtro=${thisFilter}&ordenacao=${thisOrderBy}`)
  },
  postNotification(payload) {
    return api.post('/notificacao', payload)
  },
}

export default NotificacoesService
