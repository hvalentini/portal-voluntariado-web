// TODO: utilizar apiSauce para criar e exportar a API comunicando com o
// enpoint do portal-voluntariado-api
import { create } from 'apisauce'
import URL from '../../../server/constant/urls'
import Store from '../state/store'
import * as types from '../actions/types'

// define the api
const api = create({
  baseURL: URL.baseDNS,
})

api.addMonitor((response) => {
  if (response.problem && response.problem !== 'CLIENT_ERROR') {
    Store.dispatch({ type: types.API_WARNING, payload: true })
  }
})

export default api
