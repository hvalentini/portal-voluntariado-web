import api from './api'

const TipoDeAcaoService = {
  getFilteredActionTypes(payload) {
    const thisFilter = encodeURIComponent(JSON.stringify(payload.filtro) || {})
    const thisOrderBy = !payload.ordenacao ? '{ "nomeNormalizado" : 1}' : payload.ordenacao

    return api.get(`/tipo-de-acao?filtro=${thisFilter}&ordenacao=${thisOrderBy}&naoOrdernadarPersonalizado=${payload.naoOrdernadarPersonalizado || false}`)
  },
  handleGetFetchActionsTypes(payload = {}) {
    const thisOrderBy = payload.ordenacao || '{ "nomeNormalizado": 1 }'
    return api.get(`/tipo-de-acao?ordenacao=${thisOrderBy}&naoOrdernadarPersonalizado=${payload.naoOrdernadarPersonalizado || false}`)
  },
  getOneActionType(id) {
    return api.get(`/tipo-de-acao/${id}`)
  },
  postActionTypes(payload) {
    return api.post('/tipo-de-acao', payload)
  },
  putActionsTypes(payload) {
    return api.put('/tipo-de-acao', payload)
  },
  deleteActionsTypes(_id) {
    return api.delete(`/tipo-de-acao/${_id}`)
  },
}

export default TipoDeAcaoService
