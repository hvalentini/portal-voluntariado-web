import api from './api'

const ParametroSistemaService = {

  changePassword(data) {
    return api.post('parametro-sistema/mudar-senha', data)
  },
}

export default ParametroSistemaService
