import api from './api'
import trimObject from '../utils/trimObject'

const AcoesService = {
  getAcoesRealizadas(idComite) {
    return api.get('/acao/realizadas-comite', { idComite })
  },

  getCreditHour(payload) {
    return api.get('/acao/horas', payload)
  },

  handleGetAmountBeneficiated(idComite, ano, idVoluntario) {
    return api.get('/acao/pessoas-beneficiadas', { idComite, ano, idVoluntario })
  },

  handleGetAmountActions() {
    return api.get('/acao/realizadas-ano')
  },

  handleGetgetGallery() {
    return api.get('/acao/galeria', { qtd: 3 })
  },

  handleLikeAction(payload) {
    return api.put('/acao/gostar', payload)
  },

  handleInterestAction(payload) {
    return api.put('/acao/participar', payload)
  },

  handlGetAcoesFiltros(payloadFiltro = '', ordenacao = '{ "dataCadastro" : -1}', exclusivoParaLider = false) {
    const filtro = typeof payloadFiltro === 'object' ? JSON.stringify(trimObject(payloadFiltro)) : payloadFiltro
    const res = api.get(`/acao?filtro=${encodeURIComponent(filtro)}&ordenacao=${ordenacao}&exclusivoParaLider=${exclusivoParaLider}`)
    return res
  },

  handlGetAcoesPaginacao(obj) {
    const filtro = typeof obj.filters === 'object' ? JSON.stringify(trimObject(obj.filters)) : obj.filters
    const order = obj.order
    const page = obj.page
    return api.get(`/acao?filtro=${encodeURIComponent(filtro)}&ordenacao=${order}&page=${page}&limit=${obj.limit || 6}&exclusivoParaLider=true`)
  },

  upload(fileData) {
    return api.post('/upload', fileData)
  },

  handleUpload(fileData) {
    return api.post('/upload/image', fileData)
  },

  handleAddAction(query) {
    return api.post('/acao', trimObject(query))
  },

  handleEditAction(query) {
    return api.put('/acao', trimObject(query))
  },

  handleGetAction(id) {
    return api.get(`/acao/${id}`)
  },

  handleParticipated(payload) {
    return api.put('/acao/participou', payload)
  },

  deleteAction(id) {
    return api.delete(`/acao?id=${id}`)
  },

}

export default AcoesService
