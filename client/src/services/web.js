// TODO: utilizar apiSauce para criar e exportar a API comunicando com o
// enpoint do portal-voluntariado-api
import { create } from 'apisauce'
import URL from '../../../server/constant/urls'

// define the web
const web = create({
  baseURL: URL.baseWEB,
})

export default web
