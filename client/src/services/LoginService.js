import api from './api'
import apiLocal from './api-local'

const LoginServices = {
  postLoginAuth(user) {
    return apiLocal.post('/login', user)
  },
  postLoginAuthAdmin(user) {
    return apiLocal.post('/login-admin', user)
  },
  handlePassRecover(payload) {
    return api.put('/voluntario/esqueci-senha', { emailCpf: payload })
  },
  postNewPass(payload) {
    return api.post('/voluntario/mudar-senha', payload)
  },
}

export default LoginServices
